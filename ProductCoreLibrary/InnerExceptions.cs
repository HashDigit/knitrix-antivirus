﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogMaintainanance
{
    public static class InnerExceptions
    {
        public static void Add(Exception ex,string strClassNam)
        {
            try
            {
                if (ex.InnerException != null)
                    ErrorLog.CreateLogFile(strClassNam + ": " + ex.InnerException.ToString());
                else
                {
                    ErrorLog.CreateLogFile(strClassNam + ": " + ex.ToString());
                }
            }
            catch (Exception exc)
            {
                ErrorLog.CreateLogFile("InnerExceptions-Add" + exc.Message);
            }
        }
    }
}
