﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogMaintainanance;

namespace BaseTool
{
    public class BytesProcessing
    {
        /// <summary>
        /// Converts the total bytes given as input into the desired format of Bytes, KB, MB, GB, TB
        /// Returns the converted value in the string format. Input is in the form of bytes
        /// </summary>
        /// <param name="bytes">1024</param>
        /// <returns>1KB</returns>
        public static string FormatBytes(long bytes)
        {
            string[] Suffix = { "Bytes", "KB", "MB", "GB", "TB" };
            int i = 0;
            double dblSByte = bytes;
            try
            {
                if (bytes > 1024)
                    for (i = 0; (bytes / 1024) > 0; i++, bytes /= 1024)
                        dblSByte = bytes / 1024.0;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ByteProcessing-FormatBytes" + ex.Message);
            }

            return String.Format("{0:0.##} {1}", dblSByte, Suffix[i]);
        }


        /// <summary>
        /// Obtain the given date time. Subtract from the current date time and convert into timespan.
        /// Based on the time span the message is shown as {} minutes ago, {} seconds ago, {} days ago.
        /// This method is used to display the last scanned date and time.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static string ConvertDate(DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;
            try
            {
                if (dayDiff == 0)
                {
                    // A.
                    // Less than one minute ago.
                    if (secDiff < 60)
                    {
                        return "Just Now";
                    }
                    if (secDiff < 3600)
                    {
                        return string.Format("{0} minute(s) ago",
                            Math.Floor((double)secDiff / 60));
                    }
                    // Less than one day ago.
                    if (secDiff < 86400)
                    {
                        return string.Format("{0} hour(s) ago",
                            Math.Floor((double)secDiff / 3600));
                    }
                }
                else
                {
                    return string.Format("{0} day(s) ago",
                    dayDiff);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ByteProcessing-ConvertDate" + ex.Message);
            }
            return null;
        }
    }
}
