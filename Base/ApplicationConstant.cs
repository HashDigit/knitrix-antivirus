﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseTool
{
    public class ApplicationConstant
    {

        #region Enums
        public enum ScanningFlag
        {

        }


        #endregion


        /// <summary>
        /// Name to set for the individual task in Task Scheduler
        /// </summary>
        public const string ScheduleTaskName = "{0}_scan_{1}";

        /// <summary>
        /// When the drive path A is tried to access
        /// </summary>
        public const string InAccessiblePathA = @"A:\";

        /// <summary>
        /// When the drive path B is tried to access
        /// </summary>
        public const string InAccessiblePathB = @"B:\";

        /// <summary>
        /// Message to show when the the drive paths are tried to access from within the application
        /// </summary>
        public const string InAccessibleMessage = "Selected location cannot be accessed.";

        /// <summary>
        /// Message to show when empty directory is accesssed
        /// </summary>
        public const string EmptyDirectoryMessage = "Cannot Scan an Empty Folder. Please select another location to Scan.";

        /// <summary>
        /// Message to show on unhandled exception
        /// </summary>
        public const string UnhandledExceptionMessage = "Application could not perform the desired action. Please contact {0} support team.";

        /// <summary>
        /// Message to check for the Internet
        /// </summary>
        public const string MessageToCheckForInternet = "Please Check your internet connection and try again.";

        /// <summary>
        ///Select at least one file to restore
        /// </summary>
        public const string RestoreSelectAFile = "Select a file to restore.";

        /// <summary>
        /// Confirmation message before restoring
        /// </summary>
        public const string RestoreConfirmation = "Are you sure you want to restore these files?";

        /// <summary>
        /// Message after restoration has been done successfully
        /// </summary>
        public const string RestoreSuccess = "Successfully restored the files to their respective locations.";

        /// <summary>
        /// Select at least one file to remove
        /// </summary>
        public const string RemoveSelectAFile = "Select a file to remove.";

        /// <summary>
        /// Confirmation message before removing files
        /// </summary>
        public const string RemoveConfirmation = "Are you sure you want to remove these files?";

        /// <summary>
        /// Message after files has been removed successfully
        /// </summary>
        public const string RemoveSuccess = "Successfully cleaned files from disk";
    }
}
