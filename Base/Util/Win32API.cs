﻿
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace BaseTool.Util
{
    public class Win32API
    {
        public const int WM_NCLBUTTONDOWN = 161;

        public const int HT_CAPTION = 2;

        public const int WM_DROPFILES = 563;

        public const int WM_COPYDATA = 74;

        public const int WM_COPYGLOBALDATA = 73;

        public const int MSGFLT_ADD = 1;

        public const int MAX_PATH = 260;

        public const int WS_CLIPCHILDREN = 33554432;

        public const int WS_MINIMIZEBOX = 131072;

        public const int WS_MAXIMIZEBOX = 65536;

        public const int WS_SYSMENU = 524288;

        public const int CS_DBLCLKS = 8;

        internal const ushort PROCESSOR_ARCHITECTURE_INTEL = 0;

        internal const ushort PROCESSOR_ARCHITECTURE_IA64 = 6;

        internal const ushort PROCESSOR_ARCHITECTURE_AMD64 = 9;

        internal const ushort PROCESSOR_ARCHITECTURE_UNKNOWN = 65535;

        public const int WM_CLOSE = 16;

        public const int BN_CLICKED = 245;

        public const int WM_COMMAND = 273;

        public const int HWND_BROADCAST = 65535;

        public const int SW_SHOWNORMAL = 1;

        public const int SW_SHOWHIDE = 0;

        public const uint LOAD_LIBRARY_AS_DATAFILE = 2;

        public const int TVIF_STATE = 8;

        public const int TVIS_STATEIMAGEMASK = 61440;

        public const int TV_FIRST = 4352;

        public const int TVM_SETITEM = 4415;

        private static CommonUtil oUtils;

        public readonly static int WM_SHOWFIRSTINSTANCE;



        public Win32API()
        {
        }

       

   

      
     
        public static string GetShFolderPath(Enums.CSIDL eFolderName)
        {
        
            StringBuilder stringBuilder = new StringBuilder(255);
            Win32API.SHGetFolderPath(IntPtr.Zero, (short)eFolderName, IntPtr.Zero, 0, stringBuilder);
            return stringBuilder.ToString();
        }

    

       [DllImport("wininet.dll", CharSet=CharSet.Auto, ExactSpelling=false)]
        private static extern bool InternetGetConnectedState(ref Win32API.InternetConnectionState_e lpdwFlags, int dwReserved);
       [DllImport("shlwapi.dll", CharSet = CharSet.Auto, ExactSpelling = false)]
       public static extern bool PathIsNetworkPath(string pszPath);
         
        public static bool IsInternetConnected()
        {
            Win32API.InternetConnectionState_e internetConnectionStateE = (Win32API.InternetConnectionState_e)0;
            return Win32API.InternetGetConnectedState(ref internetConnectionStateE, 0);
        }


     
      
      

     

       

        
        [DllImport("shell32.dll", CharSet=CharSet.None, ExactSpelling=false, SetLastError=true)]
        public static extern int SHGetFolderPath(IntPtr hwndOwner, short nFolder, IntPtr hToken, int dwFlags, StringBuilder lpszPath);

       

       


        [Flags]
        private enum InternetConnectionState_e
        {
            INTERNET_CONNECTION_MODEM = 1,
            INTERNET_CONNECTION_LAN = 2,
            INTERNET_CONNECTION_PROXY = 4,
            INTERNET_RAS_INSTALLED = 16,
            INTERNET_CONNECTION_OFFLINE = 32,
            INTERNET_CONNECTION_CONFIGURED = 64
        }

      

        

    }
}