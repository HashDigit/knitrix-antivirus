﻿
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using System.Xml;



namespace BaseTool.Util
{


    public class CommonUtil
    {
        public string LogFilePath = string.Empty;
        public string ProductName = string.Empty;
        private const string EXECUTABLES = "\\.js?$|\\.msi?$|\\.dll?$|\\.htm?$|\\.xbs?$|\\.php?$|\\.bat?$|\\.dat?$|\\.eml?$|\\.exe\\$?$";
        private const string COMPRESSED = "\\.zip?$|\\.rar\\$?$";
        //private const string EXECUTABLES = "\\.js?$|\\.msi?$|\\.htm?$|\\.vbs?$|\\.php?$|\\.bat?$|\\.dat?$|\\.eml?$|\\.lnk?$|\\.bin?$|\\.dll?$|\\.zix?$|\\.jar?$|\\.sys?$|\\.ctbl?$|\\.aru?$|\\.ini?$|\\.ws?$|\\.scr?$|\\.com?$|\\.cc?$|\\.mjg?$|\\.vzr?$|\\.fuj?$|\\.lok?$|\\.cyw?$|\\.pid?$|\\.qit?$|\\.rna?$|\\.pif?$|\\.exe\\$?$";

        public bool isExecutable(string fName)
        {
            return Regex.IsMatch(fName.ToLower(), EXECUTABLES);
        }

        public bool isCompressed(string fName)
        {
            return Regex.IsMatch(fName.ToLower(), COMPRESSED);
        }
        public CommonUtil()
        {

            // ProductName = ((AssemblyProductAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), true)[0]).Product;

        }




        public static string AssemblyPath()
        {

            string location = Assembly.GetExecutingAssembly().Location;
            location = location.Substring(0, location.LastIndexOf("\\"));
            return location;
        }
        public string GetCommonAppDataPath(string CommonAppDataPath)
        {

        Label0:
            string commonAppDataPath = CommonAppDataPath;
            commonAppDataPath = commonAppDataPath.Substring(0, commonAppDataPath.LastIndexOf("\\"));
            int num = 2;
            while (true)
            {
                switch (num)
                {
                    case 0:
                        {
                            Directory.CreateDirectory(commonAppDataPath);
                            num = 1;
                            continue;
                        }
                    case 1:
                        {
                            break;
                        }
                    case 2:
                        {
                            if (Directory.Exists(commonAppDataPath))
                            {
                                return commonAppDataPath;
                            }
                            num = 0;
                            continue;
                        }
                    default:
                        {
                            goto Label0;
                        }
                }
            }
            return commonAppDataPath;
        }
        public string GetUserAppDataPath(string UserAppDataPath)
        {
        Label0:

            string userAppDataPath = UserAppDataPath;
            userAppDataPath = userAppDataPath.Substring(0, userAppDataPath.LastIndexOf("\\"));
            int num = 2;
            while (true)
            {
                switch (num)
                {
                    case 0:
                        {
                            Directory.CreateDirectory(userAppDataPath);
                            num = 1;
                            continue;
                        }
                    case 1:
                        {
                            break;
                        }
                    case 2:
                        {
                            if (Directory.Exists(userAppDataPath))
                            {
                                return userAppDataPath;
                            }
                            num = 0;
                            continue;
                        }
                    default:
                        {
                            goto Label0;
                        }
                }
            }
            return userAppDataPath;
        }








        public void IgnoreFileList(ref List<string> A_0)
        {

            A_0.Add("outlook");
            A_0.Add("microsoft office");
            A_0.Add("\\application data\\microsoft");
            A_0.Add("microsoft.net");
            A_0.Add("net\\framework");
            A_0.Add("%windir%\\tracing");
            A_0.Add("mcafee");
            A_0.Add("symantec");
            A_0.Add("navw32");
            A_0.Add("navw64");
            A_0.Add("msocache");
            A_0.Add(";");
            A_0.Add("?");
            A_0.Add("*");
            A_0.Add("a:\\");
            A_0.Add("b:\\");
            A_0.Add("\\tracing");
        }


        public void GetSystemFolders(ref List<string> A_0, string CommonAppDataPath, string UserAppDataPath)
        {
            ManagementObjectCollection.ManagementObjectEnumerator enumerator = null;
            try
            {

                if (A_0 == null)
                {
                    A_0 = new List<string>();
                }
                string machineName = Environment.MachineName;
                SelectQuery selectQuery = new SelectQuery("Win32_UserAccount", string.Format("domain='{0}'", machineName));
                ManagementScope managementScope = new ManagementScope("\\\\.\\root\\cimv2");
                managementScope.Connect();
                enumerator = (new ManagementObjectSearcher(managementScope, selectQuery)).Get().GetEnumerator();

                if (enumerator != null)
                {
                    while (enumerator.MoveNext())
                    {
                        ManagementObject current = (ManagementObject)enumerator.Current;
                        A_0.Add(current["Name"].ToString().ToLower());

                    }

                }



            }
            finally
            {
                if (enumerator != null)
                {
                    ((IDisposable)enumerator).Dispose();
                }
                A_0.Add(this.GetUserAppDataPath(UserAppDataPath).ToLower());
                A_0.Add(this.GetCommonAppDataPath(CommonAppDataPath).ToLower());
                A_0.Add(CommonUtil.AssemblyPath().ToLower());
                A_0.Add(Win32API.GetShFolderPath(Enums.CSIDL.CSIDL_NETHOOD).ToLower());
                A_0.Add(string.Format("{0}\\{1}", Environment.GetEnvironmentVariable("systemdrive"), "system volume information").ToLower());


            }




        }
        public static bool isSupported()
        {
            string osName = OSInfo.Name;
            if (osName.Equals("Windows 7") || osName.Equals("Windows 8") || osName.Equals("Windows XP") || osName.Equals("unknown"))
                return true;
            return false;

        }
        public static bool isWindowsXP()
        {
            string osName = OSInfo.Name;
            if (osName.Equals("Windows XP"))
                return true;

            return false;

        }
    }


}
