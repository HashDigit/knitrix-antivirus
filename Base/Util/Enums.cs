﻿using System;

namespace BaseTool.Util
{
    public class Enums
    {
        public Enums()
        {
        }

        public enum ACTIVATION_MESSAGES
        {
            MSG_KEY_RESERVED = -1,
            MSG_KEY_EVERYTHING_OK = 0,
            MSG_KEY_GOOD_CONTINUE = 1,
            MSG_KEY_PIRATED = 2,
            MSG_KEY_FALSE = 3,
            MSG_KEY_BLOCKED = 4,
            MSG_KEY_REFUND = 5,
            MSG_KEY_EXPIRED = 6,
            MSG_KEY_LIMIT_EXCEEDED = 7,
            MSG_KEY_ALREADY_NOT_IN_DB = 8,
            MSG_KEY_FOUND_FOR_OTHER_PRODUCT = 9,
            MSG_AUTO_EXPIRE_AFTER_TIME = 21,
            MSG_REFUND = 22,
            MSG_CHARGEBACK = 23,
            MSG_EXCEED_DOWNLOAD = 24,
            MSG_MISUSED = 25
        }

        public enum ACTIVATION_STATUS
        {
            ACTIVATION_TAKE_ACTION_ON_BASE_RETURNED_FLAGS = 1,
            ONLINE_ACTIVATION_NEEDED = 1
        }

        [Flags]
        public enum AnimateWindowFlags
        {
            AW_HOR_POSITIVE = 1,
            AW_HOR_NEGATIVE = 2,
            AW_VER_POSITIVE = 4,
            AW_VER_NEGATIVE = 8,
            AW_CENTER = 16,
            AW_HIDE = 65536,
            AW_ACTIVATE = 131072,
            AW_SLIDE = 262144,
            AW_BLEND = 524288
        }

        public enum Client_Product_Version
        {
            SYSTWEAK_ASP_V2 = 17
        }

        public enum CSIDL
        {
            CSIDL_DESKTOP = 0,
            CSIDL_INTERNET = 1,
            CSIDL_PROGRAMS = 2,
            CSIDL_CONTROLS = 3,
            CSIDL_PRINTERS = 4,
            CSIDL_PERSONAL = 5,
            CSIDL_FAVORITES = 6,
            CSIDL_STARTUP = 7,
            CSIDL_RECENT = 8,
            CSIDL_SENDTO = 9,
            CSIDL_BITBUCKET = 10,
            CSIDL_STARTMENU = 11,
            CSIDL_MYDOCUMENTS = 12,
            CSIDL_MYMUSIC = 13,
            CSIDL_MYVIDEO = 14,
            CSIDL_DESKTOPDIRECTORY = 16,
            CSIDL_DRIVES = 17,
            CSIDL_NETWORK = 18,
            CSIDL_NETHOOD = 19,
            CSIDL_FONTS = 20,
            CSIDL_TEMPLATES = 21,
            CSIDL_COMMON_STARTMENU = 22,
            CSIDL_COMMON_PROGRAMS = 23,
            CSIDL_COMMON_STARTUP = 24,
            CSIDL_COMMON_DESKTOPDIRECTORY = 25,
            CSIDL_APPDATA = 26,
            CSIDL_PRINTHOOD = 27,
            CSIDL_LOCAL_APPDATA = 28,
            CSIDL_ALTSTARTUP = 29,
            CSIDL_COMMON_ALTSTARTUP = 30,
            CSIDL_COMMON_FAVORITES = 31,
            CSIDL_INTERNET_CACHE = 32,
            CSIDL_COOKIES = 33,
            CSIDL_HISTORY = 34,
            CSIDL_COMMON_APPDATA = 35,
            CSIDL_WINDOWS = 36,
            CSIDL_SYSTEM = 37,
            CSIDL_PROGRAM_FILES = 38,
            CSIDL_MYPICTURES = 39,
            CSIDL_PROFILE = 40,
            CSIDL_SYSTEMX86 = 41,
            CSIDL_PROGRAM_FILESX86 = 42,
            CSIDL_PROGRAM_FILES_COMMON = 43,
            CSIDL_PROGRAM_FILES_COMMONX86 = 44,
            CSIDL_COMMON_TEMPLATES = 45,
            CSIDL_COMMON_DOCUMENTS = 46,
            CSIDL_COMMON_ADMINTOOLS = 47,
            CSIDL_ADMINTOOLS = 48,
            CSIDL_CONNECTIONS = 49,
            CSIDL_COMMON_MUSIC = 53,
            CSIDL_COMMON_PICTURES = 54,
            CSIDL_COMMON_VIDEO = 55,
            CSIDL_CDBURN_AREA = 59
        }

        public enum eActionToPerform
        {
            None,
            ChangeValue,
            DeleteValue,
            AddValue
        }

        public enum eActionType
        {
            NoActionTaken,
            Skipped,
            Cleaned,
            Quarantine,
            Clean_Failed,
            Quarantine_Failed,
            Ignored
        }

        public enum eCurrentAction
        {
            None,
            Loading_Database,
            Scan_In_Progress,
            Scan_Finished,
            Quarantine_In_Progress,
            Quarantine_Finished,
            Delete_In_Progress,
            Delete_Finished,
            QuarantineRestore_In_Progess,
            QuarantineRestore_Finished,
            QuarantineDelete_In_Progress,
            QuarantineDelete_In_Finished,
            Update_In_Progress,
            Update_Finished
        }

        public enum eCurrentStatus
        {
            NotScannedYet,
            InfectionFound,
            InfectionCleaned
        }

        public enum eDataType
        {
            String,
            uLong
        }

 
        public enum eDetectedRegistryType
        {
            None,
            RegistryKey,
            RegistryValue,
            RegistryValueData
        }

        public enum eDownloadStatus
        {
            DownloadSucessFull,
            DownloadAborted,
            DownloadError,
            PartialDownload,
            InstallError,
            DownloadFailed_LowDiskSpace
        }

        public enum eFeedbackType
        {
            None,
            ReportFalsePositive,
            SubmitSample,
            SubmitFeedback
        }

        public enum eFileType
        {
            NONPE,
            PE,
            PE_BY_EXT
        }

        public enum eIgnoreListType
        {
            Any,
            Files,
            Folder,
            Spyware
        }

        public enum eKeyActionToTake
        {
            KEY_RESERVED = -1,
            KEY_EVERYTHING_OK = 0,
            KEY_GOOD_CONTINUE = 1,
            KEY_BAD_EXPIRE_IT = 2,
            KEY_BAD_DO_NOTHING = 3,
            KEY_BAD_ONLY_SHOW_MSG = 4,
            KEY_BAD_EXPIRE_IT_SHOW_MSG = 5,
            KEY_BAD_EXPIRE_IT_SHOW_MSG_OPEN_BROWSER = 6,
            KEY_BAD_ONLY_OPEN_BROWSER = 7,
            KEY_BAD_EXPIRE_IT_OPEN_BROWSER = 8,
            KEY_BAD_DOWNLOAD_EXE_AND_EXEC_SILENT = 9,
            KEY_BAD_EXPIRE_IT_DOWNLOAD_EXE_AND_EXEC_SILENT = 10,
            INSTALL_A_NEW_KEY = 11,
            SET_DEFAULT_KEY = 12,
            GET_AND_INSTALL_A_NEW_KEY_ONBASEOF_CURRENT_KEY = 13,
            EXTEND_KEY_EXPIRY_DAYS = 14,
            KEY_GOOD_BUT_DO_NOT_ALLOW_REGISTRATION = 15,
            KEY_GOOD_EXPIRE_IT = 16,
            KEY_GOOD_ONLY_SHOW_MSG = 17,
            KEY_GOOD_EXPIRE_IT_SHOW_MSG = 18,
            KEY_GOOD_EXPIRE_IT_SHOW_MSG_OPEN_BROWSER = 19,
            KEY_GOOD_ONLY_OPEN_BROWSER = 20,
            KEY_GOOD_EXPIRE_IT_OPEN_BROWSER = 21,
            KEY_GOOD_DOWNLOAD_EXE_AND_EXEC_SILENT = 22,
            KEY_GOOD_EXPIRE_IT_DOWNLOAD_EXE_AND_EXEC_SILENT = 23,
            KEY_BAD_MAKE_IT_GOOD = 24,
            KEY_GOOD_DO_WE_HAVE_TO_SHOW_REMINDER = 25
        }

        public enum eLoadConfiguration
        {
            BEST_SYSTEM_PERFORMANCE = 1,
            BEST_APPLICATION_PERFORMANCE = 2
        }

        public enum eOpenHelpMenuItem
        {
            HelpFile,
            RegistrationDialog,
            AboutDialog,
            UpdateDialog,
            CompanyWebsite,
            SendFeedback
        }

        public enum ePackedType
        {
            NotPacked,
            Packed,
            Setup,
            CompressedZip
        }

       

        public enum eREG_STATUS
        {
            TRIAL_VERSION = 0,
            REGISTERED = 1,
            REGISTERED_AND_IS_ABOUT_TOEXPIRE = 2,
            EXPIRED = 4,
            PIRATED = 8,
            VOLUME_COPY = 16
        }

        public enum eRestartType
        {
            None,
            LogOff,
            Restart
        }

        public enum eScanArea
        {
            None,
            Cookies,
            Memory,
            Registry,
            FileSystem,
            WindowsSettings,
            RegistryValueData
        }

        public enum eScanType : byte
        {
            None,
            CustomScan,
            DeepScan,
            QuickScan,
            SingleFileScan
        }

        public enum eSettingsTab
        {
            CustomScanTab = 1,
            ExclusionTab = 2,
            SchedulerTab = 3,
            LanguageTab = 4,
            UpdaterTab = 5
        }

        public enum eShieldType
        {
            HostFileMonitor = 1,
            IEHomePageMonitor = 2
        }

     

        public enum eUpdateStatus
        {
            None,
            NewAppUpdateAvailable,
            NewUpdateAvailable,
            UptoDate,
            ConnectionError,
            NewAppUpDateOpenBrowser
        }

        public enum eUpdateType
        {
            Inc_Merged_Database,
            CompleteDatabase,
            ApplicationUpdate
        }

        public enum eVerificationTime
        {
            AppLaunch,
            Register
        }

        public enum eWebBrowserSettings
        {
            ENABLE_ACTIVEX = 1200,
            ENABLE_WEBBROSWER_SCRIPT = 1206,
            ENABLE_SCRIPTING = 1400,
            ENABLE__PROGRAMATIC_CLIPBROARD = 1407
        }

      

   

       
        public enum MoveFileFlags
        {
            MOVEFILE_REPLACE_EXISTING = 1,
            MOVEFILE_COPY_ALLOWED = 2,
            MOVEFILE_DELAY_UNTIL_REBOOT = 4,
            MOVEFILE_WRITE_THROUGH = 8,
            MOVEFILE_CREATE_HARDLINK = 16,
            MOVEFILE_FAIL_IF_NOT_TRACKABLE = 32
        }

        public enum PBInstalledState
        {
            Installed,
            NotInstalled,
            Registered,
            UnRegistered,
            Expired
        }

        public enum Platform
        {
            X86,
            X64,
            Unknown
        }

        public enum POWER_BUNDLE_MSG
        {
            POWER_BUNDLE_MSG_NONE,
            POWER_BUNDLE_MSG_LAUNCH_APP,
            POWER_BUNDLE_MSG_SCAN,
            POWER_BUNDLE_MSG_DISPLAY_RESULTS,
            POWER_BUNDLE_MSG_SET_SCHEDULE
        }

        [Flags]
        public enum ProcessAccessType
        {
            PROCESS_TERMINATE = 1,
            PROCESS_CREATE_THREAD = 2,
            PROCESS_SET_SESSIONID = 4,
            PROCESS_VM_OPERATION = 8,
            PROCESS_VM_READ = 16,
            PROCESS_VM_WRITE = 32,
            PROCESS_DUP_HANDLE = 64,
            PROCESS_CREATE_PROCESS = 128,
            PROCESS_SET_QUOTA = 256,
            PROCESS_SET_INFORMATION = 512,
            PROCESS_QUERY_INFORMATION = 1024
        }

        public enum SCHEDULE_OPTIONS
        {
            SCHEDULE_RUNONCE = 1,
            SCHEDULE_DAILY = 2,
            SCHEDULE_WEEKLY = 3,
            SCHEDULE_CUSTOM = 4,
            SCHEDULE_NEVER = 8
        }

        public enum ShutDown
        {
            LogOff = 0,
            Shutdown = 1,
            Reboot = 2,
            ForcedLogOff = 4,
            ForcedShutdown = 5,
            ForcedReboot = 6,
            PowerOff = 8,
            ForcedPowerOff = 12
        }
    }
}