﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LogMaintainanance;

namespace BaseTool.Application
{
    public class CreateConfigFile
    {

        /// <summary>
        /// Create the config file. freshclam.conf in the Application root path. The config file is needed
        /// in order for the updation of the clamwin antivirus database
        /// </summary>
        /// <param name="strConfigFilePath"></param>
        public void WriteConfigFile(string strConfigFilePath, string ApplicationPath)
        {
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(strConfigFilePath);
                file.WriteLine("DatabaseDirectory " + ApplicationPath + @"\db");
                file.WriteLine("LogFileMaxSize 1M");
                file.WriteLine("LogTime no");
                file.WriteLine("LogVerbose no");
                file.WriteLine("LogSyslog no");
                file.WriteLine("LogFacility LOG_LOCAL6");
                file.WriteLine("DatabaseOwner clamav");
                file.WriteLine("AllowSupplementaryGroups no");
                file.WriteLine("DNSDatabaseInfo current.cvd.clamav.net");
                file.WriteLine("DatabaseMirror db.XY.clamav.net");
                file.WriteLine("DatabaseMirror database.clamav.net");
                file.WriteLine("MaxAttempts 3");
                file.WriteLine("ScriptedUpdates yes");
                file.WriteLine("CompressLocalDatabase no");
                file.WriteLine("Checks 12");
                file.WriteLine("Debug no");
                file.WriteLine("ConnectTimeout 30");
                file.WriteLine("ReceiveTimeout 30");
                file.WriteLine("TestDatabases yes");
                file.WriteLine("SubmitDetectionStats no");
                file.WriteLine("Bytecode yes");
                file.Close();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("CreateConfigFile-WriteConfigFile" + ex.Message);
            }
        }


        //Update the database. Both the file extensions .cvd and .cld needs to be updated periodically
        public string[] GetUpdateStatuses(string ApplicationPath)
        {
            if (File.Exists(ApplicationPath + @"\db\daily.cvd"))
            {
                string[] lines = System.IO.File.ReadAllLines(ApplicationPath + @"\db\daily.cvd");
                return lines;
            }
            else if (File.Exists(ApplicationPath + @"\db\daily.cld"))
            {
                string[] lines = System.IO.File.ReadAllLines(ApplicationPath + @"\db\daily.cld");
                return lines;
            }
            else
            {
                return new[] { "" };
            }
        }
    }
}
