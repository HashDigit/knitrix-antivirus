﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using LogMaintainanance;

namespace BaseTool.Application
{
    class ProcessMonitor
    {
        //Private declarations.
        private PerformanceCounter pcProcess;

        /// <summary>
        /// Monitors the running program. 
        /// This method was created for monitioring the running process in the PC. If the memory and cpu 
        /// utilization crosses more than 50% the application is supposed to be killed.
        /// This code hasnt been used for now as using the code increases the memory consumption of the application
        /// more than 40-60%
        /// </summary>
        public void Monitor(string ApplicationName)
        {
            //int intInterval = 1000; // 1 seconds
            while (true)
            {
                Process[] runningNow = Process.GetProcesses();
                foreach (Process process in runningNow)
                {
                    try
                    {
                        pcProcess = new PerformanceCounter("Process", "% Processor Time", process.ProcessName);
                        pcProcess.NextValue();

                        if (process.ProcessName.Equals(ApplicationName))
                            continue;

                        if (process.ProcessName.Equals("IDLE") || process.ProcessName.Equals("Idle") || process.ProcessName.Equals("idle"))
                            continue;

                        if (IsUsingToMuchResources(process.ProcessName))
                        {
                            //Console.WriteLine(string.Format("Application {0} has exceeded CPU utilization limit", process.ProcessName));
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.CreateLogFile("ProcessMonitor-Monitor" + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the process is using to much resources. Also the CPU utilization rate of the application is
        /// being checked everytime when the list of process is being more than 50%
        /// </summary>
        /// <returns></returns>
        private bool IsUsingToMuchResources(string processName)
        {
            pcProcess = new PerformanceCounter("Process", "% Processor Time", processName);
            pcProcess.NextValue();
            return pcProcess.NextValue() > float.Parse("50") ? true : false;
        }
    }
}
