﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;
using System.Management;
using BaseTool;
using BaseTool.Util;
using LogMaintainanance;
using System.Collections;

namespace BaseTool
{
    public static class AuthenticationAndSecurity
    {
        /// <summary>
        /// Check the Internet Connection
        /// </summary>
        /// <returns></returns>
        public static bool CheckInternet()
        {
            return Win32API.IsInternetConnected();
        }

        /// <summary>
        /// Get the mac id of the user's pc
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddress()
        {
            string MacID = string.Empty;
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                IEnumerable<ManagementObject> objects = searcher.Get().Cast<ManagementObject>();
                MacID = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();

            }
            catch (ManagementException ex)
            {
                ErrorLog.CreateLogFile("AuthenticationAndSecurity-GetMacAddress" + ex.Message);
                MacID = " ";

            }
            return MacID;
        }

        /// <summary>
        /// For validation of the Product Key
        /// </summary>
        /// <param name="ActivationURL"></param>
        /// <param name="DomainURL"></param>
        /// <returns></returns>
        public static string GetWebRequest(string ActivationURL, string DomainURL)
        {
            while (true)
            {
                if (RemoteFileExists(DomainURL))
                {
                    break;
                }
            }

            HttpWebRequest requests = (HttpWebRequest)WebRequest.Create(ActivationURL);
            requests.MaximumAutomaticRedirections = 4;
            requests.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                HttpWebResponse response = (HttpWebResponse)requests.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string sResponse = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
                return sResponse;
            }

            catch (UriFormatException ex)
            {
                return "<response_code>1500</response_code><response_text>Our Servers seem to be busy, Please try after sometime</response_text><activation_date></activation_date><expiry_date></expiry_date>";
            }
            catch (WebException ex)
            {
                return "<response_code>1500</response_code><response_text>Our Servers seem to be busy, Please try after sometime</response_text><activation_date></activation_date><expiry_date></expiry_date>";
            }
            catch (ProtocolViolationException ex)
            {
                return "<response_code>1500</response_code><response_text>Our Servers seem to be busy, Please try after sometime</response_text><activation_date></activation_date><expiry_date></expiry_date>";
            }
            catch (HttpListenerException ex)
            {
                return "<response_code>1500</response_code><response_text>Our Servers seem to be busy, Please try after sometime</response_text><activation_date></activation_date><expiry_date></expiry_date>";
            }

        }

        /// <summary>
        /// Check if the domain exists
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;

            }
        }

        public static int intResponse = 0;
        public static string Response = string.Empty;
        //Returns the remaing  days availble for tha product to expire
        public static ArrayList GetRemainingDaysDetails(string ActivationURL, string DomainURL, out bool isExceptionThrown)
        {
            ArrayList DaysDetails = new ArrayList();
            try
            {
                string strXmlResponse = GetWebRequest(ActivationURL, DomainURL);

                //return array list from here with the set of responses. Which can be parsed later
                DaysDetails = ReturnResponseDays(strXmlResponse);
                isExceptionThrown = false;
            }
            catch (Exception ex)
            {
                isExceptionThrown = true;
            }

            return DaysDetails;
        }

        /// <summary>
        /// The response string from the Url is in the form <days_left>daysleft</days_left>
        /// This method is used to split the xml format and just get the number of days left
        /// </summary>
        /// <param name="xmlResponse"></param>
        /// <returns></returns>
        public static ArrayList ReturnResponseDays(string xmlResponse)
        {
            string[] elementNames = new string[] { "<days>", "<activation_date>", "<expiry_date>", "<response_code>", "<response_text>" };
            ArrayList arrrReturnElement = new ArrayList();
            try
            {
                foreach (string elementName in elementNames)
                {
                    int startingIndex = xmlResponse.IndexOf(elementName);
                    string value = xmlResponse.Substring(startingIndex + elementName.Length,
                        xmlResponse.IndexOf(elementName.Insert(1, "/"))
                        - (startingIndex + elementName.Length));

                    arrrReturnElement.Add(value);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Activation-ReturnResponseDays" + ex.Message);
            }
            return arrrReturnElement;
        }

        /// <summary>
        /// During the activation of full version the product key and the mac id are being passed.
        /// The return is response code and response text
        /// Ex: for activation successfull the response code is 1000
        /// and response text is- activation successfull  !!! You product has been activated sucessfully.
        /// </summary>
        /// <param name="xmlResponse"><response_code>1000</response_code><response_text>activation successfull  !!! You product has been activated sucessfully.</response_text></param>
        /// <returns>The values in the array list</returns>
        public static ArrayList ReturnResponseText(string xmlResponse)
        {
            string[] elementNames = new string[] { "<response_code>", "<response_text>" };
            ArrayList arrrReturnElement = new ArrayList();
            try
            {
                foreach (string elementName in elementNames)
                {
                    int startingIndex = xmlResponse.IndexOf(elementName);
                    string value = xmlResponse.Substring(startingIndex + elementName.Length,
                        xmlResponse.IndexOf(elementName.Insert(1, "/"))
                        - (startingIndex + elementName.Length));

                    arrrReturnElement.Add(value);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Activation-ReturnResponseText" + ex.Message);
            }
            return arrrReturnElement;
        }
    }
}
