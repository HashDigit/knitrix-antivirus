﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Data;
using LogMaintainanance;
using SQLiteConnectionLibrary;



namespace SQLiteConnectionLibrary
{
    public static class SQliteTables
    {
        
        //This method checks if a table is available. The method returns the value 0 if table is not available
        // the method returns 1 if the table already exists
        public static int CheckForTable(string strTableName)
        {
            DataConnection _DataConnection = new DataConnection();

            string strQuery = string.Format(Queries.CheckTableExists, strTableName);

            return _DataConnection.SelectQueryCount(strQuery);
        }

        //Based on the return from the above function the table is created in the SQLite database
        public static void CreateTable(string strTableName, string strQuery)
        {
            DataConnection _DataConnection = new DataConnection();
            try
            {
                if (CheckForTable(strTableName) == 0)
                {
                    _DataConnection.CreateSQLiteTable(strQuery);
                }
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("SQliteTables-CreateTable" + ex.Message);
            }

        }

        
    }
}
