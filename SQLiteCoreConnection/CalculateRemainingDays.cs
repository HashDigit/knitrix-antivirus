﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using LogMaintainanance;

namespace SQLiteConnectionLibrary
{
    public class CalculateRemainingDays
    {
        /// <summary>
        /// Initialize the DB Connection
        /// </summary>
        DataConnection dbConn = new DataConnection();

        /// <summary>
        /// Returns the number of days remaing in the trial version by verifying with the local SQLite tables
        /// </summary>
        /// <returns></returns>
        public int ReturnTrialVersionReaminingDays()
        {
            int returndays = 30;

            //string str = "Select * from VersionTrial";
            SQLiteDataReader _SQLiteDataReader = dbConn.SelectQuery(Queries.TrialSelect);
            //SQLiteDataReader _SQLiteDataReader = _DataConnection.SelectQuery(str);
            try
            {
                while (_SQLiteDataReader.Read())
                {

                    DateTime dt1 = Convert.ToDateTime(_SQLiteDataReader[0].ToString());
                    DateTime dt2 = Convert.ToDateTime(_SQLiteDataReader[1].ToString());
                    DateTime dt3 = Convert.ToDateTime(DateTime.Now.ToString());
                    string days = _SQLiteDataReader[2].ToString();

                    returndays = (dt1 - dt3).Days + 1;
                    returndays = Convert.ToInt32(days);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Activation-ReturnTrialVersionReaminingDays" + ex.Message);
            }

            return returndays;
        }


        /// <summary>
        /// Returns the number of days remaing in the full version by verifying with the local SQLite tables
        /// </summary>
        /// <returns></returns>
        public int ReturnFullVersionReaminingDays()
        {
            int returndays = 30;

            SQLiteDataReader _SQLiteDataReader = dbConn.SelectQuery(Queries.FullSelect);
            try
            {
                while (_SQLiteDataReader.Read())
                {

                    DateTime dt1 = Convert.ToDateTime(_SQLiteDataReader[0].ToString());
                    DateTime dt2 = Convert.ToDateTime(_SQLiteDataReader[1].ToString());
                    DateTime dt3 = DateTime.Now;

                    string days = _SQLiteDataReader[2].ToString();

                    returndays = (dt1 - dt3).Days + 1;
                    //returndays = Convert.ToInt32(days);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Activation-ReturnFullVersionReaminingDays" + ex.Message);
            }
            return returndays;
        }

        /// <summary>
        /// Returns the number of days remaing in the trial version by verifying with the local SQLite tables
        /// </summary>
        /// <returns></returns>
        public ArrayList ReturnTrialVersionReaminingDaysList()
        {
            ArrayList ReturnDays = new ArrayList();

            SQLiteDataReader _SQLiteDataReader = dbConn.SelectQuery(Queries.TrialSelect);
            try
            {
                while (_SQLiteDataReader.Read())
                {
                    ReturnDays.Add(_SQLiteDataReader[2].ToString());
                    ReturnDays.Add(_SQLiteDataReader[1].ToString());
                    ReturnDays.Add(_SQLiteDataReader[0].ToString());
                }
            }
            catch (Exception ex)
            {
                ReturnDays.Add(ExternalUrls.TrialDaysNumber);

                ErrorLog.CreateLogFile("Activation-ReturnFullVersionReaminingDays" + ex.Message);
            }
            return ReturnDays;
        }

        /// <summary>
        /// Returns the number of days remaing in the full version by verifying with the local SQLite tables
        /// </summary>
        /// <returns></returns>
        public ArrayList ReturnFullVersionReaminingDaysList()
        {

            ArrayList ReturnDays = new ArrayList();

            SQLiteDataReader _SQLiteDataReader = dbConn.SelectQuery(Queries.FullSelect);
            try
            {
                while (_SQLiteDataReader.Read())
                {
                    ReturnDays.Add(_SQLiteDataReader[2].ToString());
                    ReturnDays.Add(_SQLiteDataReader[1].ToString());
                    ReturnDays.Add(_SQLiteDataReader[0].ToString());
                }
            }
            catch (Exception ex)
            {
                ReturnDays.Add(ExternalUrls.FullDaysNumber);

                ErrorLog.CreateLogFile("Activation-ReturnFullVersionReaminingDays" + ex.Message);
            }
            return ReturnDays;
        }
    }
}
