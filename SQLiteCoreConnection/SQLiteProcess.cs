﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using LogMaintainanance;
using BaseTool;

namespace SQLiteConnectionLibrary
{
    public class SQLiteProcess
    {
        DataConnection _DataConnection = new DataConnection();

        #region Trial Table
        public void InsertIntoTrial()
        {
            try
            {
                int colCount = _DataConnection.SelectQueryCount(Queries.TrialColumnCount);

                if (colCount == 0)
                {
                    string strInsertQuery = string.Format(Queries.TrailInsert, AuthenticationAndSecurity.GetMacAddress(), DateTime.Now.ToString(), DateTime.Now.AddDays(ExternalUrls.TrialDaysNumber).ToString(), ExternalUrls.TrialDays);
                    _DataConnection.InsertQuery(strInsertQuery);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-InsertIntoTrial" + ex.Message);
            }

        }

        public SQLiteDataReader SelectFromTrial()
        {
            return _DataConnection.SelectQuery(Queries.TrialSelect);
        }

        public void UpdateTrial(string strMacId, string strActivationDate, string strExpiryDate, int days)
        {
            try
            {
                _DataConnection.UpdateQuery(string.Format(Queries.TrialUpdate, strMacId, strActivationDate, strExpiryDate, days));
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-UpdateTrial" + ex.Message);
            }
        }
        #endregion

        #region Full Table
        public void InserIntoFull()
        {
            try
            {
                int colCount = _DataConnection.SelectQueryCount(Queries.FullColumnCount);
                if (colCount == 0)
                {
                    string strInsertQuery = string.Format(Queries.FullInsert, AuthenticationAndSecurity.GetMacAddress(), DateTime.Now.ToString(), DateTime.Now.AddDays(ExternalUrls.FullDaysNumber).ToString(), ExternalUrls.FullDays);
                    _DataConnection.InsertQuery(strInsertQuery);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-InserIntoFull" + ex.Message);
            }
        }

        public SQLiteDataReader SelectFromFull()
        {
            return _DataConnection.SelectQuery(Queries.FullSelect);
        }

        public void UpdateFull(string strMacId, string strActivationDate, string strExpiryDate, int days)
        {
            try
            {
                _DataConnection.UpdateQuery(string.Format(Queries.FullUpdate, strMacId, strActivationDate, strExpiryDate, days));
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-UpdateFull" + ex.Message);
            }
        }
        #endregion



        public void InsertScanHistory(string strFileName, string strFilePath, string strFileHashName)
        {
            try
            {
                int colCount = _DataConnection.SelectQueryCount(string.Format(Queries.ScanHistoryCount, strFileName, strFilePath));
                if (colCount == 0)
                {
                    string strInsertQuery = string.Format(Queries.ScanHistoryInsert, strFileName, strFilePath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), strFileHashName);
                    _DataConnection.InsertQuery(strInsertQuery);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-InsertScanHistory" + ex.Message);
            }
        }

        #region Scan Results
        public void InsertIntoScanResultDetails(string strScanType, string strScanDate, string strScanResult, string strTimeElapsed, string strItemScanned, string strThreatsDetected, string strThreatsNeutralised, long strTotalDataScanned)
        {
            try
            {
                string strInsertScanResult = string.Format(Queries.ScanResultInsert, strScanType, strScanDate, strScanResult);

                if (_DataConnection.InsertQuery(strInsertScanResult))
                {
                    string strScanDetails = string.Format(Queries.ScanDetailsInsert, strScanDate, strScanType, strTimeElapsed, strItemScanned, strThreatsDetected, strThreatsNeutralised, strTotalDataScanned);
                    //                string strScanDetails = @"Insert Into CompletedScanDetails
                    //(Date,ScanType,TimeElapsed,ItemScanned,ThreatsDetected,ThreatsNeutralised,TotalDataScanned)
                    //Values
                    //('" + strScanDate + "','" + strScanType + "','" + strTimeElapsed + "','" + strItemScanned + "','" + strThreatsDetected + "','" + strThreatsNeutralised + "','" + strTotalDataScanned + "')";

                    _DataConnection.InsertQuery(strScanDetails);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-InsertIntoScanResultDetails" + ex.Message);
            }



        }

        public SQLiteDataReader GetScanHistory(bool AllowFullHistory)
        {
            string strQuery = string.Empty;
            try
            {
                if (AllowFullHistory)
                    strQuery = Queries.ScanHistoryFull.ToString();
                else
                    strQuery = Queries.ScanHistoryPartial.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-GetScanHistory" + ex.Message);
            }
            return _DataConnection.SelectQuery(strQuery);
        }

        public SQLiteDataReader GetScanResult()
        {
            return _DataConnection.SelectQuery(Queries.ScanResultSelect.ToString());
        }

        public int GetResultsRowCount()
        {
            string strQuery = string.Empty;
            strQuery = @"Select count(*) From CompletedScanResults";


            int _SQLiteDataReader = _DataConnection.SelectQueryCount(strQuery);

            return _SQLiteDataReader;
        }

        public SQLiteDataReader DisplayScanResult(int rowid)
        {
            return _DataConnection.SelectQuery(string.Format(Queries.ScanDetailsDescription, rowid));
        }

        public SQLiteDataReader DisplayScanResult(string rowid)
        {
            return _DataConnection.SelectQuery(string.Format(Queries.ScanResultString, rowid));
        }
        #endregion


        #region Restore
        public SQLiteDataReader SelectFilesRestore(string strFileName)
        {
            return _DataConnection.SelectQuery(string.Format(Queries.ScanHistoryRestoreSelect, strFileName));
        }

        public bool RestoreFilesUpdate(string strFileName, string strFilePath)
        {
            return _DataConnection.UpdateQuery(string.Format(Queries.ScanHistoryRestoreUpdate, strFileName, strFilePath));
        }
        #endregion

        #region Remove

        public SQLiteDataReader ScanHistoryRemoveSelect(string strFileName)
        {
            return _DataConnection.SelectQuery(string.Format(Queries.ScanHistoryRemoveSelect, strFileName));
        }

        public int ScanHistoryRemoveSelectCount(string strFileName)
        {
            return _DataConnection.SelectQueryCount(string.Format(Queries.ScanHistoryRemoveSelectCount, strFileName));
        }

        public int ScanHistoryRemoveFilePathCount(string strFileName, string strFilePath)
        {
            return _DataConnection.SelectQueryCount(string.Format(Queries.ScanHistoryRemoveFilePathCount, strFileName, strFilePath));
        }

        public bool ScanHistoryRemoveUpdate(string strFileName, string strFilePath)
        {
            return _DataConnection.UpdateQuery(string.Format(Queries.ScanHistoryRemoveUpdate, strFileName, strFilePath));
        }

        public SQLiteDataReader ScanHistoryGetFileHashValue(string strFileName, string strFilePath)
        {
            return _DataConnection.SelectQuery(string.Format(Queries.ScanHistoryGetFileHashValue, strFileName, strFilePath));
        }

        public bool ScanHistoryRemoveDelete(string strFileName, string strFilePath)
        {
            return _DataConnection.DeleteQuery(string.Format(Queries.ScanHistoryRemoveDelete, strFileName, strFilePath));
        }

        #endregion

        #region Remove All

        public SQLiteDataReader ScanHistoryRemoveAll()
        {
            return _DataConnection.SelectQuery(Queries.ScanHistoryRemoveAll);
        }

        #endregion

        #region Validity Selection
        public SQLiteDataReader QuarantineValiditySelect()
        {
            string strDate = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd HH:mm:ss");
            return _DataConnection.SelectQuery(string.Format(Queries.QuarantineValiditySelect, strDate));
        }

        public bool QuarantineValidityUpdate(string strFileName, string strFilePath)
        {
            return _DataConnection.UpdateQuery(string.Format(Queries.QuarantineValidityUpdate, strFileName, strFilePath));
        }
        #endregion


        public void InsertHashedFiles(string strFileHash)
        {
            try
            {
                int colCount = _DataConnection.SelectQueryCount(string.Format(Queries.HashSelect, strFileHash));
                if (colCount == 0)
                {
                    _DataConnection.InsertQuery(string.Format(Queries.HashInsert, strFileHash));
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-InsertHashedFiles" + ex.Message);
            }
        }

        public int SelectCountHash(string strFileHash)
        {
            return _DataConnection.SelectQueryCount(string.Format(Queries.HashSelect, strFileHash));
        }

        public void DeleteHash()
        {
            try
            {
                _DataConnection.DeleteQuery("Delete from HashedFiles");
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-DeleteHash" + ex.Message);
            }

        }


        #region Activation

        public void UpdateDatabase(int days, string macid, string startdate, string expirydate, bool value)
        {
            try
            {
                if (value)
                {
                    UpdateTrial(macid, startdate, expirydate, days);
                }
                else
                {

                    UpdateFull(macid, startdate, expirydate, days);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SQLiteProcess-UpdateDatabase" + ex.Message);
            }
        }

        #endregion
    }
}
