﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLiteConnectionLibrary
{
    /// <summary>
    /// Class which has the URLS for the application
    /// </summary>
    public static class ExternalUrls
    {
        /// <summary>
        /// Base domain name for the Application where the product key is stored
        /// </summary>
        public static string Domain = "product.hashdigit.com";

        /// <summary>
        /// Buy Now URL
        /// </summary>
        public static string BuyNow = "https://{0}/payment.php";

        /// <summary>
        /// Contact Us URL
        /// </summary>
        public static string Contact = "http://{0}/contact.php";

        /// <summary>
        /// Base Domain Url
        /// </summary>
        public static string DomainUrl = "http://{0}/";
        
        /// <summary>
        /// Chat Now URL
        /// </summary>
        public static string LiveChatURL = "http://{0}/chatnow.php";

        /// <summary>
        /// Support URL
        /// </summary>
        public static string Support = "http://{0}/support.php";

        /// <summary>
        /// Trial Days URL
        /// </summary>
        public static string TrialDaysReamining = "http://{0}/tracking/30day.php?mac={1}";
        
        /// <summary>
        /// Full Days URL
        /// </summary>
        public static string FullDaysReamining = "http://{0}/tracking/verify.php?mac={1}&key={2}";

        /// <summary>
        /// Full Version Registration URL
        /// </summary>
        public static string FullVersionRegister = "http://{0}/tracking/verify.php?key={1}&mac={2}";

        /// <summary>
        /// Number of days Trial Version
        /// </summary>
        public static string TrialDays = "7";

        /// <summary>
        /// Number of days for Full Version
        /// </summary>
        public static string FullDays = "365";

        /// <summary>
        /// Number of days Trial Version
        /// </summary>
        public const int TrialDaysNumber = 7;

        /// <summary>
        /// Number of days Full Version
        /// </summary>
        public const int FullDaysNumber = 365;

    }
}
