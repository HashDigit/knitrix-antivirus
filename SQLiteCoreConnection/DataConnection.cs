﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Data;
using LogMaintainanance;
using SQLiteConnectionLibrary;

namespace SQLiteConnectionLibrary
{
    /// <summary>
    /// Class which deals with the data connection for the whole application.
    /// The data connection is with the SQLite database
    /// </summary>
    public class DataConnection
    {
        SQLiteConnection _SQLiteConnection;
        SQLiteCommand _SQLiteCommand;
        SQLiteDataAdapter _SQLiteDataAdapter;

        /// <summary>
        /// Enable the Connection
        /// connection from the app config page.
        /// The database name, version and the password are called from the Queries.resx file.
        /// If want to change any of the above then change in the Queries.resx file
        /// </summary>
        public DataConnection()
        {
            //Data string for the SQLite database
            string strDataString = "Data Source='" + Queries.DataBaseName + "';Version='" + Queries.DataBaseVersion + "';Password='" + Queries.DatabasePassword + "'";
            _SQLiteConnection = new SQLiteConnection(strDataString);
        }


        #region Open and Close the Connection
        /// <summary>
        /// Connection open
        /// </summary>
        /// <returns></returns>
        private SQLiteConnection Open()
        {
            try
            {
                if (_SQLiteConnection.State == ConnectionState.Closed || _SQLiteConnection.State == ConnectionState.Broken)
                {
                    _SQLiteConnection.Open();
                }
            }

            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("DataConnection-Open" + ex.Message);
            }
            return _SQLiteConnection;
        }

        /// <summary>
        /// Connection close
        /// </summary>
        /// <returns></returns>
        private SQLiteConnection Close()
        {
            try
            {
                if (_SQLiteConnection.State == ConnectionState.Open)
                {
                    _SQLiteConnection.Close();
                }
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("DataConnection-Close" + ex.Message);
            }
            return _SQLiteConnection;
        }
        #endregion


        /// <summary>
        /// Create the SQLite DB file
        /// </summary>
        /// <param name="strFileName"></param>
        public void CreateFile(string strFileName)
        {
            try
            {
                SQLiteConnection.CreateFile(strFileName);
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("DataConnection-CreateFile" + ex.Message);
            }
        }

        /// <summary>
        /// Base method for creating a table
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public bool CreateSQLiteTable(string strQuery)
        {
            try
            {
                Open();
                _SQLiteCommand = new SQLiteCommand(strQuery, _SQLiteConnection);
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Base method for inserting values into the table
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public bool InsertQuery(string strQuery)
        {
            _SQLiteDataAdapter = new SQLiteDataAdapter();
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            try
            {
                _SQLiteCommand.Connection = Open();
                _SQLiteCommand.CommandText = strQuery;
                _SQLiteDataAdapter.InsertCommand = _SQLiteCommand;
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("Connection-InsertQuery: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Base method for updating values in a table
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public bool UpdateQuery(string strQuery)
        {
            _SQLiteDataAdapter = new SQLiteDataAdapter();
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            try
            {
                _SQLiteCommand.Connection = Open();
                _SQLiteCommand.CommandText = strQuery;
                _SQLiteDataAdapter.UpdateCommand = _SQLiteCommand;
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("Connection-UpdateQuery: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Base method for deleting values in a table
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public bool DeleteQuery(string strQuery)
        {
            _SQLiteDataAdapter = new SQLiteDataAdapter();
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            try
            {
                _SQLiteCommand.Connection = Open();
                _SQLiteCommand.CommandText = strQuery;
                _SQLiteDataAdapter.DeleteCommand = _SQLiteCommand;
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("Connection-DeleteQuery: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Base method to view the value from a select query. the datareader is returned
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public SQLiteDataReader SelectQuery(string strQuery)
        {
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            SQLiteDataReader _SQLiteDataReader;

            try
            {
                Open();
                _SQLiteCommand = new SQLiteCommand(strQuery, _SQLiteConnection);
                _SQLiteDataReader = _SQLiteCommand.ExecuteReader();
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("Connection-SelectQuery: " + ex.Message);
                return null;
            }
            return _SQLiteDataReader;
        }

        /// <summary>
        ///  Returns the count value from a select query in the integer format
        /// </summary>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public int SelectQueryCount(string strQuery)
        {
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            int _SQLiteDataReader;

            try
            {
                Open();
                _SQLiteCommand = new SQLiteCommand(strQuery, _SQLiteConnection);
                _SQLiteDataReader = Convert.ToInt32(_SQLiteCommand.ExecuteScalar());

            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("Connection-SelectQuery: " + ex.Message);
                return 0;
            }
            return _SQLiteDataReader;
        }
    }
}
