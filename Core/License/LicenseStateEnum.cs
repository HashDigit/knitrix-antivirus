namespace Data
{
	public enum LicenseStateEnum
	{
		Trial = 0, // default
		TrialExpired,
		BlackList,
		Registered,
		LicenseExpired,
	}
}