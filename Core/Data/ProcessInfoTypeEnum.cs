using System.ComponentModel;

namespace Data
{
    public enum ProcessInfoTypeEnum
    {
        System = 0, // default
        [Description("User Installed")]
        UserInstalled = 1,
        Other = 2,
        Malware = 3,
        //Malware = 4,
    }
}