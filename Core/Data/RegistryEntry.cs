namespace Data
{
    /// <summary>
    /// ������������ �� ������ "Optimize Windows"->"Registry Manager"
    /// </summary>
    public class RegistryEntry : BaseObject
    {
        public string Section { get; set; }
        public string ProblemDescription { get; set; }

        /// <summary>
        /// Is entry checked in grid?
        /// </summary>
        public bool IsChecked { get; set; }

        public RegistryEntry()
        {
            Section = "";
            ProblemDescription = "";
        }
    }
}