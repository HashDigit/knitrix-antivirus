using System;
using System.Drawing;

using Common;

namespace Data
{
	public class ProcessInfo : BaseObject//, INotifyPropertyChanged
	{
		//public event PropertyChangedEventHandler PropertyChanged;

		public ProcessInfoTypeEnum InfoType { get; set; }
		public string ProcessName { get; set; }
		public ulong Memory { get; set; }
		public string Description { get; set; }
		public string StartTime { get; set; }
		public string Path
		{
			get
			{
				return _path;
			}
			set
			{
				if (value != _path && !string.IsNullOrEmpty(value))
				{
					try
					{
						_image = new Bitmap(Icon.ExtractAssociatedIcon(value).ToBitmap(), 16, 16);
					}
					catch
					{
						_image = new Bitmap(16, 16);
					}
					try
					{
						_bigImage = Icon.ExtractAssociatedIcon(value).ToBitmap();
					}
					catch
					{
						_bigImage = new Bitmap(32, 32);
					}
				}
				_path = value;
			}
		}

		public int ProcessId { get; set; }
		public string Company { get; set; }
		public int Priority { get; set; }
		public int Handles { get; set; }
		public int Threads { get; set; }
		public TimeSpan TotalProcessorTime
		{
			get
			{
				return _totalProcessorTime;
			}
			set
			{
				_prevTotalProcessorTime = _totalProcessorTime.TotalMilliseconds == 0 ? value : _totalProcessorTime;
				_totalProcessorTime = value;
			}
		}
		public ulong VirtualMemory { get; set; }
		public int CpuPercent { get; set; }

		/// <summary>
		/// Image 16x16
		/// </summary>
		public Bitmap Image
		{
			get
			{
				return _image;
				//return null;
			}
		}

		/// <summary>
		/// Image 32x32
		/// </summary>
		public Bitmap BigImage
		{
			get
			{
				return _bigImage;
				//return null;
			}
		}

		public string FormattedMemory
		{
			get
			{
				return SizeHelper.GetSize(Memory);
			}
		}

		public string FormattedVirtualMemory
		{
			get
			{
				return SizeHelper.GetSize(VirtualMemory);
			}
		}

		public string FormattedTotalProcessorTime
		{
			get
			{
				return string.Format("{0:00}:{1:00}:{2:00}", _totalProcessorTime.TotalHours, _totalProcessorTime.Minutes, _totalProcessorTime.Seconds);
			}
		}

		private string _path;
		private TimeSpan _totalProcessorTime { get; set; }
		/// <summary>
		/// Prev total processor time.
		/// Used to calculate CPU.
		/// </summary>
		private TimeSpan _prevTotalProcessorTime { get; set; }
		private Bitmap _image;
		private Bitmap _bigImage;

		public ProcessInfo()
		{
			ProcessName = "";
			Description = "";
			StartTime = "";
			Path = "";
			Company = "";
			//TotalProcessorTime = "";
		}

		public void Update(ProcessInfo processInfo)
		{
			InfoType = processInfo.InfoType;
			ProcessName = processInfo.ProcessName;
			var oldMemory = Memory;
			Memory = processInfo.Memory;
			// Notify properties that we want to auto update in grid
			if (Memory != oldMemory)
				this.NotifyPropertyChanged("FormattedMemory");
			Description = processInfo.Description;
			StartTime = processInfo.StartTime;
			Path = processInfo.Path;
			//ProcessId = processInfo.ProcessId;
			Company = processInfo.Company;
			Priority = processInfo.Priority;
			Handles = processInfo.Handles;
			Threads = processInfo.Threads;
			TotalProcessorTime = processInfo.TotalProcessorTime;
			VirtualMemory = processInfo.VirtualMemory;
			var oldCpuPercent = CpuPercent;
			CpuPercent = (int)(_totalProcessorTime - _prevTotalProcessorTime).TotalMilliseconds / 100;
			if (CpuPercent != oldCpuPercent)
				this.NotifyPropertyChanged("CpuPercent");
		}

		private void NotifyPropertyChanged(string name)
		{
			//if (PropertyChanged != null)
			//	PropertyChanged(this, new PropertyChangedEventArgs(name));
		}
	}
}