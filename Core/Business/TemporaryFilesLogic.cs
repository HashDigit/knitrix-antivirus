using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using Common;
using Data;

namespace Business
{
	public class TemporaryFilesLogic
	{
		/// <summary>
		/// "Clear Temporary Files" functionality
		/// </summary>
		public static void Clear()
		{
			if (Config.EnableFixing)
			{
				var pathsToClear = new List<string>();
				// TODO :: (AK) �������� � ���� ������ ���-����?
				pathsToClear.Add(@"{userprofile}\appdata\local\temp".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"{userprofile}\Local Settings\Temp".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"c:\temp");
				pathsToClear.Add(@"c:\windows\temp");
				pathsToClear.Add(@"{userprofile}\appdata\roaming\microsoft\internet explorer\userdata".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"{userprofile}\UserData".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"c:\windows\softwaredistributions\download");
				pathsToClear.Add(@"{userprofile}\appdata\local\microsoft\windows\temporary internet files".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				pathsToClear.Add(@"{userprofile}\Local Settings\Temporary Internet Files".Replace("{userprofile}", Environment.GetEnvironmentVariable("USERPROFILE").ToLower())); // name without domain
				foreach (var pathToClear in pathsToClear)
					DirectoryHelper.Clear(pathToClear);
			}
			else
			{
				// dummy functionality, just sleeping
				Thread.Sleep(2000);
			}
		}
	}
}