using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using Common;
using Data;
using Microsoft.Win32;

namespace Business
{
	/// <summary>
	/// Browser Object Helper
	/// </summary>
	public class BhoLogic
	{
		private readonly string HKEY_CURRENT_USER = "HKEY_CURRENT_USER";

		/// <summary>
		/// Get list of all BHO 
		/// </summary>
		/// <returns></returns>
		public static List<Bho> List()
		{
			var bhos = new List<Bho>();
			try
			{
				var bhoGuids = new List<string>();
				// getting list of guid for all exising BHO
				try
				{
					//var rk = RegistryHelper.Is64BitOS ?
					//    Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\explorer\Browser Helper Objects") :
					//    Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\explorer\Browser Helper Objects");
					//bhoGuids.AddRange(rk.GetSubKeyNames());
					bhoGuids.AddRange(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\explorer\Browser Helper Objects").GetSubKeyNames());
					if (RegistryHelper.Is64BitOS)
						bhoGuids.AddRange(Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\explorer\Browser Helper Objects").GetSubKeyNames());
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}

				// init bho objects
				foreach (var bhoGuid in bhoGuids)
				{
					try
					{
						var rk = RegistryHelper.Is64BitOS ?
							Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(string.Format(@"Wow6432Node\CLSID\{0}\InprocServer32", bhoGuid)) :
							Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(string.Format(@"CLSID\{0}\InprocServer32", bhoGuid));
						var path = rk.GetValue("").ToString();
						rk = RegistryHelper.Is64BitOS ?
							Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(string.Format(@"Wow6432Node\CLSID\{0}", bhoGuid)) :
							Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(string.Format(@"CLSID\{0}", bhoGuid));
						var friendlyName = rk.GetValue("").ToString();

						if (string.IsNullOrEmpty(friendlyName))
							continue;

						// init bho properties
						var bho = new Bho();
						bho.Guid = bhoGuid;
						bho.Path = path;
						bho.FriendlyName = friendlyName;
						try
						{
							var fileInfo = FileVersionInfo.GetVersionInfo(path);
							bho.Description = fileInfo.FileDescription ?? "";
							bho.Company = fileInfo.CompanyName ?? "";
							bho.ProductName = fileInfo.ProductName ?? "";
							bho.ProductVersion = fileInfo.ProductVersion ?? "";
						}
						catch
						{
							bho.Description = "";
							bho.Company = "";
							bho.ProductName = "";
							bho.ProductVersion = "";
						}
						try
						{
							bho.Size = (ulong)new FileInfo(path).Length;
							bho.DateModified = new FileInfo(path).LastWriteTime;
						}
						catch
						{
						}
						bho.IsEnabled = IsEnabled(bhoGuid);

						bhos.Add(bho);
					}
					catch (Exception ex)
					{
						CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
					}
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
			return bhos;
		}

		/// <summary>
		/// Enable/Disable bho in IE
		/// </summary>
		/// <param name="bhoGuid"></param>
		public static void SetIsEnabled(string bhoGuid, bool isEnabled)
		{
			try
			{
				if (isEnabled)
				{
					// remove "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Ext\Settings"
					Microsoft.Win32.Registry.CurrentUser.DeleteSubKey(String.Format(@"Software\Microsoft\Windows\CurrentVersion\Ext\Settings\{0}", bhoGuid));
				}
				else
				{
					// create "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Ext\Settings"
					var rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(string.Format(@"Software\Microsoft\Windows\CurrentVersion\Ext\Settings\{0}", bhoGuid), true);
					if (rk == null)
						rk = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(string.Format(@"Software\Microsoft\Windows\CurrentVersion\Ext\Settings\{0}", bhoGuid));
					// add "Vesrion" = "*"
					if (rk.GetValue("Version") == null || rk.GetValue("Version").ToString() != "*")
						rk.SetValue("Version", "*");
					// add "Flags" = "1"
					if (rk.GetValue("Flags") == null || (int)rk.GetValue("Flags") != 1)
						rk.SetValue("Flags", 1);
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Does requested bho currently enabled/disabled in IE?
		/// </summary>
		/// <param name="bhoGuid"></param>
		/// <returns></returns>
		private static bool IsEnabled(string bhoGuid)
		{
			try
			{
				var rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(string.Format(@"Software\Microsoft\Windows\CurrentVersion\Ext\Settings\{0}", bhoGuid));
				return rk.GetValue("Flags").ToString() != "1";
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				return true;
			}
		}
	}
}