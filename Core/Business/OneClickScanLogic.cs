using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Business.Privacy;
using Business.Processes;
using Business.Registry;
using Business.Startup;
using Common;
using Common.Xml;
using Common_Tools.TreeViewAdv.Tree;
using Data;
using Data.Privacy;
using Data.Registry;

namespace Business
{
	public class OneClickScanLogic
	{
		/// <summary>
		/// Thread where main scanning is executed
		/// </summary>
		private static Thread _threadScanMain, _threadFixMain;

		/// <summary>
		/// Scanning of Registry area was successfully finished
		/// </summary>
		public static event EventHandler ScanRegistryFinished;

		/// <summary>
		/// Scanning of Process area was successfully finished
		/// </summary>
		public static event EventHandler ScanProcessFinished;

		/// <summary>
		/// Scanning of Privacy area was successfully finished
		/// </summary>
		public static event EventHandler ScanPrivacyFinished;

		/// <summary>
		/// Scanning was successfully finished
		/// </summary>
		public static event EventHandler ScanFinished;

		/// <summary>
		/// Scanning was manually aborted
		/// </summary>
		public static event EventHandler ScanAborted;

		/// <summary>
		/// Fixing was successfully finished
		/// </summary>
		public static event EventHandler FixFinished;

		/// <summary>
		/// Fixing was manually aborted
		/// </summary>
		public static event EventHandler FixAborted;

		/// <summary>
		/// Aborted scan does not throw Finished event
		/// </summary>
		private static bool _isScanAborted = false;

		/// <summary>
		/// Aborted fix does not throw Finished event
		/// </summary>
		private static bool _isFixAborted = false;

		/// <summary>
		/// Is in scanning method?
		/// </summary>
		public static bool IsScanning { get; private set; }

		/// <summary>
		/// Is in fixing method?
		/// </summary>
		public static bool IsFixing { get; private set; }

		/// <summary>
		/// Start One-Click scan
		/// </summary>
		public static void StartScan(TreeModel tmHomeStep2Problem)
		{
			AbortScan();
			_threadScanMain = new Thread(new ParameterizedThreadStart(StartScan_Threaded));
			_threadScanMain.IsBackground = true;
			_threadScanMain.Start(tmHomeStep2Problem);
		}

		/// <summary>
		/// One-Click scan in separate thread
		/// </summary>
		private static void StartScan_Threaded(object obj)
		{
			try
			{
				_isScanAborted = false;
				IsScanning = true;
				var tmHomeStep2Problem = (TreeModel)obj;

				DataContext.OneClickScanState = new OneClickScanState();

				DataContext.OneClickScanState.ScanningProgress = 0;
				DataContext.OneClickScanState.ScanArea = OneClickScanAreaEnum.Registry;
				ScanRegistryLogic.StartScanning(tmHomeStep2Problem, Options.Instance.RegistryScanSettings, 0, 80);
				if (ScanRegistryFinished != null)
					ScanRegistryFinished(null, new EventArgs());

				DataContext.OneClickScanState.ScanningProgress = 80;
				DataContext.OneClickScanState.ScanArea = OneClickScanAreaEnum.Process;
				ScanProcessLogic.StartScanning(tmHomeStep2Problem, 80, 90);
				if (ScanProcessFinished != null)
					ScanProcessFinished(null, new EventArgs());

				DataContext.OneClickScanState.ScanningProgress = 90;
				DataContext.OneClickScanState.ScanArea = OneClickScanAreaEnum.Privacy;
				ScanPrivacyLogic.StartScanning(tmHomeStep2Problem, 90, 100);
				if (ScanPrivacyFinished != null)
					ScanPrivacyFinished(null, new EventArgs());
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Erro while One-Click Scan.\r\n{0}", ex));
			}
			finally
			{
				DataContext.OneClickScanState.ScanningProgress = 100;
				_threadScanMain = null;
				IsScanning = false;
				if (!_isScanAborted && ScanFinished != null)
					ScanFinished(null, new EventArgs());
				_isScanAborted = false;
			}
		}

		/// <summary>
		/// Force stop scanning
		/// </summary>
		public static void StopScan()
		{
			IsScanning = false;
			if (_threadScanMain != null)
			{
				try
				{
					_threadScanMain.Abort();
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}

				//DataContext.OneClickScanState = new OneClickScanState();
				_threadScanMain = null;
			}
		}

		/// <summary>
		/// Force abort scanning
		/// </summary>
		public static void AbortScan()
		{
			IsScanning = false;
			_isScanAborted = true;
			if (_threadScanMain != null)
			{
				try
				{
					_threadScanMain.Abort();
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}

				//DataContext.OneClickScanState = new OneClickScanState();
				_threadScanMain = null;
				if (ScanAborted != null)
					ScanAborted(null, new EventArgs());
			}
		}

		/// <summary>
		/// Start One-Click Fix
		/// </summary>
		public static void StartFix(List<OneClickScanOverallAreaEnum> overallAreasToFix)
		{
			AbortFix();
			_threadFixMain = new Thread(new ParameterizedThreadStart(StartFix_Threaded));
			_threadFixMain.IsBackground = true;
			_threadFixMain.Start(overallAreasToFix);
		}

		/// <summary>
		/// One-Click Fix in separate thread
		/// </summary>
		private static void StartFix_Threaded(object obj)
		{
			try
			{
				_isFixAborted = false;
				IsFixing = true;
				// selecting root nodes to fix
				var overallAreasToFix = (List<OneClickScanOverallAreaEnum>)obj;
				var nodes = new List<Data.Registry.BadRegistryKey>();
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.PrivacyFiles))
					nodes.AddRange(DataContext.OneClickScanState.PrivacyNodes);
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.MalwareProcesses) ||
					overallAreasToFix.Contains(OneClickScanOverallAreaEnum.StartupProcesses) ||
					overallAreasToFix.Contains(OneClickScanOverallAreaEnum.ActiveProcesses))
					nodes.AddRange(DataContext.OneClickScanState.ProcessesNodes);
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.RegistryProblems))
					nodes.AddRange(DataContext.OneClickScanState.RegistryNodes);

				// calculate items (leaf) to fix
				int itemsToFixCount = 0;
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.PrivacyFiles))
					itemsToFixCount += DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount;
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.MalwareProcesses))
					itemsToFixCount += DataContext.OneClickScanState.MalwareProcessesItemsSelectedCount;
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.StartupProcesses))
					itemsToFixCount += DataContext.OneClickScanState.StartupProcessesItemsSelectedCount;
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.ActiveProcesses))
					itemsToFixCount += DataContext.OneClickScanState.ActiveProcessesItemsSelectedCount;
				if (overallAreasToFix.Contains(OneClickScanOverallAreaEnum.RegistryProblems))
					itemsToFixCount += DataContext.OneClickScanState.RegistryProblemsItemsSelectedCount;

				long lSeqNum = 0;
				// Create Restore Point
				if (Options.Instance.GeneralSettings.CreateRestorePointBeforeFixing)
					SysRestoreHelper.StartRestore(string.Format("Before {0} Registry Fix", Application.ProductName), out lSeqNum);

				// fixing
				int fixedItemsCount = 0;
				foreach (var badRegistryKey in nodes)
				{
					FixNode_Recursive(badRegistryKey, overallAreasToFix, ref fixedItemsCount, itemsToFixCount);
				}

				if (Options.Instance.GeneralSettings.CreateRestorePointBeforeFixing)
					SysRestoreHelper.EndRestore(lSeqNum);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.ERROR, string.Format("Erro while One-Click Fix.\r\n{0}", ex));
			}
			finally
			{
				DataContext.OneClickScanState.FixingProgress = 100;
				_threadFixMain = null;
				IsFixing = false;
				if (!_isFixAborted && FixFinished != null)
					FixFinished(null, new EventArgs());
				_isFixAborted = false;
			}
		}

		/// <summary>
		/// Fixing items of node
		/// </summary>
		/// <param name="node"></param>
		private static void FixNode_Recursive(BadRegistryKey node, List<OneClickScanOverallAreaEnum> overallAreasToFix, ref int fixedItemsCount, int itemsToFixCount)
		{
			if (node.IsLeaf && node.Checked == CheckState.Checked)
			{
				DataContext.OneClickScanState.CurrentFixedObject = node.RegKeyPath;
				foreach (var oneClickScanOverallArea in overallAreasToFix)
				{
					if (node.OneClickScanOverallAreas.Contains(oneClickScanOverallArea))
					{
						// fixing
						if (Config.EnableFixing)
						{
							if (node.OneClickScanOverallAreas.Contains(OneClickScanOverallAreaEnum.PrivacyFiles))
							{
								var privacyScannerPath = node.PrivacyScanner as PrivacyScannerPath;
								if (privacyScannerPath.IsDirectory && Directory.Exists(privacyScannerPath.Path) && !DirectoryHelper.IsClear(privacyScannerPath.Path))
									// clear privacy folder
									DirectoryHelper.Clear(privacyScannerPath.Path);
								if (!privacyScannerPath.IsDirectory)
								{
									// clear privacy files
									var folder = Path.GetDirectoryName(privacyScannerPath.Path);
									var fileMask = Path.GetFileName(privacyScannerPath.Path);
									var files = Directory.GetFiles(folder, fileMask);
									foreach (var file in files)
										try
										{
											File.Delete(file);
										}
										catch (Exception ex)
										{
											CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
										}
								}

								//if (Directory.Exists(node.RegKeyPath) && !DirectoryHelper.IsClear(node.RegKeyPath))
								//    // clear privacy folder
								//    DirectoryHelper.Clear(node.RegKeyPath);
								//else if (RegistryHelper.RegKeyExists(node.RegKeyPath))
								//{
								//    // clear privacy registry
								//    xmlRegistry xmlReg = new xmlRegistry();
								//    xmlReg.ClearRegistryKey(node.baseRegKey, node.subRegKey, node.ValueName);
								//}
							}
							if (node.OneClickScanOverallAreas.Contains(OneClickScanOverallAreaEnum.MalwareProcesses) ||
								node.OneClickScanOverallAreas.Contains(OneClickScanOverallAreaEnum.ActiveProcesses))
							{
								// kill process
								ProcessInfoLogic.KillByProcessId(int.Parse(node.subRegKey));
							}
							if (node.OneClickScanOverallAreas.Contains(OneClickScanOverallAreaEnum.StartupProcesses))
							{
								// remove process from startup
								StartupProcessLogic.Delete(node.StartupProcess);
							}
							if (node.OneClickScanOverallAreas.Contains(OneClickScanOverallAreaEnum.RegistryProblems))
							{
								// clean registry
								xmlRegistry xmlReg = new xmlRegistry();
								xmlReg.deleteRegistryKey(node.baseRegKey, node.subRegKey, node.ValueName);
							}
						}
						else
							Thread.Sleep(50);
						fixedItemsCount++;
						DataContext.OneClickScanState.FixingProgress = 100 * fixedItemsCount / itemsToFixCount;
						break;
					}
				}
			}
			else
			{
				DataContext.OneClickScanState.CurrentFixerName = string.Format("{0} {1}", node.Problem, node.SectionName);
				foreach (var childNode in node.Nodes)
					FixNode_Recursive((BadRegistryKey)childNode, overallAreasToFix, ref fixedItemsCount, itemsToFixCount);
			}
		}

		/// <summary>
		/// Force stop fixing
		/// </summary>
		public static void AbortFix()
		{
			_isFixAborted = true;
			IsFixing = false;
			if (_threadFixMain != null)
			{
				try
				{
					_threadFixMain.Abort();
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
				}

				_threadFixMain = null;
				if (FixAborted != null)
					FixAborted(null, new EventArgs());
			}
		}
	}
}