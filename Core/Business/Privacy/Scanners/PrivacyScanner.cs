﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Common;
using Common.Xml;
using Data;
using Data.Privacy;
using Data.Registry;

namespace Business.Privacy.Scanners
{
	public class PrivacyScanner : ScannerBase
	{
		public override string ScannerName
		{
			get { return _privacyScannerRoot.Name; }
		}
		private static PrivacyScannerRoot _privacyScannerRoot;

		public PrivacyScanner(PrivacyScannerRoot privacyScannerRoot)
		{
			_privacyScannerRoot = privacyScannerRoot;
		}

		/// <summary>
		/// Scan processes
		/// </summary>
		public static void Scan()
		{
			try
			{
				BadRegistryKey badRegistryKey;
				foreach (var privacyScannerSubNode in _privacyScannerRoot.ChildItems)
				{
					var badRegistryKeyParent = new BadRegistryKey(privacyScannerSubNode.Name, "", "", "");

					foreach (var privacyScannerSubSubNode in ((PrivacyScannerApplication)privacyScannerSubNode).ChildItems)
					{
						var leaf = (PrivacyScannerPath)privacyScannerSubSubNode;
						string description = "";
						if (!Options.Instance.PrivacySettings.Contains(leaf.Id))
							continue;

						if (leaf.IsDirectory)
						{
							if (Directory.Exists(leaf.Path) && !DirectoryHelper.IsClear(leaf.Path))
							{
								try
								{
									description = string.Format("{0} Files - {1}",
																Directory.GetFiles(leaf.Path, "*.*", SearchOption.AllDirectories).Length,
																SizeHelper.GetSize(DirectoryHelper.DirSize(new DirectoryInfo(leaf.Path))));
								}
								catch (Exception ex)
								{
									CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
								}
								badRegistryKey = new BadRegistryKey(CheckState.Checked,
																	privacyScannerSubSubNode.Name,
																	leaf.Path, "", description);
								badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.PrivacyFiles);
								badRegistryKey.PrivacyScanner = leaf;
								badRegistryKeyParent.Nodes.Add(badRegistryKey);
								DataContext.OneClickScanState.ItemsToFixCount++;
								DataContext.OneClickScanState.PrivacyFilesItemsToFixCount++;
								DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount++;
							}
						}
						else
						{
							var folder = Path.GetDirectoryName(leaf.Path);
							var fileMask = Path.GetFileName(leaf.Path);
							var files = Directory.GetFiles(folder, fileMask);
							ulong fileCount = 0;
							ulong fileSize = 0;
							foreach (var file in files)
							{
								if (File.Exists(file))
								{
									try
									{
										fileCount ++;
										fileSize += (ulong)new FileInfo(file).Length;
									}
									catch (Exception ex)
									{
										CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
									}
								}
							}
							description = string.Format("{0} Files - {1}", fileCount, SizeHelper.GetSize(fileSize));

							badRegistryKey = new BadRegistryKey(CheckState.Checked,
																privacyScannerSubSubNode.Name,
																leaf.Path, "", description);
							badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.PrivacyFiles);
							badRegistryKey.PrivacyScanner = leaf;
							badRegistryKeyParent.Nodes.Add(badRegistryKey);
							DataContext.OneClickScanState.ItemsToFixCount++;
							DataContext.OneClickScanState.PrivacyFilesItemsToFixCount++;
							DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount++;
						}
						/*
						{
							string baseKey, subKey;
							RegistryHelper.ParseRegKeyPath(leaf.Path, out baseKey, out subKey);
							if (RegistryHelper.RegKeyExists(leaf.Path) && !xmlRegistry.IsClear(baseKey, subKey))
							{
								badRegistryKey = new BadRegistryKey(CheckState.Checked,
												   privacyScannerSubSubNode.Name,
												   baseKey, subKey, description);
								badRegistryKey.OneClickScanOverallAreas.Add(OneClickScanOverallAreaEnum.PrivacyFiles);
								badRegistryKeyParent.Nodes.Add(badRegistryKey);
								DataContext.OneClickScanState.ItemsToFixCount++;
								DataContext.OneClickScanState.PrivacyFilesItemsToFixCount++;
								DataContext.OneClickScanState.PrivacyFilesItemsSelectedCount++;
							}
						}
						*/
					}

					badRegistryKeyParent.VerifyCheckState();
					if (badRegistryKeyParent.Nodes.Count > 0)
						DataContext.OneClickScanState.CurrentScanner.RootNode.Nodes.Add(badRegistryKeyParent);
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
	}
}
