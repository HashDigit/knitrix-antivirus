using System;
using System.Collections.Generic;
using System.IO;
using Common;
using Data.Startup;
using Microsoft.Win32;

namespace Business.Startup
{
	public class StartupProcessLogic
	{
		#region List
		/// <summary>
		/// Return list of all startup processes
		/// </summary>
		/// <returns></returns>
		public static List<StartupProcess> List()
		{
			var result = new List<StartupProcess>();
			// Adds registry keys
			try
			{
				// all user keys
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));

				if (RegistryHelper.Is64BitOS)
				{
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));
				}

				// current user keys
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
				LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"));

				if (RegistryHelper.Is64BitOS)
				{
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer\\Run"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunServices"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\Setup"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunOnce"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\RunEx"));
					LoadRegistryAutoRun(result, Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"));
				}

				// Adds startup folders
				AddStartupFolder(result, RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_STARTUP));
				AddStartupFolder(result, RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_COMMON_STARTUP));
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}

			return result;
		}

		/// <summary>
		/// Loads registry sub key into result
		/// </summary>
		private static void LoadRegistryAutoRun(List<StartupProcess> result, RegistryKey regKey)
		{
			try
			{
				if (regKey == null)
					return;

				if (regKey.ValueCount <= 0)
					return;

				//StartupProcess nodeRoot = new StartupProcess(regKey.Name);

				foreach (string strItem in regKey.GetValueNames())
				{
					string strFilePath = regKey.GetValue(strItem) as string;

					if (!string.IsNullOrEmpty(strFilePath))
					{
						// Get file arguments
						string strFile = "", strArgs = "";

						if (RegistryHelper.FileExists(strFilePath))
							strFile = strFilePath;
						else
						{
							if (!RegistryHelper.ExtractArguments(strFilePath, out strFile, out strArgs))
								if (!RegistryHelper.ExtractArguments2(strFilePath, out strFile, out strArgs))
									// If command line cannot be extracted, set file path to command line
									strFile = strFilePath;
						}

						StartupProcess node = new StartupProcess();
						node.Section = regKey.Name;
						node.Item = strItem;
						node.Path = strFile.Trim('"');
						node.Args = strArgs;

						result.Add(node);
					}
				}
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}

		/// <summary>
		/// Loads startup folder into result
		/// </summary>
		private static void AddStartupFolder(List<StartupProcess> result, string strFolder)
		{
			try
			{
				if (string.IsNullOrEmpty(strFolder) || !Directory.Exists(strFolder))
					return;

				//StartupProcess nodeRoot = new StartupProcess(strFolder);

				foreach (string strShortcut in Directory.GetFiles(strFolder))
				{
					string strShortcutName = Path.GetFileName(strShortcut);
					string strFilePath, strFileArgs;

					if (Path.GetExtension(strShortcut) == ".lnk")
					{
						if (!RegistryHelper.ResolveShortcut(strShortcut, out strFilePath, out strFileArgs))
							continue;

						StartupProcess node = new StartupProcess();
						node.Folder = strFolder;
						node.Item = strShortcutName;
						node.Path = strFilePath.Trim('"');
						node.Args = strFileArgs;

						result.Add(node);
					}
				}

				//if (nodeRoot.Nodes.Count <= 0)
				//	return;

				//this.treeModel.Nodes.Add(nodeRoot);
			}
			catch (Exception ex)
			{
				CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
			}
		}
		#endregion List

		/// <summary>
		/// Remove process from startup
		/// </summary>
		/// <param name="startupProcess"></param>
		public static void Delete(StartupProcess startupProcess)
		{
			bool bFailed = false;

			if (Directory.Exists(startupProcess.Folder))
			{
				try
				{
					// Startup folder
					string strPath = Path.Combine(startupProcess.Folder, startupProcess.Item);
					if (File.Exists(strPath))
						File.Delete(strPath);
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
					bFailed = true;
				}
			}
			else
			{
				// Registry key
				string strMainKey = startupProcess.Section.Substring(0, startupProcess.Section.IndexOf('\\'));
				string strSubKey = startupProcess.Section.Substring(startupProcess.Section.IndexOf('\\') + 1);
				RegistryKey rk = null;

				try
				{
					rk = RegistryHelper.RegOpenKey(strMainKey, strSubKey);
					if (rk != null)
						rk.DeleteValue(startupProcess.Item);
				}
				catch (Exception ex)
				{
					CLogger.WriteLog(ELogLevel.WARN, ex.ToString());
					bFailed = true;
				}
				finally
				{
					if (rk != null)
						rk.Close();
				}
			}
		}
	}
}