﻿using System;
using System.Collections.Generic;
using Data.Registry;

namespace Business
{
    public class Fixer
    {
        /// <summary>
        /// Returns the fixer name
        /// </summary>
        public string FixerName { get; set;}

        /// <summary>
        /// The root node (used for scan dialog)
        /// </summary>
        public BadRegistryKey RootNode = new BadRegistryKey();

        /// <summary>
        /// Returns the fixer name
        /// </summary>
        public override string ToString()
        {
			return (string)FixerName.Clone();
        }

        //public virtual void Scan()
        //{
        //    return;
        //}
 
    }
}
