using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;

namespace Common
{
	public class HttpRequestHelper
	{
		static HttpRequestHelper()
		{
		}

		public static string Request(string url, Encoding encoding, string method, byte[] data)
		{
			HttpWebRequest myRequest = GetRequest(url, method, data);

			string result;
			using (WebResponse myResponse = myRequest.GetResponse())
			{
				using (StreamReader sr = new StreamReader(myResponse.GetResponseStream(), encoding))
				{
					result = sr.ReadToEnd();
				}
			}
			return result;
		}

		public static byte[] Request(string url)
		{
			HttpWebRequest myRequest = GetRequest(url, "GET", null);

			using (WebResponse myResponse = myRequest.GetResponse())
			{
				using (Stream s = myResponse.GetResponseStream())
				{
					return ReadStream(s);
				}
			}
		}

		public static long RequestUrlSize(string url)
		{
			HttpWebRequest myRequest = GetRequest(url, "GET", null);

			using (WebResponse myResponse = myRequest.GetResponse())
			{
				return myResponse.ContentLength;
			}
		}

		public static HttpWebRequest GetRequest(string url, string method, byte[] data)
		{
			HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
			myRequest.Method = method;
			//myRequest.Referer = url; �� �����������������, ��������� �������������� ������
			myRequest.UserAgent = "Opera/9.25 (Windows NT 6.0; U; en)";
			myRequest.Accept = "text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1";
			myRequest.Headers.Add("Accept-Language", "en,ru-RU;q=0.9,ru;q=0.8");
			myRequest.Headers.Add("Accept-Charset", "utf-8, *;q=0.1");
			myRequest.KeepAlive = true;

			if (method.ToLower() == "post")
			{
				myRequest.ContentType = "application/x-www-form-urlencoded";
				myRequest.ContentLength = data.Length;
				using (Stream myReqStream = myRequest.GetRequestStream())
				{
					// Send the data.
					myReqStream.Write(data, 0, data.Length);
				}
			}
			return myRequest;
		}

		/// <summary>
		/// Reads data from a stream until the end is reached. The
		/// data is returned as a byte array. An IOException is
		/// thrown if any of the underlying IO calls fail.
		/// </summary>
		/// <param name="stream">The stream to read data from</param>
		public static byte[] ReadStream(Stream stream)
		{
			byte[] buffer = new byte[32768];
			using (MemoryStream ms = new MemoryStream())
			{
				while (true)
				{
					int read = stream.Read(buffer, 0, buffer.Length);
					if (read <= 0)
						return ms.ToArray();
					ms.Write(buffer, 0, read);
				}
			}
		}

		/// <summary>
		/// Reads data from a stream until the end is reached. The
		/// data is returned as a byte array. An IOException is
		/// thrown if any of the underlying IO calls fail.
		/// </summary>
		/// <param name="stream">The stream to read data from</param>
		/// <param name="initialLength">The initial buffer length</param>
		public static byte[] ReadStream(Stream stream, long initialLength)
		{
			// If we've been passed an unhelpful initial length, just
			// use 32K.
			if (initialLength < 1)
			{
				initialLength = 32768;
			}

			byte[] buffer = new byte[initialLength];
			int read = 0;

			int chunk;
			while ((chunk = stream.Read(buffer, read, buffer.Length - read)) > 0)
			{
				read += chunk;

				// If we've reached the end of our buffer, check to see if there's
				// any more information
				if (read == buffer.Length)
				{
					int nextByte = stream.ReadByte();

					// End of stream? If so, we're done
					if (nextByte == -1)
					{
						return buffer;
					}

					// Nope. Resize the buffer, put in the byte we've just
					// read, and continue
					byte[] newBuffer = new byte[buffer.Length * 2];
					Array.Copy(buffer, newBuffer, buffer.Length);
					newBuffer[read] = (byte)nextByte;
					buffer = newBuffer;
					read++;
				}
			}
			// Buffer is now too big. Shrink it.
			byte[] ret = new byte[read];
			Array.Copy(buffer, ret, read);
			return ret;
		}

		public static XmlDocument RequestXml(string url, Encoding encoding, string method, byte[] data)
		{
			string text = HttpRequestHelper.Request(url, encoding, method, data);
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(text);
			return xmlDoc;
		}

		public static XmlDocument RequestXml(string url, Encoding encoding)
		{
			return RequestXml(url, encoding, "GET", null);
		}
	}
}
