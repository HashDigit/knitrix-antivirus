using System.IO;
using Data;

namespace Common
{
	public class ProcessInfoHelper
	{
		/// <summary>
		/// Detect ProcessInfoType using path to process file
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static ProcessInfoTypeEnum GetProcessInfoTypeEnum(string path)
		{
			if (string.IsNullOrEmpty(path))
				return ProcessInfoTypeEnum.Other;

			// NOTE :: (AK) �������, �� ����� ������
			var infoType = path.ToLower().StartsWith(@"c:\window") ? ProcessInfoTypeEnum.System : ProcessInfoTypeEnum.UserInstalled;
			var fileName = Path.GetFileName(path);
			if (ProcessInfoHelper.IsUnwanted(fileName))
			{
				infoType = ProcessInfoTypeEnum.Malware;
			}
			return infoType;
		}

		/// <summary>
		/// Does this process file exist in Unwanted Process List?
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		private static bool IsUnwanted(string fileName)
		{
			foreach (var unwantedProcesses in DataContext.MalwareProcesses)
			{
				if (fileName.ToLower() == unwantedProcesses.ToLower())
					return true;
			}
			return false;
		}

	}
}