﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using FileListGenerator.Classes;

namespace FileListGenerator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Enumeration _Enumeration = new Enumeration();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            _Enumeration.InitializeFileOperation();
        }
    }
}
