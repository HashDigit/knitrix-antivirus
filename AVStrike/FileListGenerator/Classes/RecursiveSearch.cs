﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Collections.Specialized;
using System.IO;

namespace FileListGenerator.Classes
{
    public class RecursiveFileSearch
    {

        private StringCollection log = new System.Collections.Specialized.StringCollection();
        private long totalScanSize = 0;
        private List<FileInfo> filteredFiles = null;

        public List<FileInfo> GetFilesToBeScanned()
        {
            return filteredFiles;
        }

        public int FileCountToBeScanned()
        {
            return filteredFiles.Count;
        }

        public StringCollection GetLog()
        {
            return log;
        }

        public List<FileInfo> GetFilteredFiles()
        {
            return filteredFiles;
        }

        private long fileignored = 0;

        public long getTotalScanSize(string measurement)
        {
            if (measurement == null || measurement.Equals("") || measurement.Equals("B"))
                return totalScanSize;
            else if (measurement.ToUpper().Equals("GB"))
            {
                return totalScanSize / 1073741824;
            }
            else if (measurement.ToUpper().Equals("MB"))
            {
                return (totalScanSize / 1073741824) * 1024;
            }
            else
                return totalScanSize;
        }

        public void GetAllFiles(int sizeFilter, List<string> fileExtensionFilter, List<string> excludedFolders)
        {
            filteredFiles = null;
            totalScanSize = 0;
            // Start with drives if you have to search the entire computer.
            string[] drives = System.Environment.GetLogicalDrives();
            foreach (string dr in drives)
            {
                string strDriveType = dr.ToString();
                if (strDriveType == @"A:\" || strDriveType == @"B:\\")
                    continue;

                if (!excludedFolders.Contains(dr))
                {
                    System.IO.DriveInfo di = new System.IO.DriveInfo(dr);
                    // Here we skip the drive if it is not ready to be read. This
                    // is not necessarily the appropriate action in all scenarios.
                    if (!di.IsReady)
                    {
                        continue;
                    }
                    DirectoryInfo rootDir = di.RootDirectory;
                    if (!excludedFolders.Contains(rootDir.FullName))
                    {
                        SearchDirectoryRecursive(rootDir, sizeFilter, fileExtensionFilter, excludedFolders);
                    }
                }
            }
        }

        public List<FileInfo> GetFilesForDirectory(string folderPath, int sizeFilter, List<string> fileExtensionFilter, List<string> excludedFolders)
        {
            filteredFiles = null;
            totalScanSize = 0;
            // Start with drives if you have to search the entire computer.
            DirectoryInfo dirInfo = new DirectoryInfo(folderPath);
            //   DirectoryInfo rootDir = di.RootDirectory;
            if (!excludedFolders.Contains(dirInfo.FullName))
            {
                SearchDirectoryRecursive(dirInfo, sizeFilter, fileExtensionFilter, excludedFolders);
            }
            return filteredFiles;
        }

        private void SearchDirectoryRecursive(System.IO.DirectoryInfo root, int sizeFilter, List<string> fileExtensionFilter, List<string> excludedFolders)
        {
            DirectoryInfo[] subDirs = null;
            FileInfo[] files = null;

            // First, process all the files directly under this folder
            try
            {
                files = root.GetFiles("*.*");
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException e)
            {
                // This code just writes out the message and continues to recurse.
                // You may decide to do something different here. For example, you
                // can try to elevate your privileges and access the file again.
                log.Add(e.Message);
                //ErrorLog.ErrorFile(fileignored.ToString());
                fileignored++;
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                // Console.WriteLine(e.Message);
                //ErrorLog.ErrorFile(fileignored.ToString());
                fileignored++;
            }
            if (files != null)
            {
                int totalFilesCount = files.Length;
                if (filteredFiles == null)
                {
                    filteredFiles = new List<FileInfo>();
                }
                foreach (System.IO.FileInfo fi in files)
                {
                    // In this example, we only access the existing FileInfo object. If we
                    // want to open, delete or modify the file, then
                    // a try-catch block is required here to handle the case
                    // where the file has been deleted since the call to TraverseTree().
                    //Console.WriteLine(fi.FullName);
                    if (fi.Exists)
                    {
                        try
                        {
                            if (fi.Length < (sizeFilter * 1048576) || (fileExtensionFilter != null && !fileExtensionFilter.Contains(fi.Extension)))
                            {
                                totalScanSize += fi.Length;
                                filteredFiles.Add(fi);
                            }
                        }
                        catch (System.IO.PathTooLongException ex)
                        {
                            //ErrorLog.ErrorFile(fileignored.ToString());
                            fileignored++;
                        }
                        catch (FileNotFoundException ex)
                        {
                            //ErrorLog.ErrorFile(fileignored.ToString());
                            fileignored++;
                        }
                    }
                    else
                    {
                        // ErrorLog.ErrorFile("Does not exist" + fi.FullName);
                    }
                }
                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories();
                foreach (DirectoryInfo dirInfo in subDirs)
                {

                    try
                    {
                        // Resursive call for each subdirectory.
                        if (!excludedFolders.Contains(dirInfo.FullName))
                        {
                            SearchDirectoryRecursive(dirInfo, sizeFilter, fileExtensionFilter, excludedFolders);
                        }
                    }
                    catch (System.IO.PathTooLongException ex)
                    {
                        //ErrorLog.ErrorFile(fileignored.ToString());
                        fileignored++;
                    }
                }
            }
        }


        public void GetQuickScanFiles()
        {
            filteredFiles = null;
            totalScanSize = 0;
            if (filteredFiles == null)
            {
                filteredFiles = new List<FileInfo>();
            }
            Process[] local = Process.GetProcesses();

            foreach (Process p in local)
            {
                try
                {
                    ProcessModuleCollection myprocess = p.Modules;
                    foreach (ProcessModule pm in myprocess)
                    {
                        FileInfo fi = new FileInfo(pm.FileName);
                        filteredFiles.Add(fi);
                        totalScanSize += fi.Length;
                    }
                }
                catch
                { }
            }
        }
    }
}
