﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Data;

namespace FileListGenerator.Classes
{
    class DataConnection
    {
        //ErrorLog _ErrorLog = new ErrorLog();


        SQLiteConnection _SQLiteConnection;
        SQLiteCommand _SQLiteCommand;
        SQLiteDataAdapter _SQLiteDataAdapter;


        //Enable the Connection
        //connection from the app config page.
        public DataConnection()
        {
            string str = Queries.DataBaseName;
            string strDataString = "Data Source='" + Queries.DataBaseName + "';Version='" + Queries.DataBaseVersion + "';Password='" + Queries.DatabasePassword + "'";
            _SQLiteConnection = new SQLiteConnection(strDataString);
        }


        #region Open and Close the Connection
        //Connection open
        private SQLiteConnection Open()
        {
            if (_SQLiteConnection.State == ConnectionState.Closed || _SQLiteConnection.State == ConnectionState.Broken)
            {
                _SQLiteConnection.Open();
            }
            return _SQLiteConnection;
        }

        //Connection close
        private SQLiteConnection Close()
        {
            if (_SQLiteConnection.State == ConnectionState.Open)
            {
                _SQLiteConnection.Close();
            }
            return _SQLiteConnection;
        }
        #endregion


        //Create the SQLite DB file
        public void CreateFile(string strFileName)
        {
            SQLiteConnection.CreateFile(strFileName);
        }


        public bool CreateSQLiteTable(string strQuery)
        {

            try
            {
                Open();
                _SQLiteCommand = new SQLiteCommand(strQuery, _SQLiteConnection);
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            { return false; }
        }

        public bool InsertQuery(string strQuery)
        {
            _SQLiteDataAdapter = new SQLiteDataAdapter();
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            try
            {
                _SQLiteCommand.Connection = Open();
                _SQLiteCommand.CommandText = strQuery;
                _SQLiteDataAdapter.InsertCommand = _SQLiteCommand;
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            { return false; }
        }

        public bool UpdateQuery(string strQuery)
        {
            _SQLiteDataAdapter = new SQLiteDataAdapter();
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            try
            {
                _SQLiteCommand.Connection = Open();
                _SQLiteCommand.CommandText = strQuery;
                _SQLiteDataAdapter.UpdateCommand = _SQLiteCommand;
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            { return false; }
        }


        public bool DeleteQuery(string strQuery)
        {
            _SQLiteDataAdapter = new SQLiteDataAdapter();
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            try
            {
                _SQLiteCommand.Connection = Open();
                _SQLiteCommand.CommandText = strQuery;
                _SQLiteDataAdapter.DeleteCommand = _SQLiteCommand;
                _SQLiteCommand.ExecuteNonQuery();
                return true;
            }
            catch (SQLiteException ex)
            { return false; }
        }

        //to view datas
        public SQLiteDataReader SelectQuery(string strQuery)
        {
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            SQLiteDataReader _SQLiteDataReader;

            try
            {
                Open();
                _SQLiteCommand = new SQLiteCommand(strQuery, _SQLiteConnection);
                _SQLiteDataReader = _SQLiteCommand.ExecuteReader();
            }
            catch (SQLiteException ex)
            { return null; }
            finally
            { }

            return _SQLiteDataReader;
        }

        public int SelectQueryCount(string strQuery)
        {
            SQLiteCommand _SQLiteCommand = new SQLiteCommand();
            int _SQLiteDataReader;

            try
            {
                Open();
                _SQLiteCommand = new SQLiteCommand(strQuery, _SQLiteConnection);
                _SQLiteDataReader = Convert.ToInt32(_SQLiteCommand.ExecuteScalar());

            }
            catch (SQLiteException ex)
            { return 0; }
            finally
            { }

            return _SQLiteDataReader;
        }
    }
}
