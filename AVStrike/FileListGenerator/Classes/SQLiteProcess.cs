﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileListGenerator.Classes
{
    class SQLiteProcess
    {
        DataConnection _DataConnection = new DataConnection();
        public void InsertHashedFiles(string strFileHash)
        {
            int colCount = _DataConnection.SelectQueryCount(string.Format(Queries.HashSelect, strFileHash));
            if (colCount == 0)
            {
                _DataConnection.InsertQuery(string.Format(Queries.HashInsert, strFileHash));
            }
        }

        public int SelectCountHash(string strFileHash)
        {
            return _DataConnection.SelectQueryCount(string.Format(Queries.HashSelect, strFileHash));
        }

        public void DeleteHash()
        {
            _DataConnection.DeleteQuery("Delete from HashedFiles");
        }
    }
}
