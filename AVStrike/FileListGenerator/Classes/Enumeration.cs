﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Security.Cryptography;

namespace FileListGenerator.Classes
{
    class Enumeration
    {
        RecursiveFileSearch _RecursiveFileSearch = new RecursiveFileSearch();
        SQLiteProcess _SQLiteProcess = new SQLiteProcess();

        public List<string> excludedfiles()
        {
            List<string> lines = new List<string>();
            lines.Add("");
            return lines;
        }
        public List<string> excludedfolders()
        {
            List<string> lines = new List<string>();
            lines.Add("");
            return lines;
        }

        public void InitializeFileOperation()
        {
            _SQLiteProcess.DeleteHash();
            Thread main = new Thread(new ThreadStart(StartEnumeration));
            main.Start();
            Thread.Sleep(2500);

            List<FileInfo> filesToBeScanned = null;
            filesToBeScanned = _RecursiveFileSearch.GetFilesToBeScanned();

            for (int i = 0; i < _RecursiveFileSearch.FileCountToBeScanned(); i++)
            {
                FileInfo file = filesToBeScanned[i];



                try
                {
                    file.OpenRead();
                }
                catch (UnauthorizedAccessException e)
                { continue; }
                catch (System.IO.DirectoryNotFoundException e)
                { continue; }
                catch (IOException e)
                { continue; }

                if (IsFileLocked(file))
                    continue;

                if (Path.GetExtension(file.FullName.ToLower()) == ".sys")
                    continue;

                string hashvalue = MD5Hash(file.FullName);

                _SQLiteProcess.InsertHashedFiles(hashvalue);

                if ((_RecursiveFileSearch.FileCountToBeScanned() - i) < 500)
                    Thread.Sleep(500);
            }


        }
        public void StartEnumeration()
        {
            _RecursiveFileSearch.GetAllFiles(0, excludedfiles(), excludedfolders());
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        public string MD5Hash(string strFilePath)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            FileStream stream = new FileStream(strFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
            //compute hash from the bytes of text
            //md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            md5.ComputeHash(stream);

            stream.Close();

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }
}
