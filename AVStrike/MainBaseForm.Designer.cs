﻿using System.Windows.Forms;
namespace AVStrike
{
    partial class MainBaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ColumnHeader colHeaderDate;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainBaseForm));
            this.cmnuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.niMain = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eXITToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colOptimizeManagerSection = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.colOptimizeManagerPath = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.colOptimizeManagerArgs = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.backgroundWorkerAntivirus = new System.ComponentModel.BackgroundWorker();
            this.treeColumn4 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn5 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.treeColumn6 = new Common_Tools.TreeViewAdv.Tree.TreeColumn();
            this.nodeCheckBox = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox();
            this.nodeIcon = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon();
            this.nodeSection = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeProblem = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeLocation = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeValueName = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.imageListTreeView = new System.Windows.Forms.ImageList(this.components);
            this.imageListBrowserObjectManager = new System.Windows.Forms.ImageList(this.components);
            this.ilStartupManager = new System.Windows.Forms.ImageList(this.components);
            this.nodeIconOptimizeManagerIcon = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon();
            this.nodeTextBoxOptimizeManagerSection = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeTextBoxOptimizeManagerItem = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeTextBoxOptimizeManagerPath = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.nodeTextBoxOptimizeManagerArguments = new Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.cmnuOptimizeRegistryManager = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiOptimizeRegistryManagerSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeRegistryManagerSelectNone = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeRegistryManagerInvertSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiOptimizeRegistryManagerExcludeSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOptimizeRegistryManagerViewinRegEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMainBackground = new AVStrike.Controls.Panels.MainPanel();
            this.pnlHomeMessageLayout = new AVStrike.Controls.Panels.MainPanel();
            this.lblHomeMessageDescription3 = new System.Windows.Forms.Label();
            this.lblHomeMessageDescription1 = new System.Windows.Forms.Label();
            this.lblHomeMessageDescription2 = new System.Windows.Forms.Label();
            this.pnlMainCover = new System.Windows.Forms.Panel();
            this.pnlHomeMainBasic = new AVStrike.Controls.Panels.MainPanel();
            this.pnlHomeMainCover = new AVStrike.Controls.Panels.MainPanel();
            this.pnlHomeMainDefault = new AVStrike.Controls.Panels.MainPanel();
            this.btnSubHomeQuickScan = new AVStrike.Controls.CustomButtons.SubProcessButton();
            this.lblHomeMainLicenseTypeDescription = new System.Windows.Forms.Label();
            this.lblHomeMainLastScannedOnDescription = new System.Windows.Forms.Label();
            this.lblHomeMainLicenseExpiryDateDescription = new System.Windows.Forms.Label();
            this.lblHomeMainVirusDefinitionVersionDescription = new System.Windows.Forms.Label();
            this.lblHomeMainDefinitionAutoUpdateDescription = new System.Windows.Forms.Label();
            this.lblHomeMainLicenseTypeDescriber = new System.Windows.Forms.Label();
            this.lblHomeMainLastScannedOnDescriber = new System.Windows.Forms.Label();
            this.lblHomeMainLicenseExpiryDateDescriber = new System.Windows.Forms.Label();
            this.lblHomeMainVirusDefinitionVersionDescriber = new System.Windows.Forms.Label();
            this.lblHomeMainDefinitionAutoUpdateDescriber = new System.Windows.Forms.Label();
            this.lblHomeMainRealTimeProtectionDescription = new System.Windows.Forms.Label();
            this.lblHomeMainRealTimeProtectionDescriber = new System.Windows.Forms.Label();
            this.pnlHomeImageLayout = new AVStrike.Controls.Panels.MainPanel();
            this.pnlHomeMainScanProcess = new AVStrike.Controls.Panels.MainPanel();
            this.prgAntivirusScan = new System.Windows.Forms.ProgressBar();
            this.splBtnStopScanning = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.lblNuetralizedDescriber = new System.Windows.Forms.Label();
            this.lblNeutralizedDescription = new System.Windows.Forms.Label();
            this.lblThreatsDetectedDescriber = new System.Windows.Forms.Label();
            this.lblThreatsDetectedDescription = new System.Windows.Forms.Label();
            this.lblTimeElapsed = new System.Windows.Forms.Label();
            this.lblTimeElapsedDescription = new System.Windows.Forms.Label();
            this.lblScanInProcess = new System.Windows.Forms.Label();
            this.lblScanInProcessDescription = new System.Windows.Forms.Label();
            this.lblItemsScannedCount = new System.Windows.Forms.Label();
            this.lblItemsScannedCountDescription = new System.Windows.Forms.Label();
            this.pnlScanMainBasic = new AVStrike.Controls.Panels.MainPanel();
            this.pnlScanCover = new AVStrike.Controls.Panels.MainPanel();
            this.pnlScanDefault = new AVStrike.Controls.Panels.MainPanel();
            this.btnSubHomeFullScan = new AVStrike.Controls.CustomButtons.SubProcessButton();
            this.btnSubHomeCustomScan = new AVStrike.Controls.CustomButtons.SubProcessButton();
            this.subProcessButton2 = new AVStrike.Controls.CustomButtons.SubProcessButton();
            this.mainPanel5 = new AVStrike.Controls.Panels.MainPanel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.specialButtons3 = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.pnlSettingsBasic = new AVStrike.Controls.Panels.MainPanel();
            this.pnlSettingsHolder = new AVStrike.Controls.Panels.MainPanel();
            this.pnlSettingsHolderAntivirus = new AVStrike.Controls.Panels.MainPanel();
            this.splBtnSettingsAntivirusCancel = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnSettingsAntivirusApply = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.tabControlSettingsSelection = new System.Windows.Forms.TabControl();
            this.tabPageScheduleScan = new System.Windows.Forms.TabPage();
            this.chkSettingRunAVStrikeWhenComputerIsOn = new System.Windows.Forms.CheckBox();
            this.lblSettingScanScheduleDayOfTheWeekDescriber = new System.Windows.Forms.Label();
            this.cmbBxSettingScanScheduleDayOfTheWeek = new System.Windows.Forms.ComboBox();
            this.lblSettingScanScheduleScanTypeDescriber = new System.Windows.Forms.Label();
            this.cmbBxSettingScanScheduleScanType = new System.Windows.Forms.ComboBox();
            this.chkSettingScanScheduleRunAScan = new System.Windows.Forms.CheckBox();
            this.tabPageAdvanced = new System.Windows.Forms.TabPage();
            this.lblSettingsAdvancedKeepQuarantinedFiles = new System.Windows.Forms.Label();
            this.lblSettingsAdvancedAllowFullHistory = new System.Windows.Forms.Label();
            this.chkSettingsAdvancedKeepQuarantinedFiles = new System.Windows.Forms.CheckBox();
            this.chkSettingsAdvancedAllowFullHistory = new System.Windows.Forms.CheckBox();
            this.tabPageExcludeFiles = new System.Windows.Forms.TabPage();
            this.splBtnSettingsAntivirusExcludeFilesRemove = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnSettingsAntivirusExcludeFilesAdd = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownSizeMaximum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.lvAntSetExcludedFiles = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPageExcludeFolders = new System.Windows.Forms.TabPage();
            this.splBtnSettingsAntivirusExcludeFolderRemove = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnSettingsAntivirusExcludeFoldersAdd = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.lvAntSetExcludedFolders = new System.Windows.Forms.ListView();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPageRealTimeProtection = new System.Windows.Forms.TabPage();
            this.lblSettingsRealTimeProtectionDescriber2 = new System.Windows.Forms.Label();
            this.lblSettingsRealTimeProtectionDescriber1 = new System.Windows.Forms.Label();
            this.chkSettingsRealTimeProtectionTurnOn = new System.Windows.Forms.CheckBox();
            this.pnlAntivirusBasic = new AVStrike.Controls.Panels.MainPanel();
            this.pnlAntivirusCover = new AVStrike.Controls.Panels.MainPanel();
            this.pnlAntivirusQuarantine = new AVStrike.Controls.Panels.MainPanel();
            this.chkQuarantineSelectUnselect = new System.Windows.Forms.CheckBox();
            this.splBtnRestore = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnRemoveAll = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnRemove = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.lvDiskDiskCleaner = new System.Windows.Forms.ListView();
            this.colFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderFilePath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colVirusName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderAlertLevel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colRecommendedAction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeaderAction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlHistoryBasic = new AVStrike.Controls.Panels.MainPanel();
            this.pnlHistoryCover = new AVStrike.Controls.Panels.MainPanel();
            this.pnlAntivirusScanResult = new AVStrike.Controls.Panels.MainPanel();
            this.pnlAntivirusScanHistoryValuesShow = new AVStrike.Controls.Panels.MainPanel();
            this.lblScanResultTotalDataScannedDescription = new System.Windows.Forms.Label();
            this.lblScanResultTotalDataScannedDescriber = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblScanResultThreatsNeutralizedDescription = new System.Windows.Forms.Label();
            this.lblScanResultThreatsDetectedDescription = new System.Windows.Forms.Label();
            this.lblScanResultScannedFilesDescription = new System.Windows.Forms.Label();
            this.lblScanResultRunTimeDescription = new System.Windows.Forms.Label();
            this.lblScanResultThreatsNeutralizedDescriber = new System.Windows.Forms.Label();
            this.lblScanResultTypeOfScanDescription = new System.Windows.Forms.Label();
            this.lblScanResultThreatsDetectedDescriber = new System.Windows.Forms.Label();
            this.lblScanResultScannedFilesDescriber = new System.Windows.Forms.Label();
            this.lblScanResultRunTimeDescriber = new System.Windows.Forms.Label();
            this.lblScanResultTypeOfScanDescriber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lvScanResult = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlLicenseExpireAlert = new AVStrike.Controls.Panels.MainPanel();
            this.lblLicenseExpireText4 = new System.Windows.Forms.Label();
            this.lblLicenseExpireText3 = new System.Windows.Forms.Label();
            this.lblLicenseExpireText2 = new System.Windows.Forms.Label();
            this.lblLicenseExpireText1 = new System.Windows.Forms.Label();
            this.btnLicenseAlertRenewNow = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.pnlMainTop = new AVStrike.Controls.Panels.MainPanel();
            this.splBtnClose = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnMinimize = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.btnMainScan = new AVStrike.Controls.CustomButtons.MainButton();
            this.specialButtons2 = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnUpgrade = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.specialButtons1 = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnBuyNow = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.btnMainHome = new AVStrike.Controls.CustomButtons.MainButton();
            this.btnMainSettings = new AVStrike.Controls.CustomButtons.MainButton();
            this.splBtnLiveChat = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.btnMainHistory = new AVStrike.Controls.CustomButtons.MainButton();
            this.btnMainAntivirus = new AVStrike.Controls.CustomButtons.MainButton();
            this.splBtnScanNow = new AVStrike.Controls.CustomButtons.SpecialButtons();
            colHeaderDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmnuTray.SuspendLayout();
            this.TrayMenu.SuspendLayout();
            this.cmnuOptimizeRegistryManager.SuspendLayout();
            this.pnlMainBackground.SuspendLayout();
            this.pnlHomeMessageLayout.SuspendLayout();
            this.pnlMainCover.SuspendLayout();
            this.pnlHomeMainBasic.SuspendLayout();
            this.pnlHomeMainCover.SuspendLayout();
            this.pnlHomeMainDefault.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubHomeQuickScan)).BeginInit();
            this.pnlHomeMainScanProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnStopScanning)).BeginInit();
            this.pnlScanMainBasic.SuspendLayout();
            this.pnlScanCover.SuspendLayout();
            this.pnlScanDefault.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubHomeFullScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubHomeCustomScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subProcessButton2)).BeginInit();
            this.mainPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specialButtons3)).BeginInit();
            this.pnlSettingsBasic.SuspendLayout();
            this.pnlSettingsHolder.SuspendLayout();
            this.pnlSettingsHolderAntivirus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusApply)).BeginInit();
            this.tabControlSettingsSelection.SuspendLayout();
            this.tabPageScheduleScan.SuspendLayout();
            this.tabPageAdvanced.SuspendLayout();
            this.tabPageExcludeFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFilesRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFilesAdd)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSizeMaximum)).BeginInit();
            this.tabPageExcludeFolders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFolderRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFoldersAdd)).BeginInit();
            this.tabPageRealTimeProtection.SuspendLayout();
            this.pnlAntivirusBasic.SuspendLayout();
            this.pnlAntivirusCover.SuspendLayout();
            this.pnlAntivirusQuarantine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnRemoveAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnRemove)).BeginInit();
            this.pnlHistoryBasic.SuspendLayout();
            this.pnlHistoryCover.SuspendLayout();
            this.pnlAntivirusScanResult.SuspendLayout();
            this.pnlAntivirusScanHistoryValuesShow.SuspendLayout();
            this.pnlLicenseExpireAlert.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnLicenseAlertRenewNow)).BeginInit();
            this.pnlMainTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specialButtons2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnUpgrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specialButtons1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnBuyNow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnLiveChat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainAntivirus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnScanNow)).BeginInit();
            this.SuspendLayout();
            // 
            // colHeaderDate
            // 
            colHeaderDate.Text = "Date Time";
            colHeaderDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            colHeaderDate.Width = 150;
            // 
            // cmnuTray
            // 
            this.cmnuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.cmnuTray.Name = "cmnuTray";
            this.cmnuTray.Size = new System.Drawing.Size(93, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(92, 22);
            this.toolStripMenuItem1.Text = "Exit";
            // 
            // niMain
            // 
            this.niMain.ContextMenuStrip = this.cmnuTray;
            this.niMain.Click += new System.EventHandler(this.niMain_Click);
            this.niMain.DoubleClick += new System.EventHandler(this.niMain_DoubleClick);
            // 
            // TrayIcon
            // 
            this.TrayIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.TrayIcon.BalloonTipText = "AKICK is Securing your PC";
            this.TrayIcon.BalloonTipTitle = "AKICK Antivirus";
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "AVStrike";
            this.TrayIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseClick);
            // 
            // TrayMenu
            // 
            this.TrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eXITToolStripMenuItem});
            this.TrayMenu.Name = "TrayMenu";
            this.TrayMenu.Size = new System.Drawing.Size(93, 26);
            // 
            // eXITToolStripMenuItem
            // 
            this.eXITToolStripMenuItem.Name = "eXITToolStripMenuItem";
            this.eXITToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.eXITToolStripMenuItem.Text = "Exit";
            // 
            // colOptimizeManagerSection
            // 
            this.colOptimizeManagerSection.Header = "Section";
            this.colOptimizeManagerSection.SortOrder = System.Windows.Forms.SortOrder.None;
            this.colOptimizeManagerSection.TooltipText = null;
            this.colOptimizeManagerSection.Width = 200;
            // 
            // colOptimizeManagerPath
            // 
            this.colOptimizeManagerPath.Header = "Path";
            this.colOptimizeManagerPath.SortOrder = System.Windows.Forms.SortOrder.None;
            this.colOptimizeManagerPath.TooltipText = null;
            this.colOptimizeManagerPath.Width = 250;
            // 
            // colOptimizeManagerArgs
            // 
            this.colOptimizeManagerArgs.Header = "Arguments";
            this.colOptimizeManagerArgs.SortOrder = System.Windows.Forms.SortOrder.None;
            this.colOptimizeManagerArgs.TooltipText = null;
            this.colOptimizeManagerArgs.Width = 60;
            // 
            // treeColumn4
            // 
            this.treeColumn4.Header = "Problem";
            this.treeColumn4.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn4.TooltipText = null;
            this.treeColumn4.Width = 100;
            // 
            // treeColumn5
            // 
            this.treeColumn5.Header = "Location";
            this.treeColumn5.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn5.TooltipText = null;
            this.treeColumn5.Width = 200;
            // 
            // treeColumn6
            // 
            this.treeColumn6.Header = "Value";
            this.treeColumn6.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn6.TooltipText = null;
            // 
            // nodeCheckBox
            // 
            this.nodeCheckBox.EditEnabled = true;
            this.nodeCheckBox.LeftMargin = 0;
            this.nodeCheckBox.ParentColumn = this.treeColumn4;
            // 
            // nodeIcon
            // 
            this.nodeIcon.DataPropertyName = "Img";
            this.nodeIcon.LeftMargin = 1;
            this.nodeIcon.ParentColumn = this.treeColumn4;
            this.nodeIcon.ScaleMode = Common_Tools.TreeViewAdv.Tree.ImageScaleMode.Clip;
            // 
            // nodeSection
            // 
            this.nodeSection.DataPropertyName = "SectionName";
            this.nodeSection.IncrementalSearchEnabled = true;
            this.nodeSection.LeftMargin = 3;
            this.nodeSection.ParentColumn = this.treeColumn4;
            this.nodeSection.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeProblem
            // 
            this.nodeProblem.DataPropertyName = "Problem";
            this.nodeProblem.IncrementalSearchEnabled = true;
            this.nodeProblem.LeftMargin = 3;
            this.nodeProblem.ParentColumn = this.treeColumn4;
            this.nodeProblem.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeLocation
            // 
            this.nodeLocation.DataPropertyName = "RegKeyPath";
            this.nodeLocation.IncrementalSearchEnabled = true;
            this.nodeLocation.LeftMargin = 3;
            this.nodeLocation.ParentColumn = this.treeColumn5;
            this.nodeLocation.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeValueName
            // 
            this.nodeValueName.DataPropertyName = "ValueName";
            this.nodeValueName.IncrementalSearchEnabled = true;
            this.nodeValueName.LeftMargin = 3;
            this.nodeValueName.ParentColumn = this.treeColumn6;
            this.nodeValueName.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // imageListTreeView
            // 
            this.imageListTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTreeView.ImageStream")));
            this.imageListTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTreeView.Images.SetKeyName(0, "activexcom.ico");
            this.imageListTreeView.Images.SetKeyName(1, "drivers.ico");
            this.imageListTreeView.Images.SetKeyName(2, "fonts.ico");
            this.imageListTreeView.Images.SetKeyName(3, "helpfiles.ico");
            this.imageListTreeView.Images.SetKeyName(4, "historylist.ico");
            this.imageListTreeView.Images.SetKeyName(5, "mycomputer.ico");
            this.imageListTreeView.Images.SetKeyName(6, "shareddlls.ico");
            this.imageListTreeView.Images.SetKeyName(7, "softwaresettings.ico");
            this.imageListTreeView.Images.SetKeyName(8, "soundevents.ico");
            this.imageListTreeView.Images.SetKeyName(9, "startup.ico");
            this.imageListTreeView.Images.SetKeyName(10, "appinfo.ico");
            this.imageListTreeView.Images.SetKeyName(11, "programlocations.ico");
            // 
            // imageListBrowserObjectManager
            // 
            this.imageListBrowserObjectManager.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListBrowserObjectManager.ImageSize = new System.Drawing.Size(16, 16);
            this.imageListBrowserObjectManager.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // ilStartupManager
            // 
            this.ilStartupManager.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilStartupManager.ImageStream")));
            this.ilStartupManager.TransparentColor = System.Drawing.Color.Transparent;
            this.ilStartupManager.Images.SetKeyName(0, "User");
            this.ilStartupManager.Images.SetKeyName(1, "Users");
            // 
            // nodeIconOptimizeManagerIcon
            // 
            this.nodeIconOptimizeManagerIcon.DataPropertyName = "Image";
            this.nodeIconOptimizeManagerIcon.LeftMargin = 1;
            this.nodeIconOptimizeManagerIcon.ParentColumn = this.colOptimizeManagerSection;
            this.nodeIconOptimizeManagerIcon.ScaleMode = Common_Tools.TreeViewAdv.Tree.ImageScaleMode.Clip;
            // 
            // nodeTextBoxOptimizeManagerSection
            // 
            this.nodeTextBoxOptimizeManagerSection.IncrementalSearchEnabled = true;
            this.nodeTextBoxOptimizeManagerSection.LeftMargin = 3;
            this.nodeTextBoxOptimizeManagerSection.ParentColumn = this.colOptimizeManagerSection;
            // 
            // nodeTextBoxOptimizeManagerItem
            // 
            this.nodeTextBoxOptimizeManagerItem.DataPropertyName = "Item";
            this.nodeTextBoxOptimizeManagerItem.IncrementalSearchEnabled = true;
            this.nodeTextBoxOptimizeManagerItem.LeftMargin = 3;
            this.nodeTextBoxOptimizeManagerItem.ParentColumn = this.colOptimizeManagerSection;
            // 
            // nodeTextBoxOptimizeManagerPath
            // 
            this.nodeTextBoxOptimizeManagerPath.DataPropertyName = "Path";
            this.nodeTextBoxOptimizeManagerPath.IncrementalSearchEnabled = true;
            this.nodeTextBoxOptimizeManagerPath.LeftMargin = 3;
            this.nodeTextBoxOptimizeManagerPath.ParentColumn = this.colOptimizeManagerPath;
            // 
            // nodeTextBoxOptimizeManagerArguments
            // 
            this.nodeTextBoxOptimizeManagerArguments.DataPropertyName = "Args";
            this.nodeTextBoxOptimizeManagerArguments.IncrementalSearchEnabled = true;
            this.nodeTextBoxOptimizeManagerArguments.LeftMargin = 3;
            this.nodeTextBoxOptimizeManagerArguments.ParentColumn = this.colOptimizeManagerArgs;
            // 
            // cmnuOptimizeRegistryManager
            // 
            this.cmnuOptimizeRegistryManager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOptimizeRegistryManagerSelectAll,
            this.tsmiOptimizeRegistryManagerSelectNone,
            this.tsmiOptimizeRegistryManagerInvertSelection,
            this.toolStripSeparator2,
            this.tsmiOptimizeRegistryManagerExcludeSelected,
            this.tsmiOptimizeRegistryManagerViewinRegEdit});
            this.cmnuOptimizeRegistryManager.Name = "contextMenuStrip1";
            this.cmnuOptimizeRegistryManager.Size = new System.Drawing.Size(162, 120);
            // 
            // tsmiOptimizeRegistryManagerSelectAll
            // 
            this.tsmiOptimizeRegistryManagerSelectAll.Name = "tsmiOptimizeRegistryManagerSelectAll";
            this.tsmiOptimizeRegistryManagerSelectAll.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerSelectAll.Text = "Select All";
            // 
            // tsmiOptimizeRegistryManagerSelectNone
            // 
            this.tsmiOptimizeRegistryManagerSelectNone.Name = "tsmiOptimizeRegistryManagerSelectNone";
            this.tsmiOptimizeRegistryManagerSelectNone.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerSelectNone.Text = "Select None";
            // 
            // tsmiOptimizeRegistryManagerInvertSelection
            // 
            this.tsmiOptimizeRegistryManagerInvertSelection.Name = "tsmiOptimizeRegistryManagerInvertSelection";
            this.tsmiOptimizeRegistryManagerInvertSelection.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerInvertSelection.Text = "Invert Selection";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(158, 6);
            // 
            // tsmiOptimizeRegistryManagerExcludeSelected
            // 
            this.tsmiOptimizeRegistryManagerExcludeSelected.Enabled = false;
            this.tsmiOptimizeRegistryManagerExcludeSelected.Name = "tsmiOptimizeRegistryManagerExcludeSelected";
            this.tsmiOptimizeRegistryManagerExcludeSelected.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerExcludeSelected.Text = "Exclude Selected";
            // 
            // tsmiOptimizeRegistryManagerViewinRegEdit
            // 
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Enabled = false;
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Name = "tsmiOptimizeRegistryManagerViewinRegEdit";
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Size = new System.Drawing.Size(161, 22);
            this.tsmiOptimizeRegistryManagerViewinRegEdit.Text = "View in RegEdit";
            // 
            // pnlMainBackground
            // 
            this.pnlMainBackground.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlMainBackground.BackgroundImage = global::AVStrike.Properties.Resources.bg;
            this.pnlMainBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlMainBackground.Controls.Add(this.pnlHomeMessageLayout);
            this.pnlMainBackground.Controls.Add(this.pnlMainCover);
            this.pnlMainBackground.Controls.Add(this.pnlMainTop);
            this.pnlMainBackground.Controls.Add(this.btnMainScan);
            this.pnlMainBackground.Controls.Add(this.specialButtons2);
            this.pnlMainBackground.Controls.Add(this.splBtnUpgrade);
            this.pnlMainBackground.Controls.Add(this.specialButtons1);
            this.pnlMainBackground.Controls.Add(this.splBtnBuyNow);
            this.pnlMainBackground.Controls.Add(this.btnMainHome);
            this.pnlMainBackground.Controls.Add(this.btnMainSettings);
            this.pnlMainBackground.Controls.Add(this.splBtnLiveChat);
            this.pnlMainBackground.Controls.Add(this.btnMainHistory);
            this.pnlMainBackground.Controls.Add(this.btnMainAntivirus);
            this.pnlMainBackground.Controls.Add(this.splBtnScanNow);
            this.pnlMainBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMainBackground.Location = new System.Drawing.Point(0, 0);
            this.pnlMainBackground.Name = "pnlMainBackground";
            this.pnlMainBackground.Size = new System.Drawing.Size(800, 566);
            this.pnlMainBackground.TabIndex = 0;
            // 
            // pnlHomeMessageLayout
            // 
            this.pnlHomeMessageLayout.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlHomeMessageLayout.BackgroundImage = global::AVStrike.Properties.Resources.green_bg;
            this.pnlHomeMessageLayout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlHomeMessageLayout.Controls.Add(this.lblHomeMessageDescription3);
            this.pnlHomeMessageLayout.Controls.Add(this.lblHomeMessageDescription1);
            this.pnlHomeMessageLayout.Controls.Add(this.lblHomeMessageDescription2);
            this.pnlHomeMessageLayout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlHomeMessageLayout.Location = new System.Drawing.Point(181, 403);
            this.pnlHomeMessageLayout.Name = "pnlHomeMessageLayout";
            this.pnlHomeMessageLayout.Size = new System.Drawing.Size(619, 87);
            this.pnlHomeMessageLayout.TabIndex = 3;
            // 
            // lblHomeMessageDescription3
            // 
            this.lblHomeMessageDescription3.AutoSize = true;
            this.lblHomeMessageDescription3.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMessageDescription3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMessageDescription3.ForeColor = System.Drawing.Color.Transparent;
            this.lblHomeMessageDescription3.Location = new System.Drawing.Point(159, 40);
            this.lblHomeMessageDescription3.Name = "lblHomeMessageDescription3";
            this.lblHomeMessageDescription3.Size = new System.Drawing.Size(460, 15);
            this.lblHomeMessageDescription3.TabIndex = 5;
            this.lblHomeMessageDescription3.Text = "WE RECOMMEND YOU TO BUY FULL VERSION OF AKICK TO STAY PROTECTED.";
            // 
            // lblHomeMessageDescription1
            // 
            this.lblHomeMessageDescription1.AutoSize = true;
            this.lblHomeMessageDescription1.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMessageDescription1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMessageDescription1.ForeColor = System.Drawing.Color.Transparent;
            this.lblHomeMessageDescription1.Location = new System.Drawing.Point(43, 12);
            this.lblHomeMessageDescription1.Name = "lblHomeMessageDescription1";
            this.lblHomeMessageDescription1.Size = new System.Drawing.Size(543, 15);
            this.lblHomeMessageDescription1.TabIndex = 3;
            this.lblHomeMessageDescription1.Tag = "YOU ARE USING {0} VERSION OF AKICK ANTIVIRUS. YOUR PROTECTION WILL EXPIRE IN NEXT" +
    "";
            this.lblHomeMessageDescription1.Text = "YOU ARE USING {0} VERSION OF AKICK ANTIVIRUS. YOUR PROTECTION WILL EXPIRE IN NEXT" +
    "";
            // 
            // lblHomeMessageDescription2
            // 
            this.lblHomeMessageDescription2.AutoSize = true;
            this.lblHomeMessageDescription2.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMessageDescription2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMessageDescription2.ForeColor = System.Drawing.Color.Red;
            this.lblHomeMessageDescription2.Location = new System.Drawing.Point(97, 40);
            this.lblHomeMessageDescription2.Name = "lblHomeMessageDescription2";
            this.lblHomeMessageDescription2.Size = new System.Drawing.Size(67, 15);
            this.lblHomeMessageDescription2.TabIndex = 4;
            this.lblHomeMessageDescription2.Tag = "{0} DAY(S).";
            this.lblHomeMessageDescription2.Text = "{0} DAY(S).";
            // 
            // pnlMainCover
            // 
            this.pnlMainCover.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlMainCover.Controls.Add(this.pnlHomeMainBasic);
            this.pnlMainCover.Controls.Add(this.pnlScanMainBasic);
            this.pnlMainCover.Controls.Add(this.pnlSettingsBasic);
            this.pnlMainCover.Controls.Add(this.pnlAntivirusBasic);
            this.pnlMainCover.Controls.Add(this.pnlHistoryBasic);
            this.pnlMainCover.Controls.Add(this.pnlLicenseExpireAlert);
            this.pnlMainCover.Location = new System.Drawing.Point(182, 89);
            this.pnlMainCover.Name = "pnlMainCover";
            this.pnlMainCover.Size = new System.Drawing.Size(619, 311);
            this.pnlMainCover.TabIndex = 10;
            // 
            // pnlHomeMainBasic
            // 
            this.pnlHomeMainBasic.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlHomeMainBasic.Controls.Add(this.pnlHomeMainCover);
            this.pnlHomeMainBasic.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeMainBasic.Name = "pnlHomeMainBasic";
            this.pnlHomeMainBasic.Size = new System.Drawing.Size(619, 311);
            this.pnlHomeMainBasic.TabIndex = 9;
            // 
            // pnlHomeMainCover
            // 
            this.pnlHomeMainCover.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlHomeMainCover.BackgroundImage = global::AVStrike.Properties.Resources.middle_bg;
            this.pnlHomeMainCover.Controls.Add(this.pnlHomeMainDefault);
            this.pnlHomeMainCover.Controls.Add(this.pnlHomeMainScanProcess);
            this.pnlHomeMainCover.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeMainCover.Name = "pnlHomeMainCover";
            this.pnlHomeMainCover.Size = new System.Drawing.Size(619, 311);
            this.pnlHomeMainCover.TabIndex = 1;
            // 
            // pnlHomeMainDefault
            // 
            this.pnlHomeMainDefault.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlHomeMainDefault.BackgroundImage = global::AVStrike.Properties.Resources.middle_bg;
            this.pnlHomeMainDefault.Controls.Add(this.btnSubHomeQuickScan);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainLicenseTypeDescription);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainLastScannedOnDescription);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainLicenseExpiryDateDescription);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainVirusDefinitionVersionDescription);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainDefinitionAutoUpdateDescription);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainLicenseTypeDescriber);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainLastScannedOnDescriber);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainLicenseExpiryDateDescriber);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainVirusDefinitionVersionDescriber);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainDefinitionAutoUpdateDescriber);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainRealTimeProtectionDescription);
            this.pnlHomeMainDefault.Controls.Add(this.lblHomeMainRealTimeProtectionDescriber);
            this.pnlHomeMainDefault.Controls.Add(this.pnlHomeImageLayout);
            this.pnlHomeMainDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHomeMainDefault.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeMainDefault.Name = "pnlHomeMainDefault";
            this.pnlHomeMainDefault.Size = new System.Drawing.Size(619, 311);
            this.pnlHomeMainDefault.TabIndex = 0;
            // 
            // btnSubHomeQuickScan
            // 
            this.btnSubHomeQuickScan.BackColor = System.Drawing.Color.Transparent;
            this.btnSubHomeQuickScan.BackgroundImage = global::AVStrike.Properties.Resources.quick_scan;
            this.btnSubHomeQuickScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubHomeQuickScan.Caption = "";
            this.btnSubHomeQuickScan.CaptionColor = System.Drawing.Color.Black;
            this.btnSubHomeQuickScan.CaptionColorActive = System.Drawing.Color.Black;
            this.btnSubHomeQuickScan.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnSubHomeQuickScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSubHomeQuickScan.Image = null;
            this.btnSubHomeQuickScan.ImageActive = null;
            this.btnSubHomeQuickScan.ImageDisabled = null;
            this.btnSubHomeQuickScan.ImageNormal = null;
            this.btnSubHomeQuickScan.ImageRollover = global::AVStrike.Properties.Resources.quick_scan_hover;
            this.btnSubHomeQuickScan.IsActive = false;
            this.btnSubHomeQuickScan.Location = new System.Drawing.Point(392, 16);
            this.btnSubHomeQuickScan.Name = "btnSubHomeQuickScan";
            this.btnSubHomeQuickScan.Size = new System.Drawing.Size(98, 98);
            this.btnSubHomeQuickScan.TabIndex = 1;
            this.btnSubHomeQuickScan.TabStop = false;
            this.btnSubHomeQuickScan.Click += new System.EventHandler(this.btnSubHomeScanType_Click);
            // 
            // lblHomeMainLicenseTypeDescription
            // 
            this.lblHomeMainLicenseTypeDescription.AutoSize = true;
            this.lblHomeMainLicenseTypeDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainLicenseTypeDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainLicenseTypeDescription.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainLicenseTypeDescription.Location = new System.Drawing.Point(463, 231);
            this.lblHomeMainLicenseTypeDescription.Name = "lblHomeMainLicenseTypeDescription";
            this.lblHomeMainLicenseTypeDescription.Size = new System.Drawing.Size(48, 18);
            this.lblHomeMainLicenseTypeDescription.TabIndex = 21;
            this.lblHomeMainLicenseTypeDescription.Text = "TRIAL";
            this.lblHomeMainLicenseTypeDescription.Visible = false;
            // 
            // lblHomeMainLastScannedOnDescription
            // 
            this.lblHomeMainLastScannedOnDescription.AutoSize = true;
            this.lblHomeMainLastScannedOnDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainLastScannedOnDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainLastScannedOnDescription.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainLastScannedOnDescription.Location = new System.Drawing.Point(460, 206);
            this.lblHomeMainLastScannedOnDescription.Name = "lblHomeMainLastScannedOnDescription";
            this.lblHomeMainLastScannedOnDescription.Size = new System.Drawing.Size(82, 18);
            this.lblHomeMainLastScannedOnDescription.TabIndex = 20;
            this.lblHomeMainLastScannedOnDescription.Text = "10-03-2014";
            this.lblHomeMainLastScannedOnDescription.Visible = false;
            // 
            // lblHomeMainLicenseExpiryDateDescription
            // 
            this.lblHomeMainLicenseExpiryDateDescription.AutoSize = true;
            this.lblHomeMainLicenseExpiryDateDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainLicenseExpiryDateDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainLicenseExpiryDateDescription.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainLicenseExpiryDateDescription.Location = new System.Drawing.Point(460, 178);
            this.lblHomeMainLicenseExpiryDateDescription.Name = "lblHomeMainLicenseExpiryDateDescription";
            this.lblHomeMainLicenseExpiryDateDescription.Size = new System.Drawing.Size(82, 18);
            this.lblHomeMainLicenseExpiryDateDescription.TabIndex = 19;
            this.lblHomeMainLicenseExpiryDateDescription.Text = "11-03-2015";
            this.lblHomeMainLicenseExpiryDateDescription.Visible = false;
            // 
            // lblHomeMainVirusDefinitionVersionDescription
            // 
            this.lblHomeMainVirusDefinitionVersionDescription.AutoSize = true;
            this.lblHomeMainVirusDefinitionVersionDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainVirusDefinitionVersionDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainVirusDefinitionVersionDescription.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainVirusDefinitionVersionDescription.Location = new System.Drawing.Point(461, 148);
            this.lblHomeMainVirusDefinitionVersionDescription.Name = "lblHomeMainVirusDefinitionVersionDescription";
            this.lblHomeMainVirusDefinitionVersionDescription.Size = new System.Drawing.Size(52, 18);
            this.lblHomeMainVirusDefinitionVersionDescription.TabIndex = 18;
            this.lblHomeMainVirusDefinitionVersionDescription.Text = "1.2.2.6";
            this.lblHomeMainVirusDefinitionVersionDescription.Visible = false;
            // 
            // lblHomeMainDefinitionAutoUpdateDescription
            // 
            this.lblHomeMainDefinitionAutoUpdateDescription.AutoSize = true;
            this.lblHomeMainDefinitionAutoUpdateDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainDefinitionAutoUpdateDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainDefinitionAutoUpdateDescription.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainDefinitionAutoUpdateDescription.Location = new System.Drawing.Point(459, 117);
            this.lblHomeMainDefinitionAutoUpdateDescription.Name = "lblHomeMainDefinitionAutoUpdateDescription";
            this.lblHomeMainDefinitionAutoUpdateDescription.Size = new System.Drawing.Size(38, 18);
            this.lblHomeMainDefinitionAutoUpdateDescription.TabIndex = 17;
            this.lblHomeMainDefinitionAutoUpdateDescription.Text = "OFF";
            this.lblHomeMainDefinitionAutoUpdateDescription.Visible = false;
            // 
            // lblHomeMainLicenseTypeDescriber
            // 
            this.lblHomeMainLicenseTypeDescriber.AutoSize = true;
            this.lblHomeMainLicenseTypeDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainLicenseTypeDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainLicenseTypeDescriber.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainLicenseTypeDescriber.Location = new System.Drawing.Point(213, 231);
            this.lblHomeMainLicenseTypeDescriber.Name = "lblHomeMainLicenseTypeDescriber";
            this.lblHomeMainLicenseTypeDescriber.Size = new System.Drawing.Size(240, 18);
            this.lblHomeMainLicenseTypeDescriber.TabIndex = 15;
            this.lblHomeMainLicenseTypeDescriber.Text = "LICENSE TYPE                       ";
            this.lblHomeMainLicenseTypeDescriber.Visible = false;
            // 
            // lblHomeMainLastScannedOnDescriber
            // 
            this.lblHomeMainLastScannedOnDescriber.AutoSize = true;
            this.lblHomeMainLastScannedOnDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainLastScannedOnDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainLastScannedOnDescriber.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainLastScannedOnDescriber.Location = new System.Drawing.Point(212, 206);
            this.lblHomeMainLastScannedOnDescriber.Name = "lblHomeMainLastScannedOnDescriber";
            this.lblHomeMainLastScannedOnDescriber.Size = new System.Drawing.Size(243, 18);
            this.lblHomeMainLastScannedOnDescriber.TabIndex = 14;
            this.lblHomeMainLastScannedOnDescriber.Text = "LAST SCANNED ON                ";
            this.lblHomeMainLastScannedOnDescriber.Visible = false;
            // 
            // lblHomeMainLicenseExpiryDateDescriber
            // 
            this.lblHomeMainLicenseExpiryDateDescriber.AutoSize = true;
            this.lblHomeMainLicenseExpiryDateDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainLicenseExpiryDateDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainLicenseExpiryDateDescriber.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainLicenseExpiryDateDescriber.Location = new System.Drawing.Point(211, 178);
            this.lblHomeMainLicenseExpiryDateDescriber.Name = "lblHomeMainLicenseExpiryDateDescriber";
            this.lblHomeMainLicenseExpiryDateDescriber.Size = new System.Drawing.Size(245, 18);
            this.lblHomeMainLicenseExpiryDateDescriber.TabIndex = 13;
            this.lblHomeMainLicenseExpiryDateDescriber.Text = "LICENSE EXPIRY DATE           ";
            this.lblHomeMainLicenseExpiryDateDescriber.Visible = false;
            // 
            // lblHomeMainVirusDefinitionVersionDescriber
            // 
            this.lblHomeMainVirusDefinitionVersionDescriber.AutoSize = true;
            this.lblHomeMainVirusDefinitionVersionDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainVirusDefinitionVersionDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainVirusDefinitionVersionDescriber.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainVirusDefinitionVersionDescriber.Location = new System.Drawing.Point(213, 148);
            this.lblHomeMainVirusDefinitionVersionDescriber.Name = "lblHomeMainVirusDefinitionVersionDescriber";
            this.lblHomeMainVirusDefinitionVersionDescriber.Size = new System.Drawing.Size(242, 18);
            this.lblHomeMainVirusDefinitionVersionDescriber.TabIndex = 12;
            this.lblHomeMainVirusDefinitionVersionDescriber.Text = "VIRUS DEFINITION VERSION  ";
            this.lblHomeMainVirusDefinitionVersionDescriber.Visible = false;
            // 
            // lblHomeMainDefinitionAutoUpdateDescriber
            // 
            this.lblHomeMainDefinitionAutoUpdateDescriber.AutoSize = true;
            this.lblHomeMainDefinitionAutoUpdateDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainDefinitionAutoUpdateDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainDefinitionAutoUpdateDescriber.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainDefinitionAutoUpdateDescriber.Location = new System.Drawing.Point(213, 117);
            this.lblHomeMainDefinitionAutoUpdateDescriber.Name = "lblHomeMainDefinitionAutoUpdateDescriber";
            this.lblHomeMainDefinitionAutoUpdateDescriber.Size = new System.Drawing.Size(241, 18);
            this.lblHomeMainDefinitionAutoUpdateDescriber.TabIndex = 11;
            this.lblHomeMainDefinitionAutoUpdateDescriber.Text = "DEFINITION AUTO UPDATE    ";
            this.lblHomeMainDefinitionAutoUpdateDescriber.Visible = false;
            // 
            // lblHomeMainRealTimeProtectionDescription
            // 
            this.lblHomeMainRealTimeProtectionDescription.AutoSize = true;
            this.lblHomeMainRealTimeProtectionDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainRealTimeProtectionDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainRealTimeProtectionDescription.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainRealTimeProtectionDescription.Location = new System.Drawing.Point(459, 86);
            this.lblHomeMainRealTimeProtectionDescription.Name = "lblHomeMainRealTimeProtectionDescription";
            this.lblHomeMainRealTimeProtectionDescription.Size = new System.Drawing.Size(31, 18);
            this.lblHomeMainRealTimeProtectionDescription.TabIndex = 10;
            this.lblHomeMainRealTimeProtectionDescription.Text = "ON";
            this.lblHomeMainRealTimeProtectionDescription.Visible = false;
            // 
            // lblHomeMainRealTimeProtectionDescriber
            // 
            this.lblHomeMainRealTimeProtectionDescriber.AutoSize = true;
            this.lblHomeMainRealTimeProtectionDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblHomeMainRealTimeProtectionDescriber.Font = new System.Drawing.Font("Pericles", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomeMainRealTimeProtectionDescriber.ForeColor = System.Drawing.Color.Black;
            this.lblHomeMainRealTimeProtectionDescriber.Location = new System.Drawing.Point(213, 86);
            this.lblHomeMainRealTimeProtectionDescriber.Name = "lblHomeMainRealTimeProtectionDescriber";
            this.lblHomeMainRealTimeProtectionDescriber.Size = new System.Drawing.Size(258, 21);
            this.lblHomeMainRealTimeProtectionDescriber.TabIndex = 9;
            this.lblHomeMainRealTimeProtectionDescriber.Text = "REAL TIME PROTECTION       ";
            this.lblHomeMainRealTimeProtectionDescriber.Visible = false;
            // 
            // pnlHomeImageLayout
            // 
            this.pnlHomeImageLayout.BackColor = System.Drawing.Color.Transparent;
            this.pnlHomeImageLayout.BackgroundImage = global::AVStrike.Properties.Resources._protected;
            this.pnlHomeImageLayout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlHomeImageLayout.Location = new System.Drawing.Point(33, 83);
            this.pnlHomeImageLayout.Name = "pnlHomeImageLayout";
            this.pnlHomeImageLayout.Size = new System.Drawing.Size(130, 149);
            this.pnlHomeImageLayout.TabIndex = 0;
            // 
            // pnlHomeMainScanProcess
            // 
            this.pnlHomeMainScanProcess.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlHomeMainScanProcess.BackgroundImage = global::AVStrike.Properties.Resources.middle_bg;
            this.pnlHomeMainScanProcess.Controls.Add(this.prgAntivirusScan);
            this.pnlHomeMainScanProcess.Controls.Add(this.splBtnStopScanning);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblNuetralizedDescriber);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblNeutralizedDescription);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblThreatsDetectedDescriber);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblThreatsDetectedDescription);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblTimeElapsed);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblTimeElapsedDescription);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblScanInProcess);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblScanInProcessDescription);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblItemsScannedCount);
            this.pnlHomeMainScanProcess.Controls.Add(this.lblItemsScannedCountDescription);
            this.pnlHomeMainScanProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHomeMainScanProcess.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeMainScanProcess.Name = "pnlHomeMainScanProcess";
            this.pnlHomeMainScanProcess.Size = new System.Drawing.Size(619, 311);
            this.pnlHomeMainScanProcess.TabIndex = 9;
            // 
            // prgAntivirusScan
            // 
            this.prgAntivirusScan.Location = new System.Drawing.Point(20, 236);
            this.prgAntivirusScan.MarqueeAnimationSpeed = 5;
            this.prgAntivirusScan.Name = "prgAntivirusScan";
            this.prgAntivirusScan.Size = new System.Drawing.Size(443, 40);
            this.prgAntivirusScan.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.prgAntivirusScan.TabIndex = 102;
            // 
            // splBtnStopScanning
            // 
            this.splBtnStopScanning.BackColor = System.Drawing.Color.Transparent;
            this.splBtnStopScanning.BackgroundImage = global::AVStrike.Properties.Resources.ak_stopscaning;
            this.splBtnStopScanning.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnStopScanning.Caption = "";
            this.splBtnStopScanning.CaptionColor = System.Drawing.Color.Black;
            this.splBtnStopScanning.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnStopScanning.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnStopScanning.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnStopScanning.Image = null;
            this.splBtnStopScanning.ImageActive = null;
            this.splBtnStopScanning.ImageDisabled = null;
            this.splBtnStopScanning.ImageNormal = null;
            this.splBtnStopScanning.ImageRollover = global::AVStrike.Properties.Resources.ak_stopscaning_hover;
            this.splBtnStopScanning.IsActive = false;
            this.splBtnStopScanning.Location = new System.Drawing.Point(489, 243);
            this.splBtnStopScanning.Name = "splBtnStopScanning";
            this.splBtnStopScanning.Size = new System.Drawing.Size(118, 27);
            this.splBtnStopScanning.TabIndex = 101;
            this.splBtnStopScanning.TabStop = false;
            this.splBtnStopScanning.Click += new System.EventHandler(this.splBtnStopScanning_Click);
            // 
            // lblNuetralizedDescriber
            // 
            this.lblNuetralizedDescriber.AutoSize = true;
            this.lblNuetralizedDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblNuetralizedDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNuetralizedDescriber.ForeColor = System.Drawing.Color.White;
            this.lblNuetralizedDescriber.Location = new System.Drawing.Point(17, 147);
            this.lblNuetralizedDescriber.Name = "lblNuetralizedDescriber";
            this.lblNuetralizedDescriber.Size = new System.Drawing.Size(148, 16);
            this.lblNuetralizedDescriber.TabIndex = 97;
            this.lblNuetralizedDescriber.Text = "Threats Neutralized:";
            // 
            // lblNeutralizedDescription
            // 
            this.lblNeutralizedDescription.AutoSize = true;
            this.lblNeutralizedDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblNeutralizedDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNeutralizedDescription.ForeColor = System.Drawing.Color.White;
            this.lblNeutralizedDescription.Location = new System.Drawing.Point(236, 150);
            this.lblNeutralizedDescription.Name = "lblNeutralizedDescription";
            this.lblNeutralizedDescription.Size = new System.Drawing.Size(16, 16);
            this.lblNeutralizedDescription.TabIndex = 98;
            this.lblNeutralizedDescription.Text = "0";
            // 
            // lblThreatsDetectedDescriber
            // 
            this.lblThreatsDetectedDescriber.AutoSize = true;
            this.lblThreatsDetectedDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblThreatsDetectedDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreatsDetectedDescriber.ForeColor = System.Drawing.Color.White;
            this.lblThreatsDetectedDescriber.Location = new System.Drawing.Point(17, 110);
            this.lblThreatsDetectedDescriber.Name = "lblThreatsDetectedDescriber";
            this.lblThreatsDetectedDescriber.Size = new System.Drawing.Size(170, 16);
            this.lblThreatsDetectedDescriber.TabIndex = 95;
            this.lblThreatsDetectedDescriber.Text = "Total Threats detected:";
            // 
            // lblThreatsDetectedDescription
            // 
            this.lblThreatsDetectedDescription.AutoSize = true;
            this.lblThreatsDetectedDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblThreatsDetectedDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreatsDetectedDescription.ForeColor = System.Drawing.Color.White;
            this.lblThreatsDetectedDescription.Location = new System.Drawing.Point(236, 110);
            this.lblThreatsDetectedDescription.Name = "lblThreatsDetectedDescription";
            this.lblThreatsDetectedDescription.Size = new System.Drawing.Size(16, 16);
            this.lblThreatsDetectedDescription.TabIndex = 96;
            this.lblThreatsDetectedDescription.Text = "0";
            // 
            // lblTimeElapsed
            // 
            this.lblTimeElapsed.AutoSize = true;
            this.lblTimeElapsed.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeElapsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeElapsed.ForeColor = System.Drawing.Color.White;
            this.lblTimeElapsed.Location = new System.Drawing.Point(17, 41);
            this.lblTimeElapsed.Name = "lblTimeElapsed";
            this.lblTimeElapsed.Size = new System.Drawing.Size(149, 16);
            this.lblTimeElapsed.TabIndex = 92;
            this.lblTimeElapsed.Text = "Total Time Elapsed:";
            // 
            // lblTimeElapsedDescription
            // 
            this.lblTimeElapsedDescription.AutoSize = true;
            this.lblTimeElapsedDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeElapsedDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeElapsedDescription.ForeColor = System.Drawing.Color.White;
            this.lblTimeElapsedDescription.Location = new System.Drawing.Point(236, 44);
            this.lblTimeElapsedDescription.Name = "lblTimeElapsedDescription";
            this.lblTimeElapsedDescription.Size = new System.Drawing.Size(64, 16);
            this.lblTimeElapsedDescription.TabIndex = 93;
            this.lblTimeElapsedDescription.Text = "00:00:00";
            // 
            // lblScanInProcess
            // 
            this.lblScanInProcess.AutoSize = true;
            this.lblScanInProcess.BackColor = System.Drawing.Color.Transparent;
            this.lblScanInProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScanInProcess.ForeColor = System.Drawing.Color.White;
            this.lblScanInProcess.Location = new System.Drawing.Point(17, 183);
            this.lblScanInProcess.Name = "lblScanInProcess";
            this.lblScanInProcess.Size = new System.Drawing.Size(91, 16);
            this.lblScanInProcess.TabIndex = 90;
            this.lblScanInProcess.Text = "Current File:";
            // 
            // lblScanInProcessDescription
            // 
            this.lblScanInProcessDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblScanInProcessDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScanInProcessDescription.ForeColor = System.Drawing.Color.White;
            this.lblScanInProcessDescription.Location = new System.Drawing.Point(236, 183);
            this.lblScanInProcessDescription.Name = "lblScanInProcessDescription";
            this.lblScanInProcessDescription.Size = new System.Drawing.Size(371, 33);
            this.lblScanInProcessDescription.TabIndex = 91;
            this.lblScanInProcessDescription.Text = "Collecting system information...";
            // 
            // lblItemsScannedCount
            // 
            this.lblItemsScannedCount.AutoSize = true;
            this.lblItemsScannedCount.BackColor = System.Drawing.Color.Transparent;
            this.lblItemsScannedCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemsScannedCount.ForeColor = System.Drawing.Color.White;
            this.lblItemsScannedCount.Location = new System.Drawing.Point(17, 73);
            this.lblItemsScannedCount.Name = "lblItemsScannedCount";
            this.lblItemsScannedCount.Size = new System.Drawing.Size(189, 16);
            this.lblItemsScannedCount.TabIndex = 88;
            this.lblItemsScannedCount.Text = "Number of Items Scanned:";
            // 
            // lblItemsScannedCountDescription
            // 
            this.lblItemsScannedCountDescription.AutoSize = true;
            this.lblItemsScannedCountDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblItemsScannedCountDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemsScannedCountDescription.ForeColor = System.Drawing.Color.White;
            this.lblItemsScannedCountDescription.Location = new System.Drawing.Point(236, 77);
            this.lblItemsScannedCountDescription.Name = "lblItemsScannedCountDescription";
            this.lblItemsScannedCountDescription.Size = new System.Drawing.Size(16, 16);
            this.lblItemsScannedCountDescription.TabIndex = 89;
            this.lblItemsScannedCountDescription.Text = "0";
            // 
            // pnlScanMainBasic
            // 
            this.pnlScanMainBasic.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlScanMainBasic.Controls.Add(this.pnlScanCover);
            this.pnlScanMainBasic.Location = new System.Drawing.Point(0, 0);
            this.pnlScanMainBasic.Name = "pnlScanMainBasic";
            this.pnlScanMainBasic.Size = new System.Drawing.Size(619, 311);
            this.pnlScanMainBasic.TabIndex = 10;
            // 
            // pnlScanCover
            // 
            this.pnlScanCover.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlScanCover.BackgroundImage = global::AVStrike.Properties.Resources.ak_mid_panel;
            this.pnlScanCover.Controls.Add(this.pnlScanDefault);
            this.pnlScanCover.Controls.Add(this.mainPanel5);
            this.pnlScanCover.Location = new System.Drawing.Point(0, 0);
            this.pnlScanCover.Name = "pnlScanCover";
            this.pnlScanCover.Size = new System.Drawing.Size(619, 311);
            this.pnlScanCover.TabIndex = 1;
            // 
            // pnlScanDefault
            // 
            this.pnlScanDefault.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlScanDefault.Controls.Add(this.btnSubHomeFullScan);
            this.pnlScanDefault.Controls.Add(this.btnSubHomeCustomScan);
            this.pnlScanDefault.Controls.Add(this.subProcessButton2);
            this.pnlScanDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlScanDefault.Location = new System.Drawing.Point(0, 0);
            this.pnlScanDefault.Name = "pnlScanDefault";
            this.pnlScanDefault.Size = new System.Drawing.Size(619, 311);
            this.pnlScanDefault.TabIndex = 0;
            // 
            // btnSubHomeFullScan
            // 
            this.btnSubHomeFullScan.BackColor = System.Drawing.Color.Transparent;
            this.btnSubHomeFullScan.BackgroundImage = global::AVStrike.Properties.Resources.full_scan;
            this.btnSubHomeFullScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSubHomeFullScan.Caption = "";
            this.btnSubHomeFullScan.CaptionColor = System.Drawing.Color.Black;
            this.btnSubHomeFullScan.CaptionColorActive = System.Drawing.Color.Black;
            this.btnSubHomeFullScan.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnSubHomeFullScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSubHomeFullScan.Image = null;
            this.btnSubHomeFullScan.ImageActive = null;
            this.btnSubHomeFullScan.ImageDisabled = null;
            this.btnSubHomeFullScan.ImageNormal = null;
            this.btnSubHomeFullScan.ImageRollover = global::AVStrike.Properties.Resources.full_scan_hover;
            this.btnSubHomeFullScan.IsActive = false;
            this.btnSubHomeFullScan.Location = new System.Drawing.Point(242, 41);
            this.btnSubHomeFullScan.Name = "btnSubHomeFullScan";
            this.btnSubHomeFullScan.Size = new System.Drawing.Size(134, 135);
            this.btnSubHomeFullScan.TabIndex = 4;
            this.btnSubHomeFullScan.TabStop = false;
            // 
            // btnSubHomeCustomScan
            // 
            this.btnSubHomeCustomScan.BackColor = System.Drawing.Color.Transparent;
            this.btnSubHomeCustomScan.BackgroundImage = global::AVStrike.Properties.Resources.custom_scan;
            this.btnSubHomeCustomScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSubHomeCustomScan.Caption = "";
            this.btnSubHomeCustomScan.CaptionColor = System.Drawing.Color.Black;
            this.btnSubHomeCustomScan.CaptionColorActive = System.Drawing.Color.Black;
            this.btnSubHomeCustomScan.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnSubHomeCustomScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSubHomeCustomScan.Image = null;
            this.btnSubHomeCustomScan.ImageActive = null;
            this.btnSubHomeCustomScan.ImageDisabled = null;
            this.btnSubHomeCustomScan.ImageNormal = null;
            this.btnSubHomeCustomScan.ImageRollover = global::AVStrike.Properties.Resources.custom_scan_hover;
            this.btnSubHomeCustomScan.IsActive = false;
            this.btnSubHomeCustomScan.Location = new System.Drawing.Point(434, 41);
            this.btnSubHomeCustomScan.Name = "btnSubHomeCustomScan";
            this.btnSubHomeCustomScan.Size = new System.Drawing.Size(134, 135);
            this.btnSubHomeCustomScan.TabIndex = 3;
            this.btnSubHomeCustomScan.TabStop = false;
            // 
            // subProcessButton2
            // 
            this.subProcessButton2.BackColor = System.Drawing.Color.Transparent;
            this.subProcessButton2.BackgroundImage = global::AVStrike.Properties.Resources.process_scan;
            this.subProcessButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.subProcessButton2.Caption = "";
            this.subProcessButton2.CaptionColor = System.Drawing.Color.Black;
            this.subProcessButton2.CaptionColorActive = System.Drawing.Color.Black;
            this.subProcessButton2.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.subProcessButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subProcessButton2.Image = null;
            this.subProcessButton2.ImageActive = null;
            this.subProcessButton2.ImageDisabled = null;
            this.subProcessButton2.ImageNormal = null;
            this.subProcessButton2.ImageRollover = global::AVStrike.Properties.Resources.process_scan_hover;
            this.subProcessButton2.IsActive = false;
            this.subProcessButton2.Location = new System.Drawing.Point(53, 41);
            this.subProcessButton2.Name = "subProcessButton2";
            this.subProcessButton2.Size = new System.Drawing.Size(134, 135);
            this.subProcessButton2.TabIndex = 1;
            this.subProcessButton2.TabStop = false;
            // 
            // mainPanel5
            // 
            this.mainPanel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.mainPanel5.BackgroundImage = global::AVStrike.Properties.Resources.ak_mid_panel;
            this.mainPanel5.Controls.Add(this.progressBar1);
            this.mainPanel5.Controls.Add(this.specialButtons3);
            this.mainPanel5.Controls.Add(this.label17);
            this.mainPanel5.Controls.Add(this.label18);
            this.mainPanel5.Controls.Add(this.label19);
            this.mainPanel5.Controls.Add(this.label20);
            this.mainPanel5.Controls.Add(this.label21);
            this.mainPanel5.Controls.Add(this.label22);
            this.mainPanel5.Controls.Add(this.label23);
            this.mainPanel5.Controls.Add(this.label24);
            this.mainPanel5.Controls.Add(this.label25);
            this.mainPanel5.Controls.Add(this.label26);
            this.mainPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel5.Location = new System.Drawing.Point(0, 0);
            this.mainPanel5.Name = "mainPanel5";
            this.mainPanel5.Size = new System.Drawing.Size(619, 311);
            this.mainPanel5.TabIndex = 9;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(69, 255);
            this.progressBar1.MarqueeAnimationSpeed = 5;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(443, 40);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 102;
            // 
            // specialButtons3
            // 
            this.specialButtons3.BackColor = System.Drawing.Color.Transparent;
            this.specialButtons3.BackgroundImage = global::AVStrike.Properties.Resources.ak_stopscaning;
            this.specialButtons3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.specialButtons3.Caption = "";
            this.specialButtons3.CaptionColor = System.Drawing.Color.Black;
            this.specialButtons3.CaptionColorActive = System.Drawing.Color.Black;
            this.specialButtons3.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.specialButtons3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.specialButtons3.Image = null;
            this.specialButtons3.ImageActive = null;
            this.specialButtons3.ImageDisabled = null;
            this.specialButtons3.ImageNormal = null;
            this.specialButtons3.ImageRollover = global::AVStrike.Properties.Resources.ak_stopscaning_hover;
            this.specialButtons3.IsActive = false;
            this.specialButtons3.Location = new System.Drawing.Point(559, 262);
            this.specialButtons3.Name = "specialButtons3";
            this.specialButtons3.Size = new System.Drawing.Size(118, 27);
            this.specialButtons3.TabIndex = 101;
            this.specialButtons3.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(66, 166);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(148, 16);
            this.label17.TabIndex = 97;
            this.label17.Text = "Threats Neutralized:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(285, 169);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 16);
            this.label18.TabIndex = 98;
            this.label18.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(66, 129);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(170, 16);
            this.label19.TabIndex = 95;
            this.label19.Text = "Total Threats detected:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(285, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 16);
            this.label20.TabIndex = 96;
            this.label20.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(66, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(149, 16);
            this.label21.TabIndex = 92;
            this.label21.Text = "Total Time Elapsed:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(285, 63);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 16);
            this.label22.TabIndex = 93;
            this.label22.Text = "00:00:00";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(66, 202);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 16);
            this.label23.TabIndex = 90;
            this.label23.Text = "Current File:";
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(285, 202);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(450, 33);
            this.label24.TabIndex = 91;
            this.label24.Text = "Collecting system information...";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(66, 92);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(189, 16);
            this.label25.TabIndex = 88;
            this.label25.Text = "Number of Items Scanned:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(285, 96);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(16, 16);
            this.label26.TabIndex = 89;
            this.label26.Text = "0";
            // 
            // pnlSettingsBasic
            // 
            this.pnlSettingsBasic.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlSettingsBasic.BackgroundImage = global::AVStrike.Properties.Resources.middle_bg;
            this.pnlSettingsBasic.Controls.Add(this.pnlSettingsHolder);
            this.pnlSettingsBasic.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsBasic.Name = "pnlSettingsBasic";
            this.pnlSettingsBasic.Size = new System.Drawing.Size(619, 311);
            this.pnlSettingsBasic.TabIndex = 22;
            // 
            // pnlSettingsHolder
            // 
            this.pnlSettingsHolder.Controls.Add(this.pnlSettingsHolderAntivirus);
            this.pnlSettingsHolder.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsHolder.Name = "pnlSettingsHolder";
            this.pnlSettingsHolder.Size = new System.Drawing.Size(619, 311);
            this.pnlSettingsHolder.TabIndex = 22;
            // 
            // pnlSettingsHolderAntivirus
            // 
            this.pnlSettingsHolderAntivirus.BackColor = System.Drawing.Color.Transparent;
            this.pnlSettingsHolderAntivirus.Controls.Add(this.splBtnSettingsAntivirusCancel);
            this.pnlSettingsHolderAntivirus.Controls.Add(this.splBtnSettingsAntivirusApply);
            this.pnlSettingsHolderAntivirus.Controls.Add(this.tabControlSettingsSelection);
            this.pnlSettingsHolderAntivirus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSettingsHolderAntivirus.Location = new System.Drawing.Point(0, 0);
            this.pnlSettingsHolderAntivirus.Name = "pnlSettingsHolderAntivirus";
            this.pnlSettingsHolderAntivirus.Size = new System.Drawing.Size(619, 311);
            this.pnlSettingsHolderAntivirus.TabIndex = 23;
            // 
            // splBtnSettingsAntivirusCancel
            // 
            this.splBtnSettingsAntivirusCancel.BackgroundImage = global::AVStrike.Properties.Resources.ak_setdeafult;
            this.splBtnSettingsAntivirusCancel.Caption = "";
            this.splBtnSettingsAntivirusCancel.CaptionColor = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusCancel.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusCancel.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnSettingsAntivirusCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnSettingsAntivirusCancel.Image = null;
            this.splBtnSettingsAntivirusCancel.ImageActive = null;
            this.splBtnSettingsAntivirusCancel.ImageDisabled = null;
            this.splBtnSettingsAntivirusCancel.ImageNormal = null;
            this.splBtnSettingsAntivirusCancel.ImageRollover = global::AVStrike.Properties.Resources.ak_setdeafult_hover;
            this.splBtnSettingsAntivirusCancel.IsActive = false;
            this.splBtnSettingsAntivirusCancel.Location = new System.Drawing.Point(477, 268);
            this.splBtnSettingsAntivirusCancel.Name = "splBtnSettingsAntivirusCancel";
            this.splBtnSettingsAntivirusCancel.Size = new System.Drawing.Size(95, 27);
            this.splBtnSettingsAntivirusCancel.TabIndex = 102;
            this.splBtnSettingsAntivirusCancel.TabStop = false;
            this.splBtnSettingsAntivirusCancel.Click += new System.EventHandler(this.splBtnSettingsAntivirusCancel_Click);
            // 
            // splBtnSettingsAntivirusApply
            // 
            this.splBtnSettingsAntivirusApply.BackgroundImage = global::AVStrike.Properties.Resources.ak_apply;
            this.splBtnSettingsAntivirusApply.Caption = "";
            this.splBtnSettingsAntivirusApply.CaptionColor = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusApply.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusApply.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnSettingsAntivirusApply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnSettingsAntivirusApply.Image = null;
            this.splBtnSettingsAntivirusApply.ImageActive = null;
            this.splBtnSettingsAntivirusApply.ImageDisabled = null;
            this.splBtnSettingsAntivirusApply.ImageNormal = null;
            this.splBtnSettingsAntivirusApply.ImageRollover = global::AVStrike.Properties.Resources.ak_apply_hover;
            this.splBtnSettingsAntivirusApply.IsActive = false;
            this.splBtnSettingsAntivirusApply.Location = new System.Drawing.Point(385, 268);
            this.splBtnSettingsAntivirusApply.Name = "splBtnSettingsAntivirusApply";
            this.splBtnSettingsAntivirusApply.Size = new System.Drawing.Size(75, 27);
            this.splBtnSettingsAntivirusApply.TabIndex = 96;
            this.splBtnSettingsAntivirusApply.TabStop = false;
            this.splBtnSettingsAntivirusApply.Click += new System.EventHandler(this.splBtnSettingsAntivirusApply_Click);
            // 
            // tabControlSettingsSelection
            // 
            this.tabControlSettingsSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlSettingsSelection.Controls.Add(this.tabPageScheduleScan);
            this.tabControlSettingsSelection.Controls.Add(this.tabPageAdvanced);
            this.tabControlSettingsSelection.Controls.Add(this.tabPageExcludeFiles);
            this.tabControlSettingsSelection.Controls.Add(this.tabPageExcludeFolders);
            this.tabControlSettingsSelection.Controls.Add(this.tabPageRealTimeProtection);
            this.tabControlSettingsSelection.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabControlSettingsSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.tabControlSettingsSelection.ItemSize = new System.Drawing.Size(100, 25);
            this.tabControlSettingsSelection.Location = new System.Drawing.Point(32, 18);
            this.tabControlSettingsSelection.Name = "tabControlSettingsSelection";
            this.tabControlSettingsSelection.Padding = new System.Drawing.Point(10, 5);
            this.tabControlSettingsSelection.SelectedIndex = 0;
            this.tabControlSettingsSelection.Size = new System.Drawing.Size(554, 238);
            this.tabControlSettingsSelection.TabIndex = 101;
            // 
            // tabPageScheduleScan
            // 
            this.tabPageScheduleScan.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageScheduleScan.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.tabPageScheduleScan.Controls.Add(this.chkSettingRunAVStrikeWhenComputerIsOn);
            this.tabPageScheduleScan.Controls.Add(this.lblSettingScanScheduleDayOfTheWeekDescriber);
            this.tabPageScheduleScan.Controls.Add(this.cmbBxSettingScanScheduleDayOfTheWeek);
            this.tabPageScheduleScan.Controls.Add(this.lblSettingScanScheduleScanTypeDescriber);
            this.tabPageScheduleScan.Controls.Add(this.cmbBxSettingScanScheduleScanType);
            this.tabPageScheduleScan.Controls.Add(this.chkSettingScanScheduleRunAScan);
            this.tabPageScheduleScan.Location = new System.Drawing.Point(4, 29);
            this.tabPageScheduleScan.Name = "tabPageScheduleScan";
            this.tabPageScheduleScan.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageScheduleScan.Size = new System.Drawing.Size(546, 205);
            this.tabPageScheduleScan.TabIndex = 3;
            this.tabPageScheduleScan.Text = "Scan Schedule";
            // 
            // chkSettingRunAVStrikeWhenComputerIsOn
            // 
            this.chkSettingRunAVStrikeWhenComputerIsOn.AutoSize = true;
            this.chkSettingRunAVStrikeWhenComputerIsOn.BackColor = System.Drawing.Color.Transparent;
            this.chkSettingRunAVStrikeWhenComputerIsOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSettingRunAVStrikeWhenComputerIsOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkSettingRunAVStrikeWhenComputerIsOn.ForeColor = System.Drawing.Color.White;
            this.chkSettingRunAVStrikeWhenComputerIsOn.Location = new System.Drawing.Point(19, 116);
            this.chkSettingRunAVStrikeWhenComputerIsOn.Name = "chkSettingRunAVStrikeWhenComputerIsOn";
            this.chkSettingRunAVStrikeWhenComputerIsOn.Size = new System.Drawing.Size(224, 20);
            this.chkSettingRunAVStrikeWhenComputerIsOn.TabIndex = 18;
            this.chkSettingRunAVStrikeWhenComputerIsOn.Text = "Start AKICK when the PC turns ON";
            this.chkSettingRunAVStrikeWhenComputerIsOn.UseVisualStyleBackColor = false;
            // 
            // lblSettingScanScheduleDayOfTheWeekDescriber
            // 
            this.lblSettingScanScheduleDayOfTheWeekDescriber.AutoSize = true;
            this.lblSettingScanScheduleDayOfTheWeekDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblSettingScanScheduleDayOfTheWeekDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettingScanScheduleDayOfTheWeekDescriber.ForeColor = System.Drawing.Color.White;
            this.lblSettingScanScheduleDayOfTheWeekDescriber.Location = new System.Drawing.Point(39, 80);
            this.lblSettingScanScheduleDayOfTheWeekDescriber.Name = "lblSettingScanScheduleDayOfTheWeekDescriber";
            this.lblSettingScanScheduleDayOfTheWeekDescriber.Size = new System.Drawing.Size(109, 16);
            this.lblSettingScanScheduleDayOfTheWeekDescriber.TabIndex = 15;
            this.lblSettingScanScheduleDayOfTheWeekDescriber.Text = "Scan Frequency:";
            // 
            // cmbBxSettingScanScheduleDayOfTheWeek
            // 
            this.cmbBxSettingScanScheduleDayOfTheWeek.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbBxSettingScanScheduleDayOfTheWeek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBxSettingScanScheduleDayOfTheWeek.Enabled = false;
            this.cmbBxSettingScanScheduleDayOfTheWeek.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.cmbBxSettingScanScheduleDayOfTheWeek.FormattingEnabled = true;
            this.cmbBxSettingScanScheduleDayOfTheWeek.Items.AddRange(new object[] {
            "Daily",
            "Weekly"});
            this.cmbBxSettingScanScheduleDayOfTheWeek.Location = new System.Drawing.Point(149, 73);
            this.cmbBxSettingScanScheduleDayOfTheWeek.Name = "cmbBxSettingScanScheduleDayOfTheWeek";
            this.cmbBxSettingScanScheduleDayOfTheWeek.Size = new System.Drawing.Size(121, 21);
            this.cmbBxSettingScanScheduleDayOfTheWeek.TabIndex = 14;
            // 
            // lblSettingScanScheduleScanTypeDescriber
            // 
            this.lblSettingScanScheduleScanTypeDescriber.AutoSize = true;
            this.lblSettingScanScheduleScanTypeDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblSettingScanScheduleScanTypeDescriber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettingScanScheduleScanTypeDescriber.ForeColor = System.Drawing.Color.White;
            this.lblSettingScanScheduleScanTypeDescriber.Location = new System.Drawing.Point(40, 47);
            this.lblSettingScanScheduleScanTypeDescriber.Name = "lblSettingScanScheduleScanTypeDescriber";
            this.lblSettingScanScheduleScanTypeDescriber.Size = new System.Drawing.Size(77, 16);
            this.lblSettingScanScheduleScanTypeDescriber.TabIndex = 13;
            this.lblSettingScanScheduleScanTypeDescriber.Text = "Scan Type:";
            // 
            // cmbBxSettingScanScheduleScanType
            // 
            this.cmbBxSettingScanScheduleScanType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbBxSettingScanScheduleScanType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBxSettingScanScheduleScanType.Enabled = false;
            this.cmbBxSettingScanScheduleScanType.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.cmbBxSettingScanScheduleScanType.FormattingEnabled = true;
            this.cmbBxSettingScanScheduleScanType.Items.AddRange(new object[] {
            "Quick Scan",
            "Full Scan"});
            this.cmbBxSettingScanScheduleScanType.Location = new System.Drawing.Point(149, 45);
            this.cmbBxSettingScanScheduleScanType.Name = "cmbBxSettingScanScheduleScanType";
            this.cmbBxSettingScanScheduleScanType.Size = new System.Drawing.Size(121, 21);
            this.cmbBxSettingScanScheduleScanType.TabIndex = 12;
            // 
            // chkSettingScanScheduleRunAScan
            // 
            this.chkSettingScanScheduleRunAScan.AutoSize = true;
            this.chkSettingScanScheduleRunAScan.BackColor = System.Drawing.Color.Transparent;
            this.chkSettingScanScheduleRunAScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSettingScanScheduleRunAScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkSettingScanScheduleRunAScan.ForeColor = System.Drawing.Color.White;
            this.chkSettingScanScheduleRunAScan.Location = new System.Drawing.Point(17, 24);
            this.chkSettingScanScheduleRunAScan.Name = "chkSettingScanScheduleRunAScan";
            this.chkSettingScanScheduleRunAScan.Size = new System.Drawing.Size(327, 20);
            this.chkSettingScanScheduleRunAScan.TabIndex = 9;
            this.chkSettingScanScheduleRunAScan.Text = "Run a Scheduled scan on my PC(Recommended).";
            this.chkSettingScanScheduleRunAScan.UseVisualStyleBackColor = false;
            this.chkSettingScanScheduleRunAScan.CheckedChanged += new System.EventHandler(this.chkSettingScanScheduleRunAScan_CheckedChanged);
            // 
            // tabPageAdvanced
            // 
            this.tabPageAdvanced.BackColor = System.Drawing.Color.Transparent;
            this.tabPageAdvanced.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.tabPageAdvanced.Controls.Add(this.lblSettingsAdvancedKeepQuarantinedFiles);
            this.tabPageAdvanced.Controls.Add(this.lblSettingsAdvancedAllowFullHistory);
            this.tabPageAdvanced.Controls.Add(this.chkSettingsAdvancedKeepQuarantinedFiles);
            this.tabPageAdvanced.Controls.Add(this.chkSettingsAdvancedAllowFullHistory);
            this.tabPageAdvanced.Location = new System.Drawing.Point(4, 29);
            this.tabPageAdvanced.Name = "tabPageAdvanced";
            this.tabPageAdvanced.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdvanced.Size = new System.Drawing.Size(546, 205);
            this.tabPageAdvanced.TabIndex = 0;
            this.tabPageAdvanced.Text = "Advanced";
            // 
            // lblSettingsAdvancedKeepQuarantinedFiles
            // 
            this.lblSettingsAdvancedKeepQuarantinedFiles.AutoSize = true;
            this.lblSettingsAdvancedKeepQuarantinedFiles.BackColor = System.Drawing.Color.Transparent;
            this.lblSettingsAdvancedKeepQuarantinedFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettingsAdvancedKeepQuarantinedFiles.ForeColor = System.Drawing.Color.White;
            this.lblSettingsAdvancedKeepQuarantinedFiles.Location = new System.Drawing.Point(31, 122);
            this.lblSettingsAdvancedKeepQuarantinedFiles.Name = "lblSettingsAdvancedKeepQuarantinedFiles";
            this.lblSettingsAdvancedKeepQuarantinedFiles.Size = new System.Drawing.Size(467, 18);
            this.lblSettingsAdvancedKeepQuarantinedFiles.TabIndex = 11;
            this.lblSettingsAdvancedKeepQuarantinedFiles.Text = "Quarantined files remain disabled until you allow them or remove them";
            // 
            // lblSettingsAdvancedAllowFullHistory
            // 
            this.lblSettingsAdvancedAllowFullHistory.AutoSize = true;
            this.lblSettingsAdvancedAllowFullHistory.BackColor = System.Drawing.Color.Transparent;
            this.lblSettingsAdvancedAllowFullHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettingsAdvancedAllowFullHistory.ForeColor = System.Drawing.Color.White;
            this.lblSettingsAdvancedAllowFullHistory.Location = new System.Drawing.Point(29, 62);
            this.lblSettingsAdvancedAllowFullHistory.Name = "lblSettingsAdvancedAllowFullHistory";
            this.lblSettingsAdvancedAllowFullHistory.Size = new System.Drawing.Size(371, 18);
            this.lblSettingsAdvancedAllowFullHistory.TabIndex = 10;
            this.lblSettingsAdvancedAllowFullHistory.Text = "Allow users to see all detected items on the History tab.";
            // 
            // chkSettingsAdvancedKeepQuarantinedFiles
            // 
            this.chkSettingsAdvancedKeepQuarantinedFiles.AutoSize = true;
            this.chkSettingsAdvancedKeepQuarantinedFiles.BackColor = System.Drawing.Color.Transparent;
            this.chkSettingsAdvancedKeepQuarantinedFiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSettingsAdvancedKeepQuarantinedFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkSettingsAdvancedKeepQuarantinedFiles.ForeColor = System.Drawing.Color.White;
            this.chkSettingsAdvancedKeepQuarantinedFiles.Location = new System.Drawing.Point(13, 97);
            this.chkSettingsAdvancedKeepQuarantinedFiles.Name = "chkSettingsAdvancedKeepQuarantinedFiles";
            this.chkSettingsAdvancedKeepQuarantinedFiles.Size = new System.Drawing.Size(225, 22);
            this.chkSettingsAdvancedKeepQuarantinedFiles.TabIndex = 8;
            this.chkSettingsAdvancedKeepQuarantinedFiles.Text = "Keep Quarantined files forever";
            this.chkSettingsAdvancedKeepQuarantinedFiles.UseVisualStyleBackColor = false;
            // 
            // chkSettingsAdvancedAllowFullHistory
            // 
            this.chkSettingsAdvancedAllowFullHistory.AutoSize = true;
            this.chkSettingsAdvancedAllowFullHistory.BackColor = System.Drawing.Color.Transparent;
            this.chkSettingsAdvancedAllowFullHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSettingsAdvancedAllowFullHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkSettingsAdvancedAllowFullHistory.ForeColor = System.Drawing.Color.White;
            this.chkSettingsAdvancedAllowFullHistory.Location = new System.Drawing.Point(13, 38);
            this.chkSettingsAdvancedAllowFullHistory.Name = "chkSettingsAdvancedAllowFullHistory";
            this.chkSettingsAdvancedAllowFullHistory.Size = new System.Drawing.Size(239, 22);
            this.chkSettingsAdvancedAllowFullHistory.TabIndex = 7;
            this.chkSettingsAdvancedAllowFullHistory.Text = "Allow User\'s to View Full History";
            this.chkSettingsAdvancedAllowFullHistory.UseVisualStyleBackColor = false;
            // 
            // tabPageExcludeFiles
            // 
            this.tabPageExcludeFiles.BackColor = System.Drawing.Color.Transparent;
            this.tabPageExcludeFiles.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.tabPageExcludeFiles.Controls.Add(this.splBtnSettingsAntivirusExcludeFilesRemove);
            this.tabPageExcludeFiles.Controls.Add(this.splBtnSettingsAntivirusExcludeFilesAdd);
            this.tabPageExcludeFiles.Controls.Add(this.groupBox5);
            this.tabPageExcludeFiles.Controls.Add(this.lvAntSetExcludedFiles);
            this.tabPageExcludeFiles.Location = new System.Drawing.Point(4, 29);
            this.tabPageExcludeFiles.Name = "tabPageExcludeFiles";
            this.tabPageExcludeFiles.Size = new System.Drawing.Size(546, 205);
            this.tabPageExcludeFiles.TabIndex = 10;
            this.tabPageExcludeFiles.Text = "Filters";
            // 
            // splBtnSettingsAntivirusExcludeFilesRemove
            // 
            this.splBtnSettingsAntivirusExcludeFilesRemove.BackgroundImage = global::AVStrike.Properties.Resources.ak_remove;
            this.splBtnSettingsAntivirusExcludeFilesRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnSettingsAntivirusExcludeFilesRemove.Caption = "";
            this.splBtnSettingsAntivirusExcludeFilesRemove.CaptionColor = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFilesRemove.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFilesRemove.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnSettingsAntivirusExcludeFilesRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnSettingsAntivirusExcludeFilesRemove.Image = null;
            this.splBtnSettingsAntivirusExcludeFilesRemove.ImageActive = null;
            this.splBtnSettingsAntivirusExcludeFilesRemove.ImageDisabled = null;
            this.splBtnSettingsAntivirusExcludeFilesRemove.ImageNormal = null;
            this.splBtnSettingsAntivirusExcludeFilesRemove.ImageRollover = global::AVStrike.Properties.Resources.ak_remove_hover;
            this.splBtnSettingsAntivirusExcludeFilesRemove.IsActive = false;
            this.splBtnSettingsAntivirusExcludeFilesRemove.Location = new System.Drawing.Point(114, 171);
            this.splBtnSettingsAntivirusExcludeFilesRemove.Name = "splBtnSettingsAntivirusExcludeFilesRemove";
            this.splBtnSettingsAntivirusExcludeFilesRemove.Size = new System.Drawing.Size(76, 27);
            this.splBtnSettingsAntivirusExcludeFilesRemove.TabIndex = 95;
            this.splBtnSettingsAntivirusExcludeFilesRemove.TabStop = false;
            this.splBtnSettingsAntivirusExcludeFilesRemove.Click += new System.EventHandler(this.splBtnSettingsAntivirusExcludeFilesRemove_Click);
            // 
            // splBtnSettingsAntivirusExcludeFilesAdd
            // 
            this.splBtnSettingsAntivirusExcludeFilesAdd.BackgroundImage = global::AVStrike.Properties.Resources.ak_add;
            this.splBtnSettingsAntivirusExcludeFilesAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnSettingsAntivirusExcludeFilesAdd.Caption = "";
            this.splBtnSettingsAntivirusExcludeFilesAdd.CaptionColor = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFilesAdd.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFilesAdd.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnSettingsAntivirusExcludeFilesAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnSettingsAntivirusExcludeFilesAdd.Image = null;
            this.splBtnSettingsAntivirusExcludeFilesAdd.ImageActive = null;
            this.splBtnSettingsAntivirusExcludeFilesAdd.ImageDisabled = null;
            this.splBtnSettingsAntivirusExcludeFilesAdd.ImageNormal = null;
            this.splBtnSettingsAntivirusExcludeFilesAdd.ImageRollover = global::AVStrike.Properties.Resources.add_hover;
            this.splBtnSettingsAntivirusExcludeFilesAdd.IsActive = false;
            this.splBtnSettingsAntivirusExcludeFilesAdd.Location = new System.Drawing.Point(37, 171);
            this.splBtnSettingsAntivirusExcludeFilesAdd.Name = "splBtnSettingsAntivirusExcludeFilesAdd";
            this.splBtnSettingsAntivirusExcludeFilesAdd.Size = new System.Drawing.Size(55, 27);
            this.splBtnSettingsAntivirusExcludeFilesAdd.TabIndex = 94;
            this.splBtnSettingsAntivirusExcludeFilesAdd.TabStop = false;
            this.splBtnSettingsAntivirusExcludeFilesAdd.Click += new System.EventHandler(this.splBtnSettingsAntivirusExcludeFilesAdd_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.numericUpDownSizeMaximum);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(406, 9);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(244, 103);
            this.groupBox5.TabIndex = 93;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Size";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(248, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "MB";
            // 
            // numericUpDownSizeMaximum
            // 
            this.numericUpDownSizeMaximum.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.numericUpDownSizeMaximum.Location = new System.Drawing.Point(181, 34);
            this.numericUpDownSizeMaximum.Maximum = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            this.numericUpDownSizeMaximum.Name = "numericUpDownSizeMaximum";
            this.numericUpDownSizeMaximum.Size = new System.Drawing.Size(58, 22);
            this.numericUpDownSizeMaximum.TabIndex = 2;
            this.numericUpDownSizeMaximum.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Exclude Files Greater than:";
            // 
            // lvAntSetExcludedFiles
            // 
            this.lvAntSetExcludedFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAntSetExcludedFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7});
            this.lvAntSetExcludedFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvAntSetExcludedFiles.Location = new System.Drawing.Point(3, 3);
            this.lvAntSetExcludedFiles.MultiSelect = false;
            this.lvAntSetExcludedFiles.Name = "lvAntSetExcludedFiles";
            this.lvAntSetExcludedFiles.Size = new System.Drawing.Size(345, 136);
            this.lvAntSetExcludedFiles.TabIndex = 1;
            this.lvAntSetExcludedFiles.UseCompatibleStateImageBehavior = false;
            this.lvAntSetExcludedFiles.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "List of file types to exclude";
            this.columnHeader7.Width = 200;
            // 
            // tabPageExcludeFolders
            // 
            this.tabPageExcludeFolders.BackColor = System.Drawing.Color.Transparent;
            this.tabPageExcludeFolders.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.tabPageExcludeFolders.Controls.Add(this.splBtnSettingsAntivirusExcludeFolderRemove);
            this.tabPageExcludeFolders.Controls.Add(this.splBtnSettingsAntivirusExcludeFoldersAdd);
            this.tabPageExcludeFolders.Controls.Add(this.lvAntSetExcludedFolders);
            this.tabPageExcludeFolders.Location = new System.Drawing.Point(4, 29);
            this.tabPageExcludeFolders.Name = "tabPageExcludeFolders";
            this.tabPageExcludeFolders.Size = new System.Drawing.Size(546, 205);
            this.tabPageExcludeFolders.TabIndex = 2;
            this.tabPageExcludeFolders.Text = "Excluded Folders";
            // 
            // splBtnSettingsAntivirusExcludeFolderRemove
            // 
            this.splBtnSettingsAntivirusExcludeFolderRemove.BackgroundImage = global::AVStrike.Properties.Resources.ak_remove;
            this.splBtnSettingsAntivirusExcludeFolderRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnSettingsAntivirusExcludeFolderRemove.Caption = "";
            this.splBtnSettingsAntivirusExcludeFolderRemove.CaptionColor = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFolderRemove.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFolderRemove.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnSettingsAntivirusExcludeFolderRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnSettingsAntivirusExcludeFolderRemove.Image = null;
            this.splBtnSettingsAntivirusExcludeFolderRemove.ImageActive = null;
            this.splBtnSettingsAntivirusExcludeFolderRemove.ImageDisabled = null;
            this.splBtnSettingsAntivirusExcludeFolderRemove.ImageNormal = null;
            this.splBtnSettingsAntivirusExcludeFolderRemove.ImageRollover = global::AVStrike.Properties.Resources.ak_remove_hover;
            this.splBtnSettingsAntivirusExcludeFolderRemove.IsActive = false;
            this.splBtnSettingsAntivirusExcludeFolderRemove.Location = new System.Drawing.Point(121, 165);
            this.splBtnSettingsAntivirusExcludeFolderRemove.Name = "splBtnSettingsAntivirusExcludeFolderRemove";
            this.splBtnSettingsAntivirusExcludeFolderRemove.Size = new System.Drawing.Size(76, 27);
            this.splBtnSettingsAntivirusExcludeFolderRemove.TabIndex = 97;
            this.splBtnSettingsAntivirusExcludeFolderRemove.TabStop = false;
            this.splBtnSettingsAntivirusExcludeFolderRemove.Click += new System.EventHandler(this.splBtnSettingsAntivirusExcludeFolderRemove_Click);
            // 
            // splBtnSettingsAntivirusExcludeFoldersAdd
            // 
            this.splBtnSettingsAntivirusExcludeFoldersAdd.BackgroundImage = global::AVStrike.Properties.Resources.ak_add;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.Caption = "";
            this.splBtnSettingsAntivirusExcludeFoldersAdd.CaptionColor = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnSettingsAntivirusExcludeFoldersAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.Image = null;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.ImageActive = null;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.ImageDisabled = null;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.ImageNormal = null;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.ImageRollover = global::AVStrike.Properties.Resources.add_hover;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.IsActive = false;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.Location = new System.Drawing.Point(38, 165);
            this.splBtnSettingsAntivirusExcludeFoldersAdd.Name = "splBtnSettingsAntivirusExcludeFoldersAdd";
            this.splBtnSettingsAntivirusExcludeFoldersAdd.Size = new System.Drawing.Size(55, 27);
            this.splBtnSettingsAntivirusExcludeFoldersAdd.TabIndex = 96;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.TabStop = false;
            this.splBtnSettingsAntivirusExcludeFoldersAdd.Click += new System.EventHandler(this.splBtnSettingsAntivirusExcludeFoldersAdd_Click);
            // 
            // lvAntSetExcludedFolders
            // 
            this.lvAntSetExcludedFolders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAntSetExcludedFolders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8});
            this.lvAntSetExcludedFolders.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lvAntSetExcludedFolders.Location = new System.Drawing.Point(3, 3);
            this.lvAntSetExcludedFolders.MultiSelect = false;
            this.lvAntSetExcludedFolders.Name = "lvAntSetExcludedFolders";
            this.lvAntSetExcludedFolders.Size = new System.Drawing.Size(661, 132);
            this.lvAntSetExcludedFolders.TabIndex = 0;
            this.lvAntSetExcludedFolders.UseCompatibleStateImageBehavior = false;
            this.lvAntSetExcludedFolders.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "List of folders to exclude";
            this.columnHeader8.Width = 200;
            // 
            // tabPageRealTimeProtection
            // 
            this.tabPageRealTimeProtection.BackColor = System.Drawing.Color.Transparent;
            this.tabPageRealTimeProtection.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.tabPageRealTimeProtection.Controls.Add(this.lblSettingsRealTimeProtectionDescriber2);
            this.tabPageRealTimeProtection.Controls.Add(this.lblSettingsRealTimeProtectionDescriber1);
            this.tabPageRealTimeProtection.Controls.Add(this.chkSettingsRealTimeProtectionTurnOn);
            this.tabPageRealTimeProtection.Location = new System.Drawing.Point(4, 29);
            this.tabPageRealTimeProtection.Name = "tabPageRealTimeProtection";
            this.tabPageRealTimeProtection.Size = new System.Drawing.Size(546, 205);
            this.tabPageRealTimeProtection.TabIndex = 9;
            this.tabPageRealTimeProtection.Text = "Real-Time Protection";
            // 
            // lblSettingsRealTimeProtectionDescriber2
            // 
            this.lblSettingsRealTimeProtectionDescriber2.AutoSize = true;
            this.lblSettingsRealTimeProtectionDescriber2.BackColor = System.Drawing.Color.Transparent;
            this.lblSettingsRealTimeProtectionDescriber2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettingsRealTimeProtectionDescriber2.ForeColor = System.Drawing.Color.White;
            this.lblSettingsRealTimeProtectionDescriber2.Location = new System.Drawing.Point(14, 62);
            this.lblSettingsRealTimeProtectionDescriber2.Name = "lblSettingsRealTimeProtectionDescriber2";
            this.lblSettingsRealTimeProtectionDescriber2.Size = new System.Drawing.Size(205, 18);
            this.lblSettingsRealTimeProtectionDescriber2.TabIndex = 5;
            this.lblSettingsRealTimeProtectionDescriber2.Text = "install itself or run on your PC.";
            // 
            // lblSettingsRealTimeProtectionDescriber1
            // 
            this.lblSettingsRealTimeProtectionDescriber1.AutoSize = true;
            this.lblSettingsRealTimeProtectionDescriber1.BackColor = System.Drawing.Color.Transparent;
            this.lblSettingsRealTimeProtectionDescriber1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettingsRealTimeProtectionDescriber1.ForeColor = System.Drawing.Color.White;
            this.lblSettingsRealTimeProtectionDescriber1.Location = new System.Drawing.Point(15, 39);
            this.lblSettingsRealTimeProtectionDescriber1.Name = "lblSettingsRealTimeProtectionDescriber1";
            this.lblSettingsRealTimeProtectionDescriber1.Size = new System.Drawing.Size(650, 18);
            this.lblSettingsRealTimeProtectionDescriber1.TabIndex = 4;
            this.lblSettingsRealTimeProtectionDescriber1.Text = "Real-Time protection alerts you whenever malicious or potentially unwanted softwr" +
    "are attempts to ";
            // 
            // chkSettingsRealTimeProtectionTurnOn
            // 
            this.chkSettingsRealTimeProtectionTurnOn.AutoSize = true;
            this.chkSettingsRealTimeProtectionTurnOn.BackColor = System.Drawing.Color.Transparent;
            this.chkSettingsRealTimeProtectionTurnOn.Checked = true;
            this.chkSettingsRealTimeProtectionTurnOn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSettingsRealTimeProtectionTurnOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSettingsRealTimeProtectionTurnOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkSettingsRealTimeProtectionTurnOn.ForeColor = System.Drawing.Color.White;
            this.chkSettingsRealTimeProtectionTurnOn.Location = new System.Drawing.Point(16, 13);
            this.chkSettingsRealTimeProtectionTurnOn.Name = "chkSettingsRealTimeProtectionTurnOn";
            this.chkSettingsRealTimeProtectionTurnOn.Size = new System.Drawing.Size(326, 22);
            this.chkSettingsRealTimeProtectionTurnOn.TabIndex = 3;
            this.chkSettingsRealTimeProtectionTurnOn.Text = "Turn On real-time protection. (recommended)";
            this.chkSettingsRealTimeProtectionTurnOn.UseVisualStyleBackColor = false;
            // 
            // pnlAntivirusBasic
            // 
            this.pnlAntivirusBasic.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlAntivirusBasic.BackgroundImage = global::AVStrike.Properties.Resources.ak_mid_panel;
            this.pnlAntivirusBasic.Controls.Add(this.pnlAntivirusCover);
            this.pnlAntivirusBasic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAntivirusBasic.Location = new System.Drawing.Point(0, 0);
            this.pnlAntivirusBasic.Name = "pnlAntivirusBasic";
            this.pnlAntivirusBasic.Size = new System.Drawing.Size(619, 311);
            this.pnlAntivirusBasic.TabIndex = 12;
            // 
            // pnlAntivirusCover
            // 
            this.pnlAntivirusCover.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlAntivirusCover.BackgroundImage = global::AVStrike.Properties.Resources.ak_mid_panel;
            this.pnlAntivirusCover.Controls.Add(this.pnlAntivirusQuarantine);
            this.pnlAntivirusCover.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAntivirusCover.Location = new System.Drawing.Point(0, 0);
            this.pnlAntivirusCover.Name = "pnlAntivirusCover";
            this.pnlAntivirusCover.Size = new System.Drawing.Size(619, 311);
            this.pnlAntivirusCover.TabIndex = 1;
            // 
            // pnlAntivirusQuarantine
            // 
            this.pnlAntivirusQuarantine.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlAntivirusQuarantine.BackgroundImage = global::AVStrike.Properties.Resources.ak_mid_panel;
            this.pnlAntivirusQuarantine.Controls.Add(this.chkQuarantineSelectUnselect);
            this.pnlAntivirusQuarantine.Controls.Add(this.splBtnRestore);
            this.pnlAntivirusQuarantine.Controls.Add(this.splBtnRemoveAll);
            this.pnlAntivirusQuarantine.Controls.Add(this.splBtnRemove);
            this.pnlAntivirusQuarantine.Controls.Add(this.lvDiskDiskCleaner);
            this.pnlAntivirusQuarantine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAntivirusQuarantine.Location = new System.Drawing.Point(0, 0);
            this.pnlAntivirusQuarantine.Name = "pnlAntivirusQuarantine";
            this.pnlAntivirusQuarantine.Size = new System.Drawing.Size(619, 311);
            this.pnlAntivirusQuarantine.TabIndex = 11;
            // 
            // chkQuarantineSelectUnselect
            // 
            this.chkQuarantineSelectUnselect.AutoSize = true;
            this.chkQuarantineSelectUnselect.BackColor = System.Drawing.Color.Transparent;
            this.chkQuarantineSelectUnselect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.chkQuarantineSelectUnselect.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkQuarantineSelectUnselect.Checked = true;
            this.chkQuarantineSelectUnselect.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkQuarantineSelectUnselect.ForeColor = System.Drawing.Color.White;
            this.chkQuarantineSelectUnselect.Location = new System.Drawing.Point(15, 4);
            this.chkQuarantineSelectUnselect.Name = "chkQuarantineSelectUnselect";
            this.chkQuarantineSelectUnselect.Size = new System.Drawing.Size(123, 17);
            this.chkQuarantineSelectUnselect.TabIndex = 120;
            this.chkQuarantineSelectUnselect.Text = "Select / Unselect All";
            this.chkQuarantineSelectUnselect.UseVisualStyleBackColor = false;
            this.chkQuarantineSelectUnselect.CheckedChanged += new System.EventHandler(this.chkQuarantineSelectUnselect_CheckedChanged);
            // 
            // splBtnRestore
            // 
            this.splBtnRestore.BackColor = System.Drawing.Color.Transparent;
            this.splBtnRestore.BackgroundImage = global::AVStrike.Properties.Resources.ak_restore;
            this.splBtnRestore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnRestore.Caption = "";
            this.splBtnRestore.CaptionColor = System.Drawing.Color.Black;
            this.splBtnRestore.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnRestore.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnRestore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnRestore.Image = null;
            this.splBtnRestore.ImageActive = null;
            this.splBtnRestore.ImageDisabled = null;
            this.splBtnRestore.ImageNormal = null;
            this.splBtnRestore.ImageRollover = global::AVStrike.Properties.Resources.ak_restore_hover;
            this.splBtnRestore.IsActive = false;
            this.splBtnRestore.Location = new System.Drawing.Point(527, 276);
            this.splBtnRestore.Name = "splBtnRestore";
            this.splBtnRestore.Size = new System.Drawing.Size(75, 27);
            this.splBtnRestore.TabIndex = 112;
            this.splBtnRestore.TabStop = false;
            this.splBtnRestore.Click += new System.EventHandler(this.splBtnRestore_Click);
            // 
            // splBtnRemoveAll
            // 
            this.splBtnRemoveAll.BackColor = System.Drawing.Color.Transparent;
            this.splBtnRemoveAll.BackgroundImage = global::AVStrike.Properties.Resources.ak_removeall;
            this.splBtnRemoveAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnRemoveAll.Caption = "";
            this.splBtnRemoveAll.CaptionColor = System.Drawing.Color.Black;
            this.splBtnRemoveAll.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnRemoveAll.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnRemoveAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnRemoveAll.Image = null;
            this.splBtnRemoveAll.ImageActive = null;
            this.splBtnRemoveAll.ImageDisabled = null;
            this.splBtnRemoveAll.ImageNormal = null;
            this.splBtnRemoveAll.ImageRollover = global::AVStrike.Properties.Resources.ak_removeall_hover;
            this.splBtnRemoveAll.IsActive = false;
            this.splBtnRemoveAll.Location = new System.Drawing.Point(412, 276);
            this.splBtnRemoveAll.Name = "splBtnRemoveAll";
            this.splBtnRemoveAll.Size = new System.Drawing.Size(99, 27);
            this.splBtnRemoveAll.TabIndex = 111;
            this.splBtnRemoveAll.TabStop = false;
            this.splBtnRemoveAll.Click += new System.EventHandler(this.splBtnRemoveAll_Click);
            // 
            // splBtnRemove
            // 
            this.splBtnRemove.BackColor = System.Drawing.Color.Transparent;
            this.splBtnRemove.BackgroundImage = global::AVStrike.Properties.Resources.ak_remove;
            this.splBtnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnRemove.Caption = "";
            this.splBtnRemove.CaptionColor = System.Drawing.Color.Black;
            this.splBtnRemove.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnRemove.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnRemove.Image = null;
            this.splBtnRemove.ImageActive = null;
            this.splBtnRemove.ImageDisabled = null;
            this.splBtnRemove.ImageNormal = null;
            this.splBtnRemove.ImageRollover = global::AVStrike.Properties.Resources.ak_remove_hover;
            this.splBtnRemove.IsActive = false;
            this.splBtnRemove.Location = new System.Drawing.Point(318, 276);
            this.splBtnRemove.Name = "splBtnRemove";
            this.splBtnRemove.Size = new System.Drawing.Size(76, 27);
            this.splBtnRemove.TabIndex = 17;
            this.splBtnRemove.TabStop = false;
            this.splBtnRemove.Click += new System.EventHandler(this.splBtnRemove_Click);
            // 
            // lvDiskDiskCleaner
            // 
            this.lvDiskDiskCleaner.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvDiskDiskCleaner.BackgroundImageTiled = true;
            this.lvDiskDiskCleaner.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvDiskDiskCleaner.CheckBoxes = true;
            this.lvDiskDiskCleaner.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFileName,
            this.colHeaderFilePath,
            this.colVirusName,
            this.colHeaderAlertLevel,
            this.colRecommendedAction,
            colHeaderDate,
            this.colHeaderAction});
            this.lvDiskDiskCleaner.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvDiskDiskCleaner.FullRowSelect = true;
            this.lvDiskDiskCleaner.GridLines = true;
            this.lvDiskDiskCleaner.HideSelection = false;
            this.lvDiskDiskCleaner.Location = new System.Drawing.Point(15, 27);
            this.lvDiskDiskCleaner.Name = "lvDiskDiskCleaner";
            this.lvDiskDiskCleaner.Size = new System.Drawing.Size(587, 237);
            this.lvDiskDiskCleaner.TabIndex = 110;
            this.lvDiskDiskCleaner.UseCompatibleStateImageBehavior = false;
            this.lvDiskDiskCleaner.View = System.Windows.Forms.View.Details;
            // 
            // colFileName
            // 
            this.colFileName.Text = "File Name";
            this.colFileName.Width = 90;
            // 
            // colHeaderFilePath
            // 
            this.colHeaderFilePath.Text = "Location";
            this.colHeaderFilePath.Width = 220;
            // 
            // colVirusName
            // 
            this.colVirusName.Text = "Virus Name";
            this.colVirusName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colVirusName.Width = 100;
            // 
            // colHeaderAlertLevel
            // 
            this.colHeaderAlertLevel.Text = "Alert Level";
            this.colHeaderAlertLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colHeaderAlertLevel.Width = 70;
            // 
            // colRecommendedAction
            // 
            this.colRecommendedAction.Text = "Recommended Action";
            this.colRecommendedAction.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colRecommendedAction.Width = 150;
            // 
            // colHeaderAction
            // 
            this.colHeaderAction.Text = "Action";
            this.colHeaderAction.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colHeaderAction.Width = 150;
            // 
            // pnlHistoryBasic
            // 
            this.pnlHistoryBasic.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlHistoryBasic.BackgroundImage = global::AVStrike.Properties.Resources.middle_bg;
            this.pnlHistoryBasic.Controls.Add(this.pnlHistoryCover);
            this.pnlHistoryBasic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHistoryBasic.Location = new System.Drawing.Point(0, 0);
            this.pnlHistoryBasic.Name = "pnlHistoryBasic";
            this.pnlHistoryBasic.Size = new System.Drawing.Size(619, 311);
            this.pnlHistoryBasic.TabIndex = 24;
            // 
            // pnlHistoryCover
            // 
            this.pnlHistoryCover.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlHistoryCover.BackgroundImage = global::AVStrike.Properties.Resources.middle_bg;
            this.pnlHistoryCover.Controls.Add(this.pnlAntivirusScanResult);
            this.pnlHistoryCover.Location = new System.Drawing.Point(0, 0);
            this.pnlHistoryCover.Name = "pnlHistoryCover";
            this.pnlHistoryCover.Size = new System.Drawing.Size(800, 367);
            this.pnlHistoryCover.TabIndex = 1;
            // 
            // pnlAntivirusScanResult
            // 
            this.pnlAntivirusScanResult.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlAntivirusScanResult.BackgroundImage = global::AVStrike.Properties.Resources.middle_bg;
            this.pnlAntivirusScanResult.Controls.Add(this.pnlAntivirusScanHistoryValuesShow);
            this.pnlAntivirusScanResult.Controls.Add(this.label1);
            this.pnlAntivirusScanResult.Controls.Add(this.lvScanResult);
            this.pnlAntivirusScanResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAntivirusScanResult.Location = new System.Drawing.Point(0, 0);
            this.pnlAntivirusScanResult.Name = "pnlAntivirusScanResult";
            this.pnlAntivirusScanResult.Size = new System.Drawing.Size(800, 367);
            this.pnlAntivirusScanResult.TabIndex = 0;
            // 
            // pnlAntivirusScanHistoryValuesShow
            // 
            this.pnlAntivirusScanHistoryValuesShow.BackColor = System.Drawing.Color.Transparent;
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultTotalDataScannedDescription);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultTotalDataScannedDescriber);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.label2);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultThreatsNeutralizedDescription);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultThreatsDetectedDescription);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultScannedFilesDescription);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultRunTimeDescription);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultThreatsNeutralizedDescriber);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultTypeOfScanDescription);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultThreatsDetectedDescriber);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultScannedFilesDescriber);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultRunTimeDescriber);
            this.pnlAntivirusScanHistoryValuesShow.Controls.Add(this.lblScanResultTypeOfScanDescriber);
            this.pnlAntivirusScanHistoryValuesShow.Location = new System.Drawing.Point(8, 197);
            this.pnlAntivirusScanHistoryValuesShow.Name = "pnlAntivirusScanHistoryValuesShow";
            this.pnlAntivirusScanHistoryValuesShow.Size = new System.Drawing.Size(600, 109);
            this.pnlAntivirusScanHistoryValuesShow.TabIndex = 187;
            this.pnlAntivirusScanHistoryValuesShow.Visible = false;
            // 
            // lblScanResultTotalDataScannedDescription
            // 
            this.lblScanResultTotalDataScannedDescription.AutoSize = true;
            this.lblScanResultTotalDataScannedDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultTotalDataScannedDescription.CausesValidation = false;
            this.lblScanResultTotalDataScannedDescription.ForeColor = System.Drawing.Color.White;
            this.lblScanResultTotalDataScannedDescription.Location = new System.Drawing.Point(515, 82);
            this.lblScanResultTotalDataScannedDescription.Name = "lblScanResultTotalDataScannedDescription";
            this.lblScanResultTotalDataScannedDescription.Size = new System.Drawing.Size(27, 13);
            this.lblScanResultTotalDataScannedDescription.TabIndex = 210;
            this.lblScanResultTotalDataScannedDescription.Text = "N/A";
            // 
            // lblScanResultTotalDataScannedDescriber
            // 
            this.lblScanResultTotalDataScannedDescriber.AutoSize = true;
            this.lblScanResultTotalDataScannedDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultTotalDataScannedDescriber.CausesValidation = false;
            this.lblScanResultTotalDataScannedDescriber.ForeColor = System.Drawing.Color.White;
            this.lblScanResultTotalDataScannedDescriber.Location = new System.Drawing.Point(336, 82);
            this.lblScanResultTotalDataScannedDescriber.Name = "lblScanResultTotalDataScannedDescriber";
            this.lblScanResultTotalDataScannedDescriber.Size = new System.Drawing.Size(147, 13);
            this.lblScanResultTotalDataScannedDescriber.TabIndex = 209;
            this.lblScanResultTotalDataScannedDescriber.Text = "TOTAL DATA SCANNED     :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 208;
            this.label2.Text = "SCAN DETAILS:";
            // 
            // lblScanResultThreatsNeutralizedDescription
            // 
            this.lblScanResultThreatsNeutralizedDescription.AutoSize = true;
            this.lblScanResultThreatsNeutralizedDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultThreatsNeutralizedDescription.CausesValidation = false;
            this.lblScanResultThreatsNeutralizedDescription.ForeColor = System.Drawing.Color.White;
            this.lblScanResultThreatsNeutralizedDescription.Location = new System.Drawing.Point(515, 56);
            this.lblScanResultThreatsNeutralizedDescription.Name = "lblScanResultThreatsNeutralizedDescription";
            this.lblScanResultThreatsNeutralizedDescription.Size = new System.Drawing.Size(27, 13);
            this.lblScanResultThreatsNeutralizedDescription.TabIndex = 207;
            this.lblScanResultThreatsNeutralizedDescription.Text = "N/A";
            // 
            // lblScanResultThreatsDetectedDescription
            // 
            this.lblScanResultThreatsDetectedDescription.AutoSize = true;
            this.lblScanResultThreatsDetectedDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultThreatsDetectedDescription.CausesValidation = false;
            this.lblScanResultThreatsDetectedDescription.ForeColor = System.Drawing.Color.White;
            this.lblScanResultThreatsDetectedDescription.Location = new System.Drawing.Point(515, 31);
            this.lblScanResultThreatsDetectedDescription.Name = "lblScanResultThreatsDetectedDescription";
            this.lblScanResultThreatsDetectedDescription.Size = new System.Drawing.Size(27, 13);
            this.lblScanResultThreatsDetectedDescription.TabIndex = 206;
            this.lblScanResultThreatsDetectedDescription.Text = "N/A";
            // 
            // lblScanResultScannedFilesDescription
            // 
            this.lblScanResultScannedFilesDescription.AutoSize = true;
            this.lblScanResultScannedFilesDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultScannedFilesDescription.CausesValidation = false;
            this.lblScanResultScannedFilesDescription.ForeColor = System.Drawing.Color.White;
            this.lblScanResultScannedFilesDescription.Location = new System.Drawing.Point(207, 82);
            this.lblScanResultScannedFilesDescription.Name = "lblScanResultScannedFilesDescription";
            this.lblScanResultScannedFilesDescription.Size = new System.Drawing.Size(27, 13);
            this.lblScanResultScannedFilesDescription.TabIndex = 205;
            this.lblScanResultScannedFilesDescription.Text = "N/A";
            // 
            // lblScanResultRunTimeDescription
            // 
            this.lblScanResultRunTimeDescription.AutoSize = true;
            this.lblScanResultRunTimeDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultRunTimeDescription.CausesValidation = false;
            this.lblScanResultRunTimeDescription.ForeColor = System.Drawing.Color.White;
            this.lblScanResultRunTimeDescription.Location = new System.Drawing.Point(207, 56);
            this.lblScanResultRunTimeDescription.Name = "lblScanResultRunTimeDescription";
            this.lblScanResultRunTimeDescription.Size = new System.Drawing.Size(27, 13);
            this.lblScanResultRunTimeDescription.TabIndex = 204;
            this.lblScanResultRunTimeDescription.Text = "N/A";
            // 
            // lblScanResultThreatsNeutralizedDescriber
            // 
            this.lblScanResultThreatsNeutralizedDescriber.AutoSize = true;
            this.lblScanResultThreatsNeutralizedDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultThreatsNeutralizedDescriber.CausesValidation = false;
            this.lblScanResultThreatsNeutralizedDescriber.ForeColor = System.Drawing.Color.White;
            this.lblScanResultThreatsNeutralizedDescriber.Location = new System.Drawing.Point(336, 56);
            this.lblScanResultThreatsNeutralizedDescriber.Name = "lblScanResultThreatsNeutralizedDescriber";
            this.lblScanResultThreatsNeutralizedDescriber.Size = new System.Drawing.Size(149, 13);
            this.lblScanResultThreatsNeutralizedDescriber.TabIndex = 202;
            this.lblScanResultThreatsNeutralizedDescriber.Text = "THREATS NEUTRALIZED   :";
            // 
            // lblScanResultTypeOfScanDescription
            // 
            this.lblScanResultTypeOfScanDescription.AutoSize = true;
            this.lblScanResultTypeOfScanDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultTypeOfScanDescription.CausesValidation = false;
            this.lblScanResultTypeOfScanDescription.ForeColor = System.Drawing.Color.White;
            this.lblScanResultTypeOfScanDescription.Location = new System.Drawing.Point(207, 30);
            this.lblScanResultTypeOfScanDescription.Name = "lblScanResultTypeOfScanDescription";
            this.lblScanResultTypeOfScanDescription.Size = new System.Drawing.Size(27, 13);
            this.lblScanResultTypeOfScanDescription.TabIndex = 203;
            this.lblScanResultTypeOfScanDescription.Text = "N/A";
            // 
            // lblScanResultThreatsDetectedDescriber
            // 
            this.lblScanResultThreatsDetectedDescriber.AutoSize = true;
            this.lblScanResultThreatsDetectedDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultThreatsDetectedDescriber.CausesValidation = false;
            this.lblScanResultThreatsDetectedDescriber.ForeColor = System.Drawing.Color.White;
            this.lblScanResultThreatsDetectedDescriber.Location = new System.Drawing.Point(336, 31);
            this.lblScanResultThreatsDetectedDescriber.Name = "lblScanResultThreatsDetectedDescriber";
            this.lblScanResultThreatsDetectedDescriber.Size = new System.Drawing.Size(149, 13);
            this.lblScanResultThreatsDetectedDescriber.TabIndex = 201;
            this.lblScanResultThreatsDetectedDescriber.Text = "THREATS DETECTED         :";
            // 
            // lblScanResultScannedFilesDescriber
            // 
            this.lblScanResultScannedFilesDescriber.AutoSize = true;
            this.lblScanResultScannedFilesDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultScannedFilesDescriber.CausesValidation = false;
            this.lblScanResultScannedFilesDescriber.ForeColor = System.Drawing.Color.White;
            this.lblScanResultScannedFilesDescriber.Location = new System.Drawing.Point(66, 82);
            this.lblScanResultScannedFilesDescriber.Name = "lblScanResultScannedFilesDescriber";
            this.lblScanResultScannedFilesDescriber.Size = new System.Drawing.Size(115, 13);
            this.lblScanResultScannedFilesDescriber.TabIndex = 200;
            this.lblScanResultScannedFilesDescriber.Text = "SCANNED FILES       :";
            // 
            // lblScanResultRunTimeDescriber
            // 
            this.lblScanResultRunTimeDescriber.AutoSize = true;
            this.lblScanResultRunTimeDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultRunTimeDescriber.CausesValidation = false;
            this.lblScanResultRunTimeDescriber.ForeColor = System.Drawing.Color.White;
            this.lblScanResultRunTimeDescriber.Location = new System.Drawing.Point(66, 56);
            this.lblScanResultRunTimeDescriber.Name = "lblScanResultRunTimeDescriber";
            this.lblScanResultRunTimeDescriber.Size = new System.Drawing.Size(117, 13);
            this.lblScanResultRunTimeDescriber.TabIndex = 199;
            this.lblScanResultRunTimeDescriber.Text = "RUN TIME                  :";
            // 
            // lblScanResultTypeOfScanDescriber
            // 
            this.lblScanResultTypeOfScanDescriber.AutoSize = true;
            this.lblScanResultTypeOfScanDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblScanResultTypeOfScanDescriber.CausesValidation = false;
            this.lblScanResultTypeOfScanDescriber.ForeColor = System.Drawing.Color.White;
            this.lblScanResultTypeOfScanDescriber.Location = new System.Drawing.Point(66, 30);
            this.lblScanResultTypeOfScanDescriber.Name = "lblScanResultTypeOfScanDescriber";
            this.lblScanResultTypeOfScanDescriber.Size = new System.Drawing.Size(114, 13);
            this.lblScanResultTypeOfScanDescriber.TabIndex = 198;
            this.lblScanResultTypeOfScanDescriber.Text = "TYPE OF SCAN         :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(9, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 181;
            this.label1.Text = "COMPLETED SCANS:";
            // 
            // lvScanResult
            // 
            this.lvScanResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvScanResult.BackgroundImageTiled = true;
            this.lvScanResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvScanResult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvScanResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lvScanResult.FullRowSelect = true;
            this.lvScanResult.GridLines = true;
            this.lvScanResult.HideSelection = false;
            this.lvScanResult.Location = new System.Drawing.Point(8, 27);
            this.lvScanResult.MultiSelect = false;
            this.lvScanResult.Name = "lvScanResult";
            this.lvScanResult.Size = new System.Drawing.Size(600, 165);
            this.lvScanResult.TabIndex = 170;
            this.lvScanResult.UseCompatibleStateImageBehavior = false;
            this.lvScanResult.View = System.Windows.Forms.View.Details;
            this.lvScanResult.Click += new System.EventHandler(this.lvScanResult_Click);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            this.columnHeader3.Width = 150;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Date";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 150;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Result";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 400;
            // 
            // pnlLicenseExpireAlert
            // 
            this.pnlLicenseExpireAlert.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlLicenseExpireAlert.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlLicenseExpireAlert.BackgroundImage")));
            this.pnlLicenseExpireAlert.Controls.Add(this.lblLicenseExpireText4);
            this.pnlLicenseExpireAlert.Controls.Add(this.lblLicenseExpireText3);
            this.pnlLicenseExpireAlert.Controls.Add(this.lblLicenseExpireText2);
            this.pnlLicenseExpireAlert.Controls.Add(this.lblLicenseExpireText1);
            this.pnlLicenseExpireAlert.Controls.Add(this.btnLicenseAlertRenewNow);
            this.pnlLicenseExpireAlert.Location = new System.Drawing.Point(0, 0);
            this.pnlLicenseExpireAlert.Name = "pnlLicenseExpireAlert";
            this.pnlLicenseExpireAlert.Size = new System.Drawing.Size(750, 341);
            this.pnlLicenseExpireAlert.TabIndex = 23;
            // 
            // lblLicenseExpireText4
            // 
            this.lblLicenseExpireText4.AutoSize = true;
            this.lblLicenseExpireText4.BackColor = System.Drawing.Color.Transparent;
            this.lblLicenseExpireText4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicenseExpireText4.ForeColor = System.Drawing.Color.White;
            this.lblLicenseExpireText4.Location = new System.Drawing.Point(332, 180);
            this.lblLicenseExpireText4.Name = "lblLicenseExpireText4";
            this.lblLicenseExpireText4.Size = new System.Drawing.Size(139, 16);
            this.lblLicenseExpireText4.TabIndex = 13;
            this.lblLicenseExpireText4.Text = "to extend your validity.";
            // 
            // lblLicenseExpireText3
            // 
            this.lblLicenseExpireText3.AutoSize = true;
            this.lblLicenseExpireText3.BackColor = System.Drawing.Color.Transparent;
            this.lblLicenseExpireText3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicenseExpireText3.ForeColor = System.Drawing.Color.White;
            this.lblLicenseExpireText3.Location = new System.Drawing.Point(331, 158);
            this.lblLicenseExpireText3.Name = "lblLicenseExpireText3";
            this.lblLicenseExpireText3.Size = new System.Drawing.Size(372, 16);
            this.lblLicenseExpireText3.TabIndex = 12;
            this.lblLicenseExpireText3.Text = "protection your computer is at risk. Please Click \"Renew Now\"";
            // 
            // lblLicenseExpireText2
            // 
            this.lblLicenseExpireText2.AutoSize = true;
            this.lblLicenseExpireText2.BackColor = System.Drawing.Color.Transparent;
            this.lblLicenseExpireText2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicenseExpireText2.ForeColor = System.Drawing.Color.White;
            this.lblLicenseExpireText2.Location = new System.Drawing.Point(331, 138);
            this.lblLicenseExpireText2.Name = "lblLicenseExpireText2";
            this.lblLicenseExpireText2.Size = new System.Drawing.Size(336, 16);
            this.lblLicenseExpireText2.TabIndex = 11;
            this.lblLicenseExpireText2.Tag = "The {0} Version of AVStrike has expired. With no current";
            this.lblLicenseExpireText2.Text = "The {0} Version of AVStrike has expired. With no current";
            // 
            // lblLicenseExpireText1
            // 
            this.lblLicenseExpireText1.AutoSize = true;
            this.lblLicenseExpireText1.BackColor = System.Drawing.Color.Transparent;
            this.lblLicenseExpireText1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicenseExpireText1.ForeColor = System.Drawing.Color.White;
            this.lblLicenseExpireText1.Location = new System.Drawing.Point(331, 108);
            this.lblLicenseExpireText1.Name = "lblLicenseExpireText1";
            this.lblLicenseExpireText1.Size = new System.Drawing.Size(129, 20);
            this.lblLicenseExpireText1.TabIndex = 10;
            this.lblLicenseExpireText1.Text = "License Expired";
            // 
            // btnLicenseAlertRenewNow
            // 
            this.btnLicenseAlertRenewNow.BackColor = System.Drawing.Color.Transparent;
            this.btnLicenseAlertRenewNow.BackgroundImage = global::AVStrike.Properties.Resources.renew;
            this.btnLicenseAlertRenewNow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLicenseAlertRenewNow.Caption = "";
            this.btnLicenseAlertRenewNow.CaptionColor = System.Drawing.Color.Black;
            this.btnLicenseAlertRenewNow.CaptionColorActive = System.Drawing.Color.Black;
            this.btnLicenseAlertRenewNow.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnLicenseAlertRenewNow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLicenseAlertRenewNow.Image = null;
            this.btnLicenseAlertRenewNow.ImageActive = null;
            this.btnLicenseAlertRenewNow.ImageDisabled = null;
            this.btnLicenseAlertRenewNow.ImageNormal = null;
            this.btnLicenseAlertRenewNow.ImageRollover = null;
            this.btnLicenseAlertRenewNow.IsActive = false;
            this.btnLicenseAlertRenewNow.Location = new System.Drawing.Point(428, 245);
            this.btnLicenseAlertRenewNow.Name = "btnLicenseAlertRenewNow";
            this.btnLicenseAlertRenewNow.Size = new System.Drawing.Size(184, 55);
            this.btnLicenseAlertRenewNow.TabIndex = 9;
            this.btnLicenseAlertRenewNow.TabStop = false;
            this.btnLicenseAlertRenewNow.Click += new System.EventHandler(this.btnLicenseAlertRenewNow_Click);
            // 
            // pnlMainTop
            // 
            this.pnlMainTop.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlMainTop.BackgroundImage = global::AVStrike.Properties.Resources.top_bg;
            this.pnlMainTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlMainTop.Controls.Add(this.splBtnClose);
            this.pnlMainTop.Controls.Add(this.splBtnMinimize);
            this.pnlMainTop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlMainTop.Location = new System.Drawing.Point(182, 0);
            this.pnlMainTop.Name = "pnlMainTop";
            this.pnlMainTop.Size = new System.Drawing.Size(619, 91);
            this.pnlMainTop.TabIndex = 0;
            this.pnlMainTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlMainTop_MouseDown);
            // 
            // splBtnClose
            // 
            this.splBtnClose.BackColor = System.Drawing.Color.White;
            this.splBtnClose.BackgroundImage = global::AVStrike.Properties.Resources.exit;
            this.splBtnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.splBtnClose.Caption = "";
            this.splBtnClose.CaptionColor = System.Drawing.Color.Black;
            this.splBtnClose.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnClose.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnClose.Image = null;
            this.splBtnClose.ImageActive = null;
            this.splBtnClose.ImageDisabled = null;
            this.splBtnClose.ImageNormal = null;
            this.splBtnClose.ImageRollover = global::AVStrike.Properties.Resources.exit_hover;
            this.splBtnClose.IsActive = false;
            this.splBtnClose.Location = new System.Drawing.Point(581, 10);
            this.splBtnClose.Name = "splBtnClose";
            this.splBtnClose.Size = new System.Drawing.Size(24, 24);
            this.splBtnClose.TabIndex = 2;
            this.splBtnClose.TabStop = false;
            this.splBtnClose.Click += new System.EventHandler(this.splBtnClose_Click);
            // 
            // splBtnMinimize
            // 
            this.splBtnMinimize.BackColor = System.Drawing.Color.WhiteSmoke;
            this.splBtnMinimize.BackgroundImage = global::AVStrike.Properties.Resources.minimize;
            this.splBtnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.splBtnMinimize.Caption = "";
            this.splBtnMinimize.CaptionColor = System.Drawing.Color.Black;
            this.splBtnMinimize.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnMinimize.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnMinimize.Image = null;
            this.splBtnMinimize.ImageActive = null;
            this.splBtnMinimize.ImageDisabled = null;
            this.splBtnMinimize.ImageNormal = null;
            this.splBtnMinimize.ImageRollover = global::AVStrike.Properties.Resources.minimize_hover;
            this.splBtnMinimize.IsActive = false;
            this.splBtnMinimize.Location = new System.Drawing.Point(555, 28);
            this.splBtnMinimize.Name = "splBtnMinimize";
            this.splBtnMinimize.Size = new System.Drawing.Size(13, 5);
            this.splBtnMinimize.TabIndex = 1;
            this.splBtnMinimize.TabStop = false;
            this.splBtnMinimize.Click += new System.EventHandler(this.splBtnMinimize_Click);
            // 
            // btnMainScan
            // 
            this.btnMainScan.BackColor = System.Drawing.Color.Transparent;
            this.btnMainScan.BackgroundImage = global::AVStrike.Properties.Resources.scan1;
            this.btnMainScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMainScan.Caption = "";
            this.btnMainScan.CaptionColor = System.Drawing.Color.Black;
            this.btnMainScan.CaptionColorActive = System.Drawing.Color.Black;
            this.btnMainScan.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnMainScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMainScan.Image = null;
            this.btnMainScan.ImageActive = null;
            this.btnMainScan.ImageDisabled = null;
            this.btnMainScan.ImageNormal = null;
            this.btnMainScan.ImageRollover = global::AVStrike.Properties.Resources.scan_hover1;
            this.btnMainScan.IsActive = false;
            this.btnMainScan.Location = new System.Drawing.Point(0, 68);
            this.btnMainScan.Name = "btnMainScan";
            this.btnMainScan.Size = new System.Drawing.Size(181, 63);
            this.btnMainScan.TabIndex = 23;
            this.btnMainScan.TabStop = false;
            this.btnMainScan.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // specialButtons2
            // 
            this.specialButtons2.BackColor = System.Drawing.Color.Transparent;
            this.specialButtons2.BackgroundImage = global::AVStrike.Properties.Resources.help;
            this.specialButtons2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.specialButtons2.Caption = "";
            this.specialButtons2.CaptionColor = System.Drawing.Color.Black;
            this.specialButtons2.CaptionColorActive = System.Drawing.Color.Black;
            this.specialButtons2.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.specialButtons2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.specialButtons2.Image = null;
            this.specialButtons2.ImageActive = null;
            this.specialButtons2.ImageDisabled = null;
            this.specialButtons2.ImageNormal = null;
            this.specialButtons2.ImageRollover = global::AVStrike.Properties.Resources.help_hover;
            this.specialButtons2.IsActive = false;
            this.specialButtons2.Location = new System.Drawing.Point(0, 420);
            this.specialButtons2.Name = "specialButtons2";
            this.specialButtons2.Size = new System.Drawing.Size(181, 69);
            this.specialButtons2.TabIndex = 8;
            this.specialButtons2.TabStop = false;
            // 
            // splBtnUpgrade
            // 
            this.splBtnUpgrade.BackColor = System.Drawing.Color.Transparent;
            this.splBtnUpgrade.BackgroundImage = global::AVStrike.Properties.Resources.activate;
            this.splBtnUpgrade.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.splBtnUpgrade.Caption = "";
            this.splBtnUpgrade.CaptionColor = System.Drawing.Color.Black;
            this.splBtnUpgrade.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnUpgrade.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnUpgrade.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnUpgrade.Image = null;
            this.splBtnUpgrade.ImageActive = null;
            this.splBtnUpgrade.ImageDisabled = null;
            this.splBtnUpgrade.ImageNormal = null;
            this.splBtnUpgrade.ImageRollover = global::AVStrike.Properties.Resources.activate_hover;
            this.splBtnUpgrade.IsActive = false;
            this.splBtnUpgrade.Location = new System.Drawing.Point(518, 520);
            this.splBtnUpgrade.Name = "splBtnUpgrade";
            this.splBtnUpgrade.Size = new System.Drawing.Size(66, 11);
            this.splBtnUpgrade.TabIndex = 22;
            this.splBtnUpgrade.TabStop = false;
            this.splBtnUpgrade.Click += new System.EventHandler(this.splBtnUpgrade_Click);
            // 
            // specialButtons1
            // 
            this.specialButtons1.BackColor = System.Drawing.Color.Transparent;
            this.specialButtons1.BackgroundImage = global::AVStrike.Properties.Resources.info;
            this.specialButtons1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.specialButtons1.Caption = "";
            this.specialButtons1.CaptionColor = System.Drawing.Color.Black;
            this.specialButtons1.CaptionColorActive = System.Drawing.Color.Black;
            this.specialButtons1.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.specialButtons1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.specialButtons1.Image = null;
            this.specialButtons1.ImageActive = null;
            this.specialButtons1.ImageDisabled = null;
            this.specialButtons1.ImageNormal = null;
            this.specialButtons1.ImageRollover = global::AVStrike.Properties.Resources.info_hover;
            this.specialButtons1.IsActive = false;
            this.specialButtons1.Location = new System.Drawing.Point(0, 349);
            this.specialButtons1.Name = "specialButtons1";
            this.specialButtons1.Size = new System.Drawing.Size(181, 71);
            this.specialButtons1.TabIndex = 7;
            this.specialButtons1.TabStop = false;
            // 
            // splBtnBuyNow
            // 
            this.splBtnBuyNow.BackColor = System.Drawing.Color.Transparent;
            this.splBtnBuyNow.BackgroundImage = global::AVStrike.Properties.Resources.buy_now;
            this.splBtnBuyNow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.splBtnBuyNow.Caption = "";
            this.splBtnBuyNow.CaptionColor = System.Drawing.Color.Black;
            this.splBtnBuyNow.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnBuyNow.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnBuyNow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnBuyNow.Image = null;
            this.splBtnBuyNow.ImageActive = null;
            this.splBtnBuyNow.ImageDisabled = null;
            this.splBtnBuyNow.ImageNormal = null;
            this.splBtnBuyNow.ImageRollover = global::AVStrike.Properties.Resources.buy_now_hover;
            this.splBtnBuyNow.IsActive = false;
            this.splBtnBuyNow.Location = new System.Drawing.Point(598, 519);
            this.splBtnBuyNow.Name = "splBtnBuyNow";
            this.splBtnBuyNow.Size = new System.Drawing.Size(69, 12);
            this.splBtnBuyNow.TabIndex = 8;
            this.splBtnBuyNow.TabStop = false;
            this.splBtnBuyNow.Click += new System.EventHandler(this.splBtnBuyNow_Click);
            // 
            // btnMainHome
            // 
            this.btnMainHome.BackColor = System.Drawing.Color.White;
            this.btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.home;
            this.btnMainHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMainHome.Caption = "";
            this.btnMainHome.CaptionColor = System.Drawing.Color.Black;
            this.btnMainHome.CaptionColorActive = System.Drawing.Color.Black;
            this.btnMainHome.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnMainHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMainHome.Image = null;
            this.btnMainHome.ImageActive = null;
            this.btnMainHome.ImageDisabled = null;
            this.btnMainHome.ImageNormal = null;
            this.btnMainHome.ImageRollover = global::AVStrike.Properties.Resources.home_hover;
            this.btnMainHome.IsActive = false;
            this.btnMainHome.Location = new System.Drawing.Point(0, 0);
            this.btnMainHome.Name = "btnMainHome";
            this.btnMainHome.Size = new System.Drawing.Size(181, 68);
            this.btnMainHome.TabIndex = 1;
            this.btnMainHome.TabStop = false;
            this.btnMainHome.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // btnMainSettings
            // 
            this.btnMainSettings.BackColor = System.Drawing.Color.Transparent;
            this.btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.settings;
            this.btnMainSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMainSettings.Caption = "";
            this.btnMainSettings.CaptionColor = System.Drawing.Color.Black;
            this.btnMainSettings.CaptionColorActive = System.Drawing.Color.Black;
            this.btnMainSettings.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnMainSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMainSettings.Image = null;
            this.btnMainSettings.ImageActive = null;
            this.btnMainSettings.ImageDisabled = null;
            this.btnMainSettings.ImageNormal = null;
            this.btnMainSettings.ImageRollover = global::AVStrike.Properties.Resources.settings_hover;
            this.btnMainSettings.IsActive = false;
            this.btnMainSettings.Location = new System.Drawing.Point(0, 279);
            this.btnMainSettings.Name = "btnMainSettings";
            this.btnMainSettings.Size = new System.Drawing.Size(181, 70);
            this.btnMainSettings.TabIndex = 5;
            this.btnMainSettings.TabStop = false;
            this.btnMainSettings.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // splBtnLiveChat
            // 
            this.splBtnLiveChat.BackColor = System.Drawing.Color.Transparent;
            this.splBtnLiveChat.BackgroundImage = global::AVStrike.Properties.Resources.chat;
            this.splBtnLiveChat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.splBtnLiveChat.Caption = "";
            this.splBtnLiveChat.CaptionColor = System.Drawing.Color.Black;
            this.splBtnLiveChat.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnLiveChat.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnLiveChat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnLiveChat.Image = null;
            this.splBtnLiveChat.ImageActive = null;
            this.splBtnLiveChat.ImageDisabled = null;
            this.splBtnLiveChat.ImageNormal = null;
            this.splBtnLiveChat.ImageRollover = global::AVStrike.Properties.Resources.chat_hover;
            this.splBtnLiveChat.IsActive = false;
            this.splBtnLiveChat.Location = new System.Drawing.Point(680, 520);
            this.splBtnLiveChat.Name = "splBtnLiveChat";
            this.splBtnLiveChat.Size = new System.Drawing.Size(78, 11);
            this.splBtnLiveChat.TabIndex = 6;
            this.splBtnLiveChat.TabStop = false;
            this.splBtnLiveChat.Click += new System.EventHandler(this.splBtnLiveChat_Click);
            // 
            // btnMainHistory
            // 
            this.btnMainHistory.BackColor = System.Drawing.Color.Transparent;
            this.btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.history;
            this.btnMainHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMainHistory.Caption = "";
            this.btnMainHistory.CaptionColor = System.Drawing.Color.Black;
            this.btnMainHistory.CaptionColorActive = System.Drawing.Color.Black;
            this.btnMainHistory.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnMainHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMainHistory.Image = null;
            this.btnMainHistory.ImageActive = null;
            this.btnMainHistory.ImageDisabled = null;
            this.btnMainHistory.ImageNormal = null;
            this.btnMainHistory.ImageRollover = global::AVStrike.Properties.Resources.history_hover;
            this.btnMainHistory.IsActive = false;
            this.btnMainHistory.Location = new System.Drawing.Point(0, 209);
            this.btnMainHistory.Name = "btnMainHistory";
            this.btnMainHistory.Size = new System.Drawing.Size(181, 70);
            this.btnMainHistory.TabIndex = 6;
            this.btnMainHistory.TabStop = false;
            this.btnMainHistory.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // btnMainAntivirus
            // 
            this.btnMainAntivirus.BackColor = System.Drawing.Color.Transparent;
            this.btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.threats;
            this.btnMainAntivirus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMainAntivirus.Caption = "";
            this.btnMainAntivirus.CaptionColor = System.Drawing.Color.Black;
            this.btnMainAntivirus.CaptionColorActive = System.Drawing.Color.Black;
            this.btnMainAntivirus.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnMainAntivirus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMainAntivirus.Image = null;
            this.btnMainAntivirus.ImageActive = null;
            this.btnMainAntivirus.ImageDisabled = null;
            this.btnMainAntivirus.ImageNormal = null;
            this.btnMainAntivirus.ImageRollover = global::AVStrike.Properties.Resources.threats_hover;
            this.btnMainAntivirus.IsActive = false;
            this.btnMainAntivirus.Location = new System.Drawing.Point(0, 131);
            this.btnMainAntivirus.Name = "btnMainAntivirus";
            this.btnMainAntivirus.Size = new System.Drawing.Size(181, 78);
            this.btnMainAntivirus.TabIndex = 2;
            this.btnMainAntivirus.TabStop = false;
            this.btnMainAntivirus.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // splBtnScanNow
            // 
            this.splBtnScanNow.BackColor = System.Drawing.Color.Transparent;
            this.splBtnScanNow.BackgroundImage = global::AVStrike.Properties.Resources.scan_now;
            this.splBtnScanNow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnScanNow.Caption = "";
            this.splBtnScanNow.CaptionColor = System.Drawing.Color.Black;
            this.splBtnScanNow.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnScanNow.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnScanNow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnScanNow.Image = null;
            this.splBtnScanNow.ImageActive = null;
            this.splBtnScanNow.ImageDisabled = null;
            this.splBtnScanNow.ImageNormal = null;
            this.splBtnScanNow.ImageRollover = global::AVStrike.Properties.Resources.scannow_hover;
            this.splBtnScanNow.IsActive = false;
            this.splBtnScanNow.Location = new System.Drawing.Point(12, 508);
            this.splBtnScanNow.Name = "splBtnScanNow";
            this.splBtnScanNow.Size = new System.Drawing.Size(124, 40);
            this.splBtnScanNow.TabIndex = 16;
            this.splBtnScanNow.TabStop = false;
            this.splBtnScanNow.Visible = false;
            this.splBtnScanNow.Click += new System.EventHandler(this.splBtnScanNow_Click);
            // 
            // MainBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AVStrike.Properties.Resources.bg;
            this.ClientSize = new System.Drawing.Size(800, 566);
            this.Controls.Add(this.pnlMainBackground);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainBaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AVStrike";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainBaseForm_FormClosing);
            this.Load += new System.EventHandler(this.MainBaseForm_Load);
            this.cmnuTray.ResumeLayout(false);
            this.TrayMenu.ResumeLayout(false);
            this.cmnuOptimizeRegistryManager.ResumeLayout(false);
            this.pnlMainBackground.ResumeLayout(false);
            this.pnlHomeMessageLayout.ResumeLayout(false);
            this.pnlHomeMessageLayout.PerformLayout();
            this.pnlMainCover.ResumeLayout(false);
            this.pnlHomeMainBasic.ResumeLayout(false);
            this.pnlHomeMainCover.ResumeLayout(false);
            this.pnlHomeMainDefault.ResumeLayout(false);
            this.pnlHomeMainDefault.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubHomeQuickScan)).EndInit();
            this.pnlHomeMainScanProcess.ResumeLayout(false);
            this.pnlHomeMainScanProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnStopScanning)).EndInit();
            this.pnlScanMainBasic.ResumeLayout(false);
            this.pnlScanCover.ResumeLayout(false);
            this.pnlScanDefault.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSubHomeFullScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubHomeCustomScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subProcessButton2)).EndInit();
            this.mainPanel5.ResumeLayout(false);
            this.mainPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specialButtons3)).EndInit();
            this.pnlSettingsBasic.ResumeLayout(false);
            this.pnlSettingsHolder.ResumeLayout(false);
            this.pnlSettingsHolderAntivirus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusApply)).EndInit();
            this.tabControlSettingsSelection.ResumeLayout(false);
            this.tabPageScheduleScan.ResumeLayout(false);
            this.tabPageScheduleScan.PerformLayout();
            this.tabPageAdvanced.ResumeLayout(false);
            this.tabPageAdvanced.PerformLayout();
            this.tabPageExcludeFiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFilesRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFilesAdd)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSizeMaximum)).EndInit();
            this.tabPageExcludeFolders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFolderRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnSettingsAntivirusExcludeFoldersAdd)).EndInit();
            this.tabPageRealTimeProtection.ResumeLayout(false);
            this.tabPageRealTimeProtection.PerformLayout();
            this.pnlAntivirusBasic.ResumeLayout(false);
            this.pnlAntivirusCover.ResumeLayout(false);
            this.pnlAntivirusQuarantine.ResumeLayout(false);
            this.pnlAntivirusQuarantine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnRemoveAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnRemove)).EndInit();
            this.pnlHistoryBasic.ResumeLayout(false);
            this.pnlHistoryCover.ResumeLayout(false);
            this.pnlAntivirusScanResult.ResumeLayout(false);
            this.pnlAntivirusScanResult.PerformLayout();
            this.pnlAntivirusScanHistoryValuesShow.ResumeLayout(false);
            this.pnlAntivirusScanHistoryValuesShow.PerformLayout();
            this.pnlLicenseExpireAlert.ResumeLayout(false);
            this.pnlLicenseExpireAlert.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnLicenseAlertRenewNow)).EndInit();
            this.pnlMainTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splBtnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specialButtons2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnUpgrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specialButtons1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnBuyNow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnLiveChat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMainAntivirus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnScanNow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AVStrike.Controls.Panels.MainPanel pnlMainBackground;
        private AVStrike.Controls.Panels.MainPanel pnlMainTop;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnClose;
        private AVStrike.Controls.CustomButtons.MainButton btnMainHome;
        private AVStrike.Controls.CustomButtons.MainButton btnMainSettings;
        private AVStrike.Controls.CustomButtons.MainButton btnMainAntivirus;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnLiveChat;
        private System.Windows.Forms.ContextMenuStrip cmnuTray;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.NotifyIcon niMain;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.ContextMenuStrip TrayMenu;
        private System.Windows.Forms.ToolStripMenuItem eXITToolStripMenuItem;
        private AVStrike.Controls.Panels.MainPanel pnlHomeMainBasic;
        private AVStrike.Controls.Panels.MainPanel pnlHomeMainCover;
        private AVStrike.Controls.Panels.MainPanel pnlHomeMainDefault;
        private System.Windows.Forms.Label lblHomeMainLicenseTypeDescriber;
        private System.Windows.Forms.Label lblHomeMainLastScannedOnDescriber;
        private System.Windows.Forms.Label lblHomeMainLicenseExpiryDateDescriber;
        private System.Windows.Forms.Label lblHomeMainVirusDefinitionVersionDescriber;
        private System.Windows.Forms.Label lblHomeMainDefinitionAutoUpdateDescriber;
        private System.Windows.Forms.Label lblHomeMainRealTimeProtectionDescription;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnBuyNow;
        private AVStrike.Controls.Panels.MainPanel pnlHomeImageLayout;
        private AVStrike.Controls.CustomButtons.SubProcessButton btnSubHomeQuickScan;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnScanNow;
        private System.Windows.Forms.Label lblHomeMainLicenseTypeDescription;
        private System.Windows.Forms.Label lblHomeMainLastScannedOnDescription;
        private System.Windows.Forms.Label lblHomeMainLicenseExpiryDateDescription;
        private System.Windows.Forms.Label lblHomeMainVirusDefinitionVersionDescription;
        private System.Windows.Forms.Label lblHomeMainDefinitionAutoUpdateDescription;
        private System.Windows.Forms.Label lblHomeMainRealTimeProtectionDescriber;
        private AVStrike.Controls.Panels.MainPanel pnlHomeMainScanProcess;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnStopScanning;
        private System.Windows.Forms.Label lblNuetralizedDescriber;
        private System.Windows.Forms.Label lblNeutralizedDescription;
        private System.Windows.Forms.Label lblThreatsDetectedDescriber;
        private System.Windows.Forms.Label lblThreatsDetectedDescription;
        private System.Windows.Forms.Label lblTimeElapsed;
        private System.Windows.Forms.Label lblTimeElapsedDescription;
        private System.Windows.Forms.Label lblScanInProcess;
        private System.Windows.Forms.Label lblScanInProcessDescription;
        private System.Windows.Forms.Label lblItemsScannedCount;
        private System.Windows.Forms.Label lblItemsScannedCountDescription;
        private System.Windows.Forms.Panel pnlMainCover;
        private AVStrike.Controls.Panels.MainPanel pnlAntivirusBasic;
        private AVStrike.Controls.Panels.MainPanel pnlAntivirusCover;
        private AVStrike.Controls.Panels.MainPanel pnlAntivirusScanResult;
        private AVStrike.Controls.Panels.MainPanel pnlAntivirusQuarantine;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnRestore;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnRemoveAll;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnRemove;
        private System.Windows.Forms.ListView lvDiskDiskCleaner;
        private System.Windows.Forms.ColumnHeader colFileName;
        private System.Windows.Forms.ColumnHeader colVirusName;
        private System.Windows.Forms.ColumnHeader colHeaderAlertLevel;
        private System.Windows.Forms.ColumnHeader colRecommendedAction;
        private System.Windows.Forms.ColumnHeader colHeaderAction;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvScanResult;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private AVStrike.Controls.Panels.MainPanel pnlSettingsBasic;
        private AVStrike.Controls.Panels.MainPanel pnlSettingsHolder;
        private AVStrike.Controls.Panels.MainPanel pnlSettingsHolderAntivirus;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnSettingsAntivirusCancel;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnSettingsAntivirusApply;
        private System.Windows.Forms.TabControl tabControlSettingsSelection;
        private System.Windows.Forms.TabPage tabPageScheduleScan;
        private System.Windows.Forms.CheckBox chkSettingRunAVStrikeWhenComputerIsOn;
        private System.Windows.Forms.Label lblSettingScanScheduleDayOfTheWeekDescriber;
        private System.Windows.Forms.ComboBox cmbBxSettingScanScheduleDayOfTheWeek;
        private System.Windows.Forms.Label lblSettingScanScheduleScanTypeDescriber;
        private System.Windows.Forms.ComboBox cmbBxSettingScanScheduleScanType;
        private System.Windows.Forms.CheckBox chkSettingScanScheduleRunAScan;
        private System.Windows.Forms.TabPage tabPageAdvanced;
        private System.Windows.Forms.Label lblSettingsAdvancedKeepQuarantinedFiles;
        private System.Windows.Forms.Label lblSettingsAdvancedAllowFullHistory;
        private System.Windows.Forms.CheckBox chkSettingsAdvancedKeepQuarantinedFiles;
        private System.Windows.Forms.CheckBox chkSettingsAdvancedAllowFullHistory;
        private System.Windows.Forms.TabPage tabPageExcludeFiles;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnSettingsAntivirusExcludeFilesRemove;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnSettingsAntivirusExcludeFilesAdd;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDownSizeMaximum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView lvAntSetExcludedFiles;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.TabPage tabPageExcludeFolders;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnSettingsAntivirusExcludeFolderRemove;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnSettingsAntivirusExcludeFoldersAdd;
        private System.Windows.Forms.ListView lvAntSetExcludedFolders;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.TabPage tabPageRealTimeProtection;
        private System.Windows.Forms.Label lblSettingsRealTimeProtectionDescriber2;
        private System.Windows.Forms.Label lblSettingsRealTimeProtectionDescriber1;
        private System.Windows.Forms.CheckBox chkSettingsRealTimeProtectionTurnOn;
        private Common_Tools.TreeViewAdv.Tree.TreeColumn colOptimizeManagerSection;
        private Common_Tools.TreeViewAdv.Tree.TreeColumn colOptimizeManagerPath;
        private Common_Tools.TreeViewAdv.Tree.TreeColumn colOptimizeManagerArgs;
        private System.ComponentModel.BackgroundWorker backgroundWorkerAntivirus;
        private System.Windows.Forms.ImageList imageListTreeView;
        private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn4;
        private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn5;
        private Common_Tools.TreeViewAdv.Tree.TreeColumn treeColumn6;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeCheckBox nodeCheckBox;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon nodeIcon;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeSection;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeProblem;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeLocation;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeValueName;
        private System.Windows.Forms.ImageList imageListBrowserObjectManager;
        private System.Windows.Forms.ImageList ilStartupManager;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeIcon nodeIconOptimizeManagerIcon;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxOptimizeManagerItem;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxOptimizeManagerPath;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxOptimizeManagerArguments;
        private Common_Tools.TreeViewAdv.Tree.NodeControls.NodeTextBox nodeTextBoxOptimizeManagerSection;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private AVStrike.Controls.Panels.MainPanel pnlAntivirusScanHistoryValuesShow;
        private System.Windows.Forms.Label lblScanResultTotalDataScannedDescription;
        private System.Windows.Forms.Label lblScanResultTotalDataScannedDescriber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblScanResultThreatsNeutralizedDescription;
        private System.Windows.Forms.Label lblScanResultThreatsDetectedDescription;
        private System.Windows.Forms.Label lblScanResultScannedFilesDescription;
        private System.Windows.Forms.Label lblScanResultRunTimeDescription;
        private System.Windows.Forms.Label lblScanResultThreatsNeutralizedDescriber;
        private System.Windows.Forms.Label lblScanResultTypeOfScanDescription;
        private System.Windows.Forms.Label lblScanResultThreatsDetectedDescriber;
        private System.Windows.Forms.Label lblScanResultScannedFilesDescriber;
        private System.Windows.Forms.Label lblScanResultRunTimeDescriber;
        private System.Windows.Forms.Label lblScanResultTypeOfScanDescriber;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnUpgrade;
        private System.Windows.Forms.Label lblHomeMessageDescription3;
        private System.Windows.Forms.Label lblHomeMessageDescription2;
        private System.Windows.Forms.Label lblHomeMessageDescription1;
       // private AVStrike.Controls.CustomProgressBar prgAntivirusScan;
        private System.Windows.Forms.ProgressBar prgAntivirusScan;
        private System.Windows.Forms.ContextMenuStrip cmnuOptimizeRegistryManager;
        private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerSelectAll;
        private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerSelectNone;
        private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerInvertSelection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerExcludeSelected;
        private System.Windows.Forms.ToolStripMenuItem tsmiOptimizeRegistryManagerViewinRegEdit;
        private CheckBox chkQuarantineSelectUnselect;
        private ColumnHeader colHeaderFilePath;
        private Controls.Panels.MainPanel pnlLicenseExpireAlert;
        private Controls.CustomButtons.SpecialButtons btnLicenseAlertRenewNow;
        private Label lblLicenseExpireText2;
        private Label lblLicenseExpireText1;
        private Label lblLicenseExpireText4;
        private Label lblLicenseExpireText3;
        private Controls.CustomButtons.SpecialButtons splBtnMinimize;
        private Controls.Panels.MainPanel pnlHistoryBasic;
        private Controls.Panels.MainPanel pnlHistoryCover;
        private Controls.CustomButtons.MainButton btnMainHistory;
        private Controls.CustomButtons.SpecialButtons specialButtons2;
        private Controls.CustomButtons.SpecialButtons specialButtons1;
        private Controls.CustomButtons.MainButton btnMainScan;
        private Controls.Panels.MainPanel pnlScanMainBasic;
        private Controls.Panels.MainPanel pnlScanCover;
        private Controls.Panels.MainPanel pnlScanDefault;
        private Controls.CustomButtons.SubProcessButton subProcessButton2;
        private Controls.Panels.MainPanel mainPanel5;
        private ProgressBar progressBar1;
        private Controls.CustomButtons.SpecialButtons specialButtons3;
        private Label label17;
        private Label label18;
        private Label label19;
        private Label label20;
        private Label label21;
        private Label label22;
        private Label label23;
        private Label label24;
        private Label label25;
        private Label label26;
        private Controls.Panels.MainPanel pnlHomeMessageLayout;
        private Controls.CustomButtons.SubProcessButton btnSubHomeFullScan;
        private Controls.CustomButtons.SubProcessButton btnSubHomeCustomScan;
    }
}