﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using AVStrike.Classes.Antivirus;
using Common;
using IWshRuntimeLibrary;
using LogMaintainanance;

namespace AVStrike.Classes
{
    public class ApplicationProperties
    {

        //Set the entry of the application in the registry. Based on the parameter passed
        //If true is given then enters the application value in the registry
        //If false is given then delete the application value in the registry
        public void SetStartupInRegistry(bool blRealTimeScanOn)
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                //bool blRealTimeScanOn = (bool)Properties.Settings.Default["SettingsRealTimeProtectionTurnOn"];
                if (blRealTimeScanOn)
                    rk.SetValue(Application.ProductName, Application.ExecutablePath.ToString());
                else
                    rk.DeleteValue(Application.ProductName, false);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ApplicationProperties-SetStartupInRegistry" + ex.Message);
            }

        }

        //Creates the shortcut for the application int Startup path
        //If the parameter passed is true the shortcut is created
        //If the parameter passed is false then the shortcut is deleted
        public void SetStartupInAppData(string path, bool val)
        {
            try
            {
                if (val)
                {
                    WshShellClass wsh = new WshShellClass();
                    IWshRuntimeLibrary.IWshShortcut shortcut = wsh.CreateShortcut(
                        path + "\\" + Application.ProductName + ".lnk") as IWshRuntimeLibrary.IWshShortcut;
                    shortcut.Arguments = "";
                    shortcut.TargetPath = Application.ExecutablePath;
                    shortcut.WindowStyle = 1;
                    shortcut.Description = "AVStrike Antivirus for Windows";
                    shortcut.WorkingDirectory = Application.StartupPath;
                    shortcut.Save();
                }
                else
                {
                    System.IO.File.Delete(path + "\\" + Application.ProductName + ".lnk");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ApplicationProperties-SetStartupInAppData" + ex.Message);
            }
        }

        //Converts the MD5Hash value of the given application path
        public string MD5Hash(string strFilePath)
        {
            StringBuilder strBuilder = new StringBuilder();
            MD5 md5 = new MD5CryptoServiceProvider();
            try
            {
                FileStream stream = new FileStream(strFilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read);
                //compute hash from the bytes of text
                //md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
                md5.ComputeHash(stream);

                stream.Close();

                //get hash result after compute it
                byte[] result = md5.Hash;

               
                for (int i = 0; i < result.Length; i++)
                {
                    //change it into 2 hexadecimal digits
                    //for each byte
                    strBuilder.Append(result[i].ToString("x2"));
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ApplicationProperties-MD5Hash" + ex.Message);
            }
            return strBuilder.ToString();
        }

        /// <summary>
        /// Gets the source file encrypts it and writes as a new file in the destination path
        /// </summary>
        /// <param name="sourceFilePath">Get the source path</param>
        /// <param name="destinationFilePath">Get the destination path</param>
        public void FileEncryption(string sourceFilePath, string destinationFilePath)
        {
            try
            {
                byte[] bufferSource = System.IO.File.ReadAllBytes(sourceFilePath);
                byte[] bufferDestination = new byte[bufferSource.Length];
                int i = 0;
                while (i < bufferSource.Length)
                {
                    bufferDestination[i] = byte.Parse((bufferSource[i] ^ 0x99).ToString());
                    i++;
                }
                System.IO.File.WriteAllBytes(destinationFilePath, bufferDestination);
            }
            catch (FileNotFoundException ioEx)
            {
                ErrorLog.CreateLogFile("ApplicationProperties-FileEncryption" + ioEx.Message);
            }
        }
    }
}
