﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace AVStrike.Classes.WindowsOperation
{
    public static class FormDragDrop
    {
        //Panels are created on various forms this class enables the forms to be dragged and dropped whenever 
        //the panels are clicked and moved

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
    }
}
