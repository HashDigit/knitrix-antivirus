﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using LogMaintainanance;

namespace AVStrike.Classes.WindowsOperation
{
    public class ListViewScrollOperation
    {
        //Enables the horrizontal scroll bar in the list view. By default there is not horrizontal scroll bar in
        //the list view. The horrizontal scroll bar is created by using the user32.dll and importing it into
        //the project and using its ShowScrollBar method and passing the handle values and enabling and
        //disabling the required ScrollBars (ie horrizontal and vertical)


        [DllImport("user32.dll")]

        static public extern bool ShowScrollBar(System.IntPtr hWnd, int wBar, bool bShow);

        private const uint SB_HORZ = 0; //Horrizontal Scroll

        private const uint SB_VERT = 1; //Vertical Scroll

        private const uint ESB_DISABLE_BOTH = 0x3;

        private const uint ESB_ENABLE_BOTH = 0x0;


        public  void HideHorizontalScrollBar(ListView lv,int value, bool isNeeded)
        {
            try
            {
                ShowScrollBar(lv.Handle, value, isNeeded);
                //ShowScrollBar(this.listViewExcludedFiles.Handle, (int)SB_VERT, true);

                //ShowScrollBar(this.lvScanResult.Handle, (int)SB_HORZ, false);
                //ShowScrollBar(this.lvScanResult.Handle, (int)SB_VERT, true);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ListViewScrollOperation-HideHorizontalScrollBar" + ex.Message);
            }
        }
    }
}
