﻿/******************************** Module Header ****************************\
Module Name:  ProcessWatcher.cs
Project:      CSProcessWatcher
Copyright (c) Microsoft Corporation.

using WMI Query Language (WQL) to query process events.

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

using System;
using System.Management;
using LogMaintainanance;

namespace AVStrike.Classes.RealTimeScan
{
    public delegate void ProcessEventHandler(WMI.Win32.Process proc);

    public class ProcessWatcher : ManagementEventWatcher
    {

        // Process Events
        public event ProcessEventHandler ProcessCreated;

        //Watch all the process that are running
        static readonly string processEventQuery = @"SELECT * FROM 
             __InstanceOperationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_Process'";

        public ProcessWatcher()
        {
            Init();
        }

        private void Init()
        {
            try
            {
                this.Query.QueryLanguage = "WQL";
                this.Query.QueryString = string.Format(processEventQuery);
                this.EventArrived += new EventArrivedEventHandler(watcher_EventArrived);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ProcessWatcher-Init" + ex.Message);
            }
        }

        private void watcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            try
            {
                string eventType = e.NewEvent.ClassPath.ClassName;
                WMI.Win32.Process proc = new WMI.Win32.Process(e.NewEvent["TargetInstance"] as ManagementBaseObject);

                if (eventType == "__InstanceCreationEvent")
                {
                    if (ProcessCreated != null)
                    {
                        ProcessCreated(proc);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ProcessWatcher-watcher_EventArrived" + ex.Message);
            }
        }
    }
}
