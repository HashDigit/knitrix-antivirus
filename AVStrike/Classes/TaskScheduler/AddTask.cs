﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AVStrike.Classes.TaskScheduler;
using Business.TaskScheduler;
using LogMaintainanance;

namespace AVStrike.Classes.TaskScheduler
{
    class AddTask
    {
        public TriggerList Triggers { get; set; }

        public void AddNewTask(TriggerList triggers)
        {
            try
            {
                Triggers = triggers;
                //SettingsScanScheduleNewAction();
                CreateTrigger();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddTask-AddNewTask"+ex.Message);
            }

        }

        /// <summary>
        /// After this method completed Save the Task
        /// </summary>
        public void CreateTrigger()
        {
            // recreate Triggers
            Triggers.Clear();

            try
            {
                if (Properties.Settings.Default.AntScanFrequency == 0)
                {
                    TriggerDaily();
                }
                else
                {
                    TriggerWeekly();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddTask-CreateTrigger" + ex.Message);
            }
        }

        public void TriggerWeekly()
        {
            string strCurrentDay = System.DateTime.Now.DayOfWeek.ToString();
            DateTime dtDate = DateTime.Now;
            string datedd = dtDate.ToString("HH:mm");
            var time = DateTime.ParseExact(datedd, "HH:mm", null);
            try
            {
                switch (strCurrentDay)
                {
                    case "Sunday":
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Sunday));
                        break;
                    case "Monday":
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Monday));
                        break;
                    case "Tuesday":
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Tuesday));
                        break;
                    case "Wednesday":
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Wednesday));
                        break;
                    case "Thursday":
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Thursday));
                        break;
                    case "Friday":
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Friday));
                        break;
                    case "Saturday":
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Saturday));
                        break;
                    default:
                        Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Sunday));
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddTask-TriggerWeekly" + ex.Message);
            }
        }

        public void TriggerDaily()
        {
            try
            {
                DateTime dtDate = DateTime.Now;
                string datedd = dtDate.ToString("HH:mm");
                var time = DateTime.ParseExact(datedd, "HH:mm", null);
                Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Sunday));
                Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Monday));
                Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Tuesday));
                Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Wednesday));
                Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Thursday));
                Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Friday));
                Triggers.Add(new WeeklyTrigger((short)time.Hour, (short)time.Minute, DaysOfTheWeek.Saturday));
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddTask-TriggerDaily" + ex.Message);
            }
        }
    }
}
