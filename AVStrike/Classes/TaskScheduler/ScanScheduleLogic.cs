using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BaseTool;
using Business.TaskScheduler;
using Common;
using LogMaintainanance;

namespace AVStrike.Classes.TaskScheduler
{
    /// <summary>
    /// Class to set the Scan Schedule
    /// </summary>
    public class ScanScheduleLogic
    {
        /// <summary>
        /// Allocate the task name
        /// </summary>
        public static string strSetTaskName = string.Format(ApplicationConstant.ScheduleTaskName, Application.ProductName, Guid.NewGuid().ToString());

        /// <summary>
        /// Get the list of all the available tasks
        /// </summary>
        /// <returns></returns>
        public static List<Task> List()
        {
            var scheduledTasks = new ScheduledTasks();
            var allTaskNames = scheduledTasks.GetTaskNames();
            var tasks = new List<Task>();
            try
            {
                foreach (var allTaskName in allTaskNames)
                {
                    if (allTaskName.ToLower().Contains(strSetTaskName.ToLower()))
                        tasks.Add(scheduledTasks.OpenTask(allTaskName));
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ScanScheduleLogic-List" + ex.Message);
            }

            return tasks;
        }

        /// <summary>
        /// Create new scan schedule task with default settings
        /// </summary>
        /// <returns></returns>
        public static Task Create(string strParameter)
        {
            try
            {
                var scheduledTasks = new ScheduledTasks();
                var newTask = scheduledTasks.CreateTask(strSetTaskName);
                newTask.ApplicationName = Application.ExecutablePath;
                newTask.Parameters = strParameter;
                newTask.Comment = "";
                newTask.Creator = "";
                newTask.WorkingDirectory = Application.StartupPath;
                newTask.SetAccountInformation(Environment.UserName, (string)null);

                newTask.Flags = TaskFlags.RunOnlyIfLoggedOn;
                newTask.IdleWaitDeadlineMinutes = 20;
                newTask.IdleWaitMinutes = 10;
                newTask.MaxRunTime = new TimeSpan(50, 0, 0);
                newTask.Priority = System.Diagnostics.ProcessPriorityClass.Normal;

                AVStrike.Properties.Settings.Default.AntTaskId = strSetTaskName;
                AVStrike.Properties.Settings.Default.Save();

                return newTask;
            }
            catch (Exception ex)
            {
                CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while scan schedule task creation.\r\nError:\r\n{0}", ex));
                return null;
            }
        }

        /// <summary>
        /// Save the Task
        /// </summary>
        /// <param name="task"></param>
        public static void Save(Task task)
        {
            try
            {
                task.Save();
            }
            catch (Exception ex)
            {
                CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while saving scan schedule task.\r\nError:\r\n{0}", ex));
            }
        }

        /// <summary>
        /// Open existing task
        /// </summary>
        /// <returns></returns>
        public static Task Open(string taskName)
        {
            try
            {
                var scheduledTasks = new ScheduledTasks();
                return scheduledTasks.OpenTask(taskName);
            }
            catch (Exception ex)
            {
                CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while opening scan schedule task '{0}'.\r\nError:\r\n{1}", taskName, ex));
                return null;
            }
        }

        /// <summary>
        /// Delete existing task
        /// </summary>
        /// <returns></returns>
        public static void Delete(string taskName)
        {
            try
            {
                var scheduledTasks = new ScheduledTasks();
                scheduledTasks.DeleteTask(taskName);
            }
            catch (Exception ex)
            {
                CLogger.WriteLog(ELogLevel.ERROR, string.Format("Error while deletion scan schedule task '{0}'.\r\nError:\r\n{1}", taskName, ex));
            }
        }
    }
}