﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using LogMaintainanance;

namespace AVStrike.Classes.Antivirus
{
    class MoveFileAndDeleteFlag
    {
        
        public void CheckForRebootFile(string strFileName)
        {

            if (!Native.MoveFileEx(strFileName, null, 4))
            {
                ErrorLog.CreateLogFile("MoveFileAndDeleteFlag-CheckForRebootFile : Couldn't schedule a time for deletion");
            }
        }


        //Deletes a file From Quarantine
        public void DeleteFromQuarantine(string strPath)
        {
            System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(strPath);

            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch (Exception e)
                {
                    if (e.InnerException is IOException)
                    {
                        CheckForRebootFile(file.ToString());

                    }
                    return;
                }
            }
        }

        public void RemoveFromQuarantine(string strFileName)
        {
            try
            {
                System.IO.File.Delete(strFileName);
            }
            catch (Exception e)
            {
                if (e.InnerException is IOException)
                {
                    CheckForRebootFile(strFileName);

                }
                return;
            }
        }

        public void CopyFromQuarantine(string strSource, string strDestination)
        {
            try
            {
                System.IO.File.Copy(strSource, strDestination);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MoveFileAndDeleteFlag-CopyFromQuarantine" + ex.Message);
            }
        }

        public void RestoreFromQuarantine(string strSource, string strDestination)
        {
            System.IO.File.Copy(strSource, strDestination);

            try
            {
                System.IO.File.Delete(strSource);
            }
            catch (IOException e)
            {
                if (e.InnerException is IOException)
                {
                    CheckForRebootFile(strSource);

                }
                if (e.InnerException is FileNotFoundException)
                {
                }
                return;
            }
            
        }
    }

    internal static class Native
    {
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool MoveFileEx(string lpExistingFileName, string lpNewFileName, int dwFlags);

    }
}
