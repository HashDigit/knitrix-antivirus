﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using Common;
using Common.Extension;
using System.Threading;
using LogMaintainanance;

namespace AVStrike.Classes.Antivirus
{
    public class AntivirusSettings
    {
        [XmlIgnore]
        public StringCollection excludedDirs;
        [XmlIgnore]
        public StringCollection excludedFileTypes;
        [XmlIgnore]
        public StringCollection includedFolders;

        [XmlIgnore]
        public StringCollection excludeFilesListForHashFiles;

        /// <summary>
        /// //Constructor loads the antivirus settings to get all the excluded file types and excluded folders during
        /// the scan
        /// </summary>
        public AntivirusSettings()
        {
            try
            {
                excludedDirs = new StringCollection();
                excludedDirs.Add(Application.StartupPath);
                excludedFileTypes = new StringCollection();
                excludedFileTypes.AddRange(new List<string>(@".dbx
.tbb
.pst
.dat
.log
.evt
.nsf
.ntf
.chm".Split('\n')).Trim(new char[] { '\r' }).ToArray());


                excludeFilesListForHashFiles = new StringCollection();
                excludeFilesListForHashFiles.AddRange(new List<string>(@".dbx
.tbb
.pst
.dat
.log
.evt
.nsf
.ntf
.chm
.sys
.doc
.txt
.xls
.docx".Split('\n')).Trim(new char[] { '\r' }).ToArray());


                includedFolders = new StringCollection();
                includedFolders.Add(Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.User));
                includedFolders.Add(Environment.GetEnvironmentVariable("TEMP", EnvironmentVariableTarget.Machine));
                includedFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.Recent));
                includedFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.InternetCache));

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AntivirusSettings-AntivirusSettings" + ex.Message);
            }
        }

        //Settings when the all the files needs to be scanned. Excluded files is made empty at that point
        //During real time scan
        public List<string> excludedfiles()
        {
            List<string> lines = new List<string>();
            lines.Add("");
            return lines;
        }

        //Settings when the all the folders needs to be scanned. Excluded folders is made empty at that point
        //During real time scan
        public List<string> excludedfolders()
        {
            List<string> lines = new List<string>();
            lines.Add("");
            return lines;
        }

        /// <summary>
        /// Load the splash screen
        /// </summary>
        public static void ShowSplashScreeen()
        {
            SplashScreen.SplashScreen.ShowSplashScreen();
            SplashScreen.SplashScreen.SetStatus("Load Threads");
            System.Threading.Thread.Sleep(500);
            SplashScreen.SplashScreen.SetStatus("Initializing Modules");
            System.Threading.Thread.Sleep(2000);
            SplashScreen.SplashScreen.SetStatus("Checking Product Activation");
            System.Threading.Thread.Sleep(3000);
            SplashScreen.SplashScreen.CloseForm();
        }
        
    }
}
