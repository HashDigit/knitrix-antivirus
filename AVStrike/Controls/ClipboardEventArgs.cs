﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AVStrike.Controls
{
    class ClipboardEventArgs : EventArgs
    {
        public string ClipBoardText { get; set; }

        public ClipboardEventArgs(string clipboardText)
        {

            ClipBoardText = clipboardText;

        }

    }
}
