﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogMaintainanance;

namespace AVStrike.Controls.Panels
{
    public partial class MainPanel : Panel
    {
        public MainPanel()
        {
            try
            {
                //ControlStyle is enum of system.windows.forms
                SetStyle(ControlStyles.UserPaint, true);
                SetStyle(ControlStyles.AllPaintingInWmPaint, true);
                SetStyle(ControlStyles.DoubleBuffer, true);
                SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
                this.SetStyle(System.Windows.Forms.ControlStyles.ResizeRedraw, false);

                this.DoubleBuffered = true;
                this.ResizeRedraw = true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainPanel-MainPanel"+ex.Message);
            }
        }
    }
}
