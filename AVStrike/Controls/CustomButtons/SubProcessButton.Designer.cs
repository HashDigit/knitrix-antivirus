﻿using System;
using LogMaintainanance;

namespace AVStrike.Controls.CustomButtons
{
    partial class SubProcessButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SubProcessButton-Dispose" + ex.Message);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            try
            {
                ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
                this.SuspendLayout();
                // 
                // PictureBoxButton
                // 
                this.MouseEnter += new System.EventHandler(this.PictureBoxButton_MouseEnter);
                this.MouseLeave += new System.EventHandler(this.PictureBoxButton_MouseLeave);
                ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
                this.ResumeLayout(false);
            }

            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("SubProcessButton-InitializeComponent" + ex.Message);
            }
        }

        #endregion
    }
}
