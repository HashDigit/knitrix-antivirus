﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogMaintainanance;

namespace AVStrike.Controls
{
    class MyTextBox : TextBox
    {

        public event EventHandler<ClipboardEventArgs> Pasted;


        private const int WM_PASTE = 0x0302;

        protected override void WndProc(ref Message m)
        {
            try
            {
                if (m.Msg == WM_PASTE)
                {
                    var evt = Pasted;
                    if (evt != null)
                    {
                        evt(this, new ClipboardEventArgs(Clipboard.GetText()));
                        // don't let the base control handle the event again
                        return;
                    }
                }
                base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MyTextBox-WndProc" + ex.Message);
            }
        }
    }
}
