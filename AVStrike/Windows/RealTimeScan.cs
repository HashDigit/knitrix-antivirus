﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AVStrike.Classes.Antivirus;
using AVStrike.Classes.WindowsOperation;
using System.IO;
using LogMaintainanance;
using SQLiteConnectionLibrary;

namespace AVStrike.Windows
{
    public partial class RealTimeScan : Form
    {
        //ScanerDllImport _ScanerDllImport = new ScanerDllImport();
        SQLiteProcess _SQLiteProcess = new SQLiteProcess();
        MoveFileAndDeleteFlag _MoveFileAndDeleteFlag = new MoveFileAndDeleteFlag();

        System.Windows.Forms.Timer TimerRealTimeScan;

        //Constructor which process the real time scanning
        public RealTimeScan(string strFile,string strPath)
        {
            InitializeComponent();
            //Rectangle workingArea = Screen.GetWorkingArea(this);
            //this.Location = new Point(workingArea.Right - Size.Width, workingArea.Bottom - Size.Height);


            ////Initialize the timer
            //TimerRealTimeScan = new System.Windows.Forms.Timer();
            //TimerRealTimeScan.Tick += new EventHandler(TimerRealTimeScan_Tick);
            //TimerRealTimeScan.Interval = 1000;
            //TimerRealTimeScan.Start();

            ////FileInfo file = new FileInfo(strPath);

            //if (strPath.Length > 20)
            //{
            //    strPath = strPath.Substring(0, 10) + "...." + strPath.Substring(strPath.Length - 10, 10);

            //}

            //lblRealTimeScanFileNameDescription.Text = strFile;
            //lblRealTimeScanFilePathDescription.Text = strPath;


            ////Return 1 if file is malicious and 0 if file is not malicious
            //int intReturnStatus = _ScanerDllImport.ScanProcess(strPath);

            //if (intReturnStatus != 0)
            //{
            //    string strFilepath = Application.StartupPath + @"\Quarantine\";
            //    string strFilename = Path.GetFileName(strPath);
            //    string strDestination = strFilepath + strFilename;
            //    string strCurrentDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            //    try
            //    {

            //        //_SQLiteProcess.InsertScanHistory(strFilename, strPath);
            //        System.IO.File.Copy(strPath, strDestination, true);
            //        System.IO.File.Delete(strPath);
            //    }
            //    catch (Exception ex)
            //    {
            //        ErrorLog.CreateLogFile("RealTimeScan-RealTimeScan" + ex.Message);
            //        //Method is used when the file is already in use and cannot be moved to the quarantine
            //        //directory. A flag is set and the file will be deleted permanently during the reboot
            //        _MoveFileAndDeleteFlag.CheckForRebootFile(strPath);

            //    }
            //    lblRealTimeScanActionTakenDescription.Text = "Moved to Quarantine Folder";
            //    lblRealTimeScanStatusDescription.Text = "File is Malicious";

            //}
            //else
            //{
            //    this.Close();
            //    //lblRealTimeScanActionTakenDescription.Text = "No Action Taken";
            //    //lblRealTimeScanStatusDescription.Text = "File is Malicious";
            //}
        }

        Int32 secondsCounter = 0;

        //Timer to automatically close the pop up window after 10 seconds
        private void TimerRealTimeScan_Tick(object sender, EventArgs e)
        {
            try
            {
                if (secondsCounter % 10 == 0)
                {
                    this.Close();
                }
                secondsCounter++;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("RealTimeScan-TimerRealTimeScan_Tick" + ex.Message);
            }
        }

        private void RealTimeScan_Load(object sender, EventArgs e)
        {

        }


        private void splBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
    }
}
