﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseTool;
using LogMaintainanance;
using SQLiteConnectionLibrary;

namespace AVStrike.Windows
{
    public partial class Notification : Form
    {
        System.Windows.Forms.Timer TimerRealTimeScan;
        public Notification()
        {
            try
            {
                InitializeComponent();

                // this.ShowInTaskbar = false;

                //Start the timer and set the interval to 1sec
                TimerRealTimeScan = new System.Windows.Forms.Timer();
                TimerRealTimeScan.Tick += new EventHandler(TimerRealTimeScan_Tick);
                TimerRealTimeScan.Interval = 1000;
                TimerRealTimeScan.Start();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Notification-Notification" + ex.Message);
            }
        }

        //Override method to show the pop up window at the bottom right of the screen when ever the flag for
        //license expiration is enabled
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                ShowBottomRight();
                base.OnLoad(e);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Notification-OnLoad" + ex.Message);
            }
        }

        //Function to get the pixels of the bottom right side of the monitor and then set the value of Top and Left
        //Function enables to show the window at the bottom right of the screen
        private void ShowBottomRight()
        {
            Screen rightmost = Screen.AllScreens[0];
            try
            {
                foreach (Screen screen in Screen.AllScreens)
                {
                    if (screen.WorkingArea.Right > rightmost.WorkingArea.Right)
                        rightmost = screen;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Notification-ShowBottomRight" + ex.Message);
            }

            this.Left = rightmost.WorkingArea.Right - this.Width;
            this.Top = rightmost.WorkingArea.Bottom - this.Height;
        }


        Int32 secondsCounter = 1;

        //Pop up window will disappear after 10 seconds.
        //Logic hasnt been used
        private void TimerRealTimeScan_Tick(object sender, EventArgs e)
        {
            try
            {
                if (secondsCounter % 10 == 0)
                {
                    // this.BackgroundImage = global::AVStrikeSecureSearch.Properties.Resources.pop_ad_1;
                    //this.Close();
                    secondsCounter = 1;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Notification-TimerRealTimeScan_Tick" + ex.Message);
            }
            secondsCounter++;
        }

        private void Notification_Click(object sender, EventArgs e)
        {

        }

        private void btnNotificationOk_Click(object sender, EventArgs e)
        {
            //ActivationForm _ActivationForm = null;
            //if (!IsFormAlreadyOpen(typeof(ActivationForm)))
            //{
            //    this.Hide();
            //    _ActivationForm = new ActivationForm();

            //    _ActivationForm.ShowDialog();

            //    return;
            //}
            this.Close();

        }

        //Check if the form is already open
        public static bool IsFormAlreadyOpen(Type FormType)
        {
            foreach (Form OpenForm in Application.OpenForms)
            {
                if (OpenForm.GetType() == FormType)
                    return true;
            }

            return false;
        }

        //Load the License expiration notification window with custome message and images
        private void Notification_Load(object sender, EventArgs e)
        {
            int versionPoint = Properties.Settings.Default.ActVersionPoint;
            try
            {
                switch (versionPoint)
                {
                    case 1:
                        lblNotificationAlertText2.Text = string.Format(lblNotificationAlertText2.Tag.ToString(), "Trial");
                        break;
                    case 2:
                        lblNotificationAlertText2.Text = string.Format(lblNotificationAlertText2.Tag.ToString(), "Full");
                        break;
                    default:
                        break;

                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Notification-Notification_Load" + ex.Message);
            }
        }

        //Redirect to the buy now page
        private void lblNotificationAlertText4_Click(object sender, EventArgs e)
        {
            try
            {
                if (!AuthenticationAndSecurity.CheckInternet())
                {
                    MessageBox.Show(this, "Please Check your internet connection and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    System.Diagnostics.Process.Start(ExternalUrls.BuyNow);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Notification-lblNotificationAlertText4_Click" + ex.Message);
            }
        }
       
    }
}
