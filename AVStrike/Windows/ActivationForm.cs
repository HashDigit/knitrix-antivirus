﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AVStrike.Classes.WindowsOperation;
using System.Diagnostics;
using BaseTool;
using SQLiteConnectionLibrary;
using AVStrike.Classes.Antivirus;
using LogMaintainanance;
using System.Collections;

namespace AVStrike
{
    public partial class ActivationForm : Form
    {
        #region Class Declaration
        DataConnection _DataConnection = new DataConnection();
        CalculateRemainingDays CalculateRemainingDays = new CalculateRemainingDays();
        SQLiteConnectionLibrary.SQLiteProcess _SQLiteProcess = new SQLiteConnectionLibrary.SQLiteProcess();
        #endregion


        #region Eliminate Flickering
        //Comment for Using on XP
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
        #endregion

        public ActivationForm()
        {
            InitializeComponent();
        }





        private void btnBuyNow_Click(object sender, EventArgs e)
        {
            try
            {
                if (!BaseTool.AuthenticationAndSecurity.CheckInternet())
                {
                    MessageBox.Show(this, "Please Check your internet connection and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    System.Diagnostics.Process.Start(ExternalUrls.BuyNow);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-btnBuyNow_Click" + ex.Message);
            }

        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            try
            {
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-btnQuit_Click" + ex.Message);
            }
        }

        private void btnActivateTrialVersion_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            int days = ExternalUrls.TrialDaysNumber;
            string StartDate = string.Empty;
            string EndDate = string.Empty;
            ArrayList RemainingDays = new ArrayList();
            try
            {
                #region Validation
                int versionPoint = Properties.Settings.Default.ActVersionPoint;
                RemainingDays = CalculateRemainingDays.ReturnTrialVersionReaminingDaysList();
                days = Convert.ToInt32(RemainingDays[0]);
                Properties.Settings.Default.ActDaysRemaining = days;
                Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(RemainingDays[2].ToString());
                Properties.Settings.Default.Save();

                if (days < 1)
                {
                    MessageBox.Show(this, "Your 30 day trial has expired.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                _MainBaseForm = new MainBaseForm();
                _MainBaseForm.Close();
                if (_MainBaseForm != null)
                {

                    _MainBaseForm.FormClosed += instanceHasBeenClosed;
                    this.Close();
                }

                _MainBaseForm.Show();
                this.Close();
                return;
                #endregion


            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-btnActivateTrialVersion_Click" + ex.Message);
            }
            Cursor.Current = Cursors.Default;
        }

        private void btnActivateFullVersion_Click(object sender, EventArgs e)
        {
            string ActivationKey = ReturnActivationKey();
            string MacID = AuthenticationAndSecurity.GetMacAddress();

            string DomainURL = string.Format(ExternalUrls.DomainUrl, ExternalUrls.Domain);
            string FullDays = string.Format(ExternalUrls.FullDaysReamining, ExternalUrls.Domain, MacID, ActivationKey);
            bool isExceptionThrown = true;

            int days = ExternalUrls.FullDaysNumber;
            string StartDate = string.Empty;
            string EndDate = string.Empty;

            ArrayList DaysDetails = new ArrayList();

            try
            {
                if (ActivationKey == string.Format("Invalid Product Key") || ActivationKey == string.Format("No Value"))
                {
                    MessageBox.Show(this, "Please enter a product key and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControls();
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;

                if (BaseTool.AuthenticationAndSecurity.CheckInternet())
                {
                    _SQLiteProcess.InserIntoFull();

                    DaysDetails = BaseTool.AuthenticationAndSecurity.GetRemainingDaysDetails(FullDays, DomainURL, out isExceptionThrown);

                    string strResponseMessage = DaysDetails[4].ToString();
                    int strResponseCode = Convert.ToInt32(DaysDetails[3].ToString());
                    //string strResponseMessage = _Activation.ReturnResponseText(strXmlResponse)[1].ToString();
                    ValidateKey(strResponseCode, strResponseMessage);

                    if (strResponseCode == 1000 || strResponseCode == 800)
                    {
                        days = Convert.ToInt32(DaysDetails[0].ToString());
                        if (days > 0)
                        {
                            Properties.Settings.Default.ActDaysRemaining = days;
                            Properties.Settings.Default.ActVersionType = "Full";
                            Properties.Settings.Default.ActActivationDate = Convert.ToDateTime(DaysDetails[1].ToString());
                            Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(DaysDetails[2].ToString());
                            Properties.Settings.Default.ActProductKey = ActivationKey;
                            Properties.Settings.Default.ActVersionPoint = 2;
                            Properties.Settings.Default.Save();
                            var _MainRelease = new MainBaseForm();
                            _MainRelease.Show();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show(this, "Your Product Validity has expired.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }

                }
                else
                {
                    MessageBox.Show(this, "Please connect to internet and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-btnActivateFullVersion_Click" + ex.Message);
            }

            Cursor.Current = Cursors.Default;

        }

        public string ReturnActivationKey()
        {
            string strActivationKey = "";
            if (!string.IsNullOrEmpty(TxtLicense1.Text) && !string.IsNullOrEmpty(TxtLicense2.Text) && !string.IsNullOrEmpty(TxtLicense3.Text) && !string.IsNullOrEmpty(TxtLicense4.Text))
            {
                if (TxtLicense1.Text.Length == 4 && TxtLicense2.Text.Length == 4 && TxtLicense3.Text.Length == 4 && TxtLicense4.Text.Length == 4)
                {
                    strActivationKey = TxtLicense1.Text + "-" + TxtLicense2.Text + "-" + TxtLicense3.Text + "-" + TxtLicense4.Text;
                    return strActivationKey;
                }
                else
                {
                    return "Invalid Product Key";
                }
            }
            else
            {
                return "No Value";
            }
        }

        public void ClearControls()
        {
            TxtLicense1.Text = "";
            TxtLicense2.Text = "";
            TxtLicense3.Text = "";
            TxtLicense4.Text = "";
        }

        public void ValidateKey(int intResponseCode, string strResponseMessage)
        {
            try
            {
                switch (intResponseCode)
                {
                    case 300:
                        MessageBox.Show(this, strResponseMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 400:
                        MessageBox.Show(this, strResponseMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 500:
                        MessageBox.Show(this, strResponseMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 700:
                        MessageBox.Show(this, "Sorry, the product key that you entered is not valid. Please try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 800:
                        MessageBox.Show(this, "Your product has been activated sucessfully.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 900:
                        MessageBox.Show(this, "Sorry, the product key that you entered is not valid. Please try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 1000:
                        MessageBox.Show(this, "Your product has been activated sucessfully.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 2000:
                        MessageBox.Show(this, strResponseMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    case 3000:
                        MessageBox.Show(this, strResponseMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //ClearControls();
                        break;
                    default:
                        MessageBox.Show(this, strResponseMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-ValidateKey" + ex.Message);
            }
        }


        private void TxtLicense_Pasted(object sender, AVStrike.Controls.ClipboardEventArgs e)
        {
            try
            {
                if (e.ClipBoardText.Split('-').Count() == 4)
                {
                    TxtLicense1.Text = e.ClipBoardText.Split('-')[0];
                    TxtLicense2.Text = e.ClipBoardText.Split('-')[1];
                    TxtLicense3.Text = e.ClipBoardText.Split('-')[2];
                    TxtLicense4.Text = e.ClipBoardText.Split('-')[3];
                }
                else
                {
                    TxtLicense1.Text = e.ClipBoardText.Substring(0, 4);
                    TxtLicense2.Text = e.ClipBoardText.Substring(4, 4);
                    TxtLicense3.Text = e.ClipBoardText.Substring(8, 4);
                    TxtLicense4.Text = e.ClipBoardText.Substring(12, 4);

                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-TxtLicense_Pasted" + ex.Message);
            }
        }

        #region Minimize and Close
        private void splBtnMinimize_Click(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Minimized;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-splBtnMinimize_Click" + ex.Message);
            }

        }

        private void splBtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.IsFirstRun)
                    Process.GetCurrentProcess().Kill();
                //return;
                _MainBaseForm = new MainBaseForm();
                if (_MainBaseForm != null)
                {

                    _MainBaseForm.FormClosed += instanceHasBeenClosed;
                }
                _MainBaseForm.Show();

                //MainBaseForm _MainBaseForm = Application.OpenForms["MainBaseForm"];

                //if (_MainBaseForm != null)
                //    _MainBaseForm.BringToFront();
                //else
                //{
                //    var _MainRelease = new MainBaseForm();
                //    _MainRelease.Show();
                //}

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-splBtnClose_Click" + ex.Message);
            }
            this.Close();// Process.GetCurrentProcess().Kill();
        }
        #endregion

        private void pnlTop_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    FormDragDrop.ReleaseCapture();
                    //Parameters are SendMessage(Handle,WM_NCLBUTTONDOWN,HT_CAPTION)
                    FormDragDrop.SendMessage(Handle, 0xA1, 0x2, 0);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-pnlTop_MouseDown" + ex.Message);
            }
        }



        private void panel3_Click(object sender, EventArgs e)
        {
            try
            {
                if (!AuthenticationAndSecurity.CheckInternet())
                {
                    MessageBox.Show(this, "Please Check your internet connection and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    System.Diagnostics.Process.Start(ExternalUrls.BuyNow);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-panel3_Click" + ex.Message);
            }

        }


        #region KeyPress Events
        private void TxtLicense1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    if (TxtLicense1.Text.Length > 0)
                        //TxtLicense1.Text = TxtLicense1.Text.Remove(TxtLicense1.Text.Length - 1, 1);
                        TxtLicense2.Select(TxtLicense2.Text.Length, 0);
                }
                else
                {
                    e.KeyChar = Char.ToUpper(e.KeyChar);

                    if (TxtLicense1.Text.Length > 3)
                    {
                        e.Handled = true;
                        //SendKeys.Send("{TAB}");
                        TxtLicense2.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-TxtLicense1_KeyPress" + ex.Message);
            }
        }

        private void TxtLicense2_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Back && TxtLicense2.Text.Length > 0)
                {
                    //TxtLicense2.Text = TxtLicense2.Text.Remove(TxtLicense2.Text.Length - 1, 1);
                    TxtLicense2.Select(TxtLicense2.Text.Length, 0);
                    if (TxtLicense2.Text.Length == 1)
                        TxtLicense1.Focus();
                }
                else
                {
                    e.KeyChar = Char.ToUpper(e.KeyChar);
                    if (TxtLicense2.Text.Length > 3)
                    {
                        e.Handled = true;
                        //SendKeys.Send("{TAB}");
                        TxtLicense3.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-TxtLicense2_KeyPress" + ex.Message);
            }
        }

        private void TxtLicense3_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Back && TxtLicense3.Text.Length > 0)
                {
                    //TxtLicense3.Text = TxtLicense3.Text.Remove(TxtLicense3.Text.Length - 1, 1);
                    TxtLicense3.Select(TxtLicense3.Text.Length, 0);
                    if (TxtLicense3.Text.Length == 1)
                        TxtLicense2.Focus();
                }
                else
                {
                    e.KeyChar = Char.ToUpper(e.KeyChar);

                    if (TxtLicense3.Text.Length > 3)
                    {
                        e.Handled = true;
                        //SendKeys.Send("{TAB}");
                        TxtLicense4.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-TxtLicense3_KeyPress" + ex.Message);
            }
        }

        private void TxtLicense4_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Back && TxtLicense4.Text.Length > 0)
                {
                    //TxtLicense4.Text = TxtLicense4.Text.Remove(TxtLicense4.Text.Length - 1, 1);
                    TxtLicense4.Select(TxtLicense4.Text.Length, 0);
                    if (TxtLicense4.Text.Length == 1)
                        TxtLicense3.Focus();
                }
                else
                {
                    e.KeyChar = Char.ToUpper(e.KeyChar);

                    if (TxtLicense4.Text.Length > 3)
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-TxtLicense4_KeyPress" + ex.Message);
            }

        }
        #endregion

        private void TxtLicense4_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Back)
                {


                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-TxtLicense4_KeyDown" + ex.Message);
            }

        }

        private void ActivationForm_Load(object sender, EventArgs e)
        {
            //if (!Properties.Settings.Default.IsFirstRun)
            //{
            //    btnActivateTrialVersion.Enabled = false;
            //    btnGoBack.Enabled = true;

            //}
        }

        private MainBaseForm _MainBaseForm = null;

        private void btnGoBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.IsFirstRun)
                    return;

                _MainBaseForm = new MainBaseForm();
                if (_MainBaseForm != null)
                {

                    _MainBaseForm.FormClosed += instanceHasBeenClosed;
                }
                _MainBaseForm.Show();

                //MainBaseForm _MainBaseForm = Application.OpenForms["MainBaseForm"];

                //if (_MainBaseForm != null)
                //    _MainBaseForm.BringToFront();
                //else
                //{
                //    var _MainRelease = new MainBaseForm();
                //    _MainRelease.Show();
                //}
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ActivationForm-btnGoBack_Click" + ex.Message);
            }
            this.Close();
        }

        private void instanceHasBeenClosed(object sender, FormClosedEventArgs e)
        {
            _MainBaseForm = null;
        }






    }
}
