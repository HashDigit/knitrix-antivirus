﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Diagnostics;
using AVStrike.Classes.Antivirus;
using AVStrike.Classes;
using LogMaintainanance;
using BaseTool.Application;

namespace AVStrike.Windows
{
    //This windows hasnt been used.
    //The current process is that the database will update automatically in the background
    public partial class UpdateDatabase : Form
    {
        CreateConfigFile _UpdateDatabaseClass = new CreateConfigFile();
        public static bool isUpdateComplete = false;
        public static bool isWindowToBeClosed = false;

        Thread ThreadUpdate;

        public System.Windows.Forms.Timer TimerForUpdateMonitor;
        public System.Windows.Forms.Timer TimerMonitor;

        public UpdateDatabase()
        {
            InitializeComponent();
            isUpdateComplete = false;
            isWindowToBeClosed = false;

            isUpdateComplete = false;
            isWindowToBeClosed = false;

            TimerForUpdateMonitor = new System.Windows.Forms.Timer();
            TimerForUpdateMonitor.Tick += new EventHandler(TimerForUpdateMonitor_Tick);
            TimerForUpdateMonitor.Start();

            TimerMonitor = new System.Windows.Forms.Timer();
            TimerMonitor.Tick += new EventHandler(TimerMonitor_Tick);
            TimerMonitor.Start();

            ThreadUpdate = new Thread(new ThreadStart(UpdateStart));
            ThreadUpdate.Start();
        }

        private void TimerForUpdateMonitor_Tick(object sender, EventArgs e)
        {
            try
            {
                if (isUpdateComplete)
                {
                    isWindowToBeClosed = true;
                    //if (MessageBox.Show(this, "AVStrike Virus and Spyware definitions have been updated. Click 'Ok' to Close the window or you can close it manually.", Application.ProductName, MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                    //{

                    this.Close();
                    //}
                    //Thread.Sleep(500);

                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ScanDevice-TimerScanProcess_Tick: " + ex.Message);
            }
        }


        private void TimerMonitor_Tick(object sender, EventArgs e)
        {
            try
            {
                if (isWindowToBeClosed)
                {
                    TimerForUpdateMonitor.Stop();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ScanDevice-TimerScanProcess_Tick: " + ex.Message);
            }
        }

        public void UpdateStart()
        {
            try
            {
                CreateConfigFileFunction();
                UpdateDatabaseTime();
                UpdateCompleteDescription();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("UpdateDatabase-UpdateStart" + ex.Message);
            }

        }

        public void CreateConfigFileFunction()
        {
            try
            {
                CreateConfigFile _CreateConfigFile = new CreateConfigFile();
                string strFilesAndFolder = Application.StartupPath + @"\freshclam.conf";
                if (!File.Exists((strFilesAndFolder)))
                {
                    File.Create(strFilesAndFolder).Close();
                    //_CreateConfigFile.WriteConfigFile(strFilesAndFolder);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("UpdateDatabase-CreateConfigFileFunction" + ex.Message);
            }

        }


        public void UpdateCompleteDescription()
        {
            try
            {
                //string strArrDatabaseDate = "";
                //if (_UpdateDatabaseClass.GetUpdateStatuses().Count() > 0)
                //{
                //    strArrDatabaseDate = _UpdateDatabaseClass.GetUpdateStatuses()[0].ToString();
                //    Properties.Settings.Default["updateDefinitionsCreatedOn"] = strArrDatabaseDate.Substring(strArrDatabaseDate.IndexOf(':') + 1, 11);
                //    Properties.Settings.Default["updateDefinitionsLastUpdated"] = DateTime.Now.ToString("dd MMM yyyy");

                //    Properties.Settings.Default.Save();
                //}
                //else
                //{
                //    //Properties.Settings.Default["updateDefinitionsCreatedOn"] = 

                //}
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("UpdateDatabase-UpdateCompleteDescription" + ex.Message);
            }
        }

        public void UpdateDatabaseTime()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "freshclam.conf");
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path);
                string freshClamFile = sr.ReadToEnd();

                if (!freshClamFile.Contains("DatabaseDirectory"))
                {

                    StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default);

                    sw.WriteLine("DatabaseDirectory" + " " + AppDomain.CurrentDomain.BaseDirectory + "db");
                    sw.Close();
                }
            }



            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = "update_db";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardInput = true;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    while (!exeProcess.HasExited)
                    {
                        //Silent process. Nothing needs to be done. The loop will terminate automatically after the execution.
                    }
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception e)
            {
                ErrorLog.CreateLogFile("MainForm-ExecuteTheCommandPrompt " + e.Message);
            }

            //  _scans.KillTheRunningProcess();
            isUpdateComplete = true;
        }

    }
}
