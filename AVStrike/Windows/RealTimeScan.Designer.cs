﻿namespace AVStrike.Windows
{
    partial class RealTimeScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRealTimeScanFileNameDescriber = new System.Windows.Forms.Label();
            this.lblRealTimeScanFileNameDescription = new System.Windows.Forms.Label();
            this.lblRealTimeScanStatusDescriber = new System.Windows.Forms.Label();
            this.lblRealTimeScanStatusDescription = new System.Windows.Forms.Label();
            this.lblRealTimeScanFilePathDescriber = new System.Windows.Forms.Label();
            this.lblRealTimeScanFilePathDescription = new System.Windows.Forms.Label();
            this.lblRealTimeScanActionTakenDescriber = new System.Windows.Forms.Label();
            this.lblRealTimeScanActionTakenDescription = new System.Windows.Forms.Label();
            this.splBtnClose = new AVStrike.Controls.CustomButtons.SpecialButtons();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRealTimeScanFileNameDescriber
            // 
            this.lblRealTimeScanFileNameDescriber.AutoSize = true;
            this.lblRealTimeScanFileNameDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanFileNameDescriber.Location = new System.Drawing.Point(13, 34);
            this.lblRealTimeScanFileNameDescriber.Name = "lblRealTimeScanFileNameDescriber";
            this.lblRealTimeScanFileNameDescriber.Size = new System.Drawing.Size(78, 13);
            this.lblRealTimeScanFileNameDescriber.TabIndex = 92;
            this.lblRealTimeScanFileNameDescriber.Text = "File Name       :";
            // 
            // lblRealTimeScanFileNameDescription
            // 
            this.lblRealTimeScanFileNameDescription.AutoSize = true;
            this.lblRealTimeScanFileNameDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanFileNameDescription.Location = new System.Drawing.Point(118, 34);
            this.lblRealTimeScanFileNameDescription.Name = "lblRealTimeScanFileNameDescription";
            this.lblRealTimeScanFileNameDescription.Size = new System.Drawing.Size(54, 13);
            this.lblRealTimeScanFileNameDescription.TabIndex = 93;
            this.lblRealTimeScanFileNameDescription.Text = "File Name";
            // 
            // lblRealTimeScanStatusDescriber
            // 
            this.lblRealTimeScanStatusDescriber.AutoSize = true;
            this.lblRealTimeScanStatusDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanStatusDescriber.Location = new System.Drawing.Point(13, 84);
            this.lblRealTimeScanStatusDescriber.Name = "lblRealTimeScanStatusDescriber";
            this.lblRealTimeScanStatusDescriber.Size = new System.Drawing.Size(82, 13);
            this.lblRealTimeScanStatusDescriber.TabIndex = 87;
            this.lblRealTimeScanStatusDescriber.Text = "Status              :";
            // 
            // lblRealTimeScanStatusDescription
            // 
            this.lblRealTimeScanStatusDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanStatusDescription.Location = new System.Drawing.Point(118, 84);
            this.lblRealTimeScanStatusDescription.Name = "lblRealTimeScanStatusDescription";
            this.lblRealTimeScanStatusDescription.Size = new System.Drawing.Size(128, 20);
            this.lblRealTimeScanStatusDescription.TabIndex = 88;
            this.lblRealTimeScanStatusDescription.Text = "Not Malicous";
            // 
            // lblRealTimeScanFilePathDescriber
            // 
            this.lblRealTimeScanFilePathDescriber.AutoSize = true;
            this.lblRealTimeScanFilePathDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanFilePathDescriber.Location = new System.Drawing.Point(13, 59);
            this.lblRealTimeScanFilePathDescriber.Name = "lblRealTimeScanFilePathDescriber";
            this.lblRealTimeScanFilePathDescriber.Size = new System.Drawing.Size(78, 13);
            this.lblRealTimeScanFilePathDescriber.TabIndex = 94;
            this.lblRealTimeScanFilePathDescriber.Text = "File Path         :";
            
            // 
            // lblRealTimeScanFilePathDescription
            // 
            this.lblRealTimeScanFilePathDescription.AutoSize = true;
            this.lblRealTimeScanFilePathDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanFilePathDescription.Location = new System.Drawing.Point(118, 59);
            this.lblRealTimeScanFilePathDescription.Name = "lblRealTimeScanFilePathDescription";
            this.lblRealTimeScanFilePathDescription.Size = new System.Drawing.Size(48, 13);
            this.lblRealTimeScanFilePathDescription.TabIndex = 95;
            this.lblRealTimeScanFilePathDescription.Text = "File Path";
            // 
            // lblRealTimeScanActionTakenDescriber
            // 
            this.lblRealTimeScanActionTakenDescriber.AutoSize = true;
            this.lblRealTimeScanActionTakenDescriber.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanActionTakenDescriber.Location = new System.Drawing.Point(13, 109);
            this.lblRealTimeScanActionTakenDescriber.Name = "lblRealTimeScanActionTakenDescriber";
            this.lblRealTimeScanActionTakenDescriber.Size = new System.Drawing.Size(83, 13);
            this.lblRealTimeScanActionTakenDescriber.TabIndex = 96;
            this.lblRealTimeScanActionTakenDescriber.Text = "Action Taken   :";
            // 
            // lblRealTimeScanActionTakenDescription
            // 
            this.lblRealTimeScanActionTakenDescription.AutoSize = true;
            this.lblRealTimeScanActionTakenDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblRealTimeScanActionTakenDescription.Location = new System.Drawing.Point(118, 109);
            this.lblRealTimeScanActionTakenDescription.Name = "lblRealTimeScanActionTakenDescription";
            this.lblRealTimeScanActionTakenDescription.Size = new System.Drawing.Size(83, 13);
            this.lblRealTimeScanActionTakenDescription.TabIndex = 97;
            this.lblRealTimeScanActionTakenDescription.Text = "No action taken";
            // 
            // splBtnClose
            // 
            this.splBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.splBtnClose.BackgroundImage = global::AVStrike.Properties.Resources.shut;
            this.splBtnClose.Caption = "";
            this.splBtnClose.CaptionColor = System.Drawing.Color.Black;
            this.splBtnClose.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnClose.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.splBtnClose.Image = null;
            this.splBtnClose.ImageActive = null;
            this.splBtnClose.ImageDisabled = null;
            this.splBtnClose.ImageNormal = null;
            this.splBtnClose.ImageRollover = global::AVStrike.Properties.Resources.power_hower;
            this.splBtnClose.IsActive = false;
            this.splBtnClose.Location = new System.Drawing.Point(256, 3);
            this.splBtnClose.Name = "splBtnClose";
            this.splBtnClose.Size = new System.Drawing.Size(26, 25);
            this.splBtnClose.TabIndex = 98;
            this.splBtnClose.TabStop = false;
            this.splBtnClose.Click += new System.EventHandler(this.splBtnClose_Click);
            // 
            // RealTimeScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AVStrike.Properties.Resources.bg_registry;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(285, 140);
            this.Controls.Add(this.splBtnClose);
            this.Controls.Add(this.lblRealTimeScanActionTakenDescription);
            this.Controls.Add(this.lblRealTimeScanActionTakenDescriber);
            this.Controls.Add(this.lblRealTimeScanFilePathDescription);
            this.Controls.Add(this.lblRealTimeScanFilePathDescriber);
            this.Controls.Add(this.lblRealTimeScanFileNameDescriber);
            this.Controls.Add(this.lblRealTimeScanFileNameDescription);
            this.Controls.Add(this.lblRealTimeScanStatusDescriber);
            this.Controls.Add(this.lblRealTimeScanStatusDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RealTimeScan";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "RealTimeScan";
            this.Load += new System.EventHandler(this.RealTimeScan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splBtnClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRealTimeScanFileNameDescriber;
        private System.Windows.Forms.Label lblRealTimeScanFileNameDescription;
        private System.Windows.Forms.Label lblRealTimeScanStatusDescriber;
        private System.Windows.Forms.Label lblRealTimeScanStatusDescription;
        private System.Windows.Forms.Label lblRealTimeScanFilePathDescriber;
        private System.Windows.Forms.Label lblRealTimeScanFilePathDescription;
        private System.Windows.Forms.Label lblRealTimeScanActionTakenDescriber;
        private System.Windows.Forms.Label lblRealTimeScanActionTakenDescription;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnClose;
    }
}