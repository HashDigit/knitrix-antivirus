﻿namespace AVStrike
{
    partial class ActivationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActivationForm));
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlLicenseForm = new System.Windows.Forms.Panel();
            this.splBtnClose = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.splBtnMinimize = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnQuit = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.btnActivateTrialVersion = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.btnActivateFullVersion = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.TxtLicense4 = new AVStrike.Controls.MyTextBox();
            this.TxtLicense3 = new AVStrike.Controls.MyTextBox();
            this.TxtLicense2 = new AVStrike.Controls.MyTextBox();
            this.TxtLicense1 = new AVStrike.Controls.MyTextBox();
            this.btnGoBack = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActivateTrialVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActivateFullVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGoBack)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.Transparent;
            this.pnlTop.Controls.Add(this.pnlLicenseForm);
            this.pnlTop.Controls.Add(this.splBtnClose);
            this.pnlTop.Controls.Add(this.splBtnMinimize);
            this.pnlTop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(693, 38);
            this.pnlTop.TabIndex = 8;
            this.pnlTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTop_MouseDown);
            // 
            // pnlLicenseForm
            // 
            this.pnlLicenseForm.BackgroundImage = global::AVStrike.Properties.Resources.licenceactivation;
            this.pnlLicenseForm.Location = new System.Drawing.Point(12, 15);
            this.pnlLicenseForm.Name = "pnlLicenseForm";
            this.pnlLicenseForm.Size = new System.Drawing.Size(217, 13);
            this.pnlLicenseForm.TabIndex = 13;
            // 
            // splBtnClose
            // 
            this.splBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.splBtnClose.BackgroundImage = global::AVStrike.Properties.Resources.shut;
            this.splBtnClose.Caption = "";
            this.splBtnClose.CaptionColor = System.Drawing.Color.Black;
            this.splBtnClose.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnClose.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnClose.Image = null;
            this.splBtnClose.ImageActive = null;
            this.splBtnClose.ImageDisabled = null;
            this.splBtnClose.ImageNormal = null;
            this.splBtnClose.ImageRollover = global::AVStrike.Properties.Resources.power_hower;
            this.splBtnClose.IsActive = false;
            this.splBtnClose.Location = new System.Drawing.Point(660, 4);
            this.splBtnClose.Name = "splBtnClose";
            this.splBtnClose.Size = new System.Drawing.Size(26, 25);
            this.splBtnClose.TabIndex = 4;
            this.splBtnClose.TabStop = false;
            this.splBtnClose.Click += new System.EventHandler(this.splBtnClose_Click);
            // 
            // splBtnMinimize
            // 
            this.splBtnMinimize.BackgroundImage = global::AVStrike.Properties.Resources.minimize;
            this.splBtnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.splBtnMinimize.Caption = "";
            this.splBtnMinimize.CaptionColor = System.Drawing.Color.Black;
            this.splBtnMinimize.CaptionColorActive = System.Drawing.Color.Black;
            this.splBtnMinimize.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.splBtnMinimize.Image = null;
            this.splBtnMinimize.ImageActive = null;
            this.splBtnMinimize.ImageDisabled = null;
            this.splBtnMinimize.ImageNormal = null;
            this.splBtnMinimize.ImageRollover = global::AVStrike.Properties.Resources.minimize_hover;
            this.splBtnMinimize.IsActive = false;
            this.splBtnMinimize.Location = new System.Drawing.Point(631, 4);
            this.splBtnMinimize.Name = "splBtnMinimize";
            this.splBtnMinimize.Size = new System.Drawing.Size(28, 26);
            this.splBtnMinimize.TabIndex = 3;
            this.splBtnMinimize.TabStop = false;
            this.splBtnMinimize.Click += new System.EventHandler(this.splBtnMinimize_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::AVStrike.Properties.Resources.activation_16digitkey;
            this.panel1.Location = new System.Drawing.Point(119, 55);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(464, 11);
            this.panel1.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::AVStrike.Properties.Resources.activation_donthavekey;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Location = new System.Drawing.Point(248, 208);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(146, 11);
            this.panel2.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::AVStrike.Properties.Resources.activation_buyno_textw;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.Location = new System.Drawing.Point(400, 208);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(70, 11);
            this.panel3.TabIndex = 16;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.BackColor = System.Drawing.Color.Transparent;
            this.btnQuit.BackgroundImage = global::AVStrike.Properties.Resources.quit;
            this.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnQuit.Caption = "";
            this.btnQuit.CaptionColor = System.Drawing.Color.Black;
            this.btnQuit.CaptionColorActive = System.Drawing.Color.Black;
            this.btnQuit.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnQuit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnQuit.Image = null;
            this.btnQuit.ImageActive = null;
            this.btnQuit.ImageDisabled = null;
            this.btnQuit.ImageNormal = null;
            this.btnQuit.ImageRollover = global::AVStrike.Properties.Resources.quit_hover;
            this.btnQuit.IsActive = false;
            this.btnQuit.Location = new System.Drawing.Point(473, 158);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(48, 27);
            this.btnQuit.TabIndex = 12;
            this.btnQuit.TabStop = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // btnActivateTrialVersion
            // 
            this.btnActivateTrialVersion.BackColor = System.Drawing.Color.Transparent;
            this.btnActivateTrialVersion.BackgroundImage = global::AVStrike.Properties.Resources.activetrailversion;
            this.btnActivateTrialVersion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnActivateTrialVersion.Caption = "";
            this.btnActivateTrialVersion.CaptionColor = System.Drawing.Color.Black;
            this.btnActivateTrialVersion.CaptionColorActive = System.Drawing.Color.Black;
            this.btnActivateTrialVersion.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnActivateTrialVersion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnActivateTrialVersion.Image = null;
            this.btnActivateTrialVersion.ImageActive = null;
            this.btnActivateTrialVersion.ImageDisabled = null;
            this.btnActivateTrialVersion.ImageNormal = null;
            this.btnActivateTrialVersion.ImageRollover = global::AVStrike.Properties.Resources.activetrailversion_hover;
            this.btnActivateTrialVersion.IsActive = false;
            this.btnActivateTrialVersion.Location = new System.Drawing.Point(291, 158);
            this.btnActivateTrialVersion.Name = "btnActivateTrialVersion";
            this.btnActivateTrialVersion.Size = new System.Drawing.Size(158, 27);
            this.btnActivateTrialVersion.TabIndex = 10;
            this.btnActivateTrialVersion.TabStop = false;
            this.btnActivateTrialVersion.Click += new System.EventHandler(this.btnActivateTrialVersion_Click);
            // 
            // btnActivateFullVersion
            // 
            this.btnActivateFullVersion.BackColor = System.Drawing.Color.Transparent;
            this.btnActivateFullVersion.BackgroundImage = global::AVStrike.Properties.Resources.activefullversion;
            this.btnActivateFullVersion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnActivateFullVersion.Caption = "";
            this.btnActivateFullVersion.CaptionColor = System.Drawing.Color.Black;
            this.btnActivateFullVersion.CaptionColorActive = System.Drawing.Color.Black;
            this.btnActivateFullVersion.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnActivateFullVersion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnActivateFullVersion.Image = null;
            this.btnActivateFullVersion.ImageActive = null;
            this.btnActivateFullVersion.ImageDisabled = null;
            this.btnActivateFullVersion.ImageNormal = null;
            this.btnActivateFullVersion.ImageRollover = global::AVStrike.Properties.Resources.activefullversion_hover;
            this.btnActivateFullVersion.IsActive = false;
            this.btnActivateFullVersion.Location = new System.Drawing.Point(111, 158);
            this.btnActivateFullVersion.Name = "btnActivateFullVersion";
            this.btnActivateFullVersion.Size = new System.Drawing.Size(158, 27);
            this.btnActivateFullVersion.TabIndex = 9;
            this.btnActivateFullVersion.TabStop = false;
            this.btnActivateFullVersion.Click += new System.EventHandler(this.btnActivateFullVersion_Click);
            // 
            // TxtLicense4
            // 
            this.TxtLicense4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLicense4.Location = new System.Drawing.Point(511, 89);
            this.TxtLicense4.Multiline = true;
            this.TxtLicense4.Name = "TxtLicense4";
            this.TxtLicense4.Size = new System.Drawing.Size(80, 30);
            this.TxtLicense4.TabIndex = 3;
            this.TxtLicense4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtLicense4.Pasted += new System.EventHandler<AVStrike.Controls.ClipboardEventArgs>(this.TxtLicense_Pasted);
            this.TxtLicense4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtLicense4_KeyDown);
            this.TxtLicense4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtLicense4_KeyPress);
            // 
            // TxtLicense3
            // 
            this.TxtLicense3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLicense3.Location = new System.Drawing.Point(377, 89);
            this.TxtLicense3.Multiline = true;
            this.TxtLicense3.Name = "TxtLicense3";
            this.TxtLicense3.Size = new System.Drawing.Size(80, 30);
            this.TxtLicense3.TabIndex = 2;
            this.TxtLicense3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtLicense3.Pasted += new System.EventHandler<AVStrike.Controls.ClipboardEventArgs>(this.TxtLicense_Pasted);
            this.TxtLicense3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtLicense3_KeyPress);
            // 
            // TxtLicense2
            // 
            this.TxtLicense2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLicense2.Location = new System.Drawing.Point(244, 89);
            this.TxtLicense2.Multiline = true;
            this.TxtLicense2.Name = "TxtLicense2";
            this.TxtLicense2.Size = new System.Drawing.Size(80, 30);
            this.TxtLicense2.TabIndex = 1;
            this.TxtLicense2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtLicense2.Pasted += new System.EventHandler<AVStrike.Controls.ClipboardEventArgs>(this.TxtLicense_Pasted);
            this.TxtLicense2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtLicense2_KeyPress);
            // 
            // TxtLicense1
            // 
            this.TxtLicense1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLicense1.Location = new System.Drawing.Point(111, 89);
            this.TxtLicense1.Multiline = true;
            this.TxtLicense1.Name = "TxtLicense1";
            this.TxtLicense1.Size = new System.Drawing.Size(80, 30);
            this.TxtLicense1.TabIndex = 0;
            this.TxtLicense1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TxtLicense1.Pasted += new System.EventHandler<AVStrike.Controls.ClipboardEventArgs>(this.TxtLicense_Pasted);
            this.TxtLicense1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtLicense1_KeyPress);
            // 
            // btnGoBack
            // 
            this.btnGoBack.BackColor = System.Drawing.Color.Transparent;
            this.btnGoBack.BackgroundImage = global::AVStrike.Properties.Resources.back;
            this.btnGoBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnGoBack.Caption = "";
            this.btnGoBack.CaptionColor = System.Drawing.Color.Black;
            this.btnGoBack.CaptionColorActive = System.Drawing.Color.Black;
            this.btnGoBack.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnGoBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGoBack.Enabled = false;
            this.btnGoBack.Image = null;
            this.btnGoBack.ImageActive = null;
            this.btnGoBack.ImageDisabled = global::AVStrike.Properties.Resources.back_disable_44x43_;
            this.btnGoBack.ImageNormal = null;
            this.btnGoBack.ImageRollover = global::AVStrike.Properties.Resources.back_hover;
            this.btnGoBack.IsActive = false;
            this.btnGoBack.Location = new System.Drawing.Point(12, 50);
            this.btnGoBack.Name = "btnGoBack";
            this.btnGoBack.Size = new System.Drawing.Size(44, 43);
            this.btnGoBack.TabIndex = 17;
            this.btnGoBack.TabStop = false;
            this.btnGoBack.Visible = false;
            this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
            // 
            // ActivationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.ClientSize = new System.Drawing.Size(693, 231);
            this.Controls.Add(this.btnGoBack);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnActivateTrialVersion);
            this.Controls.Add(this.btnActivateFullVersion);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.TxtLicense4);
            this.Controls.Add(this.TxtLicense3);
            this.Controls.Add(this.TxtLicense2);
            this.Controls.Add(this.TxtLicense1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ActivationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ActivationForm";
            this.Load += new System.EventHandler(this.ActivationForm_Load);
            this.pnlTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splBtnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splBtnMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActivateTrialVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActivateFullVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGoBack)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AVStrike.Controls.MyTextBox TxtLicense1;
        private AVStrike.Controls.MyTextBox TxtLicense2;
        private AVStrike.Controls.MyTextBox TxtLicense3;
        private AVStrike.Controls.MyTextBox TxtLicense4;
        private System.Windows.Forms.Panel pnlTop;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnClose;
        private AVStrike.Controls.CustomButtons.SpecialButtons splBtnMinimize;
        private AVStrike.Controls.CustomButtons.SpecialButtons btnActivateFullVersion;
        private AVStrike.Controls.CustomButtons.SpecialButtons btnActivateTrialVersion;
        private AVStrike.Controls.CustomButtons.SpecialButtons btnQuit;
        private System.Windows.Forms.Panel pnlLicenseForm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private AVStrike.Controls.CustomButtons.SpecialButtons btnGoBack;

    }
}