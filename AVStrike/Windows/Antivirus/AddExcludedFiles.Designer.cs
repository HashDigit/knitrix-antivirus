﻿namespace AVStrike.Windows.Antivirus
{
    partial class AddExcludedFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddExcludedFiles));
            this.txtExcludeFileList = new System.Windows.Forms.TextBox();
            this.lblFileType = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnExcludeFileCancel = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.btnExcludeFileApply = new AVStrike.Controls.CustomButtons.SpecialButtons();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFileCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFileApply)).BeginInit();
            this.SuspendLayout();
            // 
            // txtExcludeFileList
            // 
            this.txtExcludeFileList.Location = new System.Drawing.Point(71, 110);
            this.txtExcludeFileList.Name = "txtExcludeFileList";
            this.txtExcludeFileList.Size = new System.Drawing.Size(200, 20);
            this.txtExcludeFileList.TabIndex = 109;
            // 
            // lblFileType
            // 
            this.lblFileType.AutoSize = true;
            this.lblFileType.BackColor = System.Drawing.Color.Transparent;
            this.lblFileType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblFileType.ForeColor = System.Drawing.Color.White;
            this.lblFileType.Location = new System.Drawing.Point(12, 113);
            this.lblFileType.Name = "lblFileType";
            this.lblFileType.Size = new System.Drawing.Size(53, 13);
            this.lblFileType.TabIndex = 108;
            this.lblFileType.Text = "File Type:";
            // 
            // lblDescription
            // 
            this.lblDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.ForeColor = System.Drawing.Color.White;
            this.lblDescription.Location = new System.Drawing.Point(12, 37);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(259, 65);
            this.lblDescription.TabIndex = 107;
            this.lblDescription.Text = resources.GetString("lblDescription.Text");
            // 
            // btnExcludeFileCancel
            // 
            this.btnExcludeFileCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnExcludeFileCancel.BackgroundImage = global::AVStrike.Properties.Resources.cancel_main;
            this.btnExcludeFileCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExcludeFileCancel.Caption = "";
            this.btnExcludeFileCancel.CaptionColor = System.Drawing.Color.Black;
            this.btnExcludeFileCancel.CaptionColorActive = System.Drawing.Color.Black;
            this.btnExcludeFileCancel.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnExcludeFileCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcludeFileCancel.Image = null;
            this.btnExcludeFileCancel.ImageActive = null;
            this.btnExcludeFileCancel.ImageDisabled = null;
            this.btnExcludeFileCancel.ImageNormal = null;
            this.btnExcludeFileCancel.ImageRollover = global::AVStrike.Properties.Resources.cancel_hover;
            this.btnExcludeFileCancel.IsActive = false;
            this.btnExcludeFileCancel.Location = new System.Drawing.Point(168, 146);
            this.btnExcludeFileCancel.Name = "btnExcludeFileCancel";
            this.btnExcludeFileCancel.Size = new System.Drawing.Size(70, 27);
            this.btnExcludeFileCancel.TabIndex = 111;
            this.btnExcludeFileCancel.TabStop = false;
            this.btnExcludeFileCancel.Click += new System.EventHandler(this.btnExcludeFileCancel_Click);
            // 
            // btnExcludeFileApply
            // 
            this.btnExcludeFileApply.BackColor = System.Drawing.Color.Transparent;
            this.btnExcludeFileApply.BackgroundImage = global::AVStrike.Properties.Resources.apply;
            this.btnExcludeFileApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExcludeFileApply.Caption = "";
            this.btnExcludeFileApply.CaptionColor = System.Drawing.Color.Black;
            this.btnExcludeFileApply.CaptionColorActive = System.Drawing.Color.Black;
            this.btnExcludeFileApply.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnExcludeFileApply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcludeFileApply.Image = null;
            this.btnExcludeFileApply.ImageActive = null;
            this.btnExcludeFileApply.ImageDisabled = null;
            this.btnExcludeFileApply.ImageNormal = null;
            this.btnExcludeFileApply.ImageRollover = global::AVStrike.Properties.Resources.apply_hover;
            this.btnExcludeFileApply.IsActive = false;
            this.btnExcludeFileApply.Location = new System.Drawing.Point(62, 146);
            this.btnExcludeFileApply.Name = "btnExcludeFileApply";
            this.btnExcludeFileApply.Size = new System.Drawing.Size(75, 27);
            this.btnExcludeFileApply.TabIndex = 110;
            this.btnExcludeFileApply.TabStop = false;
            this.btnExcludeFileApply.Click += new System.EventHandler(this.btnExcludeFileApply_Click);
            // 
            // AddExcludedFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.ClientSize = new System.Drawing.Size(298, 196);
            this.Controls.Add(this.btnExcludeFileCancel);
            this.Controls.Add(this.btnExcludeFileApply);
            this.Controls.Add(this.txtExcludeFileList);
            this.Controls.Add(this.lblFileType);
            this.Controls.Add(this.lblDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddExcludedFiles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddExcludedFiles";
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFileCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFileApply)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AVStrike.Controls.CustomButtons.SpecialButtons btnExcludeFileCancel;
        private AVStrike.Controls.CustomButtons.SpecialButtons btnExcludeFileApply;
        private System.Windows.Forms.TextBox txtExcludeFileList;
        private System.Windows.Forms.Label lblFileType;
        private System.Windows.Forms.Label lblDescription;

    }
}