﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AVStrike.Classes.WindowsOperation;
using LogMaintainanance;

namespace AVStrike.Windows.Antivirus
{
    public partial class AddExcludedFiles : Form
    {
        MainBaseForm _MainBaseForm = new MainBaseForm();
        public AddExcludedFiles()
        {
            InitializeComponent();
        }

        //Events created for the file type to be added to the excluded list
        public event AddFileTypeEventHandler AddFileType;

        public delegate void AddFileTypeEventHandler(object sender, AddFileTypeEventArgs e);

        public class AddFileTypeEventArgs : EventArgs
        {
            public string fileType
            {
                get;
                set;
            }
        }

        //Add the provided file to the excluded file list
        private void btnExcludeFileApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.txtExcludeFileList.Text))
                {
                    MessageBox.Show(this, "Please enter a file type", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (AddFileType != null)
                {
                    AddFileTypeEventArgs eventArgs = new AddFileTypeEventArgs();
                    eventArgs.fileType = this.txtExcludeFileList.Text;
                    AddFileType(this, eventArgs);
                }
                //_MainBaseForm.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddExcludedFiles-btnExcludeFileApply_Click" + ex.Message);
            }
        }

        //Cancel and close the window
        private void btnExcludeFileCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //_MainBaseForm.Show();

                //DialogResult = DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddExcludedFiles-btnExcludeFileCancel_Click" + ex.Message);
            }
        }






    }
}
