﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AVStrike.Classes.WindowsOperation;
using LogMaintainanance;

namespace AVStrike.Windows.Antivirus
{
    public partial class AddExcludedFolders : Form
    {
        MainBaseForm _MainBaseForm = new MainBaseForm();
        public AddExcludedFolders()
        {
            InitializeComponent();
        }

        //Event created to add the provided folders to the excluded folders list
        public event AddExcludeFolderEventHandler AddExcludeFolderDelegate;
        public class AddExcludeFolderEventArgs : EventArgs
        {
            public string folderPath
            {
                get;
                set;
            }
        }
        public delegate void AddExcludeFolderEventHandler(object sender, AddExcludeFolderEventArgs e);



        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog browserDlg = new FolderBrowserDialog();
                browserDlg.ShowDialog(this);
                this.txtExcludeFolderList.Text = browserDlg.SelectedPath;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddExcludedFolders-btnBrowseFolder_Click" + ex.Message);
            }
        }

        //Add the provided folder path to the excluded folder list
        private void btnExcludeFolderApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.txtExcludeFolderList.Text))
                {
                    MessageBox.Show(this, "Please enter a folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (AddExcludeFolderDelegate != null)
                {
                    AddExcludeFolderEventArgs eventArgs = new AddExcludeFolderEventArgs();
                    eventArgs.folderPath = this.txtExcludeFolderList.Text;
                    AddExcludeFolderDelegate(this, eventArgs);
                }

                this.Close();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddExcludedFolders-btnExcludeFolderApply_Click" + ex.Message);
            }
        }


        //cancel the operation and close the window
        private void btnExcludeFolderCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //_MainBaseForm.Focus();
                //_MainBaseForm.Show();
                //_MainBaseForm.WindowState = FormWindowState.Normal;
                //_MainBaseForm.Show(this);

                //DialogResult = DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddExcludedFolders-btnExcludeFolderCancel_Click" + ex.Message);
            }
        }

        private void AddExcludedFolders_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                _MainBaseForm.Focus();
                _MainBaseForm.WindowState = FormWindowState.Normal;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("AddExcludedFolders-AddExcludedFolders_FormClosing" + ex.Message);
            }
        }
    }
}
