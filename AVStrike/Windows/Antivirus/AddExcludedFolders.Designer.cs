﻿namespace AVStrike.Windows.Antivirus
{
    partial class AddExcludedFolders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddExcludedFolders));
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.txtExcludeFolderList = new System.Windows.Forms.TextBox();
            this.lblFileType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExcludeFolderCancel = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.btnExcludeFolderApply = new AVStrike.Controls.CustomButtons.SpecialButtons();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFolderCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFolderApply)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(229, 115);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(30, 20);
            this.btnBrowseFolder.TabIndex = 115;
            this.btnBrowseFolder.Text = "...";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // txtExcludeFolderList
            // 
            this.txtExcludeFolderList.Location = new System.Drawing.Point(74, 115);
            this.txtExcludeFolderList.Name = "txtExcludeFolderList";
            this.txtExcludeFolderList.Size = new System.Drawing.Size(149, 20);
            this.txtExcludeFolderList.TabIndex = 112;
            // 
            // lblFileType
            // 
            this.lblFileType.AutoSize = true;
            this.lblFileType.BackColor = System.Drawing.Color.Transparent;
            this.lblFileType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.lblFileType.ForeColor = System.Drawing.Color.White;
            this.lblFileType.Location = new System.Drawing.Point(18, 118);
            this.lblFileType.Name = "lblFileType";
            this.lblFileType.Size = new System.Drawing.Size(53, 13);
            this.lblFileType.TabIndex = 111;
            this.lblFileType.Text = "File Type:";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(15, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(263, 65);
            this.label1.TabIndex = 110;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // btnExcludeFolderCancel
            // 
            this.btnExcludeFolderCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnExcludeFolderCancel.BackgroundImage = global::AVStrike.Properties.Resources.cancel_main;
            this.btnExcludeFolderCancel.Caption = "";
            this.btnExcludeFolderCancel.CaptionColor = System.Drawing.Color.Black;
            this.btnExcludeFolderCancel.CaptionColorActive = System.Drawing.Color.Black;
            this.btnExcludeFolderCancel.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnExcludeFolderCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcludeFolderCancel.Image = null;
            this.btnExcludeFolderCancel.ImageActive = null;
            this.btnExcludeFolderCancel.ImageDisabled = null;
            this.btnExcludeFolderCancel.ImageNormal = null;
            this.btnExcludeFolderCancel.ImageRollover = global::AVStrike.Properties.Resources.cancel_hover;
            this.btnExcludeFolderCancel.IsActive = false;
            this.btnExcludeFolderCancel.Location = new System.Drawing.Point(163, 150);
            this.btnExcludeFolderCancel.Name = "btnExcludeFolderCancel";
            this.btnExcludeFolderCancel.Size = new System.Drawing.Size(70, 27);
            this.btnExcludeFolderCancel.TabIndex = 114;
            this.btnExcludeFolderCancel.TabStop = false;
            this.btnExcludeFolderCancel.Click += new System.EventHandler(this.btnExcludeFolderCancel_Click);
            // 
            // btnExcludeFolderApply
            // 
            this.btnExcludeFolderApply.BackColor = System.Drawing.Color.Transparent;
            this.btnExcludeFolderApply.BackgroundImage = global::AVStrike.Properties.Resources.apply;
            this.btnExcludeFolderApply.Caption = "";
            this.btnExcludeFolderApply.CaptionColor = System.Drawing.Color.Black;
            this.btnExcludeFolderApply.CaptionColorActive = System.Drawing.Color.Black;
            this.btnExcludeFolderApply.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnExcludeFolderApply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExcludeFolderApply.Image = null;
            this.btnExcludeFolderApply.ImageActive = null;
            this.btnExcludeFolderApply.ImageDisabled = null;
            this.btnExcludeFolderApply.ImageNormal = null;
            this.btnExcludeFolderApply.ImageRollover = global::AVStrike.Properties.Resources.apply_hover;
            this.btnExcludeFolderApply.IsActive = false;
            this.btnExcludeFolderApply.Location = new System.Drawing.Point(57, 150);
            this.btnExcludeFolderApply.Name = "btnExcludeFolderApply";
            this.btnExcludeFolderApply.Size = new System.Drawing.Size(75, 27);
            this.btnExcludeFolderApply.TabIndex = 113;
            this.btnExcludeFolderApply.TabStop = false;
            this.btnExcludeFolderApply.Click += new System.EventHandler(this.btnExcludeFolderApply_Click);
            // 
            // AddExcludedFolders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AVStrike.Properties.Resources.img1;
            this.ClientSize = new System.Drawing.Size(298, 196);
            this.Controls.Add(this.btnBrowseFolder);
            this.Controls.Add(this.btnExcludeFolderCancel);
            this.Controls.Add(this.btnExcludeFolderApply);
            this.Controls.Add(this.txtExcludeFolderList);
            this.Controls.Add(this.lblFileType);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddExcludedFolders";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddExcludedFolders";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddExcludedFolders_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFolderCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcludeFolderApply)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowseFolder;
        private AVStrike.Controls.CustomButtons.SpecialButtons btnExcludeFolderCancel;
        private AVStrike.Controls.CustomButtons.SpecialButtons btnExcludeFolderApply;
        private System.Windows.Forms.TextBox txtExcludeFolderList;
        private System.Windows.Forms.Label lblFileType;
        private System.Windows.Forms.Label label1;


    }
}