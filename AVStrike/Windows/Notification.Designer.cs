﻿namespace AVStrike.Windows
{
    partial class Notification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Notification));
            this.lblNotificationAlertText1 = new System.Windows.Forms.Label();
            this.lblNotificationAlertText2 = new System.Windows.Forms.Label();
            this.lblNotificationAlertText3 = new System.Windows.Forms.Label();
            this.lblNotificationAlertText5 = new System.Windows.Forms.Label();
            this.btnNotificationOk = new AVStrike.Controls.CustomButtons.SpecialButtons();
            this.lblNotificationAlertText4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnNotificationOk)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNotificationAlertText1
            // 
            this.lblNotificationAlertText1.AutoSize = true;
            this.lblNotificationAlertText1.BackColor = System.Drawing.Color.Transparent;
            this.lblNotificationAlertText1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotificationAlertText1.ForeColor = System.Drawing.Color.Red;
            this.lblNotificationAlertText1.Location = new System.Drawing.Point(77, 57);
            this.lblNotificationAlertText1.Name = "lblNotificationAlertText1";
            this.lblNotificationAlertText1.Size = new System.Drawing.Size(166, 25);
            this.lblNotificationAlertText1.TabIndex = 1;
            this.lblNotificationAlertText1.Text = "License Expired";
            // 
            // lblNotificationAlertText2
            // 
            this.lblNotificationAlertText2.AutoSize = true;
            this.lblNotificationAlertText2.BackColor = System.Drawing.Color.Transparent;
            this.lblNotificationAlertText2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotificationAlertText2.ForeColor = System.Drawing.Color.Black;
            this.lblNotificationAlertText2.Location = new System.Drawing.Point(22, 113);
            this.lblNotificationAlertText2.Name = "lblNotificationAlertText2";
            this.lblNotificationAlertText2.Size = new System.Drawing.Size(197, 13);
            this.lblNotificationAlertText2.TabIndex = 2;
            this.lblNotificationAlertText2.Tag = "The {0} Version of AVStrike has expired.";
            this.lblNotificationAlertText2.Text = "The {0} Version of AVStrike has expired.";
            // 
            // lblNotificationAlertText3
            // 
            this.lblNotificationAlertText3.AutoSize = true;
            this.lblNotificationAlertText3.BackColor = System.Drawing.Color.Transparent;
            this.lblNotificationAlertText3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotificationAlertText3.ForeColor = System.Drawing.Color.Black;
            this.lblNotificationAlertText3.Location = new System.Drawing.Point(22, 133);
            this.lblNotificationAlertText3.Name = "lblNotificationAlertText3";
            this.lblNotificationAlertText3.Size = new System.Drawing.Size(169, 13);
            this.lblNotificationAlertText3.TabIndex = 4;
            this.lblNotificationAlertText3.Text = "AVStrike is not protecting your PC.";
            // 
            // lblNotificationAlertText5
            // 
            this.lblNotificationAlertText5.AutoSize = true;
            this.lblNotificationAlertText5.BackColor = System.Drawing.Color.Transparent;
            this.lblNotificationAlertText5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotificationAlertText5.ForeColor = System.Drawing.Color.Black;
            this.lblNotificationAlertText5.Location = new System.Drawing.Point(22, 214);
            this.lblNotificationAlertText5.Name = "lblNotificationAlertText5";
            this.lblNotificationAlertText5.Size = new System.Drawing.Size(159, 13);
            this.lblNotificationAlertText5.TabIndex = 5;
            this.lblNotificationAlertText5.Text = "Click \"OK\" to close this window.";
            // 
            // btnNotificationOk
            // 
            this.btnNotificationOk.BackColor = System.Drawing.Color.Transparent;
            this.btnNotificationOk.BackgroundImage = global::AVStrike.Properties.Resources.notification_ok_normal;
            this.btnNotificationOk.Caption = "";
            this.btnNotificationOk.CaptionColor = System.Drawing.Color.Black;
            this.btnNotificationOk.CaptionColorActive = System.Drawing.Color.Black;
            this.btnNotificationOk.CaptionFont = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnNotificationOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNotificationOk.Image = null;
            this.btnNotificationOk.ImageActive = null;
            this.btnNotificationOk.ImageDisabled = null;
            this.btnNotificationOk.ImageNormal = null;
            this.btnNotificationOk.ImageRollover = global::AVStrike.Properties.Resources.notification_ok_hover;
            this.btnNotificationOk.IsActive = false;
            this.btnNotificationOk.Location = new System.Drawing.Point(70, 279);
            this.btnNotificationOk.Name = "btnNotificationOk";
            this.btnNotificationOk.Size = new System.Drawing.Size(103, 34);
            this.btnNotificationOk.TabIndex = 0;
            this.btnNotificationOk.TabStop = false;
            this.btnNotificationOk.Click += new System.EventHandler(this.btnNotificationOk_Click);
            // 
            // lblNotificationAlertText4
            // 
            this.lblNotificationAlertText4.AutoSize = true;
            this.lblNotificationAlertText4.BackColor = System.Drawing.Color.Transparent;
            this.lblNotificationAlertText4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblNotificationAlertText4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotificationAlertText4.ForeColor = System.Drawing.Color.Blue;
            this.lblNotificationAlertText4.Location = new System.Drawing.Point(22, 159);
            this.lblNotificationAlertText4.Name = "lblNotificationAlertText4";
            this.lblNotificationAlertText4.Size = new System.Drawing.Size(61, 16);
            this.lblNotificationAlertText4.TabIndex = 6;
            this.lblNotificationAlertText4.Text = "Buy Now";
            this.lblNotificationAlertText4.Click += new System.EventHandler(this.lblNotificationAlertText4_Click);
            // 
            // Notification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AVStrike.Properties.Resources.notification_bg;
            this.ClientSize = new System.Drawing.Size(255, 320);
            this.Controls.Add(this.lblNotificationAlertText4);
            this.Controls.Add(this.lblNotificationAlertText5);
            this.Controls.Add(this.lblNotificationAlertText3);
            this.Controls.Add(this.lblNotificationAlertText2);
            this.Controls.Add(this.lblNotificationAlertText1);
            this.Controls.Add(this.btnNotificationOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Notification";
            this.Text = "Notification";
            this.Load += new System.EventHandler(this.Notification_Load);
            this.Click += new System.EventHandler(this.Notification_Click);
            ((System.ComponentModel.ISupportInitialize)(this.btnNotificationOk)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.CustomButtons.SpecialButtons btnNotificationOk;
        private System.Windows.Forms.Label lblNotificationAlertText1;
        private System.Windows.Forms.Label lblNotificationAlertText2;
        private System.Windows.Forms.Label lblNotificationAlertText3;
        private System.Windows.Forms.Label lblNotificationAlertText5;
        private System.Windows.Forms.Label lblNotificationAlertText4;
    }
}