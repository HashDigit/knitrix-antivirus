﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AVStrike.Classes.WindowsOperation;
using System.Diagnostics;
using AVStrike.Controls.CustomButtons;
using Business;
using Data;
using Common;
using Core.Business;
using Common_Tools.TreeViewAdv.Tree;
using Data.Registry;
using System.Threading;
using System.Security.Permissions;
using Common.Xml;
using Microsoft.Win32;
using System.IO;
using System.Collections;
using AVStrike.Classes.Antivirus;
using AVStrike.Classes;
using AVStrike.Classes.TaskScheduler;
using System.Runtime.InteropServices;
using System.Data.SQLite;
using System.Collections.Specialized;
using AVStrike.Windows.Antivirus;
using LogMaintainanance;
using SQLiteConnectionLibrary;
using BaseTool;
using SharpClam;
using ReSharpedClam;
using BaseTool.Util;


namespace AVStrike
{

    /// <summary>
    /// partial class for the Main form
    /// </summary>
    public partial class MainBaseForm : Form
    {
        #region Declarations
        #region Class Declarations

        ApplicationProperties _ApplicationProperties = new ApplicationProperties();
        ListViewScrollOperation _ListViewScrollOperation = new ListViewScrollOperation();

        MoveFileAndDeleteFlag _MoveFileAndDeleteFlag = new MoveFileAndDeleteFlag();
        AntivirusSettings _AntivirusSettings = new AntivirusSettings();
        ActivationForm _ActivationForm = new ActivationForm();
        CommonUtil cUtil = new CommonUtil();
        private List<string> ignoreFileList = new List<string>();


        CalculateRemainingDays calculateRemainingDays = new CalculateRemainingDays();
        SQLiteConnectionLibrary.SQLiteProcess _SQLiteProcess = new SQLiteConnectionLibrary.SQLiteProcess();


        ManagedClam clam = new ManagedClam();
        private IntPtr cl_enginePtr = IntPtr.Zero;

        ClamEngine cl_engine = new ClamEngine();

        ArrayList EngineValues = new ArrayList();

        #endregion Class Declaration

        /// <summary>
        /// Get the database path
        /// main.cvd and daily.cvd
        /// </summary>
        public string database_path = Environment.CurrentDirectory + "\\database";

        /// <summary>
        /// List of file extensions
        /// </summary>
        List<string> fileExtensionFilter = new List<string>();

        /// <summary>
        /// List of Directories to exclude
        /// </summary>
        List<string> excludedFolders = new List<string>();

        /// <summary>
        /// Maximum size of a file that can be scanned
        /// </summary>
        int sizeFilter = 0;

        #region Scan Process Decclarations and Initialization

        #region Strings

        /// <summary>
        /// Get the name of the current file
        /// </summary>
        public static string CurrentFile { get; set; }

        public static string FileStatusDescription { get; set; }

        public static string CurrentFileToMoveAndDelete { get; set; }

        #endregion

        #region Long and Float
        long filesizesProcessedInBytes = 0;
        #endregion

        #region Int
        public static int intFileListCount = 0;
        public static int VirusFileCount = 0;
        private int VirusScanFileCount = 0;
        public static int AntivirusDateTimeCounter = 1;
        public static int intReturnStatus = 0;
        public static int GetNumberOfFilesToScan = 0;
        public static int CountMalware = 0;
        public static int intScanProcessDateTimeCounter = 0;
        public static int progressPercentageCount = 0;

        #endregion

        #region Bool
        public static bool isScanCompleted = false;
        public static bool isScanEngineExited = true;
        public static bool blMinimizeToWindow = true;
        #endregion

        /// <summary>
        /// Get the elapsed date time when the scan is running
        /// </summary>
        public static DateTime ElapsedDateTime { get; set; }

        #region Timers and Threaads
        /// <summary>
        /// Timer mainly used to get the time elapsed during the scan process
        /// </summary>
        public System.Windows.Forms.Timer AntivirusTimerForTimeElapsed;

        /// <summary>
        /// Timer used to update the various details while scan is running
        /// 1. Update the current file
        /// 2. Update the malicious file
        /// </summary>
        public System.Windows.Forms.Timer AntivirusTimerForScanProcess;

        /// <summary>
        /// Main timer that runs continuously. For periodic tasks
        /// </summary>
        public System.Windows.Forms.Timer MainFormTimerHandling;
        #endregion

        #endregion

        #endregion

        #region Constructor Logic
        public MainBaseForm()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
        }
        #endregion

        #region Eliminate Flickering

        /// <summary>
        /// Application window flickers when too many screens are clicked simultaneously
        /// Using this code to eliminate the flickering
        /// The screen will stay still till the whole form loads
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                if (CommonUtil.isWindowsXP())
                {
                    cp.ExStyle |= 0x00080000;
                }
                else
                {
                    cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                }
                return cp;
            }
        }
        #endregion

        #region Form Events
        /// <summary>
        /// Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainBaseForm_Load(object sender, EventArgs e)
        {
            //Load the diskcleaner and antivirus settings
            LoadAntivirusSettings();

            //Display the Image which needs to be shown in the Home Page
            DisplayScanImage();

            //Shows the Validity of the Items to be show in the Quarantine Field
            DeleteMoreThanDays();

            //Load the splash screen
            //AntivirusSettings.ShowSplashScreeen();

            //Deals with the application activation process
            ApplicationActivation();

            if (Properties.Settings.Default.AntRealTimeProtection)
            {
                lblHomeMainRealTimeProtectionDescription.Text = "ON";
                lblHomeMainRealTimeProtectionDescription.ForeColor = System.Drawing.Color.Green;

            }
            else
            {
                lblHomeMainRealTimeProtectionDescription.Text = "OFF";
                lblHomeMainRealTimeProtectionDescription.ForeColor = System.Drawing.Color.Red;
            }

            lblHomeMainDefinitionAutoUpdateDescription.Text = "ON";
            lblHomeMainDefinitionAutoUpdateDescription.ForeColor = System.Drawing.Color.Green;

            lblHomeMainLicenseExpiryDateDescription.Text = Properties.Settings.Default.ActExpiryDate.ToString("dd-MM-yyyy");
            lblHomeMainLastScannedOnDescription.Text = Properties.Settings.Default.AntLastScannedDate.ToString();

            btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.home;
            btnSubHomeQuickScan.BackgroundImage = global::AVStrike.Properties.Resources.quick_scan;

            LoadTrayMenu();

            //By default Quick Scan should be enabled
            Properties.Settings.Default.ScanningFlag = 1;
            Properties.Settings.Default.Save();

            MainFormTimerHandling = new System.Windows.Forms.Timer();
            MainFormTimerHandling.Tick += new EventHandler(MainFormTimerHandling_Tick);
            MainFormTimerHandling.Interval = 1000;
            MainFormTimerHandling.Start();

            //splBtnScanNow_Click(sender, e);
        }

        #region Main Form Timer Handling
        Int32 secondsCounter = 0;

        private void MainFormTimerHandling_Tick(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ScanTaskSchedule)
            {
                Properties.Settings.Default.ScanTaskSchedule = false;
                Properties.Settings.Default.Save();
                splBtnScanNow_Click(sender, e);
            }

            if (secondsCounter % 600 == 0)
            {
                //MemoryManagement.FlushMemory();
            }

            if (isScanButtonVisible)
                splBtnStopScanning.Visible = true;

            if (isScanCompleted)
            {
                foreach (IntPtr engine in EngineValues)
                {
                    clam.FreeMemory(engine);
                }
            }

            #region Every 10 seconds
            if (secondsCounter % 100 == 0)
            {
                DateTime dtLastScanDate = Properties.Settings.Default.AntLastScannedDate;
                //Added the default Date Time value to check
                DateTime dtComparison = new DateTime(2014, 06, 15, 12, 12, 12);
                if (DateTime.Compare(dtLastScanDate, dtComparison) > 0)
                {
                    double ts = (DateTime.Now - dtLastScanDate).TotalSeconds;
                    lblHomeMainLastScannedOnDescription.Text = BaseTool.BytesProcessing.ConvertDate(DateTime.Now.AddSeconds(-ts));
                }
                else
                {
                    lblHomeMainLastScannedOnDescription.Text = "Yet to be Scanned";
                }
            }
            #endregion

            #region Every Thirty Minutes
            if (secondsCounter % 1800 == 0)
            {
                DeleteMoreThanDays();
                DisplayScanImage();
                lblHomeMainLicenseExpiryDateDescription.Text = Properties.Settings.Default.ActExpiryDate.ToString("dd-MM-yyyy");
            }
            #endregion


            #region Every 6 hours
            if (secondsCounter % 21600 == 0)
            {
                ValidityExpired();
            }
            #endregion

            #region Every 12 hours
            if (secondsCounter % 43200 == 0)
            {
                if (secondsCounter > 0)
                {
                    ApplicationActivation();
                    secondsCounter = 1;
                }
            }




            #endregion

            //increment the seconds
            secondsCounter++;
        }
        #endregion


        /// <summary>
        /// Checking the license of the application every time inside the timer and during the load event of 
        /// the application
        /// </summary>
        private void ApplicationActivation()
        {
            int days = 0;
            string StartDate = string.Empty;
            string EndDate = string.Empty;
            ArrayList DaysDetails = new ArrayList();
            int versionPoint = Properties.Settings.Default.ActVersionPoint;
            string MacID = AuthenticationAndSecurity.GetMacAddress();
            string DomainURL = string.Format(ExternalUrls.DomainUrl, ExternalUrls.Domain);
            string TrialDays = string.Format(ExternalUrls.TrialDaysReamining, ExternalUrls.Domain, MacID);
            string FullDays = string.Format(ExternalUrls.FullDaysReamining, ExternalUrls.Domain, MacID, Properties.Settings.Default.ActProductKey);
            bool isExceptionThrown = true;

            #region Validity Check
            if (AuthenticationAndSecurity.CheckInternet())
            {
                #region If Condition
                switch (versionPoint)
                {
                    case 1:

                        DaysDetails = AuthenticationAndSecurity.GetRemainingDaysDetails(TrialDays, DomainURL, out isExceptionThrown);

                        days = Convert.ToInt32(DaysDetails[0].ToString());
                        StartDate = DaysDetails[1].ToString();
                        EndDate = DaysDetails[2].ToString();

                        Properties.Settings.Default.ActDaysRemaining = days;
                        Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(EndDate);
                        _SQLiteProcess.UpdateDatabase(days, MacID, StartDate, EndDate, true);
                        Properties.Settings.Default.Save();
                        break;
                    case 2:
                        _SQLiteProcess.InserIntoFull();
                        DaysDetails = AuthenticationAndSecurity.GetRemainingDaysDetails(FullDays, DomainURL, out isExceptionThrown);

                        days = Convert.ToInt32(DaysDetails[0].ToString());
                        StartDate = DaysDetails[1].ToString();
                        EndDate = DaysDetails[2].ToString();

                        Properties.Settings.Default.ActDaysRemaining = days;
                        Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(EndDate);
                        _SQLiteProcess.UpdateDatabase(days, MacID, StartDate, EndDate, true);
                        Properties.Settings.Default.Save();
                        break;
                    default:
                        break;

                }
                #endregion
            }
            else
            {
                #region Else Condition
                switch (versionPoint)
                {
                    case 1:
                        DaysDetails = calculateRemainingDays.ReturnTrialVersionReaminingDaysList();
                        days = Convert.ToInt32(DaysDetails[0]);
                        Properties.Settings.Default.ActDaysRemaining = days;
                        Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(DaysDetails[2].ToString());
                        Properties.Settings.Default.Save();
                        break;
                    case 2:
                        DaysDetails = calculateRemainingDays.ReturnFullVersionReaminingDaysList();
                        days = Convert.ToInt32(DaysDetails[0]);
                        Properties.Settings.Default.ActDaysRemaining = days;
                        Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(DaysDetails[2].ToString());
                        Properties.Settings.Default.Save();
                        break;
                    default:
                        break;

                }
                #endregion
            }
            #endregion

            #region Enable and Disable Buttons
            if (Properties.Settings.Default.ActVersionPoint == 1)
            {
                lblHomeMainLicenseTypeDescription.Text = "TRIAL";
                lblHomeMessageDescription1.Text = string.Format(lblHomeMessageDescription1.Tag.ToString(), "TRIAL");
                lblHomeMessageDescription2.Text = string.Format(lblHomeMessageDescription2.Tag.ToString(), days.ToString());

            }
            else
            {
                lblHomeMainLicenseTypeDescription.Text = "FULL";
                lblHomeMessageDescription1.Text = string.Format(lblHomeMessageDescription1.Tag.ToString(), "FULL");
                lblHomeMessageDescription2.Text = string.Format(lblHomeMessageDescription2.Tag.ToString(), days.ToString());
                lblHomeMessageDescription3.Visible = false;
                splBtnUpgrade.Visible = false;
                splBtnBuyNow.Visible = false;
            }
            #endregion

        }

        /// <summary>
        /// Check for the application validity. If the remaining days is less than 1 then the Activation form 
        /// will be shown.
        /// </summary>
        private void ValidityExpired()
        {
            int days = Properties.Settings.Default.ActDaysRemaining;
            int versionPoint = Properties.Settings.Default.ActVersionPoint;

            //days = 0;
            if (days < 1)
            {

                TrayIcon.Icon = Properties.Resources.license_expired_icon;

                Properties.Settings.Default.AntIsProtectingPC = false;
                Properties.Settings.Default.Save();

                ShowAlertOnLicenseExpiration();

                //Show the license text
                switch (versionPoint)
                {
                    case 1:

                        lblLicenseExpireText2.Text = string.Format(lblLicenseExpireText2.Tag.ToString(), "Trial");
                        break;
                    case 2:
                        //
                        lblLicenseExpireText2.Text = string.Format(lblLicenseExpireText2.Tag.ToString(), "Full");
                        break;
                    default:
                        break;

                }

                AVStrike.Windows.Notification _Notification = null;
                if (!IsFormAlreadyOpen(typeof(AVStrike.Windows.Notification)))
                {
                    _Notification = new AVStrike.Windows.Notification();
                    _Notification.Show();
                }
            }
            else
            {
                //TrayIcon.Icon = Properties.Resources.favicon;
                Properties.Settings.Default.AntIsProtectingPC = true;
                Properties.Settings.Default.Save();
                ShowAllPanelsAfterActivation();
            }
        }

        /// <summary>
        /// Show alert in the main form after the license has expired. All the other panels are disabled after 
        /// the license expires. Only option for the user is to navigate to the Activation form and activate
        /// the application with a new key
        /// </summary>
        private void ShowAlertOnLicenseExpiration()
        {
            pnlLicenseExpireAlert.BringToFront();

            btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.av_home_main;
            btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.av_quarantine;
            btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.av_history_main;
            btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.av_settings;

            btnMainHome.Enabled = false;
            btnMainAntivirus.Enabled = false;
            btnMainHistory.Enabled = false;
            btnMainSettings.Enabled = false;
        }


        /// <summary>
        /// Enable all the panels that were being disabled during the expiration of the license. All the panels 
        /// will be enabled as soon as a new license key has been entered by the user
        /// </summary>
        private void ShowAllPanelsAfterActivation()
        {
            btnSubHomeQuickScan.BackgroundImage = global::AVStrike.Properties.Resources.quick_scan;

            btnMainHome.Enabled = true;
            btnMainAntivirus.Enabled = true;
            btnMainHistory.Enabled = true;
            btnMainSettings.Enabled = true;

            //By default Quick Scan should be enabled
            Properties.Settings.Default.ScanningFlag = 1;
            Properties.Settings.Default.Save();
        }


        /// <summary>
        /// Check if the form is already open. Opens only one instance of a form at a time.
        /// Used for enabling the navigation to the activation form at any given instance.
        /// </summary>
        /// <param name="FormType"></param>
        /// <returns></returns>
        public static bool IsFormAlreadyOpen(Type FormType)
        {
            foreach (Form OpenForm in Application.OpenForms)
            {
                if (OpenForm.GetType() == FormType)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Checks for the instance of the closed form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void instanceHasBeenClosed(object sender, FormClosedEventArgs e)
        {
            _ActivationForm = null;
        }


        /// <summary>
        ///Display the image on the Home tab. Secure or Pc Needs to be scanned 
        ///Secures, Unsecured. 
        ///UnSecured image is shown if the last scan time is more than 2 days
        /// </summary>
        private void DisplayScanImage()
        {
            DateTime dtLastScanDate = Properties.Settings.Default.AntLastScannedDate;
            DateTime dtComparison = new DateTime(2014, 06, 15, 12, 12, 12);

            //This is not Last scan date. Its first run date
            //DateTime dtComparison = Properties.Settings.Default.AntLastScannedDate;
            try
            {
                //Comparison between First
                if (DateTime.Compare(dtLastScanDate, dtComparison) > 0)
                {
                    int TotalDaysSinceLastScan = (int)(DateTime.Now - dtLastScanDate).TotalDays;
                    if (TotalDaysSinceLastScan > 1)
                    {
                        Properties.Settings.Default.ScanImageSettings = 0;
                        Properties.Settings.Default.Save();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            switch (Properties.Settings.Default.ScanImageSettings)
            {
                case 0:
                    this.pnlHomeImageLayout.BackColor = System.Drawing.Color.Transparent;
                    this.pnlHomeImageLayout.BackgroundImage = global::AVStrike.Properties.Resources.ak_new_unsecure;
                    break;
                case 1:
                    this.pnlHomeImageLayout.BackColor = System.Drawing.Color.Transparent;
                    this.pnlHomeImageLayout.BackgroundImage = global::AVStrike.Properties.Resources._protected;
                    break;
                default:
                    this.pnlHomeImageLayout.BackColor = System.Drawing.Color.Transparent;
                    this.pnlHomeImageLayout.BackgroundImage = global::AVStrike.Properties.Resources.ak_new_unsecure;
                    break;
            }
        }

        /// <summary>
        /// Delete the viruses or malwares inside the quarantine folder if they are more than 90 days older
        /// </summary>
        public void DeleteMoreThanDays()
        {
            if (!Properties.Settings.Default.AntQuarantineFolderValidity)
                return;

            try
            {
                SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.QuarantineValiditySelect();

                while (_SQLiteDataReader.Read())
                {
                    if (_SQLiteProcess.QuarantineValidityUpdate(_SQLiteDataReader[0].ToString(), _SQLiteDataReader[1].ToString()))
                    {
                        DeleteFile(_SQLiteDataReader[0].ToString());
                    }
                }
            }
            catch (SQLiteException ex)
            {
                ErrorLog.CreateLogFile("MainForm-DeleteMoreThanDays: " + ex.Message);
            }
        }

        /// <summary>
        /// Closing event of the main form. 
        /// The tray icon functionality is being added in the MinimizeToTray method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainBaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                MinimizeToTray();
                e.Cancel = true;
            }
        }

        #endregion

        #region Panel Events For Drag and Drop
        /// <summary>
        /// For the Drag and Drop option of the Form at the desired place.
        /// The static class FormDragDrop is being used to Release and Capture the events
        /// SendMessage() sends the handle to the user32.dll to enable the dragging and dropping events.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">MouseEventArgs</param>
        private void pnlMainTop_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                FormDragDrop.ReleaseCapture();
                //Parameters are SendMessage(Handle,WM_NCLBUTTONDOWN,HT_CAPTION)
                FormDragDrop.SendMessage(Handle, 0xA1, 0x2, 0);
            }
        }
        #endregion

        #region Special Button Click events
        /// <summary>
        /// Minimize the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splBtnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// Minimizes the application to the system tray. Doesnt close the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splBtnClose_Click(object sender, EventArgs e)
        {
            MinimizeToTray();
        }

        //Redirect to the Live chat url in the website
        private void splBtnLiveChat_Click(object sender, EventArgs e)
        {
            if (!AuthenticationAndSecurity.CheckInternet())
            {
                MessageBox.Show(this, ApplicationConstant.MessageToCheckForInternet, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                System.Diagnostics.Process.Start(ExternalUrls.LiveChatURL);
            }
        }

        //Redirect to the buy now url
        private void splBtnBuyNow_Click(object sender, EventArgs e)
        {
            if (!AuthenticationAndSecurity.CheckInternet())
            {
                MessageBox.Show(this, ApplicationConstant.MessageToCheckForInternet, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                System.Diagnostics.Process.Start(ExternalUrls.BuyNow);
            }
        }

        //Show the ActivationForm.cs
        private void splBtnUpgrade_Click(object sender, EventArgs e)
        {
            ActivationForm _ActivationForm = new ActivationForm();
            _ActivationForm.Show();
            this.Hide();

        }

        #endregion Special Button Click events

        #region Minimize to Tray
        private void niMain_Click(object sender, EventArgs e)
        {

        }

        //Load the values in the tray menu
        private void LoadTrayMenu()
        {
            TrayMenu.Items[0].Click += new System.EventHandler(this.Dispose_Click);
            TrayIcon.ContextMenuStrip = TrayMenu;

        }

        //Dispose when the application is closed from the tray menu
        private void Dispose_Click(object Sender, EventArgs e)
        {
            TrayIcon.Visible = false;
            TrayIcon.Icon = null;
            Application.Exit();
        }

        /// <summary>
        /// Minimizes the application to the system tray
        /// Shows the custom message for the Active License and the Expired License
        /// </summary>
        public void MinimizeToTray()
        {
            try
            {
                this.Visible = !this.Visible;
                niMain.Visible = !this.Visible;
                if (this.Visible && this.WindowState == FormWindowState.Minimized)
                    this.WindowState = FormWindowState.Normal;

                if (isScanRunning)
                {
                    TrayIcon.Text = GetBaloonText();
                    TrayIcon.BalloonTipText = GetBaloonText();
                    TrayIcon.Visible = true;
                    TrayIcon.ShowBalloonTip(1000);
                }
                else
                {
                    if (!Properties.Settings.Default.AntIsProtectingPC)
                    {
                        TrayIcon.Text = "AKICK License has expired";
                        TrayIcon.BalloonTipText = "AKICK License has expired";
                    }
                    else
                    {
                        TrayIcon.Text = "AKICK is Protecting Your PC";
                        TrayIcon.BalloonTipText = "AKICK is Protecting Your PC";
                    }
                    TrayIcon.Visible = true;
                    TrayIcon.ShowBalloonTip(1000);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainBaseForm-MinimizeToTray: " + ex.Message);
            }
        }

        //Opens the application from the system tray on the double click of the Icon in the sytem tray
        private void niMain_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = !this.Visible;
            niMain.Visible = !this.Visible;
            if (this.Visible && this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;

            if (this.Visible)
            {
                this.Activate();
                this.Focus();
                this.Select();
            }
        }

        //Opens the application from the system tray on the double click of the Icon in the sytem tray
        private void TrayIcon_MouseClick(object sender, MouseEventArgs e)
        {
            this.Visible = !this.Visible;
            TrayIcon.Visible = !this.Visible;
            if (this.Visible && this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;

            if (this.Visible)
            {
                this.Activate();
                this.Focus();
                this.Select();
                //ShowInTaskbar = true
            }
        }

        #endregion

        #region Main Tab
        /// <summary>
        /// Provides the event for the Panel selection.
        /// Certain features are enabled and disabled on the change of each tabs.
        /// The following tabs are being used
        /// Home, Antivirus, DiskCleaner, Pc Cleaner, Settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMain_Click(object sender, EventArgs e)
        {
            MainButton b = (MainButton)sender;
            switch (b.Name)
            {
                case "btnMainHome":
                    Cursor.Current = Cursors.WaitCursor;
                    btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.home_hover;
                    btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.threats;
                    btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.history;
                    btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.settings;
                    btnMainScan.BackgroundImage = global::AVStrike.Properties.Resources.scan1;
                    pnlHomeMainBasic.BringToFront();
                    DisplayScanImage();
                    Cursor.Current = Cursors.Default;
                    break;
                case "btnMainScan":
                    Cursor.Current = Cursors.WaitCursor;
                    btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.home;
                    btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.threats;
                    btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.history;
                    btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.settings;
                    btnMainScan.BackgroundImage = global::AVStrike.Properties.Resources.scan_hover1;
                    pnlScanMainBasic.BringToFront();
                    Cursor.Current = Cursors.Default;
                    break;
                case "btnMainAntivirus":
                    Cursor.Current = Cursors.WaitCursor;
                    btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.home;
                    btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.threats_hover;
                    btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.history;
                    btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.settings;
                    btnMainScan.BackgroundImage = global::AVStrike.Properties.Resources.scan1;
                    pnlAntivirusBasic.BringToFront();
                    DeleteMoreThanDays();
                    ShowScanReport();
                    if (lvDiskDiskCleaner.Items.Count > 0)
                        DisableQuarantineButtons(false);
                    else
                        DisableQuarantineButtons(true);
                    Cursor.Current = Cursors.Default;

                    break;
                case "btnMainHistory":
                    Cursor.Current = Cursors.WaitCursor;
                    btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.home;
                    btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.threats;
                    btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.history_hover;
                    btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.settings;
                    btnMainScan.BackgroundImage = global::AVStrike.Properties.Resources.scan1;
                    pnlHistoryBasic.BringToFront();
                    ShowScanResult();
                    Cursor.Current = Cursors.Default;
                    break;
                case "btnMainSettings":
                    Cursor.Current = Cursors.WaitCursor;
                    btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.home;
                    btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.threats;
                    btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.history;
                    btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.settings_hover;
                    btnMainScan.BackgroundImage = global::AVStrike.Properties.Resources.scan1;
                    pnlSettingsBasic.BringToFront();
                    //pnlSettingsDefault.BringToFront();
                    LoadAntivirusSettings();
                    Cursor.Current = Cursors.Default;
                    break;
                default:
                    break;
            }
        }

        //Disable and enable the quarantine buttons as per the requirement
        private void DisableQuarantineButtons(bool value)
        {
            if (value)
            {
                splBtnRestore.Enabled = false;
                splBtnRestore.BackgroundImage = global::AVStrike.Properties.Resources.ak_restore;
                splBtnRemoveAll.Enabled = false;
                splBtnRemoveAll.BackgroundImage = global::AVStrike.Properties.Resources.ak_removeall;
                splBtnRemove.Enabled = false;
                splBtnRemove.BackgroundImage = global::AVStrike.Properties.Resources.ak_remove;
            }
            else
            {
                splBtnRestore.Enabled = true;
                splBtnRestore.BackgroundImage = global::AVStrike.Properties.Resources.ak_restore;
                splBtnRemoveAll.Enabled = true;
                splBtnRemoveAll.BackgroundImage = global::AVStrike.Properties.Resources.ak_removeall;
                splBtnRemove.Enabled = true;
                splBtnRemove.BackgroundImage = global::AVStrike.Properties.Resources.ak_remove;
            }
        }

        #endregion

        #region External Device Scan

        //============================================================
        // WINDOWS MESSAGE HANDLERS
        // 
        // device state change
        private const int WM_DEVICECHANGE = 0x0219;

        // logical volume(A disk has been inserted, such a usb key or external HDD)
        private const int DBT_DEVTYP_VOLUME = 0x00000002;

        // detected a new device
        private const int DBT_DEVICEARRIVAL = 0x8000;

        // preparing to remove
        private const int DBT_DEVICEQUERYREMOVE = 0x8001;

        // removed
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        public bool deviceDetectFlag = false;

        /// <summary>
        /// Gets the handle and shows a pop up when ever an external device is being connected.
        /// Based on the flag obtained, the external device is being scanned.
        /// </summary>
        /// <param name="message"></param>
        protected override void WndProc(ref Message message)
        {



            base.WndProc(ref message);

            if ((message.Msg != WM_DEVICECHANGE) || (message.LParam == IntPtr.Zero))
                return;

            DEV_BROADCAST_VOLUME volume = (DEV_BROADCAST_VOLUME)Marshal.PtrToStructure(message.LParam, typeof(DEV_BROADCAST_VOLUME));


            if (volume.dbcv_devicetype == DBT_DEVTYP_VOLUME)
            {
                //deveiceDetectFlag = true;
                switch (message.WParam.ToInt32())
                {

                    // New device inserted...
                    case DBT_DEVICEARRIVAL:
                        if (!deviceDetectFlag)
                        {
                            deviceDetectFlag = true;
                            if (MessageBox.Show(string.Format("AKICK has detected a new storage device '" + ToDriveName(volume.dbcv_unitmask) + ". Do you want to scan it?'"), Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            {

                                string strCustomScanPath = ToDriveName(volume.dbcv_unitmask);


                                if (strCustomScanPath == @"A:\" || strCustomScanPath == @"B:\\")
                                {
                                    MessageBox.Show(this, "Selected location cannot be accessed.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }

                                if (strCustomScanPath != null || strCustomScanPath.Length == 0)
                                {
                                    if (IsDirectoryEmpty(strCustomScanPath))
                                    {
                                        MessageBox.Show(this, "Cannot Scan an Empty Drive.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }

                                    Properties.Settings.Default.ScanningFlag = 2;
                                    Properties.Settings.Default.ScanningPath = strCustomScanPath.ToString();
                                    Properties.Settings.Default.Save();
                                }
                                pnlHomeMainDefault.Hide();
                                pnlHomeMainScanProcess.Show();
                                ScanProcessStart();

                            }
                        }
                        break;

                    // Device Removed.
                    case DBT_DEVICEREMOVECOMPLETE:
                        if (deviceDetectFlag)
                        {
                            deviceDetectFlag = false;
                            MessageBox.Show("Storage has been removed.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                }
            }

        }

        // Convert to the Drive name (”D:”, “F:”, etc)
        private string ToDriveName(int mask)
        {
            int offset = 0;
            while ((offset < 26) && ((mask & 0x00000001) == 0))
            {
                mask = mask >> 1;
                offset++;
            }

            if (offset < 26)
                return String.Format("{0}:", Convert.ToChar(Convert.ToInt32('A') + offset));

            return "?";
        }

        // Contains information about a logical volume.
        [StructLayout(LayoutKind.Sequential)]
        private struct DEV_BROADCAST_VOLUME
        {
            public int dbcv_size;
            public int dbcv_devicetype;
            public int dbcv_reserved;
            public int dbcv_unitmask;
        }

        #endregion Real Time Scan

        #region Scan Selection

        /// <summary>
        /// Active at the Home panel. Enables to select from any one of the scan.
        /// The list of scans are Quick Scan, Full Scan and Custom Scan
        /// Enables the user to select his/her respective scan and the corresponding flag is being passed to the 
        /// scan process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubHomeScanType_Click(object sender, EventArgs e)
        {
            SubProcessButton b = (SubProcessButton)sender;
            switch (b.Name)
            {

                //Flag is 1
                case "btnSubHomeQuickScan":
                    btnSubHomeQuickScan.BackgroundImage = global::AVStrike.Properties.Resources.quick_scan;
                    btnSubHomeCustomScan.BackgroundImage = global::AVStrike.Properties.Resources.ak_customscan;
                    btnSubHomeFullScan.BackgroundImage = global::AVStrike.Properties.Resources.ak_fullscane;
                    Properties.Settings.Default.ScanningFlag = 1;
                    Properties.Settings.Default.Save();
                    splBtnScanNow_Click(sender, e);
                    break;
                //Flag is 2
                case "btnSubHomeCustomScan":
                    btnSubHomeQuickScan.BackgroundImage = global::AVStrike.Properties.Resources.quick_scan;
                    btnSubHomeCustomScan.BackgroundImage = global::AVStrike.Properties.Resources.ak_customscan;
                    btnSubHomeFullScan.BackgroundImage = global::AVStrike.Properties.Resources.ak_fullscane;
                    Properties.Settings.Default.ScanningFlag = 2;
                    Properties.Settings.Default.Save();
                    splBtnScanNow_Click(sender, e);
                    break;
                //Flag is 0
                case "btnSubHomeFullScan":
                    btnSubHomeQuickScan.BackgroundImage = global::AVStrike.Properties.Resources.quick_scan;
                    btnSubHomeCustomScan.BackgroundImage = global::AVStrike.Properties.Resources.ak_customscan;
                    btnSubHomeFullScan.BackgroundImage = global::AVStrike.Properties.Resources.ak_fullscane;
                    Properties.Settings.Default.ScanningFlag = 0;
                    Properties.Settings.Default.Save();
                    splBtnScanNow_Click(sender, e);
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Display Scan Result and Report

        /// <summary>
        /// Show the report of the quarantined items in the list view in the Antivirus>Quarantine Panel
        /// 
        /// </summary>
        public void ShowScanReport()
        {
            //Clear the list view
            this.lvDiskDiskCleaner.Items.Clear();

            SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.GetScanHistory(true);
            if (_SQLiteDataReader != null)
            {
                while (_SQLiteDataReader.Read())
                {
                    ListViewItem listViewItem = new ListViewItem(new string[] { _SQLiteDataReader[0].ToString(), _SQLiteDataReader[4].ToString(), 
                    _SQLiteDataReader[1].ToString(), _SQLiteDataReader[2].ToString(), 
                    _SQLiteDataReader[3].ToString(), _SQLiteDataReader[5].ToString(), _SQLiteDataReader[6].ToString()});
                    listViewItem.Checked = false;
                    this.lvDiskDiskCleaner.Items.Add(listViewItem);
                }

            }


            chkQuarantineSelectUnselect.Checked = false;

        }

        /// <summary>
        /// Shows the scan history.
        /// The last Scanned Type, Date Time and time taken to scan are being displayed in the ListView
        /// </summary>
        public void ShowScanResult()
        {
            this.lvScanResult.Items.Clear();
            SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.GetScanResult();
            if (_SQLiteDataReader != null)
            {
                while (_SQLiteDataReader.Read())
                {
                    string strScanType = _SQLiteDataReader[1].ToString();
                    string strDate = _SQLiteDataReader[2].ToString();
                    string strResult = _SQLiteDataReader[3].ToString();

                    ListViewItem listViewItem = new ListViewItem(new string[] { strScanType, strDate, strResult });
                    listViewItem.Checked = false;

                    this.lvScanResult.Items.Add(listViewItem);
                }
            }

        }


        /// <summary>
        /// Get the default selected Index of the Listview in the Scan History Panel.
        /// </summary>
        private void DefaultSelectedIndex()
        {

            try
            {
                int rowIndex = _SQLiteProcess.GetResultsRowCount();
                if (rowIndex > 0)
                {
                    pnlAntivirusScanHistoryValuesShow.Visible = true;
                    DisplayScanResult(rowIndex);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainForm-DefaultSelectedIndex: " + ex.Message);
            }

        }

        /// <summary>
        /// List view click event to show the details in the Scan History Panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvScanResult_Click(object sender, EventArgs e)
        {
            if (lvScanResult.Items.Count > 0)
            {
                pnlAntivirusScanHistoryValuesShow.Visible = true;
                int rows = _SQLiteProcess.GetResultsRowCount();
                int focusedItem = lvScanResult.FocusedItem.Index;

                int rowid = rows - (focusedItem);
                DisplayScanResult(rowid);
            }
            else
            {
                pnlAntivirusScanHistoryValuesShow.Visible = false;
            }



        }


        /// <summary>
        /// Displays the result of the last scan.
        /// The results are for
        /// Type of scan, Total run time for scan,Number of files scanned, Total data scanned
        /// Number of threats found, number of threats neutralized
        /// </summary>
        /// <param name="rowid"></param>
        private void DisplayScanResult(int rowid)
        {
            try
            {
                SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.DisplayScanResult(rowid);

                while (_SQLiteDataReader.Read())
                {

                    lblScanResultTypeOfScanDescription.Text = _SQLiteDataReader[1].ToString();
                    lblScanResultRunTimeDescription.Text = _SQLiteDataReader[2].ToString();
                    lblScanResultScannedFilesDescription.Text = _SQLiteDataReader[3].ToString();
                    lblScanResultThreatsDetectedDescription.Text = _SQLiteDataReader[4].ToString();
                    lblScanResultThreatsNeutralizedDescription.Text = _SQLiteDataReader[5].ToString();
                    lblScanResultTotalDataScannedDescription.Text = BaseTool.BytesProcessing.FormatBytes(Convert.ToInt64(_SQLiteDataReader[6].ToString()));

                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainForm-ShowScanResult: " + ex.Message);
            }
        }
        #endregion

        #region Scan Process

        public static bool isScanRunning = false;

        /// <summary>
        /// Click event for the Scan Now button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splBtnScanNow_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ScanningFlag == 2)
            {
                FolderBrowserDialog browserDlg = new FolderBrowserDialog();
                DialogResult dialogResult = browserDlg.ShowDialog(this);

                string strCustomScanPath = browserDlg.SelectedPath;

                if (strCustomScanPath == ApplicationConstant.InAccessiblePathA || strCustomScanPath == ApplicationConstant.InAccessiblePathB)
                {
                    MessageBox.Show(this, ApplicationConstant.InAccessibleMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (dialogResult == DialogResult.OK)
                {
                    if (strCustomScanPath != null || strCustomScanPath.Length == 0)
                    {
                        if (IsDirectoryEmpty(strCustomScanPath))
                        {
                            MessageBox.Show(this, ApplicationConstant.EmptyDirectoryMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                        Properties.Settings.Default.ScanningPath = strCustomScanPath.ToString();
                        Properties.Settings.Default.Save();
                    }
                }
                else
                {
                    return;
                }


            }
            pnlHomeMainDefault.Hide();
            pnlHomeMainScanProcess.Show();
            ScanProcessStart();
        }

        public bool IsDirectoryEmpty(string directory)
        {
            string[] dirs = System.IO.Directory.GetDirectories(directory);
            string[] files = System.IO.Directory.GetFiles(directory);

            if (dirs.Length == 0 && files.Length == 0)
                return true;
            else
                return false;
        }

        private void splBtnStopScanning_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (backgroundWorkerAntivirus.IsBusy)
            {
                //backgroundWorkerAntivirus.CancelAsync();
                backgroundWorkerAntivirus.Dispose();
            }

            if (backgroundWorkerAntivirus.WorkerSupportsCancellation == true)
            {
                backgroundWorkerAntivirus.CancelAsync();
            }

            //cl_engine.Dispose();
            clam.FreeMemory(cl_enginePtr);
            cl_enginePtr = IntPtr.Zero;
            clam = null;


            #region Initialization of variables and controls
            intScanProcessDateTimeCounter = 0;
            VirusScanFileCount = 0;
            CountMalware = 0;
            CurrentFile = "Collecting System Information...";
            //lblPercentage.Text = "0 % ";

            this.lblScanInProcessDescription.Text = "Collecting System Information...";
            this.lblItemsScannedCountDescription.Text = "0";
            this.lblThreatsDetectedDescription.Text = "0";
            this.lblNeutralizedDescription.Text = "0";
            lblTimeElapsedDescription.Text = "00:00:00";
            #endregion

            AntivirusTimerForScanProcess.Stop();
            AntivirusTimerForTimeElapsed.Stop();
            pnlHomeMainDefault.Show();
            ScanButtonsEnable(true, true, true);
            isScanRunning = false;

            Properties.Settings.Default.ScanTaskSchedule = false;
            Properties.Settings.Default.Save();
            Cursor.Current = Cursors.Default;
            Thread.Sleep(1000);
        }

        private void ScanProcessStart()
        {
            try
            {

                isScanRunning = true;

                splBtnStopScanning.Visible = false;
                isScanButtonVisible = false;
                switch (Properties.Settings.Default.ScanningFlag)
                {

                    case 0://Full Scan
                        ScanButtonsEnable(false, false, true);
                        break;
                    case 1://Quick Scan
                        ScanButtonsEnable(true, false, false);
                        break;
                    case 2://Custom Scan
                        ScanButtonsEnable(false, true, false);
                        break;
                    case 3: //External Device Scan
                        ScanButtonsEnable(false, false, false);
                        break;
                    default:
                        //do the quick scan for the flag 1
                        ScanButtonsEnable(false, false, true);
                        break;


                }

                InitializeWorkers();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainBaseForm-ScanProcessStart: " + ex.Message);
            }

        }

        private void ScanButtonsEnable(bool QuickScan, bool CustomScan, bool FullScan)
        {
            btnSubHomeQuickScan.Enabled = QuickScan;
            btnSubHomeCustomScan.Enabled = CustomScan;
            btnSubHomeFullScan.Enabled = FullScan;

        }



        private void InitializeWorkers()
        {
            isScanEngineExited = true;

            backgroundWorkerAntivirus = new BackgroundWorker();

            if (backgroundWorkerAntivirus.IsBusy)
            {
                //backgroundWorkerAntivirus.CancelAsync();
                backgroundWorkerAntivirus.Dispose();

            }

            if (backgroundWorkerAntivirus.WorkerSupportsCancellation == true)
            {
                //backgroundWorkerAntivirus.CancelAsync();
                backgroundWorkerAntivirus.Dispose();

            }
            backgroundWorkerAntivirus.WorkerSupportsCancellation = true;
            backgroundWorkerAntivirus.WorkerReportsProgress = false;
            // This event will be raised on the worker thread when the worker starts
            backgroundWorkerAntivirus.DoWork += new DoWorkEventHandler(backgroundWorkerAntivirus_DoWork);
            // This event will be raised when we call ReportProgress
            backgroundWorkerAntivirus.ProgressChanged += new ProgressChangedEventHandler(backgroundWorkerAntivirus_ProgressChanged);
            backgroundWorkerAntivirus.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerAntivirus_ScanCompleted);

            LoadTimer();

            backgroundWorkerAntivirus.RunWorkerAsync();

        }

        /// <summary>
        /// A new method created in order to Enhance the Scan process along with the Progress bar count.
        /// </summary>
        private void LoadTimer()
        {
            try
            {
                #region Initialization
                filesizesProcessedInBytes = 0;
                intScanProcessDateTimeCounter = 0;
                VirusScanFileCount = 0;
                CountMalware = 0;
                CurrentFile = "Collecting System Information...";
                //lblPercentage.Text = "0 % ";
                isScanCompleted = false;


                this.lblScanInProcessDescription.Text = "Collecting System Information...";
                this.lblItemsScannedCountDescription.Text = "0";
                this.lblThreatsDetectedDescription.Text = "0";
                this.lblNeutralizedDescription.Text = "0";
                lblTimeElapsedDescription.Text = "00:00:00";
                #endregion

                #region Timer Initialization
                AntivirusTimerForScanProcess = new System.Windows.Forms.Timer();
                AntivirusTimerForScanProcess.Tick += new EventHandler(AntivirusTimerForScanProcess_Tick);
                //AntivirusTimerForScanProcess.Interval = 1000;

                AntivirusTimerForScanProcess.Start();


                AntivirusTimerForTimeElapsed = new System.Windows.Forms.Timer();
                AntivirusTimerForTimeElapsed.Tick += new EventHandler(AntivirusTimerForTimeElapsed_Tick);
                AntivirusTimerForTimeElapsed.Interval = 1000;
                AntivirusTimerForTimeElapsed.Start();
                #endregion

                #region Progressbar
                prgAntivirusScan.Visible = true;

                prgAntivirusScan.Minimum = 0;
                prgAntivirusScan.Maximum = 100;
                prgAntivirusScan.Value = 0;

                #endregion Progressbar

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ScanDevice-EnableNewScan: " + ex.Message);
            }

        }

        private void loadFilesToBeScanned()
        {
            isScanButtonVisible = true;
            sizeFilter = Properties.Settings.Default.AntFileSizeLimit;
            fileExtensionFilter = AntivirusGetExcludedFiles();
            excludedFolders = AntivirusGetExcludedFolderList();
            cUtil.IgnoreFileList(ref ignoreFileList);

            switch (Properties.Settings.Default.ScanningFlag)
            {



                //Full Scan Running
                case 0:
                    GetAllFiles();


                    break;
                //Quick Scan Running
                case 1:
                    QuickScan();

                    break;
                //Custom Scan Running
                case 2:

                    GetFilesForDirectory(Properties.Settings.Default.ScanningPath);

                    break;
                case 3:
                    GetFilesForDirectory(Properties.Settings.Default.ScanningPath);

                    break;
                default:
                    GetAllFiles();

                    break;
            }


        }

        #region Timers
        private void AntivirusTimerForScanProcess_Tick(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(CurrentFile))
                {
                    if (CurrentFile.Length > 40)
                    {
                        CurrentFile = CurrentFile.Substring(0, 20) + "...." + CurrentFile.Substring(CurrentFile.Length - 20, 20);

                    }

                    #region Label Values
                    this.lblScanInProcessDescription.Text = string.Copy(CurrentFile);
                    this.lblItemsScannedCountDescription.Text = "" + VirusScanFileCount;
                    this.lblThreatsDetectedDescription.Text = string.Format("{0}", CountMalware.ToString());
                    this.lblNeutralizedDescription.Text = string.Format("{0}", CountMalware.ToString());
                    #endregion


                    if (isScanCompleted)
                    {
                        #region Final Process after scan completion

                        #region Variables Reset
                        isScanCompleted = false;
                        blMinimizeToWindow = false;
                        intScanProcessDateTimeCounter = 0;
                        VirusScanFileCount = 0;
                        CountMalware = 0;
                        CurrentFile = "Collecting System Information...";
                        prgAntivirusScan.Value = 0;
                        #endregion

                        //Add the datas inside the table to provide the scan result
                        CreateScanResult();
                        //Adding the format "dd-MM-yyyy HH:mm:ss" to the last scanned date didnt workout. Moving back to the default 
                        //Properties.Settings.Default.AntLastScannedDate = DateTime.Now.ToString();
                        Properties.Settings.Default.AntLastScannedDate = DateTime.Now;
                        lblHomeMainLastScannedOnDescription.Text = DateTime.Now.ToString("dd-MM-yyyy");
                        Properties.Settings.Default.ScanImageSettings = 1;
                        Properties.Settings.Default.ScanTaskSchedule = false;
                        Properties.Settings.Default.Save();

                        AntivirusTimerForScanProcess.Stop();
                        AntivirusTimerForTimeElapsed.Stop();
                        ShowAntivirusPanel();
                        pnlHomeMainDefault.Show();
                        DisplayScanImage();
                        ScanButtonsEnable(true, true, true);
                        isScanRunning = false;

                        if (isRestartNeeded)
                        {
                            if (MessageBox.Show(this, "Some viruses cannot be moved to Quarantine, to delete them permanently a restart is required. Do you want to restart?", Application.ProductName,
                                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                                PInvoke.ExitWindowsEx(0x02, PInvoke.MajorOperatingSystem | PInvoke.MinorReconfig | PInvoke.FlagPlanned);
                            else
                            {
                                isRestartNeeded = false;
                            }

                        }

                        #endregion


                        //Free the memory
                        clam.FreeMemory(cl_enginePtr);
                        cl_enginePtr = IntPtr.Zero;
                        clam = null;
                        //cl_engine.Dispose();
                    }

                }
            }
            catch (System.ArgumentOutOfRangeException)
            {
                VirusScanFileCount = intFileListCount;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("ScanDevice-TimerScanProcess_Tick: " + ex.Message);
            }
        }

        private void AntivirusTimerForTimeElapsed_Tick(object sender, EventArgs e)
        {
            this.lblTimeElapsedDescription.Text = ElapsedDateTime.AddSeconds(intScanProcessDateTimeCounter).ToString("HH:mm:ss");
            intScanProcessDateTimeCounter++;
        }
        #endregion

        #region Background workers


        private void backgroundWorkerAntivirus_DoWork(object sender, DoWorkEventArgs e)
        {

            // Allow loadFIlesToBeScanned take lead before starting scanning process.
            if (backgroundWorkerAntivirus.CancellationPending == true)
            {
                e.Cancel = true;
                return;
            }

            //uint loaded_signature = 0;
            //clam.DatabasePath = database_path;
            //cl_engine = clam.LoadClamAV(DatabaseOptions.CL_DB_STDOPT, ManagedClam.CL_INIT_STDOPT, ref loaded_signature);

            ClamMain.InitializeClamAV();

            cl_engine = new ClamEngine();

            cl_engine.LoadDatabase(database_path, ClamDatabaseOptions.CL_DB_STDOPT);

            cl_engine.CompileEngine();

            if (VirusScanFileCount == 0)
            {
                isScanButtonVisible = true;
                loadFilesToBeScanned();
            }
        }

        public void backgroundWorkerAntivirus_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // The progress percentage is a property of e
            prgAntivirusScan.Value = e.ProgressPercentage;

        }


        void backgroundWorkerAntivirus_ScanCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //ShowDialog return value will inform whether the worker finished properly or not
            if (e.Error != null)
                DialogResult = DialogResult.Abort;
            else if (e.Cancelled)
                DialogResult = DialogResult.Cancel;
            else
                DialogResult = DialogResult.OK;
            //close the form
            //Close();

            isScanCompleted = true;

        }
        #endregion


        /// <summary>
        /// Provides all the Values once the scan is finished
        /// </summary>
        private void CreateScanResult()
        {

            string strTimeElapsed = this.lblTimeElapsedDescription.Text.ToString();
            string strGetScanType = GetScanType();
            string strGetCurrentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string strItemsScannedCountDescription = lblItemsScannedCountDescription.Text.ToString();
            string strThreatsDetectedDescription = lblThreatsDetectedDescription.Text.ToString();
            string strNeutralizedDescription = lblNeutralizedDescription.Text.ToString();

            int count = Convert.ToInt32(lblThreatsDetectedDescription.Text.ToString());
            string strGetScanResult = "Scan Completed Successfully, {0} Threat(s) Were Found";
            if (count > 0)
            {
                strGetScanResult = string.Format(strGetScanResult, count.ToString());
            }
            else
            {
                strGetScanResult = string.Format(strGetScanResult, 0);
            }

            long strTotalDataScanned = filesizesProcessedInBytes;
            _SQLiteProcess.InsertIntoScanResultDetails(strGetScanType, strGetCurrentTime, strGetScanResult, strTimeElapsed, strItemsScannedCountDescription, strThreatsDetectedDescription, strNeutralizedDescription, strTotalDataScanned);

        }

        #region Antivirus Excluded Files and Folder List
        private List<string> AntivirusGetExcludedFiles()
        {
            List<string> lines = new List<string>();
            if (Properties.Settings.Default.AntExcludeFilesList != null)
            {
                foreach (string str in Properties.Settings.Default.AntExcludeFilesList)
                    lines.Add(str);
            }


            return lines;
        }

        private List<string> AntivirusGetExcludedFolderList()
        {
            List<string> lines = new List<string>();
            CommonUtil cUtil = new CommonUtil();

            cUtil.GetSystemFolders(ref lines, Application.CommonAppDataPath, Application.UserAppDataPath);
            if (Properties.Settings.Default.AntExcludeFoldersList != null)
            {
                foreach (string str in Properties.Settings.Default.AntExcludeFoldersList)
                    lines.Add(str);
            }


            return lines;
        }
        #endregion


        public static bool isRestartNeeded = false;
        public static bool isScanButtonVisible = false;


        #region File Datas
        public void GetAllFiles()
        {
            // Start with drives if you have to search the entire computer.
            string[] drives = System.Environment.GetLogicalDrives();

            foreach (string dr in drives)
            {
                string strDriveType = dr.ToString();

                if (strDriveType == @"A:\" || strDriveType == @"B:\\")
                    continue;

                if (!excludedFolders.Contains(dr))
                {
                    System.IO.DriveInfo di = new System.IO.DriveInfo(dr);
                    // Here we skip the drive if it is not ready to be read. This
                    // is not necessarily the appropriate action in all scenarios.
                    if (!di.IsReady)
                    {
                        continue;
                    }
                    DirectoryInfo rootDir = di.RootDirectory;

                    if (!excludedFolders.Contains(rootDir.FullName))
                    {
                        SearchDirectoryRecursive(rootDir);
                    }
                }
            }
        }

        /// <summary>
        /// For the Custom scan
        /// Pass the desired folder path and scan them recursively.
        /// Size filters are application.
        /// Exclude the file types and exclude the folders
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="sizeFilter"></param>
        /// <param name="fileExtensionFilter"></param>
        /// <param name="excludedFolders"></param>
        /// <returns></returns>
        public void GetFilesForDirectory(string folderPath)
        {
            // Start with drives if you have to search the entire computer.
            DirectoryInfo dirInfo = new DirectoryInfo(folderPath);
            //   DirectoryInfo rootDir = di.RootDirectory;
            if (!excludedFolders.Contains(dirInfo.FullName))
            {
                SearchDirectoryRecursive(dirInfo);
            }

        }
        private bool IsFileValidForScan(FileInfo fi)
        {
            bool isNetworkPath = false;


            try
            {
                isNetworkPath = Win32API.PathIsNetworkPath(fi.FullName.ToLower());

            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.WARN, string.Format("Error in IsNetworkPath. Directory Name : {0}", fi.FullName));

                isNetworkPath = false;
            }
            string fileNameLower = fi.Name.ToLower();

            if (!isNetworkPath)
            {
                //if (cUtil.isExecutable(fileNameLower) || cUtil.isCompressed(fileNameLower))
                //if (cUtil.isExecutable(fileNameLower) || cUtil.isCompressed(fileNameLower))
                //{
                if (fi.Exists)
                {

                    if (!(this.ignoreFileList.Count<string>((string x) => fileNameLower.Contains(x)) <= 0))
                        return false;

                    if (fileExtensionFilter.Contains(fi.Extension))
                        return false;

                    if (fi.Length < (sizeFilter * 1048576) || (fileExtensionFilter != null && !fileExtensionFilter.Contains(fi.Extension)))
                        return true;
                }
                //}
            }
            return false;

        }

        public void scanFile(FileInfo fi)
        {
            CurrentFile = fi.FullName;

            VirusScanFileCount++;
            try
            {
                if (IsFileValidForScan(fi) || cUtil.isCompressed(CurrentFile))
                {
                    scanFileForVirus(fi);
                }
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.WARN, "Error in scan engine while scanning file :" + fi.FullName);

            }

        }
        //Methos scans for the folders recursively until there are not sub directories inside a directory
        private void SearchDirectoryRecursive(System.IO.DirectoryInfo root)
        {
            if (!isScanRunning)
            {
                return;
            }
            DirectoryInfo[] subDirs = null;
            FileInfo[] files = null;
            //   GC.Collect();
            // First, process all the files directly under this folder
            if (!((root.Attributes & FileAttributes.ReparsePoint) == FileAttributes.ReparsePoint))
            {
                try
                {
                    files = root.GetFiles("*.*");
                }
                // This is thrown if even one of the files requires permissions greater
                // than the application provides.
                catch (UnauthorizedAccessException e)
                {
                    CLogger.WriteLog(ELogLevel.WARN, "Unauthorized Access " + root.FullName);
                }
                catch (System.IO.DirectoryNotFoundException e)
                {
                    CLogger.WriteLog(ELogLevel.WARN, "Directory Not Found " + root.FullName);
                }
                if (files != null)
                {
                    int totalFilesCount = files.Length;

                    string fileNameLower = string.Empty;


                    foreach (System.IO.FileInfo fi in files)
                    {
                        scanFile(fi);
                    }

                }
                // Now find all the subdirectories under this directory.
                try
                {
                    subDirs = root.GetDirectories();
                }

                catch (UnauthorizedAccessException e)
                {

                    CLogger.WriteLog(ELogLevel.WARN, "Unauthorized Access " + root.FullName);
                }
                catch (System.IO.DirectoryNotFoundException e)
                {
                    CLogger.WriteLog(ELogLevel.WARN, "Directory Not Found " + root.FullName);
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.WARN, "Other Error " + root.FullName + e.Message);
                }
                if (subDirs != null)
                {


                    foreach (DirectoryInfo dirInfo in subDirs)
                    {

                        try
                        {
                            if (dirInfo.FullName.Equals(Application.StartupPath))
                                continue;
                            // Resursive call for each subdirectory.
                            if (!excludedFolders.Contains(dirInfo.FullName))
                            {
                                SearchDirectoryRecursive(dirInfo);
                            }

                        }
                        catch (System.IO.PathTooLongException ex)
                        {
                            CLogger.WriteLog(ELogLevel.WARN, String.Format("Path too long for file name : {0}", dirInfo.Name));

                        }
                    }

                }
            }

        }

        private void QuickScan()
        {
            Process[] local = Process.GetProcesses();

            foreach (Process p in local)
            {
                try
                {
                    ProcessModuleCollection myprocess = p.Modules;
                    foreach (ProcessModule pm in myprocess)
                    {
                        FileInfo fi = new FileInfo(pm.FileName);
                        scanFile(fi);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.CreateLogFile("MainBaseForm-QuickScan: " + ex.Message);
                }
            }
        }

        private void scanFileForVirus(FileInfo file)
        {
            try
            {
                CurrentFile = file.FullName;
                CurrentFileToMoveAndDelete = file.FullName;
                intReturnStatus = 0;
                //intReturnStatus = _ScanerDllImport.ScanProcess(CurrentFile);

                ClamScanResult result = (ClamScanResult)cl_engine.ScanByFile(CurrentFile, ClamScanOptions.CL_SCAN_STDOPT);

                //string result = clam.ScanFile(CurrentFile, ScanOptions.CL_SCAN_STDOPT);

                //Send value to the progress bar
                //backgroundWorkerAntivirus.ReportProgress(progressPercentageCount == 100 ? 1 : progressPercentageCount++);

                filesizesProcessedInBytes += (file.Length);
                FileStatusDescription = "File is Not Malicious";

                if (result.IsInfected)
                {
                    CountMalware++;
                    FileStatusDescription = result + " Found";
                    try
                    {
                        string strnewDestination = Application.StartupPath + @"\Quarantine\" + _ApplicationProperties.MD5Hash(CurrentFileToMoveAndDelete);
                        _SQLiteProcess.InsertScanHistory(Path.GetFileName(CurrentFileToMoveAndDelete), CurrentFile, _ApplicationProperties.MD5Hash(CurrentFileToMoveAndDelete));

                        _ApplicationProperties.FileEncryption(CurrentFileToMoveAndDelete, strnewDestination);

                        //System.IO.File.Copy(CurrentFileToMoveAndDelete, strDestination, true);
                        System.IO.File.Delete(CurrentFileToMoveAndDelete);
                    }
                    catch (Exception e)
                    {
                        isRestartNeeded = true;
                        _MoveFileAndDeleteFlag.CheckForRebootFile(CurrentFile);

                    }

                }

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainBaseForm-ScanFiles: " + ex.Message);
            }

        }
        #endregion

        #endregion

        #region Get Scan Type and Baloon Text

        public string GetScanType()
        {
            string strScanTye = "";
            switch (Properties.Settings.Default.ScanningFlag)
            {

                //Full Scan Running
                case 0:
                    strScanTye = "Full Scan";
                    break;
                //Quick Scan Running
                case 1:
                    strScanTye = "Quick Scan";
                    break;
                //Custom Scan Running
                case 2:
                    strScanTye = "Custom Scan";
                    break;
                case 3:
                    //Removable Device Scan
                    strScanTye = "External Device";
                    break;
                default:
                    strScanTye = "Full Scan";
                    break;
            }
            return strScanTye;
        }

        public string GetBaloonText()
        {
            string strScanTye = "";

            switch (Properties.Settings.Default.ScanningFlag)
            {
                //Full Scan Running
                case 0:
                    strScanTye = "Full Scan Running";
                    break;
                //Quick Scan Running
                case 1:
                    strScanTye = "Quick Scan Running";
                    break;
                //Custom Scan Running
                case 2:
                    strScanTye = "Custom Scan Running";
                    break;
                default:
                    strScanTye = "Quick Scan Running";
                    break;
            }
            return strScanTye;
        }

        #endregion


        #region Antivirus Panel and Result Panel

        public void ShowAntivirusPanel()
        {

            btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.av_home_main;
            btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.av_quarantine;
            btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.av_history_hover;
            btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.av_settings;
            pnlHistoryBasic.BringToFront();
            ShowScanResult();
            if (lvScanResult.Items.Count > 0)
            {
                lvScanResult.Items[0].Selected = true;
                lvScanResult.Select();
                pnlAntivirusScanHistoryValuesShow.Visible = true;
            }
            DefaultSelectedIndex();

        }
        public void ShowAntivirusResultPanel(string strButtonName)
        {
            switch (strButtonName)
            {

                //Flag is 1
                case "btnSubAntivirusQuarantine":
                    //btnSubAntivirusQuarantine.BackgroundImage = global::AVStrike.Properties.Resources.quarntine_hover;
                    //btnSubAntivirusScanResult.BackgroundImage = global::AVStrike.Properties.Resources.scan_history;
                    pnlAntivirusQuarantine.BringToFront();
                    //pnlAntivirusScanResult.Hide();
                    ShowScanReport();
                    break;
                //Flag is 2
                case "btnSubAntivirusScanResult":
                    //btnSubAntivirusQuarantine.BackgroundImage = global::AVStrike.Properties.Resources.quarantine_before;
                    //btnSubAntivirusScanResult.BackgroundImage = global::AVStrike.Properties.Resources.scan_history_hover;
                    pnlAntivirusScanResult.BringToFront();
                    //pnlAntivirusQuarantine.Hide();
                    ShowScanResult();
                    if (lvScanResult.Items.Count > 0)
                    {
                        lvScanResult.Items[0].Selected = true;
                        lvScanResult.Select();
                        pnlAntivirusScanHistoryValuesShow.Visible = true;
                    }
                    DefaultSelectedIndex();
                    break;

                default:
                    break;
            }
        }
        #endregion

        #region Toggle Between Quarantine and Scan Result
        /// <summary>
        /// Toggle between the Scan History Panel and the Quarantine list panel
        /// The values are binded into list views in the Quarantine when the navigation to the quarantine panel is 
        /// done. The list view for the Scan History is binded with data when the navigation to the Scan History 
        /// panel is done.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubAntivirus_Click(object sender, EventArgs e)
        {
            SubProcessButton b = (SubProcessButton)sender;
            switch (b.Name)
            {
                //Flag is 1
                case "btnSubAntivirusQuarantine":
                    Cursor.Current = Cursors.WaitCursor;
                    //btnSubAntivirusQuarantine.BackgroundImage = global::AVStrike.Properties.Resources.quarntine_hover;
                    //btnSubAntivirusScanResult.BackgroundImage = global::AVStrike.Properties.Resources.scan_history;
                    pnlAntivirusQuarantine.BringToFront();

                    //pnlAntivirusScanResult.Hide();
                    ShowScanReport();
                    if (lvDiskDiskCleaner.Items.Count > 0)
                        DisableQuarantineButtons(false);
                    else
                        DisableQuarantineButtons(true);
                    Cursor.Current = Cursors.Default;
                    break;
                //Flag is 2
                case "btnSubAntivirusScanResult":
                    Cursor.Current = Cursors.WaitCursor;
                    //btnSubAntivirusQuarantine.BackgroundImage = global::AVStrike.Properties.Resources.quarantine_before;
                    //btnSubAntivirusScanResult.BackgroundImage = global::AVStrike.Properties.Resources.scan_history_hover;
                    pnlAntivirusScanResult.BringToFront();
                    pnlAntivirusScanHistoryValuesShow.Visible = false;
                    ShowScanResult(); //Show the Scan History datas inside the List View
                    if (lvScanResult.Items.Count > 0)
                    {
                        lvScanResult.Items[0].Selected = true;
                        lvScanResult.Select();
                    }
                    DefaultSelectedIndex(); //Show the default selected index when navigated to the Scan History Panel
                    Cursor.Current = Cursors.Default;
                    break;

                default:
                    break;
            }
        }
        #endregion


        #region Settings Tab

        /// <summary>
        /// Navigation in the settings panel between the Antivirus Settings and the Disk Cleaner Settings
        /// Registry Cleaner code has been removed hence the respective settings has been commented
        /// When the Antivirus Settings Tab is clicked then the Disk Cleaner Settings Panel should not be visible
        /// When the Disk Cleaner Settings Tab is clicked then Antivirus Settings Panel should not be visible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splBtnSettings_Click(object sender, EventArgs e)
        {
            SpecialButtons b = (SpecialButtons)sender;
            switch (b.Name)
            {
                case "splBtnSettingsAntivirus":
                    //splBtnSettingsAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.settings_tab_antivirus_hover;
                    //splBtnSettingsDiskCleaner.BackgroundImage = global::AVStrike.Properties.Resources.settings_tab_disk_cleaner;
                    pnlSettingsHolderAntivirus.BringToFront();
                    break;
                case "splBtnSettingsPcCleaner":
                    //splBtnSettingsAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.settings_tab_antivirus;
                    //splBtnSettingsDiskCleaner.BackgroundImage = global::AVStrike.Properties.Resources.settings_tab_disk_cleaner;
                    break;
                case "splBtnSettingsDiskCleaner":
                    //splBtnSettingsAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.settings_tab_antivirus;
                    //splBtnSettingsDiskCleaner.BackgroundImage = global::AVStrike.Properties.Resources.settings_tab_disk_cleaner_hover;
                    //pnlSettingsHolderDiskCleaner.BringToFront();
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Antivirus Settings

        /// <summary>
        /// Load the antivirus settings
        /// </summary>
        private void LoadAntivirusSettings()
        {
            //Set the values for the Scan Schedule
            #region Scan Schedule
            chkSettingScanScheduleRunAScan.Checked = Properties.Settings.Default.AntRunScheduleScan;
            //chkSettingScanScheduleAntiVirusAndSpyware.Checked = Properties.Settings.Default.AntCheckVirusDefinition;
            chkSettingRunAVStrikeWhenComputerIsOn.Checked = Properties.Settings.Default.AntSetInStartup;
            #endregion

            #region Advanced
            chkSettingsAdvancedAllowFullHistory.Checked = Properties.Settings.Default.AntAllowFullHistory;
            chkSettingsAdvancedKeepQuarantinedFiles.Checked = Properties.Settings.Default.AntQuarantineFolderValidity;
            //chkSettingsAdvancedVirusControlCloud.Checked = Properties.Settings.Default.AntAntivirusCloud;
            #endregion

            //List of files types to be excluded during the antivirus scan
            lvAntSetExcludedFiles.Items.Clear();
            foreach (string excludeFiles in Properties.Settings.Default.AntExcludeFilesList)
                this.lvAntSetExcludedFiles.Items.Add(excludeFiles);
            lvAntSetExcludedFiles.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.None);
            lvAntSetExcludedFiles.Columns[0].Width = 200;
            lvAntSetExcludedFiles.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);

            //List of folders to be excluded during the antivirus scan
            lvAntSetExcludedFolders.Items.Clear();
            foreach (string excludeDir in Properties.Settings.Default.AntExcludeFoldersList)
                this.lvAntSetExcludedFolders.Items.Add(excludeDir);
            lvAntSetExcludedFolders.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.None);
            lvAntSetExcludedFolders.Columns[0].Width = 200;
            lvAntSetExcludedFolders.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);

            numericUpDownSizeMaximum.Value = Convert.ToDecimal(Properties.Settings.Default.AntFileSizeLimit);

            //real time protection settings. By default its enabled
            chkSettingsRealTimeProtectionTurnOn.Checked = Properties.Settings.Default.AntRealTimeProtection;
        }

        /// <summary>
        /// Savev the antivirus settings
        /// </summary>
        private void SaveAntivirusSettings()
        {
            #region Scan Schedule
            Properties.Settings.Default.AntRunScheduleScan = chkSettingScanScheduleRunAScan.Checked;
            //Properties.Settings.Default.AntCheckVirusDefinition = chkSettingScanScheduleAntiVirusAndSpyware.Checked;
            Properties.Settings.Default.AntSetInStartup = chkSettingRunAVStrikeWhenComputerIsOn.Checked;

            if (chkSettingScanScheduleRunAScan.Checked)
            {
                //if(ScanScheduleLogic.Open(Properties.Settings.Default.AntTaskId) != null)
                //    ScanScheduleLogic.Delete(Properties.Settings.Default.AntTaskId);
                CreateTask();
            }



            #endregion

            //Set the application inside startup
            if (Properties.Settings.Default.AntSetInStartup)
            {
                _ApplicationProperties.SetStartupInRegistry(true);
                _ApplicationProperties.SetStartupInAppData(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_STARTUP), true);
                _ApplicationProperties.SetStartupInAppData(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_COMMON_STARTUP), true);
            }
            else
            {
                _ApplicationProperties.SetStartupInRegistry(false);
                _ApplicationProperties.SetStartupInAppData(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_STARTUP), false);
                _ApplicationProperties.SetStartupInAppData(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_COMMON_STARTUP), false);
            }

            #region Advanced
            Properties.Settings.Default.AntAllowFullHistory = chkSettingsAdvancedAllowFullHistory.Checked;
            Properties.Settings.Default.AntQuarantineFolderValidity = chkSettingsAdvancedKeepQuarantinedFiles.Checked;
            //Properties.Settings.Default.AntAntivirusCloud = chkSettingsAdvancedVirusControlCloud.Checked;
            #endregion

            #region List Views
            Properties.Settings.Default.AntExcludeFilesList.Clear();
            foreach (ListViewItem lvi in this.lvAntSetExcludedFiles.Items)
                Properties.Settings.Default.AntExcludeFilesList.Add(lvi.Text);

            Properties.Settings.Default.AntExcludeFoldersList.Clear();
            foreach (ListViewItem lvi in this.lvAntSetExcludedFolders.Items)
                Properties.Settings.Default.AntExcludeFoldersList.Add(lvi.Text);

            #endregion

            Properties.Settings.Default.AntFileSizeLimit = Convert.ToInt32(numericUpDownSizeMaximum.Value);

            Properties.Settings.Default.AntRealTimeProtection = chkSettingsRealTimeProtectionTurnOn.Checked;
            if (Properties.Settings.Default.AntRealTimeProtection)
            {
                lblHomeMainRealTimeProtectionDescription.Text = "ON";
                lblHomeMainRealTimeProtectionDescription.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblHomeMainRealTimeProtectionDescription.Text = "OFF";
                lblHomeMainRealTimeProtectionDescription.ForeColor = System.Drawing.Color.Red;
            }


            Properties.Settings.Default.Save();
            this.Refresh();
        }

        /// <summary>
        /// Create a task for Schedule scan
        /// </summary>
        private void CreateTask()
        {
            if (cmbBxSettingScanScheduleDayOfTheWeek.Enabled && cmbBxSettingScanScheduleScanType.Enabled)
            {

                string strScheduleType = cmbBxSettingScanScheduleDayOfTheWeek.SelectedItem.ToString();
                string strScanType = string.Empty;
                //string strDriveToScan = _ScheduleScan.GetDrivesToScan(cmbBxSettingScanScheduleScanType.SelectedItem.ToString());


                #region Condition to Get days of Week
                //for daily
                if (cmbBxSettingScanScheduleDayOfTheWeek.SelectedIndex == 0)
                {
                    Properties.Settings.Default.AntScanFrequency = 0;
                    Properties.Settings.Default.Save();
                }

                //for weekly
                if (cmbBxSettingScanScheduleDayOfTheWeek.SelectedIndex == 1)
                {
                    Properties.Settings.Default.AntScanFrequency = 1;
                    Properties.Settings.Default.Save();
                }
                #endregion

                #region Condition to get type of Scan

                if (cmbBxSettingScanScheduleScanType.SelectedIndex == 0)
                {
                    strScanType = "qs";
                }
                if (cmbBxSettingScanScheduleScanType.SelectedIndex == 1)
                {
                    strScanType = "fs";
                }
                #endregion


                AddTask add = new AddTask();
                var newTask = AVStrike.Classes.TaskScheduler.ScanScheduleLogic.Create(strScanType);
                if (newTask == null)
                    return;

                add.AddNewTask(newTask.Triggers);
                AVStrike.Classes.TaskScheduler.ScanScheduleLogic.Save(newTask);
                Thread.Sleep(1000);
            }
        }

        private void chkSettingScanScheduleRunAScan_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSettingScanScheduleRunAScan.Checked == true)
            {

                cmbBxSettingScanScheduleDayOfTheWeek.Enabled = true;
                cmbBxSettingScanScheduleScanType.Enabled = true;
                cmbBxSettingScanScheduleDayOfTheWeek.SelectedIndex = 0;
                cmbBxSettingScanScheduleScanType.SelectedIndex = 0;
            }
            else
            {
                cmbBxSettingScanScheduleDayOfTheWeek.Enabled = false;
                cmbBxSettingScanScheduleScanType.Enabled = false;
            }
        }

        private void splBtnSettingsAntivirusApply_Click(object sender, EventArgs e)
        {
            SaveAntivirusSettings();
            MessageBox.Show(this, "Your preferred settings have been saved.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Refresh();
        }

        private void splBtnSettingsAntivirusCancel_Click(object sender, EventArgs e)
        {
            LoadAntivirusSettings();
            MessageBox.Show(this, "Your Settings have been Set to default.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Refresh();
        }

        #region Exclude Files
        private void splBtnSettingsAntivirusExcludeFilesAdd_Click(object sender, EventArgs e)
        {
            AddExcludedFiles addExcludedFiles = new AddExcludedFiles();
            addExcludedFiles.AddFileType += new AddExcludedFiles.AddFileTypeEventHandler(addExcludedFiles_AddExcludeFile);
            //addExcludedFiles.ShowDialog(this);
            addExcludedFiles.Show(this);
        }

        void addExcludedFiles_AddExcludeFile(object sender, AVStrike.Windows.Antivirus.AddExcludedFiles.AddFileTypeEventArgs e)
        {
            if (Properties.Settings.Default.AntExcludeFilesList.Contains(e.fileType))
            {
                MessageBox.Show(this, "File extension cannot be added as it already exists.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            this.lvAntSetExcludedFiles.Items.Add(e.fileType);
            Properties.Settings.Default.AntExcludeFilesList.Add(e.fileType);
            Properties.Settings.Default.Save();
            lvAntSetExcludedFiles.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.None);
            lvAntSetExcludedFiles.Columns[0].Width = 200;
            lvAntSetExcludedFiles.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            //this.lvAntSetExcludedFiles.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            this.Refresh();
        }

        private void splBtnSettingsAntivirusExcludeFilesRemove_Click(object sender, EventArgs e)
        {
            if (this.lvAntSetExcludedFiles.SelectedItems.Count > 0 && this.lvAntSetExcludedFiles.SelectedItems[0] != null)
            {
                if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    string str = lvAntSetExcludedFiles.SelectedItems[0].Text;
                    Properties.Settings.Default.AntExcludeFilesList.Remove(str);
                    Properties.Settings.Default.Save();
                    this.lvAntSetExcludedFiles.SelectedItems[0].Remove();
                    this.lvAntSetExcludedFiles.Refresh();
                }
            }
        }
        #endregion

        #region Exclude Folder
        private void splBtnSettingsAntivirusExcludeFoldersAdd_Click(object sender, EventArgs e)
        {
            AddExcludedFolders addExcludedFolders = new AddExcludedFolders();
            addExcludedFolders.AddExcludeFolderDelegate += new AddExcludedFolders.AddExcludeFolderEventHandler(addExcludedFolders_AddExcludedFolder);
            //addExcludedFolders.ShowDialog(this);
            addExcludedFolders.Show(this);
        }


        void addExcludedFolders_AddExcludedFolder(object sender, AVStrike.Windows.Antivirus.AddExcludedFolders.AddExcludeFolderEventArgs e)
        {
            if (Properties.Settings.Default.AntExcludeFoldersList.Contains(e.folderPath))
            {
                MessageBox.Show(this, "Folder path cannot be added as it already exists.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            this.lvAntSetExcludedFolders.Items.Add(e.folderPath);
            Properties.Settings.Default.AntExcludeFoldersList.Add(e.folderPath);
            Properties.Settings.Default.Save();
            lvAntSetExcludedFolders.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.None);
            lvAntSetExcludedFolders.Columns[0].Width = 200;
            lvAntSetExcludedFolders.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            //this.lvAntSetExcludedFiles.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            this.Refresh();
        }

        private void splBtnSettingsAntivirusExcludeFolderRemove_Click(object sender, EventArgs e)
        {
            if (this.lvAntSetExcludedFolders.SelectedItems.Count > 0 && this.lvAntSetExcludedFolders.SelectedItems[0] != null)
            {
                if (MessageBox.Show(this, "Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    string str = lvAntSetExcludedFolders.SelectedItems[0].Text;
                    Properties.Settings.Default.AntExcludeFoldersList.Remove(str);
                    Properties.Settings.Default.Save();
                    this.lvAntSetExcludedFolders.SelectedItems[0].Remove();
                    this.lvAntSetExcludedFolders.Refresh();
                }
            }
        }
        #endregion


        private void chkSettingRunAVStrikeWhenComputerIsOn_CheckedChanged(object sender, EventArgs e)
        {
            cmbBxSettingScanScheduleDayOfTheWeek.SelectedIndex = 0;
            cmbBxSettingScanScheduleScanType.SelectedIndex = 0;
        }

        #endregion

        #region Quarantine

        /// <summary>
        /// Checkbox checked changed event to select and unselect the list of items in the Quaranting Listiview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkQuarantineSelectUnselect_CheckedChanged(object sender, EventArgs e)
        {
            if (chkQuarantineSelectUnselect.Checked == true)
            {
                int count = this.lvDiskDiskCleaner.Items.Count;
                for (int i = count - 1; i >= 0; i--)
                    this.lvDiskDiskCleaner.Items[i].Checked = true;
            }
            else
            {
                int count = this.lvDiskDiskCleaner.Items.Count;
                for (int i = count - 1; i >= 0; i--)
                    this.lvDiskDiskCleaner.Items[i].Checked = false;
            }
        }

        #region Restore
        /// <summary>
        /// Click event of the Restore button in the Quarantine Panel
        /// Restore functionality restores all the selected Malicious files to the respective locations from
        /// where they were being moved to the quarantine file
        /// Initial process is to check if the ListView is empty. And the number of items selected to restore are 
        /// more than 0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splBtnRestore_Click(object sender, EventArgs e)
        {
            if (lvDiskDiskCleaner.Items.Count > 0)
            {
                var vrSelectedIndexes = lvDiskDiskCleaner.CheckedItems;
                if (vrSelectedIndexes.Count > 0)
                {
                    FileRestoreAction();
                    ShowScanReport();
                    this.Refresh();
                }
                else
                {
                    MessageBox.Show(this, ApplicationConstant.RestoreSelectAFile, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        /// <summary>
        /// Restores the selected files to the previous location from where they were moved to Quarantine.
        /// Find the list of files that are of same name but from different locations. Mutiple file of same name
        /// are being checked in the Quarantine directory. They are decrypted and then moved to their original 
        /// locations.
        /// Also their respective flags are changed in the SQlite table as 'Restored'
        /// </summary>
        private void FileRestoreAction()
        {

            try
            {
                if (MessageBox.Show(this, ApplicationConstant.RestoreConfirmation, Application.ProductName,
                                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                    return;

                Cursor.Current = Cursors.WaitCursor;

                var vrSelectedIndexes = lvDiskDiskCleaner.CheckedItems;

                for (int i = 0; i < vrSelectedIndexes.Count; i++)
                {
                    int limit = _SQLiteProcess.ScanHistoryRemoveSelectCount(vrSelectedIndexes[i].Text);
                    if (limit > 1)
                    {
                        SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.ScanHistoryGetFileHashValue(vrSelectedIndexes[i].Text, vrSelectedIndexes[i].SubItems[1].Text);
                        while (_SQLiteDataReader.Read())
                        {
                            if (_SQLiteProcess.RestoreFilesUpdate(vrSelectedIndexes[i].Text, vrSelectedIndexes[i].SubItems[1].Text))
                            {
                                string sourcePath = Application.StartupPath + @"\Quarantine\" + _SQLiteDataReader[2].ToString();
                                _ApplicationProperties.FileEncryption(sourcePath, vrSelectedIndexes[i].SubItems[1].Text);
                            }
                        }
                    }

                    else
                    {
                        RestoreFiles(vrSelectedIndexes[i].Text);
                    }
                }

                Cursor.Current = Cursors.Default;

                MessageBox.Show(this, ApplicationConstant.RestoreSuccess, Application.ProductName, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainForm-DiskDiskCleanerCleanFilesAction: " + ex.Message);
            }
        }

        /// <summary>
        /// After getting the files that are being selected in the ListView. The flags of the files are being changed
        /// in the SQLite table and the corresponding files are being moved to their original locations
        /// </summary>
        /// <param name="strFileName"></param>
        public void RestoreFiles(string strFileName)
        {

            SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.SelectFilesRestore(strFileName);

            while (_SQLiteDataReader.Read())
            {
                string strFilePath = _SQLiteDataReader[0].ToString();
                string strFileHash = _SQLiteDataReader[1].ToString();

                if (!string.IsNullOrEmpty(strFilePath) && !string.IsNullOrEmpty(strFileName))
                {
                    if (_SQLiteProcess.RestoreFilesUpdate(strFileName, strFilePath))
                    {
                        string strQuarantineFilePath = Application.StartupPath + @"\Quarantine\" + strFileHash;
                        _ApplicationProperties.FileEncryption(strQuarantineFilePath, strFilePath);

                        _MoveFileAndDeleteFlag.RemoveFromQuarantine(strQuarantineFilePath);
                    }
                }

            }
        }
        #endregion

        #region Remove All

        /// <summary>
        /// Remove all process to remove all the malicious files that are being stored in the Quarantine directory
        /// There is no need to select any of them. On the click event of the button all the files will be
        /// deleted permanently
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splBtnRemoveAll_Click(object sender, EventArgs e)
        {
            if (lvDiskDiskCleaner.Items.Count > 0)
            {
                RemoveAllFilesAction();
                Properties.Settings.Default.ScanImageSettings = 1;
                Properties.Settings.Default.Save();
                DisplayScanImage();
                this.Refresh();
            }
        }

        /// <summary>
        /// Change the flags of the respective file names in the SQLite table to Removed and then delete the files
        /// forever
        /// </summary>
        private void RemoveAllFilesAction()
        {
            try
            {
                if (this.lvDiskDiskCleaner.Items.Count > 0)
                {
                    if (MessageBox.Show(this, ApplicationConstant.RemoveConfirmation, Application.ProductName,
                                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                        return;

                    Cursor.Current = Cursors.WaitCursor;

                    SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.ScanHistoryRemoveAll();
                    while (_SQLiteDataReader.Read())
                    {
                        DeleteFile(_SQLiteDataReader[0].ToString());
                    }
                    this.lvDiskDiskCleaner.Items.Clear();

                    Cursor.Current = Cursors.Default;

                    MessageBox.Show(this, ApplicationConstant.RemoveSuccess, Application.ProductName, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainForm-DiskDiskCleanerCleanFilesAction: " + ex.Message);
            }
        }
        #endregion

        #region Remove


        /// <summary>
        /// Remove the selected files from the Quarantine Directory permanently.
        /// Only the selected files will be removed. 
        /// Atleast one file should be selected to be removed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splBtnRemove_Click(object sender, EventArgs e)
        {
            if (lvDiskDiskCleaner.Items.Count > 0)
            {
                var vrSelectedIndexes = lvDiskDiskCleaner.CheckedItems;
                if (vrSelectedIndexes.Count > 0)
                {
                    RemoveFilesAction();
                    Properties.Settings.Default.ScanImageSettings = 1;
                    Properties.Settings.Default.Save();
                    DisplayScanImage();
                    ShowScanReport();
                    this.Refresh();
                }
                else
                {
                    MessageBox.Show(this, ApplicationConstant.RemoveSelectAFile, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// Get the duplicate files. Check if more than one file of the same name is being moved to the Quarantine
        /// Change the flag in the SQlite table as Removed
        /// </summary>
        private void RemoveFilesAction()
        {
            try
            {
                if (MessageBox.Show(this, ApplicationConstant.RemoveConfirmation, Application.ProductName,
                                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                    return;

                Cursor.Current = Cursors.WaitCursor;

                var vrSelectedIndexes = lvDiskDiskCleaner.CheckedItems;
                for (int i = 0; i < vrSelectedIndexes.Count; i++)
                {
                    int limit = _SQLiteProcess.ScanHistoryRemoveSelectCount(vrSelectedIndexes[i].Text);
                    if (limit > 1)
                    {
                        _SQLiteProcess.ScanHistoryRemoveUpdate(vrSelectedIndexes[i].Text, vrSelectedIndexes[i].SubItems[1].Text);

                    }
                    else
                    {
                        DeleteFile(vrSelectedIndexes[i].Text);
                    }
                }

                Cursor.Current = Cursors.Default;
                MessageBox.Show(this, ApplicationConstant.RemoveSuccess, Application.ProductName, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("MainForm-DiskDiskCleanerCleanFilesAction: " + ex.Message);
            }

        }

        /// <summary>
        /// Change the flag in the SQLite database and delete the file
        /// </summary>
        /// <param name="strFileName"></param>
        public void DeleteFile(string strFileName)
        {
            SQLiteDataReader _SQLiteDataReader = _SQLiteProcess.ScanHistoryRemoveSelect(strFileName);
            while (_SQLiteDataReader.Read())
            {
                if (!string.IsNullOrEmpty(_SQLiteDataReader[0].ToString()))
                {
                    if (_SQLiteProcess.ScanHistoryRemoveUpdate(strFileName, _SQLiteDataReader[0].ToString()))
                    {
                        _MoveFileAndDeleteFlag.RemoveFromQuarantine(Application.StartupPath + @"\Quarantine\" + _SQLiteDataReader[1].ToString());
                    }
                }
            }
        }

        #endregion

        #endregion


        /// <summary>
        /// Event to show the Acativation form when the license expires
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLicenseAlertRenewNow_Click(object sender, EventArgs e)
        {
            ActivationForm _ActivationForm = null;
            if (!IsFormAlreadyOpen(typeof(ActivationForm)))
            {
                _ActivationForm = new ActivationForm();
                this.Hide();
                _ActivationForm.ShowDialog();

                return;
            }
        }

        /// <summary>
        /// Click event for the Quarantine Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubHomeQuarantine_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.av_home_main;
            btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.av_quarantine_hover;
            btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.av_history_main;
            btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.av_settings;
            pnlAntivirusBasic.BringToFront();
            DeleteMoreThanDays();
            ShowScanReport();
            if (lvDiskDiskCleaner.Items.Count > 0)
                DisableQuarantineButtons(false);
            else
                DisableQuarantineButtons(true);
            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Click event for History Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubHomeHistory_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            btnMainHome.BackgroundImage = global::AVStrike.Properties.Resources.av_home_main;
            btnMainAntivirus.BackgroundImage = global::AVStrike.Properties.Resources.av_quarantine;
            btnMainHistory.BackgroundImage = global::AVStrike.Properties.Resources.av_history_hover;
            btnMainSettings.BackgroundImage = global::AVStrike.Properties.Resources.av_settings;
            pnlHistoryBasic.BringToFront();
            ShowScanResult();
            Cursor.Current = Cursors.Default;
        }
    }
}
