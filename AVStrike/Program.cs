﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AVStrike.Classes.WindowsOperation;
using System.Threading;
using System.IO;
using AVStrike.Classes.Antivirus;
using System.Diagnostics;
using AVStrike.Classes;
using AVStrike.Classes.RealTimeScan;
using AVStrike.Windows;
using Common;
using Business;
using System.Data.SQLite;
using System.Text;
using LogMaintainanance;
using BaseTool;
using SQLiteConnectionLibrary;
using System.Collections;
using BaseTool.Util;
using BaseTool.Application;

namespace AVStrike
{
    static class Program
    {
        /// <summary>
        /// Use the thread to build the initial settings for the application
        /// </summary>
        public static Thread ThreadFunctionality { get; set; }

        [System.Runtime.InteropServices.DllImport("kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode, SetLastError = true)]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        static extern bool SetDllDirectory(string lpPathName);
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            int wsize = IntPtr.Size;
            string libdir = (wsize == 4) ? "x86" : "x64";
            string appPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            SetDllDirectory(System.IO.Path.Combine(appPath, libdir));

            if (!CommonUtil.isSupported())
            {
                //MessageBox.Show(string.Format("{0} is not supported by AVStrike.", OSDetails.Name));
                return;
            }

            CalculateRemainingDays calculateRemainingDays = new CalculateRemainingDays();
            ApplicationProperties _ApplicationProperties = new ApplicationProperties();
            SQLiteConnectionLibrary.SQLiteProcess _SQLiteProcess = new SQLiteConnectionLibrary.SQLiteProcess();

            //Enable visual styles
            Application.EnableVisualStyles();

            //Check if the same instance of application is running already
            if (SingleApplication.IsAlreadyRunning())
            {
                return;
            }


            //Starts the thread to begin process like checking for the existence of the tables in sqlite database
            //quarantine folder, freshclam.conf config file and update clamwin virus database automatically
            ThreadFunctionality = new Thread(new ThreadStart(BuildApplicationSettings));
            ThreadFunctionality.Start();

            #region Scan Schedule Functionality
            if (args != null && args.Length > 0)
            {
                //Scan schedule for quick scan
                if (args[0] == "qs")
                {
                    try
                    {
                        Properties.Settings.Default.ScanningFlag = 1;
                        Properties.Settings.Default.ScanTaskSchedule = true;
                        Properties.Settings.Default.Save();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.CreateLogFile("Program-Main: " + ex.Message);
                    }
                    //return;
                }

                //Scan schedule for full scan
                if (args[0] == "fs")
                {
                    try
                    {
                        Properties.Settings.Default.ScanningFlag = 0;
                        Properties.Settings.Default.ScanTaskSchedule = true;
                        Properties.Settings.Default.Save();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.CreateLogFile("Program-Main: " + ex.Message);

                    }
                    //return;
                }
            }
            #endregion

            //Set the Ownership privillege for the application
            Permissions.SetPrivileges(true);

            //Only when application is run for the first time
            if (Properties.Settings.Default.IsFirstRun)
            {
                try
                {
                    AntivirusSettings _AntivirusSettings = new AntivirusSettings();

                    Properties.Settings.Default.AntExcludeFilesList = _AntivirusSettings.excludedFileTypes;
                    Properties.Settings.Default.AntExcludeFoldersList = _AntivirusSettings.excludedDirs;
                    Properties.Settings.Default.AntExcludeFilesListForHashFiles = _AntivirusSettings.excludeFilesListForHashFiles;


                    //Properties.Settings.Default.AntLastScannedDate = new DateTime(2014, 06, 15, 12, 12, 12);

                    Properties.Settings.Default.AntLastScannedDate = DateTime.Now;
                    Properties.Settings.Default.IsFirstRun = false;
                    Properties.Settings.Default.Save();

                    _ApplicationProperties.SetStartupInRegistry(true);
                    _ApplicationProperties.SetStartupInAppData(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_STARTUP), true);
                    _ApplicationProperties.SetStartupInAppData(RegistryHelper.GetSpecialFolderPath(RegistryHelper.CSIDL_COMMON_STARTUP), true);
                }
                catch (Exception ex)
                {
                    ErrorLog.CreateLogFile("Program-Main: " + ex.Message);

                }
            }

            //ProcessWatcher procWatcher = new ProcessWatcher();
            //procWatcher.ProcessCreated += new ProcessEventHandler(procWatcher_ProcessCreated);
            //procWatcher.Start();


            int versionPoint = Properties.Settings.Default.ActVersionPoint;
            string MacID = AuthenticationAndSecurity.GetMacAddress();

            string DomainURL = string.Format(ExternalUrls.DomainUrl, ExternalUrls.Domain);
            string TrialDays = string.Format(ExternalUrls.TrialDaysReamining, ExternalUrls.Domain, MacID);
            string FullDays = string.Format(ExternalUrls.FullDaysReamining, ExternalUrls.Domain, MacID, Properties.Settings.Default.ActProductKey);
            bool isExceptionThrown = true;

            #region Validity Check
            if (AuthenticationAndSecurity.CheckInternet())
            {
                #region If Condition
                int days = 0;
                string StartDate = string.Empty;
                string EndDate = string.Empty;
                ArrayList DaysDetails = new ArrayList();
                try
                {
                    switch (versionPoint)
                    {
                        case 1:
                            _SQLiteProcess.InsertIntoTrial();
                            DaysDetails = AuthenticationAndSecurity.GetRemainingDaysDetails(TrialDays, DomainURL, out isExceptionThrown);

                            days = Convert.ToInt32(DaysDetails[0].ToString());
                            StartDate = DaysDetails[1].ToString();
                            EndDate = DaysDetails[2].ToString();

                            Properties.Settings.Default.ActDaysRemaining = days;
                            Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(EndDate);
                            _SQLiteProcess.UpdateDatabase(days, MacID, StartDate, EndDate, true);
                            Properties.Settings.Default.Save();
                            break;
                        case 2:
                            _SQLiteProcess.InserIntoFull();
                            DaysDetails = AuthenticationAndSecurity.GetRemainingDaysDetails(FullDays, DomainURL, out isExceptionThrown);

                            days = Convert.ToInt32(DaysDetails[0].ToString());
                            StartDate = DaysDetails[1].ToString();
                            EndDate = DaysDetails[2].ToString();

                            Properties.Settings.Default.ActDaysRemaining = days;
                            Properties.Settings.Default.ActExpiryDate = Convert.ToDateTime(EndDate);
                            _SQLiteProcess.UpdateDatabase(days, MacID, StartDate, EndDate, true);
                            Properties.Settings.Default.Save();
                            break;
                        default:
                            break;

                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.CreateLogFile("Program-Main: " + ex.Message);

                }
                #endregion
            }
            else
            {
                #region Else Condition
                int days = 0;
                try
                {
                    switch (versionPoint)
                    {
                        case 1:
                            days = calculateRemainingDays.ReturnTrialVersionReaminingDays();
                            Properties.Settings.Default.ActDaysRemaining = days;
                            Properties.Settings.Default.ActExpiryDate = DateTime.Now.AddDays(days);
                            Properties.Settings.Default.Save();
                            break;
                        case 2:
                            days = calculateRemainingDays.ReturnFullVersionReaminingDays();
                            Properties.Settings.Default.ActDaysRemaining = days;
                            Properties.Settings.Default.ActExpiryDate = DateTime.Now.AddDays(days);
                            Properties.Settings.Default.Save();
                            break;
                        default:
                            break;

                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.CreateLogFile("Program-Main: " + ex.Message);

                }
                #endregion
            }
            #endregion

            //Thread exceptions
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            
            //Unhandled exception
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            if (Properties.Settings.Default.ActDaysRemaining < 1)
                Application.Run(new ActivationForm());
            else
                Application.Run(new MainBaseForm());
            //Permissions.SetPrivileges(false);
        }

        /// <summary>
        /// Region for the Unhandled exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            CLogger.WriteLog(ELogLevel.FATAL, e.ExceptionObject.ToString());
            MessageBox.Show(string.Format(ApplicationConstant.UnhandledExceptionMessage,Application.ProductName), "Error");
        }

        /// <summary>
        /// Exceptions thrown by the thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            CLogger.WriteLog(ELogLevel.FATAL, e.Exception.ToString());
            MessageBox.Show(string.Format(ApplicationConstant.UnhandledExceptionMessage, Application.ProductName), "Error");
        }



        private static void procWatcher_ProcessCreated(WMI.Win32.Process proc)
        {
            //Class declarations
            try
            {
                ApplicationProperties _ApplicationProperties = new ApplicationProperties();
                SQLiteConnectionLibrary.SQLiteProcess _SQLiteProcess = new SQLiteConnectionLibrary.SQLiteProcess();

                string strProcessName = proc.Name;
                string strExecutablePath = proc.ExecutablePath;

                if (string.IsNullOrEmpty(strExecutablePath))
                    return;

                RealTimeScan RealTimeScan = new RealTimeScan(strProcessName, strExecutablePath);
                RealTimeScan.ShowDialog();

                string hashvalue = _ApplicationProperties.MD5Hash(strExecutablePath);

                if (_SQLiteProcess.SelectCountHash(hashvalue) == 0)
                {
                    //RealTimeScan RealTimeScan = new RealTimeScan(strProcessName, strExecutablePath);
                    //RealTimeScan.ShowDialog();
                }

                //DateTime dtLastDate = System.IO.File.GetLastWriteTime(strExecutablePath);
                //DateTime dtCurrentTime = DateTime.Now;

                //int LastScannedMinutes = (int)(dtCurrentTime - dtLastDate).TotalMinutes;

                //if (LastScannedMinutes < 60)
                //{
                //    //RealTimeScan RealTimeScan = new RealTimeScan(strProcessName, strExecutablePath);
                //    //RealTimeScan.ShowDialog();
                //}
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Program-procWatcher_ProcessCreated: " + ex.Message);

            }

        }


        /// <summary>
        /// Build the application settings
        /// </summary>
        private static void BuildApplicationSettings()
        {
            try
            {
                SQLiteHandling();
                CreateQuarantineFolder();
                CreateConfigFile();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Program-FunctionalityThread: " + ex.Message);

            }
        }

        private static void ProcessMonitor()
        {
            try
            {
                ProcessMonitor _ProcessMonitor = new ProcessMonitor();
                _ProcessMonitor.Monitor();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Program-ProcessMonitor: " + ex.Message);

            }

        }

        public static void SQLiteHandling()
        {
            try
            {
                SQLiteConnectionLibrary.SQliteTables.CreateTable(Queries.TFunctionality, Queries.QFunctionality);
                SQLiteConnectionLibrary.SQliteTables.CreateTable(Queries.TScanDetails, Queries.QScanDetails);
                SQLiteConnectionLibrary.SQliteTables.CreateTable(Queries.TScanHistory, Queries.QScanHistoryTable);
                SQLiteConnectionLibrary.SQliteTables.CreateTable(Queries.TScanResult, Queries.QScanResult);
                SQLiteConnectionLibrary.SQliteTables.CreateTable(Queries.TVersionFull, Queries.QVesrionFull);
                SQLiteConnectionLibrary.SQliteTables.CreateTable(Queries.TVersionTrial, Queries.QVersionTrial);
                SQLiteConnectionLibrary.SQliteTables.CreateTable(Queries.THashed, Queries.QHashed);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Program-SQLiteHandling: " + ex.Message);
            }
        }

        public static void CreateConfigFile()
        {
            try
            {
                BaseTool.Application.CreateConfigFile _CreateConfigFile = new BaseTool.Application.CreateConfigFile();
                string strFilesAndFolder = Application.StartupPath + @"\freshclam.conf";
                if (!File.Exists((strFilesAndFolder)))
                {
                    File.Create(strFilesAndFolder).Close();
                    _CreateConfigFile.WriteConfigFile(strFilesAndFolder, Application.StartupPath);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Program-CreateConfigFile: " + ex.Message);

            }

        }

        private static void CreateQuarantineFolder()
        {
            try
            {
                string strFilePath = Application.StartupPath + @"\Quarantine";
                System.IO.Directory.CreateDirectory(strFilePath);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLogFile("Program-CreateQuarantineFolder" + ex.Message);
            }
        }
    }
}
